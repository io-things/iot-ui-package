import { OnChanges, OnInit, SimpleChanges } from "@angular/core";
import * as i0 from "@angular/core";
export declare class TimeOfDayCardComponent implements OnInit, OnChanges {
    title: string;
    outerTitle: any;
    subject: string;
    data: number[][];
    min: number;
    lowest: number;
    highest: number;
    val1: number;
    val2: number;
    val3: number;
    val4: number;
    val5: number;
    constructor();
    today: string[];
    ngOnInit(): void;
    getTooltip(hour: number, week: number): string;
    getClass(hour: number, week: number): {
        cell: boolean;
    };
    ngOnChanges(changes: SimpleChanges): void;
    showTooltip(evt: any, test: any): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<TimeOfDayCardComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<TimeOfDayCardComponent, "iot-time-of-day-card", never, { "title": "title"; "outerTitle": "outerTitle"; "subject": "subject"; "data": "data"; "min": "min"; }, {}, never, ["[iot-card-footer]"]>;
}
