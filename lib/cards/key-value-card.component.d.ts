import { EventEmitter, OnInit } from "@angular/core";
import * as i0 from "@angular/core";
export interface KeyValueCardData {
    label: string;
    value: string | number;
    error?: boolean;
    type: 'string' | 'number' | 'date' | 'booleanIcon';
    unit?: string;
    trueValue?: string;
    falseValue?: string;
}
export declare class KeyValueCardComponent implements OnInit {
    outerTitle: string;
    title: string;
    keyValueData: KeyValueCardData[];
    click: EventEmitter<any>;
    hover: EventEmitter<any>;
    unHover: EventEmitter<any>;
    displayedColumns: string[];
    constructor();
    ngOnInit(): void;
    getShowShortTime(entries: any): boolean;
    getShowLongTime(entries: any): boolean;
    getShowVeryLongTime(entries: any): boolean;
    getNoTime(entries: any): boolean;
    static ɵfac: i0.ɵɵFactoryDeclaration<KeyValueCardComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<KeyValueCardComponent, "iot-key-value-card", never, { "outerTitle": "outerTitle"; "title": "title"; "keyValueData": "keyValueData"; }, { "click": "click"; "hover": "hover"; "unHover": "unHover"; }, never, never>;
}
