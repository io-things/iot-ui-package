import { OnChanges, OnInit, SimpleChanges } from '@angular/core';
import * as i0 from "@angular/core";
export declare class AccountMenuDisplayButtonComponent implements OnInit, OnChanges {
    user: {
        displayName: string;
        email: string;
    };
    avatar: string;
    constructor();
    ngOnInit(): void;
    ngOnChanges(changes: SimpleChanges): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<AccountMenuDisplayButtonComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<AccountMenuDisplayButtonComponent, "iot-account-menu-display-button", never, { "user": "user"; }, {}, never, ["[button1]", "[button2]"]>;
}
