import { EventEmitter } from '@angular/core';
import * as i0 from "@angular/core";
export declare class CustomerSelectorButtonComponent {
    label: string;
    click: EventEmitter<any>;
    constructor();
    static ɵfac: i0.ɵɵFactoryDeclaration<CustomerSelectorButtonComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<CustomerSelectorButtonComponent, "iot-customer-selector-button", never, { "label": "label"; }, { "click": "click"; }, never, never>;
}
