import { OnInit, EventEmitter } from '@angular/core';
import * as i0 from "@angular/core";
export declare class CustomerSelectorComponent implements OnInit {
    mobile: boolean;
    label: string;
    click: EventEmitter<any>;
    constructor();
    openDialog(): void;
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<CustomerSelectorComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<CustomerSelectorComponent, "iot-customer-selector", never, { "mobile": "mobile"; "label": "label"; }, { "click": "click"; }, never, never>;
}
