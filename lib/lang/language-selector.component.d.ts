import { EventEmitter, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as i0 from "@angular/core";
export declare class LanguageSelectorComponent implements OnInit, OnChanges {
    private translate;
    lang: string;
    langList: {
        key: string;
        name: string;
    }[];
    setLang: EventEmitter<string>;
    constructor(translate: TranslateService);
    ngOnInit(): void;
    selectLang(langKey: string): void;
    ngOnChanges(changes: SimpleChanges): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<LanguageSelectorComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<LanguageSelectorComponent, "iot-language-selector", never, { "lang": "lang"; "langList": "langList"; }, { "setLang": "setLang"; }, never, never>;
}
