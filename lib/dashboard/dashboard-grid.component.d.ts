import { AfterContentInit, ElementRef, OnInit, QueryList, TemplateRef } from '@angular/core';
import { DashboardMediaQueryComponent } from "./dashboard-media-query.component";
import * as i0 from "@angular/core";
export declare class DashboardCard {
    private elementRef;
    templateRef: TemplateRef<any>;
    cardId: string;
    colspan: any;
    cols: number[];
    constructor(elementRef: ElementRef, templateRef: TemplateRef<any>);
    static ɵfac: i0.ɵɵFactoryDeclaration<DashboardCard, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<DashboardCard, "[iotCard]", never, { "cardId": "cardId"; "colspan": "colspan"; }, {}, never>;
}
export declare class DashboardGridComponent extends DashboardMediaQueryComponent implements AfterContentInit, OnInit {
    topLevelPanes: QueryList<DashboardCard>;
    rowHeight: string;
    col1: string;
    col2: string;
    col3: string;
    col4: string;
    _col: any[][];
    _templateMap: {
        [key: string]: TemplateRef<any>;
    };
    _cardMap: {
        [key: string]: DashboardCard;
    };
    row: number[];
    ngAfterContentInit(): void;
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<DashboardGridComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<DashboardGridComponent, "iot-dashboard", never, { "rowHeight": "rowHeight"; "col1": "col1"; "col2": "col2"; "col3": "col3"; "col4": "col4"; }, {}, ["topLevelPanes"], never>;
}
