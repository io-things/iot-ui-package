import { OnChanges, OnInit, SimpleChanges } from '@angular/core';
import * as i0 from "@angular/core";
export declare class DashboardMediaQueryComponent implements OnInit, OnChanges {
    offsetSize: number;
    private small;
    private medium;
    private large;
    absoluteStyle: {
        width: string;
    };
    cols: number;
    private smallEventListener;
    private mediumEventListener;
    private bigEventListener;
    constructor();
    private setMediaQueries;
    private match1Columns;
    private match2Columns;
    private match3Columns;
    private match4Columns;
    ngOnInit(): void;
    ngOnChanges(changes: SimpleChanges): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<DashboardMediaQueryComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<DashboardMediaQueryComponent, "app-dashboard-grid", never, { "offsetSize": "offsetSize"; }, {}, never, never>;
}
