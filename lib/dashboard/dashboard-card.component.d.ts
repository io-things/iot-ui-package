import { ElementRef, OnChanges, OnInit, Renderer2, SimpleChanges, TemplateRef } from '@angular/core';
import * as i0 from "@angular/core";
export declare class IotCardHeader implements OnInit {
    private elRef;
    private renderer;
    padding: boolean;
    constructor(elRef: ElementRef, renderer: Renderer2);
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<IotCardHeader, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<IotCardHeader, "[iot-card-header]", never, { "padding": "padding"; }, {}, never>;
}
export declare class IotCardContent implements OnInit {
    private elRef;
    private renderer;
    padding: boolean;
    constructor(elRef: ElementRef, renderer: Renderer2);
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<IotCardContent, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<IotCardContent, "[iot-card-content]", never, { "padding": "padding"; }, {}, never>;
}
export declare class IotCardFooter implements OnInit {
    private elRef;
    private renderer;
    padding: boolean;
    constructor(elRef: ElementRef, renderer: Renderer2);
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<IotCardFooter, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<IotCardFooter, "[iot-card-footer]", never, { "padding": "padding"; }, {}, never>;
}
export declare class DashboardCardComponent implements OnInit, OnChanges {
    template: TemplateRef<any>;
    colspan: number;
    showHeader: boolean;
    outerTitle: any;
    innerTitle: any;
    showTitle: boolean;
    height: string;
    width: string;
    cardClass: {
        'iotcol-1': boolean;
        'iotcol-2': boolean;
        'iotcol-3': boolean;
        'iotcol-4': boolean;
    };
    constructor();
    ngOnInit(): void;
    ngOnChanges(changes: SimpleChanges): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<DashboardCardComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<DashboardCardComponent, "iot-dashboard-card", never, { "colspan": "colspan"; "showHeader": "showHeader"; "outerTitle": "outerTitle"; "innerTitle": "innerTitle"; "showTitle": "showTitle"; "height": "height"; "width": "width"; }, {}, ["template"], ["[iot-card-header]", "[iot-card-content]", "[iot-card-footer]"]>;
}
