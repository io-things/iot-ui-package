import { OnInit } from '@angular/core';
import { ParamMap } from "@angular/router";
import * as i0 from "@angular/core";
export declare class CompositeListEntryComponent implements OnInit {
    entry: any;
    params: ParamMap;
    paramsExtra: {
        [key: string]: (string | number);
    };
    showSettingsMenu: boolean;
    constructor();
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<CompositeListEntryComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<CompositeListEntryComponent, "iot-composite-list-entry", never, { "entry": "entry"; "params": "params"; "paramsExtra": "paramsExtra"; }, {}, never, never>;
}
