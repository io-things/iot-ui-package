import { OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ParamMap } from "@angular/router";
import * as i0 from "@angular/core";
export declare class SingleListEntryComponent implements OnInit, OnChanges {
    entry: any;
    params: ParamMap;
    paramsExtra: {
        [key: string]: (string | number);
    };
    routerLink: string;
    constructor();
    ngOnChanges(changes: SimpleChanges): void;
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<SingleListEntryComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<SingleListEntryComponent, "iot-single-list-entry", never, { "entry": "entry"; "params": "params"; "paramsExtra": "paramsExtra"; }, {}, never, never>;
}
