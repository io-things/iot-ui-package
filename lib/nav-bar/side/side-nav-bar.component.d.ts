import { OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute, ParamMap } from "@angular/router";
import * as i0 from "@angular/core";
export declare class SideNavBarComponent implements OnInit, OnDestroy, OnChanges {
    private route;
    belowTopNav: boolean;
    isMobile: boolean;
    isOpen: boolean;
    data: any;
    params: {
        [key: string]: (string | number);
    };
    width: any;
    paramMap: ParamMap;
    items: any;
    private sub;
    constructor(route: ActivatedRoute);
    ngOnInit(): void;
    ngOnDestroy(): void;
    ngOnChanges(changes: SimpleChanges): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<SideNavBarComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<SideNavBarComponent, "iot-side-nav-bar", never, { "belowTopNav": "belowTopNav"; "isMobile": "isMobile"; "isOpen": "isOpen"; "data": "data"; "params": "params"; "width": "width"; }, {}, never, ["[sideNavTop]", "*"]>;
}
