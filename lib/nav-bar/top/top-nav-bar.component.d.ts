import { EventEmitter, OnInit } from '@angular/core';
import * as i0 from "@angular/core";
export declare class TopNavBarComponent implements OnInit {
    menuClick: EventEmitter<MouseEvent>;
    constructor();
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<TopNavBarComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<TopNavBarComponent, "iot-top-nav-bar", never, {}, { "menuClick": "menuClick"; }, never, ["[portaltitle]", "[customerselect]", "[icon1]", "[language]", "[account]"]>;
}
