import { OnInit } from '@angular/core';
import { MatDialogRef } from "@angular/material/dialog";
import * as i0 from "@angular/core";
export declare class CustomerSelectModalComponent implements OnInit {
    dialogRef: MatDialogRef<CustomerSelectModalComponent>;
    data: any;
    customerlist: any;
    constructor(dialogRef: MatDialogRef<CustomerSelectModalComponent>, data: any);
    onNoClick(): void;
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<CustomerSelectModalComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<CustomerSelectModalComponent, "iot-customer-select-modal", never, {}, {}, never, never>;
}
