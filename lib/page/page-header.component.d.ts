import { EventEmitter, OnInit } from '@angular/core';
import * as i0 from "@angular/core";
export declare class PageHeaderComponent implements OnInit {
    title: any;
    subTitle: any;
    routerLink: any;
    backLabel: any;
    back: EventEmitter<any>;
    constructor();
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<PageHeaderComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<PageHeaderComponent, "iot-page-header", never, { "title": "title"; "subTitle": "subTitle"; "routerLink": "routerLink"; "backLabel": "backLabel"; }, { "back": "back"; }, never, ["[button3]", "[button2]", "[button1]"]>;
}
