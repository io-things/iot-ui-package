import { EventEmitter, QueryList } from '@angular/core';
import { DynamicFormWidth } from "./dynamic-form-width";
import { Subscription } from "rxjs";
import * as i0 from "@angular/core";
export declare class IotFormComponent {
    readonly: boolean;
    state: number;
    emitAll: boolean;
    alwaysShowDiscard: boolean;
    saveButtonText: string;
    statesDescription: StatesDescription;
    save: EventEmitter<any>;
    discard: EventEmitter<boolean>;
    inputTabs: QueryList<DynamicFormWidth>;
    active: boolean;
    tabs: DynamicFormWidth[];
    formSubscription: Subscription;
    ngAfterContentInit(): void;
    private pristineCheck;
    _save(): void;
    constructor();
    static ɵfac: i0.ɵɵFactoryDeclaration<IotFormComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<IotFormComponent, "iot-form", never, { "readonly": "readonly"; "state": "state"; "emitAll": "emitAll"; "alwaysShowDiscard": "alwaysShowDiscard"; "saveButtonText": "saveButtonText"; "statesDescription": "statesDescription"; "active": "active"; }, { "save": "save"; "discard": "discard"; }, ["inputTabs"], ["[page-header]", "[below-header]"]>;
}
export interface StatesDescription {
    [id: number]: {
        'text': string;
        'loading': boolean;
        'check': boolean;
    };
}
