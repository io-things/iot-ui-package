import { EventEmitter, OnInit, TemplateRef } from '@angular/core';
import { DynamicFormWidth } from "./dynamic-form-width";
import * as i0 from "@angular/core";
export declare class IotInputHorizontalLineComponent extends DynamicFormWidth implements OnInit {
    _template: TemplateRef<any>;
    get template(): TemplateRef<any>;
    value: string;
    label: string;
    disabled: boolean;
    onChange: EventEmitter<string>;
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<IotInputHorizontalLineComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<IotInputHorizontalLineComponent, "iot-horizontal-line", never, { "value": "value"; "label": "label"; "disabled": "disabled"; }, { "onChange": "onChange"; }, never, never>;
}
