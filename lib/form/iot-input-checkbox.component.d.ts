import { EventEmitter, OnChanges, OnInit, SimpleChanges, TemplateRef } from '@angular/core';
import { DynamicFormWidth } from './dynamic-form-width';
import { FormGroup } from "@angular/forms";
import * as i0 from "@angular/core";
export declare class IotInputCheckboxComponent extends DynamicFormWidth implements OnInit, OnChanges {
    _template: TemplateRef<any>;
    get template(): TemplateRef<any>;
    get pristine(): any;
    get key(): string;
    get newValue(): any;
    form: FormGroup;
    leftLabel: string;
    label: string;
    attribute: string;
    description: string;
    value: boolean;
    separator: boolean;
    disabled: boolean;
    inverse: boolean;
    onChange: EventEmitter<any>;
    change(event: any): void;
    ngOnInit(): void;
    ngOnChanges(changes: SimpleChanges): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<IotInputCheckboxComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<IotInputCheckboxComponent, "iot-input-checkbox", never, { "leftLabel": "leftLabel"; "label": "label"; "attribute": "attribute"; "description": "description"; "value": "value"; "separator": "separator"; "disabled": "disabled"; "inverse": "inverse"; }, { "onChange": "onChange"; }, never, never>;
}
