import { EventEmitter, OnInit, TemplateRef } from '@angular/core';
import { DynamicFormWidth } from "./dynamic-form-width";
import * as i0 from "@angular/core";
export declare class IotInputPlaceholderComponent extends DynamicFormWidth implements OnInit {
    _template: TemplateRef<any>;
    get template(): TemplateRef<any>;
    get pristine(): any;
    get key(): string;
    get newValue(): string;
    label: any;
    fullWidth: boolean;
    attribute: string;
    value: any;
    type: string;
    disabled: boolean;
    onChange: EventEmitter<string>;
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<IotInputPlaceholderComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<IotInputPlaceholderComponent, "iot-input-placeholder", never, { "label": "label"; "fullWidth": "fullWidth"; "attribute": "attribute"; "value": "value"; "type": "type"; "disabled": "disabled"; }, { "onChange": "onChange"; }, never, ["[full-width]", "[field-positioned]"]>;
}
