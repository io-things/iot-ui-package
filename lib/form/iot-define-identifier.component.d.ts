import { OnInit, TemplateRef } from '@angular/core';
import { DynamicFormWidth } from './dynamic-form-width';
import { FormGroup } from "@angular/forms";
import * as i0 from "@angular/core";
export declare class IotDefineIdentifierComponent extends DynamicFormWidth implements OnInit {
    _template: TemplateRef<any>;
    prefix: {
        nid: string;
        name: string;
    }[];
    get template(): TemplateRef<any>;
    get pristine(): any;
    get key(): string;
    get newValue(): any;
    form: FormGroup;
    label: string;
    attribute: string;
    description: string;
    value: string[];
    _value: string[];
    separator: boolean;
    disabled: boolean;
    new: string;
    thirpartySelection: string;
    change(event: any): void;
    ngOnInit(): void;
    addIdentifier(): void;
    remove(id: string): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<IotDefineIdentifierComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<IotDefineIdentifierComponent, "iot-define-identifier", never, { "prefix": "prefix"; "label": "label"; "attribute": "attribute"; "description": "description"; "value": "value"; "separator": "separator"; "disabled": "disabled"; }, {}, never, never>;
}
