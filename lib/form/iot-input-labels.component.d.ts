import { ElementRef, EventEmitter, OnChanges, OnInit, SimpleChanges, TemplateRef } from '@angular/core';
import { DynamicFormWidth } from "./dynamic-form-width";
import { FormGroup } from "@angular/forms";
import { MatChipInputEvent } from "@angular/material/chips";
import * as i0 from "@angular/core";
export declare class IotInputLabelsComponent extends DynamicFormWidth implements OnInit, OnChanges {
    _template: TemplateRef<any>;
    fruitInput: ElementRef<HTMLInputElement>;
    separatorKeysCodes: number[];
    get template(): TemplateRef<any>;
    get pristine(): any;
    get key(): string;
    get newValue(): string[];
    form: FormGroup;
    label: any;
    attribute: string;
    description: any;
    value: string[];
    copyArray: string[];
    type: string;
    disabled: boolean;
    onChange: EventEmitter<string[]>;
    remove(fruit: string): void;
    add(event: MatChipInputEvent): void;
    ngOnInit(): void;
    ngOnChanges(changes: SimpleChanges): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<IotInputLabelsComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<IotInputLabelsComponent, "iot-input-labels", never, { "label": "label"; "attribute": "attribute"; "description": "description"; "value": "value"; "type": "type"; "disabled": "disabled"; }, { "onChange": "onChange"; }, never, never>;
}
