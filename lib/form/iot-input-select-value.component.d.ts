import { EventEmitter, OnInit, TemplateRef } from '@angular/core';
import { DynamicFormWidth } from './dynamic-form-width';
import { FormGroup } from "@angular/forms";
import * as i0 from "@angular/core";
export declare class IotInputSelectValueComponent extends DynamicFormWidth implements OnInit {
    _template: TemplateRef<any>;
    get template(): TemplateRef<any>;
    get pristine(): any;
    get key(): string;
    get newValue(): any;
    form: FormGroup;
    label: string;
    attribute: string;
    description: string;
    value: boolean;
    separator: boolean;
    values: {
        id: string;
        name: string;
    }[];
    disabled: boolean;
    onChange: EventEmitter<any>;
    change(event: any): void;
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<IotInputSelectValueComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<IotInputSelectValueComponent, "iot-input-select-values", never, { "label": "label"; "attribute": "attribute"; "description": "description"; "value": "value"; "separator": "separator"; "values": "values"; "disabled": "disabled"; }, { "onChange": "onChange"; }, never, never>;
}
