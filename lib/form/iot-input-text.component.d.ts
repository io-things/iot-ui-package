import { EventEmitter, OnChanges, OnInit, SimpleChanges, TemplateRef } from '@angular/core';
import { DynamicFormWidth } from "./dynamic-form-width";
import { FormGroup } from "@angular/forms";
import * as i0 from "@angular/core";
export declare class IotInputTextComponent extends DynamicFormWidth implements OnInit, OnChanges {
    _template: TemplateRef<any>;
    get template(): TemplateRef<any>;
    get pristine(): any;
    get key(): string;
    get newValue(): string;
    form: FormGroup;
    label: any;
    attribute: string;
    description: any;
    value: string;
    type: string;
    disabled: boolean;
    onChange: EventEmitter<string>;
    ngOnInit(): void;
    ngOnChanges(changes: SimpleChanges): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<IotInputTextComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<IotInputTextComponent, "iot-input-text", never, { "label": "label"; "attribute": "attribute"; "description": "description"; "value": "value"; "type": "type"; "disabled": "disabled"; }, { "onChange": "onChange"; }, never, never>;
}
