import { EventEmitter, OnInit, TemplateRef } from '@angular/core';
import { DynamicFormWidth } from "./dynamic-form-width";
import { FormGroup } from "@angular/forms";
import * as i0 from "@angular/core";
export declare class IotDatePickerComponent extends DynamicFormWidth implements OnInit {
    _template: TemplateRef<any>;
    timestampInSec: boolean;
    get template(): TemplateRef<any>;
    get pristine(): any;
    get key(): string;
    get newValue(): any;
    form: FormGroup;
    label: any;
    attribute: string;
    description: string;
    value: any;
    type: string;
    disabled: boolean;
    onChange: EventEmitter<string>;
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<IotDatePickerComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<IotDatePickerComponent, "iot-date-picker", never, { "label": "label"; "attribute": "attribute"; "description": "description"; "value": "value"; "type": "type"; "disabled": "disabled"; }, { "onChange": "onChange"; }, never, never>;
}
