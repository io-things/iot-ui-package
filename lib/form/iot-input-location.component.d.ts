import { EventEmitter, OnInit, TemplateRef } from '@angular/core';
import { DynamicFormWidth } from "./dynamic-form-width";
import { FormGroup } from "@angular/forms";
import * as i0 from "@angular/core";
export declare class IotInputLocationComponent extends DynamicFormWidth implements OnInit {
    _template: TemplateRef<any>;
    zoom: number;
    lat: number;
    lng: number;
    latCopy: number;
    lngCopy: number;
    zoomCopy: number;
    get template(): TemplateRef<any>;
    get pristine(): any;
    get key(): string;
    get newValue(): any;
    form: FormGroup;
    markerChange(event: any): void;
    zoomChange(event: any): void;
    label: any;
    attribute: string;
    value: any;
    type: string;
    disabled: boolean;
    onChange: EventEmitter<string>;
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<IotInputLocationComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<IotInputLocationComponent, "iot-input-location", never, { "zoom": "zoom"; "lat": "lat"; "lng": "lng"; "label": "label"; "attribute": "attribute"; "value": "value"; "type": "type"; "disabled": "disabled"; }, { "onChange": "onChange"; }, never, never>;
}
