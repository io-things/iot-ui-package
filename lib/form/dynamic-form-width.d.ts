import { MediaMatcher } from '@angular/cdk/layout';
import * as i0 from "@angular/core";
export declare class DynamicFormWidth {
    mediaMatcher: MediaMatcher;
    label: string;
    offset: number;
    isBig: boolean;
    mediaQueryList: MediaQueryList;
    private eventListener;
    constructor(mediaMatcher: MediaMatcher);
    get template(): any;
    get pristine(): any;
    get key(): string;
    get newValue(): any;
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<DynamicFormWidth, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<DynamicFormWidth, "[test]", never, { "label": "label"; }, {}, never>;
}
