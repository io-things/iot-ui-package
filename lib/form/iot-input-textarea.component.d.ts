import { EventEmitter, OnInit, TemplateRef } from '@angular/core';
import { DynamicFormWidth } from "./dynamic-form-width";
import { FormGroup } from "@angular/forms";
import * as i0 from "@angular/core";
export declare class IotInputTextareaComponent extends DynamicFormWidth implements OnInit {
    _template: TemplateRef<any>;
    get template(): TemplateRef<any>;
    get pristine(): any;
    get key(): string;
    get newValue(): any;
    form: FormGroup;
    label: any;
    attribute: string;
    value: any;
    placeholder: any;
    rows: number;
    disabled: boolean;
    onChange: EventEmitter<string>;
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<IotInputTextareaComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<IotInputTextareaComponent, "iot-input-textarea", never, { "label": "label"; "attribute": "attribute"; "value": "value"; "placeholder": "placeholder"; "rows": "rows"; "disabled": "disabled"; }, { "onChange": "onChange"; }, never, never>;
}
