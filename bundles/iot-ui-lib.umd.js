(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/material/menu'), require('@angular/material/card'), require('@angular/material/tooltip'), require('@angular/material/button'), require('@angular/material/icon'), require('@angular/router'), require('@angular/material/sidenav'), require('@angular/material/list'), require('@angular/material/core'), require('@ngx-translate/core'), require('@angular/common'), require('@angular/material/grid-list'), require('moment'), require('@angular/material/table'), require('@angular/material/dialog'), require('@angular/cdk/layout'), require('rxjs'), require('@angular/material/progress-spinner'), require('@angular/forms'), require('@angular/material/input'), require('@angular/material/select'), require('@angular/material/form-field'), require('@angular/material/datepicker'), require('@agm/core'), require('@angular/material/checkbox'), require('@angular/cdk/keycodes'), require('@angular/material/chips')) :
    typeof define === 'function' && define.amd ? define('iot-ui-lib', ['exports', '@angular/core', '@angular/material/menu', '@angular/material/card', '@angular/material/tooltip', '@angular/material/button', '@angular/material/icon', '@angular/router', '@angular/material/sidenav', '@angular/material/list', '@angular/material/core', '@ngx-translate/core', '@angular/common', '@angular/material/grid-list', 'moment', '@angular/material/table', '@angular/material/dialog', '@angular/cdk/layout', 'rxjs', '@angular/material/progress-spinner', '@angular/forms', '@angular/material/input', '@angular/material/select', '@angular/material/form-field', '@angular/material/datepicker', '@agm/core', '@angular/material/checkbox', '@angular/cdk/keycodes', '@angular/material/chips'], factory) :
    (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global['iot-ui-lib'] = {}, global.ng.core, global.ng.material.menu, global.ng.material.card, global.ng.material.tooltip, global.ng.material.button, global.ng.material.icon, global.ng.router, global.ng.material.sidenav, global.ng.material.list, global.ng.material.core, global.i5, global.ng.common, global.ng.material.gridList, global.moment, global.ng.material.table, global.ng.material.dialog, global.ng.cdk.layout, global.rxjs, global.ng.material.progressSpinner, global.ng.forms, global.ng.material.input, global.ng.material.select, global.ng.material.formField, global.ng.material.datepicker, global.i1$8, global.ng.material.checkbox, global.ng.cdk.keycodes, global.ng.material.chips));
}(this, (function (exports, i0, i4, i2, i3, i1, i2$1, i1$2, i2$2, i1$1, i4$1, i5, i4$2, i1$3, moment, i2$3, i1$4, i1$5, rxjs, i2$4, i4$3, i3$1, i1$6, i1$7, i2$5, i1$8, i1$9, keycodes, i1$a) { 'use strict';

    function _interopNamespace(e) {
        if (e && e.__esModule) return e;
        var n = Object.create(null);
        if (e) {
            Object.keys(e).forEach(function (k) {
                if (k !== 'default') {
                    var d = Object.getOwnPropertyDescriptor(e, k);
                    Object.defineProperty(n, k, d.get ? d : {
                        enumerable: true,
                        get: function () {
                            return e[k];
                        }
                    });
                }
            });
        }
        n['default'] = e;
        return Object.freeze(n);
    }

    var i0__namespace = /*#__PURE__*/_interopNamespace(i0);
    var i4__namespace = /*#__PURE__*/_interopNamespace(i4);
    var i2__namespace = /*#__PURE__*/_interopNamespace(i2);
    var i3__namespace = /*#__PURE__*/_interopNamespace(i3);
    var i1__namespace = /*#__PURE__*/_interopNamespace(i1);
    var i2__namespace$1 = /*#__PURE__*/_interopNamespace(i2$1);
    var i1__namespace$2 = /*#__PURE__*/_interopNamespace(i1$2);
    var i2__namespace$2 = /*#__PURE__*/_interopNamespace(i2$2);
    var i1__namespace$1 = /*#__PURE__*/_interopNamespace(i1$1);
    var i4__namespace$1 = /*#__PURE__*/_interopNamespace(i4$1);
    var i5__namespace = /*#__PURE__*/_interopNamespace(i5);
    var i4__namespace$2 = /*#__PURE__*/_interopNamespace(i4$2);
    var i1__namespace$3 = /*#__PURE__*/_interopNamespace(i1$3);
    var moment__namespace = /*#__PURE__*/_interopNamespace(moment);
    var i2__namespace$3 = /*#__PURE__*/_interopNamespace(i2$3);
    var i1__namespace$4 = /*#__PURE__*/_interopNamespace(i1$4);
    var i1__namespace$5 = /*#__PURE__*/_interopNamespace(i1$5);
    var i2__namespace$4 = /*#__PURE__*/_interopNamespace(i2$4);
    var i4__namespace$3 = /*#__PURE__*/_interopNamespace(i4$3);
    var i3__namespace$1 = /*#__PURE__*/_interopNamespace(i3$1);
    var i1__namespace$6 = /*#__PURE__*/_interopNamespace(i1$6);
    var i1__namespace$7 = /*#__PURE__*/_interopNamespace(i1$7);
    var i2__namespace$5 = /*#__PURE__*/_interopNamespace(i2$5);
    var i1__namespace$8 = /*#__PURE__*/_interopNamespace(i1$8);
    var i1__namespace$9 = /*#__PURE__*/_interopNamespace(i1$9);
    var i1__namespace$a = /*#__PURE__*/_interopNamespace(i1$a);

    var IotUiLibService = /** @class */ (function () {
        function IotUiLibService() {
        }
        return IotUiLibService;
    }());
    IotUiLibService.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotUiLibService, deps: [], target: i0__namespace.ɵɵFactoryTarget.Injectable });
    IotUiLibService.ɵprov = i0__namespace.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotUiLibService, providedIn: 'root' });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotUiLibService, decorators: [{
                type: i0.Injectable,
                args: [{
                        providedIn: 'root'
                    }]
            }], ctorParameters: function () { return []; } });

    var AccountMenuDisplayButtonComponent = /** @class */ (function () {
        function AccountMenuDisplayButtonComponent() {
            this.avatar = '--';
        }
        AccountMenuDisplayButtonComponent.prototype.ngOnInit = function () {
        };
        AccountMenuDisplayButtonComponent.prototype.ngOnChanges = function (changes) {
            var _this = this;
            if (changes.user) {
                this.avatar = '';
                (this.user.displayName || '-').split(' ')
                    .filter(function (w) { return w != ''; }).forEach(function (word) { return _this.avatar += word[0]; });
            }
        };
        return AccountMenuDisplayButtonComponent;
    }());
    AccountMenuDisplayButtonComponent.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: AccountMenuDisplayButtonComponent, deps: [], target: i0__namespace.ɵɵFactoryTarget.Component });
    AccountMenuDisplayButtonComponent.ɵcmp = i0__namespace.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: AccountMenuDisplayButtonComponent, selector: "iot-account-menu-display-button", inputs: { user: "user" }, usesOnChanges: true, ngImport: i0__namespace, template: "\n<!--    <button mat-button [matMenuTriggerFor]=\"aboveMenu\">Above</button>-->\n<!--    <mat-menu #aboveMenu=\"matMenu\" yPosition=\"above\">-->\n<!--      <button mat-menu-item>Item 1</button>-->\n<!--      <button mat-menu-item>Item 2</button>-->\n<!--    </mat-menu>-->\n<!--    -->\n    <a class=\"outer\" [matMenuTriggerFor]=\"accountMenu\" matTooltip=\"Account\">\n\n      <div class=\"avatar\">\n          {{avatar}}\n      </div>\n      <mat-menu #accountMenu=\"matMenu\" yPosition=\"below\" xPosition=\"before\" >\n        <mat-card  class=\"mat-elevation-z0\">\n          <mat-card-header>\n            <div class=\"avatar\" mat-card-avatar>\n              {{avatar}}\n            </div>\n\n            <mat-card-title> {{user.displayName}}</mat-card-title>\n            <mat-card-subtitle>{{user.email}}</mat-card-subtitle>\n          </mat-card-header>\n\n          <mat-card-actions>\n            <ng-content select=\"[button1]\"></ng-content>\n            <ng-content select=\"[button2]\"></ng-content>\n<!--            <button mat-button >{{'HOME.SIGNOUT'}}</button>-->\n\n          </mat-card-actions>\n        </mat-card>\n      </mat-menu>\n    </a>\n\n  ", isInline: true, styles: ["\n      .outer {\n        position: relative;\n      }\n\n      .avatar {\n        border-radius: 50%;\n        display: flex;\n        justify-content: center;\n        align-items: center;\n        color: rgba(0, 0, 0, 0.3);\n\n        right: 0px;\n        width: 40px;\n        height: 40px;\n        background: #E4E9EB 0% 0% no-repeat padding-box;\n        opacity: 1;\n      }\n    "], components: [{ type: i4__namespace.MatMenu, selector: "mat-menu", exportAs: ["matMenu"] }, { type: i2__namespace.MatCard, selector: "mat-card", exportAs: ["matCard"] }, { type: i2__namespace.MatCardHeader, selector: "mat-card-header" }], directives: [{ type: i3__namespace.MatTooltip, selector: "[matTooltip]", exportAs: ["matTooltip"] }, { type: i4__namespace.MatMenuTrigger, selector: "[mat-menu-trigger-for], [matMenuTriggerFor]", exportAs: ["matMenuTrigger"] }, { type: i2__namespace.MatCardAvatar, selector: "[mat-card-avatar], [matCardAvatar]" }, { type: i2__namespace.MatCardTitle, selector: "mat-card-title, [mat-card-title], [matCardTitle]" }, { type: i2__namespace.MatCardSubtitle, selector: "mat-card-subtitle, [mat-card-subtitle], [matCardSubtitle]" }, { type: i2__namespace.MatCardActions, selector: "mat-card-actions", inputs: ["align"], exportAs: ["matCardActions"] }] });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: AccountMenuDisplayButtonComponent, decorators: [{
                type: i0.Component,
                args: [{
                        selector: 'iot-account-menu-display-button',
                        template: "\n<!--    <button mat-button [matMenuTriggerFor]=\"aboveMenu\">Above</button>-->\n<!--    <mat-menu #aboveMenu=\"matMenu\" yPosition=\"above\">-->\n<!--      <button mat-menu-item>Item 1</button>-->\n<!--      <button mat-menu-item>Item 2</button>-->\n<!--    </mat-menu>-->\n<!--    -->\n    <a class=\"outer\" [matMenuTriggerFor]=\"accountMenu\" matTooltip=\"Account\">\n\n      <div class=\"avatar\">\n          {{avatar}}\n      </div>\n      <mat-menu #accountMenu=\"matMenu\" yPosition=\"below\" xPosition=\"before\" >\n        <mat-card  class=\"mat-elevation-z0\">\n          <mat-card-header>\n            <div class=\"avatar\" mat-card-avatar>\n              {{avatar}}\n            </div>\n\n            <mat-card-title> {{user.displayName}}</mat-card-title>\n            <mat-card-subtitle>{{user.email}}</mat-card-subtitle>\n          </mat-card-header>\n\n          <mat-card-actions>\n            <ng-content select=\"[button1]\"></ng-content>\n            <ng-content select=\"[button2]\"></ng-content>\n<!--            <button mat-button >{{'HOME.SIGNOUT'}}</button>-->\n\n          </mat-card-actions>\n        </mat-card>\n      </mat-menu>\n    </a>\n\n  ",
                        styles: [
                            "\n      .outer {\n        position: relative;\n      }\n\n      .avatar {\n        border-radius: 50%;\n        display: flex;\n        justify-content: center;\n        align-items: center;\n        color: rgba(0, 0, 0, 0.3);\n\n        right: 0px;\n        width: 40px;\n        height: 40px;\n        background: #E4E9EB 0% 0% no-repeat padding-box;\n        opacity: 1;\n      }\n    "
                        ]
                    }]
            }], ctorParameters: function () { return []; }, propDecorators: { user: [{
                    type: i0.Input
                }] } });

    var TopNavBarComponent = /** @class */ (function () {
        function TopNavBarComponent() {
            this.menuClick = new i0.EventEmitter();
        }
        TopNavBarComponent.prototype.ngOnInit = function () {
        };
        return TopNavBarComponent;
    }());
    TopNavBarComponent.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: TopNavBarComponent, deps: [], target: i0__namespace.ɵɵFactoryTarget.Component });
    TopNavBarComponent.ɵcmp = i0__namespace.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: TopNavBarComponent, selector: "iot-top-nav-bar", outputs: { menuClick: "menuClick" }, ngImport: i0__namespace, template: "\n    <nav >\n      <button\n        mat-icon-button (click)=\"menuClick.emit($event)\">\n        <mat-icon>menu</mat-icon>\n      </button>\n      <ng-content select=\"[portaltitle]\"></ng-content>\n      <ng-content select=\"[customerselect]\"></ng-content>\n      <div class=\"flex-spacer\"></div>\n\n      <ng-content select=\"[icon1]\"></ng-content>\n      <ng-content select=\"[language]\"></ng-content>\n      <ng-content select=\"[account]\"></ng-content>\n    </nav>\n  ", isInline: true, components: [{ type: i1__namespace.MatButton, selector: "button[mat-button], button[mat-raised-button], button[mat-icon-button],             button[mat-fab], button[mat-mini-fab], button[mat-stroked-button],             button[mat-flat-button]", inputs: ["disabled", "disableRipple", "color"], exportAs: ["matButton"] }, { type: i2__namespace$1.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }] });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: TopNavBarComponent, decorators: [{
                type: i0.Component,
                args: [{
                        selector: 'iot-top-nav-bar',
                        template: "\n    <nav >\n      <button\n        mat-icon-button (click)=\"menuClick.emit($event)\">\n        <mat-icon>menu</mat-icon>\n      </button>\n      <ng-content select=\"[portaltitle]\"></ng-content>\n      <ng-content select=\"[customerselect]\"></ng-content>\n      <div class=\"flex-spacer\"></div>\n\n      <ng-content select=\"[icon1]\"></ng-content>\n      <ng-content select=\"[language]\"></ng-content>\n      <ng-content select=\"[account]\"></ng-content>\n    </nav>\n  ",
                        styles: []
                    }]
            }], ctorParameters: function () { return []; }, propDecorators: { menuClick: [{
                    type: i0.Output
                }] } });

    var SingleListEntryComponent = /** @class */ (function () {
        function SingleListEntryComponent() {
            this.routerLink = '';
        }
        SingleListEntryComponent.prototype.ngOnChanges = function (changes) {
            if (this.params) {
                this.ngOnInit();
            }
        };
        SingleListEntryComponent.prototype.ngOnInit = function () {
            var _this = this;
            if (this.entry.route && this.params) {
                this.routerLink = this.entry.route;
                this.params.keys.forEach(function (key) {
                    if (_this.entry.route.indexOf(":" + key) !== -1) {
                        _this.routerLink = _this.routerLink.replace(":" + key, _this.params.get(key));
                    }
                });
                Object.keys(this.paramsExtra || {}).forEach(function (key) {
                    if (_this.entry.route.indexOf(":" + key) !== -1) {
                        _this.routerLink = _this.routerLink.replace(":" + key, '' + _this.paramsExtra[key]);
                    }
                });
            }
        };
        return SingleListEntryComponent;
    }());
    SingleListEntryComponent.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: SingleListEntryComponent, deps: [], target: i0__namespace.ɵɵFactoryTarget.Component });
    SingleListEntryComponent.ɵcmp = i0__namespace.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: SingleListEntryComponent, selector: "iot-single-list-entry", inputs: { entry: "entry", params: "params", paramsExtra: "paramsExtra" }, usesOnChanges: true, ngImport: i0__namespace, template: "\n    <mat-list-item\n      [routerLink]=\"routerLink\">\n      <mat-icon mat-list-icon>{{entry.icon }}</mat-icon>\n      <a matLine>{{entry.label | translate}} </a>\n    </mat-list-item>\n  ", isInline: true, components: [{ type: i1__namespace$1.MatListItem, selector: "mat-list-item, a[mat-list-item], button[mat-list-item]", inputs: ["disableRipple", "disabled"], exportAs: ["matListItem"] }, { type: i2__namespace$1.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }], directives: [{ type: i1__namespace$2.RouterLink, selector: ":not(a):not(area)[routerLink]", inputs: ["routerLink", "queryParams", "fragment", "queryParamsHandling", "preserveFragment", "skipLocationChange", "replaceUrl", "state", "relativeTo"] }, { type: i1__namespace$1.MatListIconCssMatStyler, selector: "[mat-list-icon], [matListIcon]" }, { type: i4__namespace$1.MatLine, selector: "[mat-line], [matLine]" }], pipes: { "translate": i5__namespace.TranslatePipe } });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: SingleListEntryComponent, decorators: [{
                type: i0.Component,
                args: [{
                        selector: 'iot-single-list-entry',
                        template: "\n    <mat-list-item\n      [routerLink]=\"routerLink\">\n      <mat-icon mat-list-icon>{{entry.icon }}</mat-icon>\n      <a matLine>{{entry.label | translate}} </a>\n    </mat-list-item>\n  ",
                        styles: []
                    }]
            }], ctorParameters: function () { return []; }, propDecorators: { entry: [{
                    type: i0.Input
                }], params: [{
                    type: i0.Input
                }], paramsExtra: [{
                    type: i0.Input
                }] } });

    var CompositeListEntryComponent = /** @class */ (function () {
        function CompositeListEntryComponent() {
            this.entry = {};
            this.showSettingsMenu = false;
        }
        CompositeListEntryComponent.prototype.ngOnInit = function () {
        };
        return CompositeListEntryComponent;
    }());
    CompositeListEntryComponent.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: CompositeListEntryComponent, deps: [], target: i0__namespace.ɵɵFactoryTarget.Component });
    CompositeListEntryComponent.ɵcmp = i0__namespace.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: CompositeListEntryComponent, selector: "iot-composite-list-entry", inputs: { entry: "entry", params: "params", paramsExtra: "paramsExtra" }, ngImport: i0__namespace, template: "\n    <mat-list-item\n      (click)=\"showSettingsMenu = !showSettingsMenu\"\n    >\n      <mat-icon mat-list-icon>{{entry.icon }}</mat-icon>\n      <a matLine>{{entry.label | translate}}</a>\n      <mat-icon class=\"menu-button\" [ngClass]=\"{'rotated' : showSettingsMenu}\">expand_more\n      </mat-icon>\n    </mat-list-item>\n    <div class=\"submenu\" [ngClass]=\"{'expanded' : showSettingsMenu}\"\n         *ngIf=\"showSettingsMenu\">\n      <iot-single-list-entry\n        *ngFor=\"let item of entry.items\"\n        [entry]=\"item\"\n        [params]=\"params\"\n        [paramsExtra]=\"paramsExtra\"\n      ></iot-single-list-entry>\n    </div>\n  ", isInline: true, styles: ["\n    .menu-button {\n      transition: 300ms ease-in-out;\n      transform: rotate(0deg);\n    }\n\n    .menu-button.rotated {\n      transform: rotate(180deg);\n    }\n\n    .submenu {\n      overflow-y: hidden;\n      height: 0px;\n      transition: transform 300ms ease;\n      transform: scaleY(0);\n      transform-origin: top;\n      padding-left: 30px;\n    }\n\n    .submenu.expanded {\n      transform: scaleY(1);\n      height: unset;\n    }\n  "], components: [{ type: i1__namespace$1.MatListItem, selector: "mat-list-item, a[mat-list-item], button[mat-list-item]", inputs: ["disableRipple", "disabled"], exportAs: ["matListItem"] }, { type: i2__namespace$1.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }, { type: SingleListEntryComponent, selector: "iot-single-list-entry", inputs: ["entry", "params", "paramsExtra"] }], directives: [{ type: i1__namespace$1.MatListIconCssMatStyler, selector: "[mat-list-icon], [matListIcon]" }, { type: i4__namespace$1.MatLine, selector: "[mat-line], [matLine]" }, { type: i4__namespace$2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i4__namespace$2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i4__namespace$2.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }], pipes: { "translate": i5__namespace.TranslatePipe } });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: CompositeListEntryComponent, decorators: [{
                type: i0.Component,
                args: [{
                        selector: 'iot-composite-list-entry',
                        template: "\n    <mat-list-item\n      (click)=\"showSettingsMenu = !showSettingsMenu\"\n    >\n      <mat-icon mat-list-icon>{{entry.icon }}</mat-icon>\n      <a matLine>{{entry.label | translate}}</a>\n      <mat-icon class=\"menu-button\" [ngClass]=\"{'rotated' : showSettingsMenu}\">expand_more\n      </mat-icon>\n    </mat-list-item>\n    <div class=\"submenu\" [ngClass]=\"{'expanded' : showSettingsMenu}\"\n         *ngIf=\"showSettingsMenu\">\n      <iot-single-list-entry\n        *ngFor=\"let item of entry.items\"\n        [entry]=\"item\"\n        [params]=\"params\"\n        [paramsExtra]=\"paramsExtra\"\n      ></iot-single-list-entry>\n    </div>\n  ",
                        styles: ["\n    .menu-button {\n      transition: 300ms ease-in-out;\n      transform: rotate(0deg);\n    }\n\n    .menu-button.rotated {\n      transform: rotate(180deg);\n    }\n\n    .submenu {\n      overflow-y: hidden;\n      height: 0px;\n      transition: transform 300ms ease;\n      transform: scaleY(0);\n      transform-origin: top;\n      padding-left: 30px;\n    }\n\n    .submenu.expanded {\n      transform: scaleY(1);\n      height: unset;\n    }\n  "
                        ]
                    }]
            }], ctorParameters: function () { return []; }, propDecorators: { entry: [{
                    type: i0.Input
                }], params: [{
                    type: i0.Input
                }], paramsExtra: [{
                    type: i0.Input
                }] } });

    var SideNavBarComponent = /** @class */ (function () {
        function SideNavBarComponent(route) {
            this.route = route;
            this.belowTopNav = true;
            this.isMobile = true;
            this.isOpen = true;
            this.items = [];
        }
        SideNavBarComponent.prototype.ngOnInit = function () {
            var _this = this;
            if (this.data) {
                this.items = this.data.items;
            }
            this.sub = this.route
                .paramMap
                .subscribe(function (pMap) {
                _this.paramMap = pMap;
            });
        };
        SideNavBarComponent.prototype.ngOnDestroy = function () {
            if (this.sub) {
                this.sub.unsubscribe();
            }
        };
        SideNavBarComponent.prototype.ngOnChanges = function (changes) {
            if (changes.data && this.data) {
                this.items = this.data.items;
            }
            if (changes.params) {
                this.items = [].concat(this.items); //[...this.items];
            }
        };
        return SideNavBarComponent;
    }());
    SideNavBarComponent.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: SideNavBarComponent, deps: [{ token: i1__namespace$2.ActivatedRoute }], target: i0__namespace.ɵɵFactoryTarget.Component });
    SideNavBarComponent.ɵcmp = i0__namespace.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: SideNavBarComponent, selector: "iot-side-nav-bar", inputs: { belowTopNav: "belowTopNav", isMobile: "isMobile", isOpen: "isOpen", data: "data", params: "params", width: "width" }, usesOnChanges: true, ngImport: i0__namespace, template: "\n    <mat-drawer-container\n      autosize [hasBackdrop]=\"false\"\n    >\n      <mat-drawer\n        [ngStyle]=\"{'width': width == undefined?'unset':width}\"\n        #drawer [mode]=\"(isMobile)?'over':'side'\"\n                  [opened]=\"isOpen\">\n        <ng-content select=\"[sideNavTop]\"></ng-content>\n        <mat-nav-list>\n          <div *ngFor=\"let item of items\">\n            <iot-single-list-entry\n              [entry]=\"item\"\n              [params]=\"paramMap\"\n              [paramsExtra]=\"params\"\n              *ngIf=\"!item.items\"\n            ></iot-single-list-entry>\n            <iot-composite-list-entry\n              [entry]=\"item\"\n              [params]=\"paramMap\"\n              [paramsExtra]=\"params\"\n              *ngIf=\"item.items\"\n            >\n            </iot-composite-list-entry>\n          </div>\n        </mat-nav-list>\n      </mat-drawer>\n      <ng-content></ng-content>\n      <router-outlet></router-outlet>\n    </mat-drawer-container>\n\n  ", isInline: true, styles: ["\n\n    "], components: [{ type: i2__namespace$2.MatDrawerContainer, selector: "mat-drawer-container", inputs: ["autosize", "hasBackdrop"], outputs: ["backdropClick"], exportAs: ["matDrawerContainer"] }, { type: i2__namespace$2.MatDrawer, selector: "mat-drawer", inputs: ["position", "mode", "disableClose", "autoFocus", "opened"], outputs: ["openedChange", "opened", "openedStart", "closed", "closedStart", "positionChanged"], exportAs: ["matDrawer"] }, { type: i1__namespace$1.MatNavList, selector: "mat-nav-list", inputs: ["disableRipple", "disabled"], exportAs: ["matNavList"] }, { type: SingleListEntryComponent, selector: "iot-single-list-entry", inputs: ["entry", "params", "paramsExtra"] }, { type: CompositeListEntryComponent, selector: "iot-composite-list-entry", inputs: ["entry", "params", "paramsExtra"] }], directives: [{ type: i4__namespace$2.NgStyle, selector: "[ngStyle]", inputs: ["ngStyle"] }, { type: i4__namespace$2.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { type: i4__namespace$2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i1__namespace$2.RouterOutlet, selector: "router-outlet", outputs: ["activate", "deactivate"], exportAs: ["outlet"] }] });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: SideNavBarComponent, decorators: [{
                type: i0.Component,
                args: [{
                        selector: 'iot-side-nav-bar',
                        template: "\n    <mat-drawer-container\n      autosize [hasBackdrop]=\"false\"\n    >\n      <mat-drawer\n        [ngStyle]=\"{'width': width == undefined?'unset':width}\"\n        #drawer [mode]=\"(isMobile)?'over':'side'\"\n                  [opened]=\"isOpen\">\n        <ng-content select=\"[sideNavTop]\"></ng-content>\n        <mat-nav-list>\n          <div *ngFor=\"let item of items\">\n            <iot-single-list-entry\n              [entry]=\"item\"\n              [params]=\"paramMap\"\n              [paramsExtra]=\"params\"\n              *ngIf=\"!item.items\"\n            ></iot-single-list-entry>\n            <iot-composite-list-entry\n              [entry]=\"item\"\n              [params]=\"paramMap\"\n              [paramsExtra]=\"params\"\n              *ngIf=\"item.items\"\n            >\n            </iot-composite-list-entry>\n          </div>\n        </mat-nav-list>\n      </mat-drawer>\n      <ng-content></ng-content>\n      <router-outlet></router-outlet>\n    </mat-drawer-container>\n\n  ",
                        styles: [
                            "\n\n    "
                        ]
                    }]
            }], ctorParameters: function () { return [{ type: i1__namespace$2.ActivatedRoute }]; }, propDecorators: { belowTopNav: [{
                    type: i0.Input
                }], isMobile: [{
                    type: i0.Input
                }], isOpen: [{
                    type: i0.Input
                }], data: [{
                    type: i0.Input
                }], params: [{
                    type: i0.Input
                }], width: [{
                    type: i0.Input
                }] } });

    var CustomerSelectorComponent = /** @class */ (function () {
        function CustomerSelectorComponent() {
            this.mobile = false;
            this.label = '';
            this.click = new i0.EventEmitter();
        }
        CustomerSelectorComponent.prototype.openDialog = function () {
            // const dialogRef = this.dialog.open(CustomerSelectComponent, {
            //   width: '760px',
            //   data: {mobile: this.mobile}
            // });
        };
        CustomerSelectorComponent.prototype.ngOnInit = function () {
        };
        return CustomerSelectorComponent;
    }());
    CustomerSelectorComponent.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: CustomerSelectorComponent, deps: [], target: i0__namespace.ɵɵFactoryTarget.Component });
    CustomerSelectorComponent.ɵcmp = i0__namespace.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: CustomerSelectorComponent, selector: "iot-customer-selector", inputs: { mobile: "mobile", label: "label" }, outputs: { click: "click" }, ngImport: i0__namespace, template: "\n    <div class=\"outer-switch-button\" [ngClass]=\"{mobile: mobile}\" (click)=\"openDialog()\">\n      <button\n        (click)=\"click.emit();$event.stopPropagation();\"\n        mat-button >\n        <div class=\"button-text\">{{label}}</div>\n      </button>\n      <button\n        mat-icon-button class=\"display-button\">\n        <mat-icon class=\"icon\">unfold_more</mat-icon>\n      </button>\n    </div>\n  ", isInline: true, styles: ["\n        .outer-switch-button {\n          box-sizing: border-box;\n            display: flex;\n            align-items: center;\n            height: 36px;\n            margin: 0 8px;\n            border-radius: 4px;\n            border: 1px solid rgba(255, 255, 255, .42);\n            overflow: hidden;\n            /*margin-bottom: 20px;*/\n        }\n\n\n        .mobile.outer-switch-button {\n            border: 1px solid #dadce0;;\n            margin-left: 20px;\n            width:244px;\n            border-radius: 8px;\n        }\n\n        .outer-switch-button .button-text {\n            font-weight: 300;\n            font-size: 14px;\n            font-family: \"roboto\";\n        }\n\n        .display-button {\n            height: 36px;\n            min-width: 36px;\n            padding: 0;\n            border-left: 1px solid rgba(255, 255, 255, .42);\n            border-radius: 0;\n            color: #fff;\n            margin-right: 0;\n        }\n\n        .icon {\n            position: absolute;\n            top: 6px;\n            left: 8px;\n            font-size: 20px;\n        }\n\n    "], components: [{ type: i1__namespace.MatButton, selector: "button[mat-button], button[mat-raised-button], button[mat-icon-button],             button[mat-fab], button[mat-mini-fab], button[mat-stroked-button],             button[mat-flat-button]", inputs: ["disabled", "disableRipple", "color"], exportAs: ["matButton"] }, { type: i2__namespace$1.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }], directives: [{ type: i4__namespace$2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }] });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: CustomerSelectorComponent, decorators: [{
                type: i0.Component,
                args: [{
                        selector: 'iot-customer-selector',
                        template: "\n    <div class=\"outer-switch-button\" [ngClass]=\"{mobile: mobile}\" (click)=\"openDialog()\">\n      <button\n        (click)=\"click.emit();$event.stopPropagation();\"\n        mat-button >\n        <div class=\"button-text\">{{label}}</div>\n      </button>\n      <button\n        mat-icon-button class=\"display-button\">\n        <mat-icon class=\"icon\">unfold_more</mat-icon>\n      </button>\n    </div>\n  ",
                        styles: ["\n        .outer-switch-button {\n          box-sizing: border-box;\n            display: flex;\n            align-items: center;\n            height: 36px;\n            margin: 0 8px;\n            border-radius: 4px;\n            border: 1px solid rgba(255, 255, 255, .42);\n            overflow: hidden;\n            /*margin-bottom: 20px;*/\n        }\n\n\n        .mobile.outer-switch-button {\n            border: 1px solid #dadce0;;\n            margin-left: 20px;\n            width:244px;\n            border-radius: 8px;\n        }\n\n        .outer-switch-button .button-text {\n            font-weight: 300;\n            font-size: 14px;\n            font-family: \"roboto\";\n        }\n\n        .display-button {\n            height: 36px;\n            min-width: 36px;\n            padding: 0;\n            border-left: 1px solid rgba(255, 255, 255, .42);\n            border-radius: 0;\n            color: #fff;\n            margin-right: 0;\n        }\n\n        .icon {\n            position: absolute;\n            top: 6px;\n            left: 8px;\n            font-size: 20px;\n        }\n\n    "]
                    }]
            }], ctorParameters: function () { return []; }, propDecorators: { mobile: [{
                    type: i0.Input
                }], label: [{
                    type: i0.Input
                }], click: [{
                    type: i0.Output
                }] } });

    var CustomerSelectorButtonComponent = /** @class */ (function () {
        function CustomerSelectorButtonComponent() {
            this.label = '';
            this.click = new i0.EventEmitter();
        }
        return CustomerSelectorButtonComponent;
    }());
    CustomerSelectorButtonComponent.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: CustomerSelectorButtonComponent, deps: [], target: i0__namespace.ɵɵFactoryTarget.Component });
    CustomerSelectorButtonComponent.ɵcmp = i0__namespace.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: CustomerSelectorButtonComponent, selector: "iot-customer-selector-button", inputs: { label: "label" }, outputs: { click: "click" }, ngImport: i0__namespace, template: "\n\n    <div class=\"outer-switch-button\"\n         (click)=\"click.emit();$event.stopPropagation();\"\n         >\n      <button mat-button>\n        <div class=\"button-text w-100\">{{label}}</div>\n      </button>\n      <!--            (click)=\"toggleUnfold()\"-->\n\n    </div>\n  ", isInline: true, styles: ["\n    .outer-switch-button {\n      box-sizing: border-box;\n      height: 36px;\n      margin: 8px;\n      border: 1px solid #dadce0;\n      margin-left: 20px;\n      width: 244px;\n      border-radius: 8px;\n      overflow: hidden;\n    }\n\n    .outer-switch-button button {\n      width: 100%;\n    }\n\n    .outer-switch-button button .button-text {\n      font-weight: 300;\n      font-size: 14px;\n      text-align: left;\n    }\n\n  "], components: [{ type: i1__namespace.MatButton, selector: "button[mat-button], button[mat-raised-button], button[mat-icon-button],             button[mat-fab], button[mat-mini-fab], button[mat-stroked-button],             button[mat-flat-button]", inputs: ["disabled", "disableRipple", "color"], exportAs: ["matButton"] }] });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: CustomerSelectorButtonComponent, decorators: [{
                type: i0.Component,
                args: [{
                        selector: 'iot-customer-selector-button',
                        template: "\n\n    <div class=\"outer-switch-button\"\n         (click)=\"click.emit();$event.stopPropagation();\"\n         >\n      <button mat-button>\n        <div class=\"button-text w-100\">{{label}}</div>\n      </button>\n      <!--            (click)=\"toggleUnfold()\"-->\n\n    </div>\n  ",
                        styles: ["\n    .outer-switch-button {\n      box-sizing: border-box;\n      height: 36px;\n      margin: 8px;\n      border: 1px solid #dadce0;\n      margin-left: 20px;\n      width: 244px;\n      border-radius: 8px;\n      overflow: hidden;\n    }\n\n    .outer-switch-button button {\n      width: 100%;\n    }\n\n    .outer-switch-button button .button-text {\n      font-weight: 300;\n      font-size: 14px;\n      text-align: left;\n    }\n\n  "]
                    }]
            }], ctorParameters: function () { return []; }, propDecorators: { label: [{
                    type: i0.Input
                }], click: [{
                    type: i0.Output
                }] } });

    var LanguageSelectorComponent = /** @class */ (function () {
        function LanguageSelectorComponent(translate) {
            this.translate = translate;
            this.lang = 'en';
            this.langList = [];
            this.setLang = new i0.EventEmitter();
            translate.setDefaultLang(this.lang);
        }
        LanguageSelectorComponent.prototype.ngOnInit = function () {
        };
        LanguageSelectorComponent.prototype.selectLang = function (langKey) {
            this.translate.use(langKey);
            this.setLang.emit(langKey);
        };
        LanguageSelectorComponent.prototype.ngOnChanges = function (changes) {
            if (changes.lang) {
                this.translate.setDefaultLang(changes.lang.currentValue);
            }
        };
        return LanguageSelectorComponent;
    }());
    LanguageSelectorComponent.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: LanguageSelectorComponent, deps: [{ token: i5__namespace.TranslateService }], target: i0__namespace.ɵɵFactoryTarget.Component });
    LanguageSelectorComponent.ɵcmp = i0__namespace.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: LanguageSelectorComponent, selector: "iot-language-selector", inputs: { lang: "lang", langList: "langList" }, outputs: { setLang: "setLang" }, usesOnChanges: true, ngImport: i0__namespace, template: "\n    <button mat-icon-button\n            [mat-menu-trigger-for]=\"themeMenu\"\n            matTooltip=\"Select a language!\"\n            tabindex=\"-1\">\n      <mat-icon>language</mat-icon>\n    </button>\n\n\n    <mat-menu #themeMenu=\"matMenu\" x-position=\"before\">\n      <button\n        *ngFor=\"let langEntry of langList\"\n        mat-menu-item\n        (click)=\"selectLang(langEntry.key)\">\n        {{langEntry.name | translate}}\n      </button>\n      <!--      <button mat-menu-item-->\n      <!--              (click)=\"selectLang('nl')\">-->\n      <!--        {{'CONFIG.LANG.NL'}}-->\n      <!--      </button>-->\n    </mat-menu>\n  ", isInline: true, components: [{ type: i1__namespace.MatButton, selector: "button[mat-button], button[mat-raised-button], button[mat-icon-button],             button[mat-fab], button[mat-mini-fab], button[mat-stroked-button],             button[mat-flat-button]", inputs: ["disabled", "disableRipple", "color"], exportAs: ["matButton"] }, { type: i2__namespace$1.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }, { type: i4__namespace.MatMenu, selector: "mat-menu", exportAs: ["matMenu"] }, { type: i4__namespace.MatMenuItem, selector: "[mat-menu-item]", inputs: ["disabled", "disableRipple", "role"], exportAs: ["matMenuItem"] }], directives: [{ type: i3__namespace.MatTooltip, selector: "[matTooltip]", exportAs: ["matTooltip"] }, { type: i4__namespace.MatMenuTrigger, selector: "[mat-menu-trigger-for], [matMenuTriggerFor]", exportAs: ["matMenuTrigger"] }, { type: i4__namespace$2.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }], pipes: { "translate": i5__namespace.TranslatePipe } });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: LanguageSelectorComponent, decorators: [{
                type: i0.Component,
                args: [{
                        selector: 'iot-language-selector',
                        template: "\n    <button mat-icon-button\n            [mat-menu-trigger-for]=\"themeMenu\"\n            matTooltip=\"Select a language!\"\n            tabindex=\"-1\">\n      <mat-icon>language</mat-icon>\n    </button>\n\n\n    <mat-menu #themeMenu=\"matMenu\" x-position=\"before\">\n      <button\n        *ngFor=\"let langEntry of langList\"\n        mat-menu-item\n        (click)=\"selectLang(langEntry.key)\">\n        {{langEntry.name | translate}}\n      </button>\n      <!--      <button mat-menu-item-->\n      <!--              (click)=\"selectLang('nl')\">-->\n      <!--        {{'CONFIG.LANG.NL'}}-->\n      <!--      </button>-->\n    </mat-menu>\n  ",
                        styles: []
                    }]
            }], ctorParameters: function () { return [{ type: i5__namespace.TranslateService }]; }, propDecorators: { lang: [{
                    type: i0.Input
                }], langList: [{
                    type: i0.Input
                }], setLang: [{
                    type: i0.Output
                }] } });

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */
    /* global Reflect, Promise */
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b)
                if (Object.prototype.hasOwnProperty.call(b, p))
                    d[p] = b[p]; };
        return extendStatics(d, b);
    };
    function __extends(d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }
    var __assign = function () {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s)
                    if (Object.prototype.hasOwnProperty.call(s, p))
                        t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };
    function __rest(s, e) {
        var t = {};
        for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
                t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }
    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function")
            r = Reflect.decorate(decorators, target, key, desc);
        else
            for (var i = decorators.length - 1; i >= 0; i--)
                if (d = decorators[i])
                    r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }
    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); };
    }
    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function")
            return Reflect.metadata(metadataKey, metadataValue);
    }
    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try {
                step(generator.next(value));
            }
            catch (e) {
                reject(e);
            } }
            function rejected(value) { try {
                step(generator["throw"](value));
            }
            catch (e) {
                reject(e);
            } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }
    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function () { if (t[0] & 1)
                throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function () { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f)
                throw new TypeError("Generator is already executing.");
            while (_)
                try {
                    if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done)
                        return t;
                    if (y = 0, t)
                        op = [op[0] & 2, t.value];
                    switch (op[0]) {
                        case 0:
                        case 1:
                            t = op;
                            break;
                        case 4:
                            _.label++;
                            return { value: op[1], done: false };
                        case 5:
                            _.label++;
                            y = op[1];
                            op = [0];
                            continue;
                        case 7:
                            op = _.ops.pop();
                            _.trys.pop();
                            continue;
                        default:
                            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
                                _ = 0;
                                continue;
                            }
                            if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
                                _.label = op[1];
                                break;
                            }
                            if (op[0] === 6 && _.label < t[1]) {
                                _.label = t[1];
                                t = op;
                                break;
                            }
                            if (t && _.label < t[2]) {
                                _.label = t[2];
                                _.ops.push(op);
                                break;
                            }
                            if (t[2])
                                _.ops.pop();
                            _.trys.pop();
                            continue;
                    }
                    op = body.call(thisArg, _);
                }
                catch (e) {
                    op = [6, e];
                    y = 0;
                }
                finally {
                    f = t = 0;
                }
            if (op[0] & 5)
                throw op[1];
            return { value: op[0] ? op[1] : void 0, done: true };
        }
    }
    var __createBinding = Object.create ? (function (o, m, k, k2) {
        if (k2 === undefined)
            k2 = k;
        Object.defineProperty(o, k2, { enumerable: true, get: function () { return m[k]; } });
    }) : (function (o, m, k, k2) {
        if (k2 === undefined)
            k2 = k;
        o[k2] = m[k];
    });
    function __exportStar(m, o) {
        for (var p in m)
            if (p !== "default" && !Object.prototype.hasOwnProperty.call(o, p))
                __createBinding(o, m, p);
    }
    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m)
            return m.call(o);
        if (o && typeof o.length === "number")
            return {
                next: function () {
                    if (o && i >= o.length)
                        o = void 0;
                    return { value: o && o[i++], done: !o };
                }
            };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }
    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m)
            return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done)
                ar.push(r.value);
        }
        catch (error) {
            e = { error: error };
        }
        finally {
            try {
                if (r && !r.done && (m = i["return"]))
                    m.call(i);
            }
            finally {
                if (e)
                    throw e.error;
            }
        }
        return ar;
    }
    /** @deprecated */
    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }
    /** @deprecated */
    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++)
            s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    }
    function __spreadArray(to, from, pack) {
        if (pack || arguments.length === 2)
            for (var i = 0, l = from.length, ar; i < l; i++) {
                if (ar || !(i in from)) {
                    if (!ar)
                        ar = Array.prototype.slice.call(from, 0, i);
                    ar[i] = from[i];
                }
            }
        return to.concat(ar || Array.prototype.slice.call(from));
    }
    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }
    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator)
            throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n])
            i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try {
            step(g[n](v));
        }
        catch (e) {
            settle(q[0][3], e);
        } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length)
            resume(q[0][0], q[0][1]); }
    }
    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }
    function __asyncValues(o) {
        if (!Symbol.asyncIterator)
            throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function (v) { resolve({ value: v, done: d }); }, reject); }
    }
    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) {
            Object.defineProperty(cooked, "raw", { value: raw });
        }
        else {
            cooked.raw = raw;
        }
        return cooked;
    }
    ;
    var __setModuleDefault = Object.create ? (function (o, v) {
        Object.defineProperty(o, "default", { enumerable: true, value: v });
    }) : function (o, v) {
        o["default"] = v;
    };
    function __importStar(mod) {
        if (mod && mod.__esModule)
            return mod;
        var result = {};
        if (mod != null)
            for (var k in mod)
                if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k))
                    __createBinding(result, mod, k);
        __setModuleDefault(result, mod);
        return result;
    }
    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }
    function __classPrivateFieldGet(receiver, state, kind, f) {
        if (kind === "a" && !f)
            throw new TypeError("Private accessor was defined without a getter");
        if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver))
            throw new TypeError("Cannot read private member from an object whose class did not declare it");
        return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
    }
    function __classPrivateFieldSet(receiver, state, value, kind, f) {
        if (kind === "m")
            throw new TypeError("Private method is not writable");
        if (kind === "a" && !f)
            throw new TypeError("Private accessor was defined without a setter");
        if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver))
            throw new TypeError("Cannot write private member to an object whose class did not declare it");
        return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
    }

    var DashboardMediaQueryComponent = /** @class */ (function () {
        function DashboardMediaQueryComponent() {
            this.offsetSize = 192;
            this.small = matchMedia("(max-width: 700px)");
            this.medium = matchMedia("(max-width: 1000px)");
            this.large = matchMedia("(min-width: 1700px)");
            this.absoluteStyle = {
                'width': "400px"
            };
        }
        DashboardMediaQueryComponent.prototype.setMediaQueries = function () {
            var _this = this;
            if (!!this.smallEventListener) {
                if (this.small.removeEventListener) {
                    this.small.removeEventListener("change", this.smallEventListener);
                }
                else {
                    this.small.removeListener(this.smallEventListener);
                }
            }
            if (!!this.mediumEventListener) {
                if (this.medium.removeEventListener) {
                    this.medium.removeEventListener("change", this.mediumEventListener);
                }
                else {
                    this.medium.removeListener(this.mediumEventListener);
                }
            }
            if (!!this.bigEventListener) {
                if (this.large.removeEventListener) {
                    this.large.removeEventListener("change", this.bigEventListener);
                }
                else {
                    this.large.removeListener(this.bigEventListener);
                }
            }
            var smallOffset = this.offsetSize + 656 + 26;
            var mediumOffset = this.offsetSize + 656 + 26 + 16 + 320;
            var bigOffset = mediumOffset + 16 + 320;
            this.small = matchMedia("(max-width: " + smallOffset + "px)");
            this.medium = matchMedia("(max-width: " + mediumOffset + "px)");
            this.large = matchMedia("(min-width: " + bigOffset + "px)");
            this.smallEventListener = function (e) {
                if (e.matches) {
                    _this.match1Columns();
                }
                else {
                    if (_this.medium.matches) {
                        _this.match2Columns();
                    }
                    else if (_this.large.matches) {
                        _this.match4Columns();
                    }
                    else {
                        // console.log(`not small, not wide` + this.large.media)
                        _this.match3Columns();
                    }
                    // console.log(`This is a not small screen — more than ${smallOffset} wide.`+e.media)
                }
            };
            if (!!this.smallEventListener) {
                // console.log("before adding ", this.smallEventListener);
                if (this.small.addEventListener) {
                    this.small.addEventListener("change", this.smallEventListener);
                }
                else {
                    this.small.addListener(this.smallEventListener);
                }
            }
            this.mediumEventListener = function (e) {
                // console.log("medium");
                if (e.matches) {
                    if (_this.small.matches) {
                        _this.match1Columns();
                    }
                    else {
                        _this.match2Columns();
                    }
                }
                else {
                    if (_this.large.matches) {
                        _this.match4Columns();
                    }
                    else {
                        // console.log(`not small, not wide` + this.large.media)
                        _this.match3Columns();
                    }
                }
            };
            if (!!this.mediumEventListener) {
                // console.log("before adding ", this.smallEventListener);
                if (this.medium.addEventListener) {
                    this.medium.addEventListener("change", this.mediumEventListener);
                }
                else {
                    this.medium.addListener(this.mediumEventListener);
                }
            }
            // console.log("---- smallE ", this.smallEventListener);
            this.bigEventListener = function (e) {
                // console.log("big");
                if (e.matches) {
                    // console.log(`exact wide match`+ e.media)
                    if (_this.large.matches) {
                        _this.match4Columns();
                    }
                    else {
                        _this.match3Columns();
                    }
                }
                else {
                    if (_this.small.matches) {
                        // console.log(`not wide, is small`+ this.small.media)
                        _this.match1Columns();
                    }
                    else if (_this.medium.matches) {
                        _this.match2Columns();
                    }
                    else {
                        // console.log(`not wide, not small`+ this.small.media)
                        _this.match3Columns();
                    }
                }
            };
            if (!!this.bigEventListener) {
                if (this.large.addEventListener) {
                    this.large.addEventListener("change", this.bigEventListener);
                }
                else {
                    this.large.addListener(this.bigEventListener);
                }
            }
            if (this.small.matches) {
                this.match1Columns();
            }
            else if (this.medium.matches) {
                this.match2Columns();
            }
            else if (this.large.matches) {
                this.match4Columns();
            }
            else {
                this.match3Columns();
            }
        };
        DashboardMediaQueryComponent.prototype.match1Columns = function () {
            this.cols = 1;
            this.absoluteStyle['width'] = '331px';
        };
        DashboardMediaQueryComponent.prototype.match2Columns = function () {
            this.cols = 2;
            this.absoluteStyle['width'] = '667px';
        };
        DashboardMediaQueryComponent.prototype.match3Columns = function () {
            this.cols = 3;
            this.absoluteStyle['width'] = '1003px';
        };
        DashboardMediaQueryComponent.prototype.match4Columns = function () {
            this.cols = 4;
            this.absoluteStyle['width'] = '1339px';
        };
        DashboardMediaQueryComponent.prototype.ngOnInit = function () {
            this.setMediaQueries();
        };
        DashboardMediaQueryComponent.prototype.ngOnChanges = function (changes) {
            this.setMediaQueries();
        };
        return DashboardMediaQueryComponent;
    }());
    DashboardMediaQueryComponent.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: DashboardMediaQueryComponent, deps: [], target: i0__namespace.ɵɵFactoryTarget.Component });
    DashboardMediaQueryComponent.ɵcmp = i0__namespace.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: DashboardMediaQueryComponent, selector: "app-dashboard-grid", inputs: { offsetSize: "offsetSize" }, usesOnChanges: true, ngImport: i0__namespace, template: '', isInline: true });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: DashboardMediaQueryComponent, decorators: [{
                type: i0.Component,
                args: [{
                        selector: 'app-dashboard-grid',
                        template: '',
                        styles: []
                    }]
            }], ctorParameters: function () { return []; }, propDecorators: { offsetSize: [{
                    type: i0.Input
                }] } });

    var DashboardCard = /** @class */ (function () {
        function DashboardCard(elementRef, templateRef) {
            this.elementRef = elementRef;
            this.templateRef = templateRef;
            this.cols = [0, 1, 2, 3, 4];
        }
        return DashboardCard;
    }());
    DashboardCard.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: DashboardCard, deps: [{ token: i0__namespace.ElementRef }, { token: i0__namespace.TemplateRef }], target: i0__namespace.ɵɵFactoryTarget.Directive });
    DashboardCard.ɵdir = i0__namespace.ɵɵngDeclareDirective({ minVersion: "12.0.0", version: "12.2.6", type: DashboardCard, selector: "[iotCard]", inputs: { cardId: "cardId", colspan: "colspan" }, ngImport: i0__namespace });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: DashboardCard, decorators: [{
                type: i0.Directive,
                args: [{ selector: '[iotCard]' }]
            }], ctorParameters: function () { return [{ type: i0__namespace.ElementRef }, { type: i0__namespace.TemplateRef }]; }, propDecorators: { cardId: [{
                    type: i0.Input
                }], colspan: [{
                    type: i0.Input
                }] } });
    var DashboardGridComponent = /** @class */ (function (_super) {
        __extends(DashboardGridComponent, _super);
        function DashboardGridComponent() {
            var _this = _super.apply(this, __spreadArray([], __read(arguments))) || this;
            _this.rowHeight = '55px';
            _this._col = [[], [], [], [], []];
            _this._templateMap = {};
            _this._cardMap = {};
            _this.row = [1, 2];
            return _this;
        }
        DashboardGridComponent.prototype.ngAfterContentInit = function () {
            var _this = this;
            this.topLevelPanes.forEach(function (card) {
                _this._templateMap[card.cardId] = card.templateRef;
                _this._cardMap[card.cardId] = card;
                if (card.colspan && card.colspan.indexOf(',') != -1) {
                    var i_1 = 1;
                    card.colspan.split(',').forEach(function (nAsString) {
                        _this._cardMap[card.cardId].cols[i_1++] = Number.parseInt(nAsString, 10);
                    });
                }
                else if (card.colspan) {
                    var defaultColspan = Number.parseInt(card.colspan, 10);
                    _this._cardMap[card.cardId].cols[1] = defaultColspan;
                    _this._cardMap[card.cardId].cols[2] = defaultColspan;
                    _this._cardMap[card.cardId].cols[3] = defaultColspan;
                    _this._cardMap[card.cardId].cols[4] = defaultColspan;
                }
            });
        };
        DashboardGridComponent.prototype.ngOnInit = function () {
            if (!this.col1) {
                this._col[1] = [];
            }
            else {
                this._col[1] = this.col1.split(",");
            }
            if (!this.col2) {
                this._col[2] = [];
            }
            else {
                this._col[2] = this.col2.split(",");
            }
            if (!this.col3) {
                this._col[3] = [];
            }
            else {
                this._col[3] = this.col3.split(",");
            }
            if (!this.col4) {
                this._col[4] = [];
            }
            else {
                this._col[4] = this.col4.split(",");
            }
        };
        return DashboardGridComponent;
    }(DashboardMediaQueryComponent));
    DashboardGridComponent.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: DashboardGridComponent, deps: null, target: i0__namespace.ɵɵFactoryTarget.Component });
    DashboardGridComponent.ɵcmp = i0__namespace.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: DashboardGridComponent, selector: "iot-dashboard", inputs: { rowHeight: "rowHeight", col1: "col1", col2: "col2", col3: "col3", col4: "col4" }, queries: [{ propertyName: "topLevelPanes", predicate: DashboardCard }], usesInheritance: true, ngImport: i0__namespace, template: "\n    <div class=\"centered-content\">\n      <mat-grid-list\n        [cols]=\"cols\" [rowHeight]=\"rowHeight\" [ngStyle]=\"absoluteStyle\">\n\n        <mat-grid-tile\n          *ngFor=\"let key of _col[cols]\"\n          [rowspan]=\"10\"\n\n          [colspan]=\"_cardMap[key].cols[cols] \"\n        >\n\n          <ng-container [ngTemplateOutlet]=\"(_templateMap[key] )\"\n                        [ngTemplateOutletContext]=\"{\n                        columnsAvailable: _cardMap[key].cols[cols] }\"\n          >\n<!--            _cardMap[key].colspan ?  (_cardMap[key].colspan < 0? cols + _cardMap[key].colspan : _cardMap[key].colspan  ) : cols-->\n          </ng-container>\n        </mat-grid-tile>\n      </mat-grid-list>\n\n\n      <!--            <div style=\"width: 100px\">test</div>-->\n    </div>\n  ", isInline: true, components: [{ type: i1__namespace$3.MatGridList, selector: "mat-grid-list", inputs: ["cols", "gutterSize", "rowHeight"], exportAs: ["matGridList"] }, { type: i1__namespace$3.MatGridTile, selector: "mat-grid-tile", inputs: ["rowspan", "colspan"], exportAs: ["matGridTile"] }], directives: [{ type: i4__namespace$2.NgStyle, selector: "[ngStyle]", inputs: ["ngStyle"] }, { type: i4__namespace$2.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { type: i4__namespace$2.NgTemplateOutlet, selector: "[ngTemplateOutlet]", inputs: ["ngTemplateOutletContext", "ngTemplateOutlet"] }] });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: DashboardGridComponent, decorators: [{
                type: i0.Component,
                args: [{
                        selector: 'iot-dashboard',
                        template: "\n    <div class=\"centered-content\">\n      <mat-grid-list\n        [cols]=\"cols\" [rowHeight]=\"rowHeight\" [ngStyle]=\"absoluteStyle\">\n\n        <mat-grid-tile\n          *ngFor=\"let key of _col[cols]\"\n          [rowspan]=\"10\"\n\n          [colspan]=\"_cardMap[key].cols[cols] \"\n        >\n\n          <ng-container [ngTemplateOutlet]=\"(_templateMap[key] )\"\n                        [ngTemplateOutletContext]=\"{\n                        columnsAvailable: _cardMap[key].cols[cols] }\"\n          >\n<!--            _cardMap[key].colspan ?  (_cardMap[key].colspan < 0? cols + _cardMap[key].colspan : _cardMap[key].colspan  ) : cols-->\n          </ng-container>\n        </mat-grid-tile>\n      </mat-grid-list>\n\n\n      <!--            <div style=\"width: 100px\">test</div>-->\n    </div>\n  "
                    }]
            }], propDecorators: { topLevelPanes: [{
                    type: i0.ContentChildren,
                    args: [DashboardCard]
                }], rowHeight: [{
                    type: i0.Input
                }], col1: [{
                    type: i0.Input
                }], col2: [{
                    type: i0.Input
                }], col3: [{
                    type: i0.Input
                }], col4: [{
                    type: i0.Input
                }] } });

    /*
     * Copyright (C) Io-Things, BV - All Rights Reserved
     * Unauthorized copying of this file, via any medium is strictly prohibited
     * Proprietary and confidential
     * Written by Stefaan Ternier <stefaan@io-things.eu>, 1/6/2020
     */
    var IotCardHeader = /** @class */ (function () {
        function IotCardHeader(elRef, renderer) {
            this.elRef = elRef;
            this.renderer = renderer;
            this.padding = false;
            this.renderer.addClass(this.elRef.nativeElement, 'box-sizing');
            this.renderer.setStyle(this.elRef.nativeElement, 'height', "56px");
        }
        IotCardHeader.prototype.ngOnInit = function () {
            if (this.padding) {
                this.renderer.setStyle(this.elRef.nativeElement, 'padding', "16px 16px 0 16px");
            }
        };
        return IotCardHeader;
    }());
    IotCardHeader.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotCardHeader, deps: [{ token: i0__namespace.ElementRef }, { token: i0__namespace.Renderer2 }], target: i0__namespace.ɵɵFactoryTarget.Directive });
    IotCardHeader.ɵdir = i0__namespace.ɵɵngDeclareDirective({ minVersion: "12.0.0", version: "12.2.6", type: IotCardHeader, selector: "[iot-card-header]", inputs: { padding: "padding" }, ngImport: i0__namespace });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotCardHeader, decorators: [{
                type: i0.Directive,
                args: [{ selector: '[iot-card-header]' }]
            }], ctorParameters: function () { return [{ type: i0__namespace.ElementRef }, { type: i0__namespace.Renderer2 }]; }, propDecorators: { padding: [{
                    type: i0.Input
                }] } });
    var IotCardContent = /** @class */ (function () {
        function IotCardContent(elRef, renderer) {
            this.elRef = elRef;
            this.renderer = renderer;
            this.padding = false;
            this.renderer.addClass(this.elRef.nativeElement, 'box-sizing');
            this.renderer.setStyle(this.elRef.nativeElement, 'height', "100%");
        }
        IotCardContent.prototype.ngOnInit = function () {
            if (this.padding) {
                this.renderer.setStyle(this.elRef.nativeElement, 'padding', "0 16px 0px 16px");
            }
        };
        return IotCardContent;
    }());
    IotCardContent.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotCardContent, deps: [{ token: i0__namespace.ElementRef }, { token: i0__namespace.Renderer2 }], target: i0__namespace.ɵɵFactoryTarget.Directive });
    IotCardContent.ɵdir = i0__namespace.ɵɵngDeclareDirective({ minVersion: "12.0.0", version: "12.2.6", type: IotCardContent, selector: "[iot-card-content]", inputs: { padding: "padding" }, ngImport: i0__namespace });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotCardContent, decorators: [{
                type: i0.Directive,
                args: [{ selector: '[iot-card-content]' }]
            }], ctorParameters: function () { return [{ type: i0__namespace.ElementRef }, { type: i0__namespace.Renderer2 }]; }, propDecorators: { padding: [{
                    type: i0.Input
                }] } });
    var IotCardFooter = /** @class */ (function () {
        function IotCardFooter(elRef, renderer) {
            this.elRef = elRef;
            this.renderer = renderer;
            this.padding = false;
            this.renderer.addClass(this.elRef.nativeElement, 'box-sizing');
            this.renderer.setStyle(this.elRef.nativeElement, 'height', "40px");
        }
        IotCardFooter.prototype.ngOnInit = function () {
            if (this.padding) {
                this.renderer.setStyle(this.elRef.nativeElement, 'padding', "0 16px 16px 16px");
            }
        };
        return IotCardFooter;
    }());
    IotCardFooter.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotCardFooter, deps: [{ token: i0__namespace.ElementRef }, { token: i0__namespace.Renderer2 }], target: i0__namespace.ɵɵFactoryTarget.Directive });
    IotCardFooter.ɵdir = i0__namespace.ɵɵngDeclareDirective({ minVersion: "12.0.0", version: "12.2.6", type: IotCardFooter, selector: "[iot-card-footer]", inputs: { padding: "padding" }, ngImport: i0__namespace });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotCardFooter, decorators: [{
                type: i0.Directive,
                args: [{ selector: '[iot-card-footer]' }]
            }], ctorParameters: function () { return [{ type: i0__namespace.ElementRef }, { type: i0__namespace.Renderer2 }]; }, propDecorators: { padding: [{
                    type: i0.Input
                }] } });
    var DashboardCardComponent = /** @class */ (function () {
        function DashboardCardComponent() {
            this.colspan = 1;
            this.showHeader = true;
            this.showTitle = true;
            this.height = "500px";
            this.width = "320px";
            this.cardClass = {
                'iotcol-1': false,
                'iotcol-2': false,
                'iotcol-3': false,
                'iotcol-4': false,
            };
        }
        DashboardCardComponent.prototype.ngOnInit = function () {
            this.cardClass['iotcol-' + this.colspan] = true;
        };
        DashboardCardComponent.prototype.ngOnChanges = function (changes) {
            if (changes.colspan) {
                this.cardClass['iotcol-' + changes.colspan.previousValue] = false;
                this.cardClass['iotcol-' + changes.colspan.currentValue] = true;
            }
        };
        return DashboardCardComponent;
    }());
    DashboardCardComponent.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: DashboardCardComponent, deps: [], target: i0__namespace.ɵɵFactoryTarget.Component });
    DashboardCardComponent.ɵcmp = i0__namespace.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: DashboardCardComponent, selector: "iot-dashboard-card", inputs: { colspan: "colspan", showHeader: "showHeader", outerTitle: "outerTitle", innerTitle: "innerTitle", showTitle: "showTitle", height: "height", width: "width" }, queries: [{ propertyName: "template", first: true, predicate: i0.TemplateRef, descendants: true }], usesOnChanges: true, ngImport: i0__namespace, template: "\n    <div class=\"outer-card\">\n\n      <div class=\"iotcard\">\n        <div\n          class=\"iot-card-outer-title\" [ngStyle]=\"{'width':width}\">\n          <div *ngIf=\"outerTitle\">{{outerTitle | translate}}</div>\n          <div *ngIf=\"!outerTitle\">&nbsp;</div>\n        </div>\n        <div [ngClass]=\"cardClass\" class=\"iot-inner-card iot-card-scss\" [ngStyle]=\"{'height':height}\">\n\n          <div class=\"custheader iot-card-title\">\n            <ng-content select=\"[iot-card-header]\"></ng-content>\n          </div>\n          <div class=\"custmiddle\">\n            <ng-content select=\"[iot-card-content]\"></ng-content>\n          </div>\n          <div class=\"custfooter\">\n            <ng-content select=\"[iot-card-footer]\"></ng-content>\n          </div>\n        </div>\n      </div>\n    </div>", isInline: true, styles: ["\n\n\n    .custheader {\n      -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */\n      -moz-box-sizing: border-box; /* Firefox, other Gecko */\n      box-sizing: border-box;\n    }\n\n    .custmiddle {\n      flex-grow: 1;\n    }\n\n    .custfooter {\n\n    }\n  "], directives: [{ type: i4__namespace$2.NgStyle, selector: "[ngStyle]", inputs: ["ngStyle"] }, { type: i4__namespace$2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i4__namespace$2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }], pipes: { "translate": i5__namespace.TranslatePipe }, encapsulation: i0__namespace.ViewEncapsulation.None });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: DashboardCardComponent, decorators: [{
                type: i0.Component,
                args: [{
                        selector: 'iot-dashboard-card',
                        template: "\n    <div class=\"outer-card\">\n\n      <div class=\"iotcard\">\n        <div\n          class=\"iot-card-outer-title\" [ngStyle]=\"{'width':width}\">\n          <div *ngIf=\"outerTitle\">{{outerTitle | translate}}</div>\n          <div *ngIf=\"!outerTitle\">&nbsp;</div>\n        </div>\n        <div [ngClass]=\"cardClass\" class=\"iot-inner-card iot-card-scss\" [ngStyle]=\"{'height':height}\">\n\n          <div class=\"custheader iot-card-title\">\n            <ng-content select=\"[iot-card-header]\"></ng-content>\n          </div>\n          <div class=\"custmiddle\">\n            <ng-content select=\"[iot-card-content]\"></ng-content>\n          </div>\n          <div class=\"custfooter\">\n            <ng-content select=\"[iot-card-footer]\"></ng-content>\n          </div>\n        </div>\n      </div>\n    </div>",
                        encapsulation: i0.ViewEncapsulation.None,
                        styles: ["\n\n\n    .custheader {\n      -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */\n      -moz-box-sizing: border-box; /* Firefox, other Gecko */\n      box-sizing: border-box;\n    }\n\n    .custmiddle {\n      flex-grow: 1;\n    }\n\n    .custfooter {\n\n    }\n  "]
                    }]
            }], ctorParameters: function () { return []; }, propDecorators: { template: [{
                    type: i0.ContentChild,
                    args: [i0.TemplateRef]
                }], colspan: [{
                    type: i0.Input
                }], showHeader: [{
                    type: i0.Input
                }], outerTitle: [{
                    type: i0.Input
                }], innerTitle: [{
                    type: i0.Input
                }], showTitle: [{
                    type: i0.Input
                }], height: [{
                    type: i0.Input
                }], width: [{
                    type: i0.Input
                }] } });

    var TimeOfDayCardComponent = /** @class */ (function () {
        function TimeOfDayCardComponent() {
            this.title = '';
            this.subject = "PIR Bewegingen";
            this.min = 10;
            this.highest = 5;
            this.today = ['', '', '', '', '', '', ''];
        }
        TimeOfDayCardComponent.prototype.ngOnInit = function () {
            var now = moment__namespace();
            for (var i = 0; i < 7; i++) {
                this.today[i] = now.format("dd");
                now = now.subtract(1, 'days');
            }
        };
        TimeOfDayCardComponent.prototype.getTooltip = function (hour, week) {
            return this.data[week][hour] + " " + this.subject;
        };
        TimeOfDayCardComponent.prototype.getClass = function (hour, week) {
            var classRet = { 'cell': true };
            if (!this.data) {
                classRet['grey-1'] = true;
            }
            else {
                if (this.data[week][hour] == 0) {
                    classRet['grey-1'] = true;
                }
                else if (this.data[week][hour] < this.val2) {
                    classRet['blue-1'] = true;
                }
                else if (this.data[week][hour] < this.val3) {
                    classRet['blue-2'] = true;
                }
                else if (this.data[week][hour] < this.val4) {
                    classRet['blue-3'] = true;
                }
                else if (this.data[week][hour] <= this.val5) {
                    classRet['blue-4'] = true;
                }
            }
            return classRet;
        };
        TimeOfDayCardComponent.prototype.ngOnChanges = function (changes) {
            var _this = this;
            this.lowest = null;
            this.highest = null;
            if (this.data != null) {
                this.data.map(function (row) { return row.map(function (cell) {
                    if (cell != 0) {
                        if (_this.lowest == null)
                            _this.lowest = cell;
                        _this.lowest = Math.min(_this.lowest, cell);
                        _this.highest = Math.max(_this.highest, cell);
                    }
                }); });
            }
            if (this.lowest == null) {
                this.lowest = 1;
            }
            this.lowest = Math.floor(this.lowest);
            this.highest = Math.ceil(this.highest);
            this.highest = Math.max(this.highest, this.min);
            if (this.lowest > 10) {
                this.lowest = Math.floor(this.lowest / 10) * 10;
            }
            var diff = this.highest - this.lowest;
            var step = Math.ceil(diff / 4);
            if (step > 10) {
                step = Math.ceil(step / 10) * 10;
            }
            if (step === 0) {
                step = 1;
            }
            this.val1 = this.lowest;
            this.val2 = this.val1 + step;
            this.val3 = this.val2 + step;
            this.val4 = this.val3 + step;
            this.val5 = this.val4 + step;
        };
        TimeOfDayCardComponent.prototype.showTooltip = function (evt, test) {
        };
        return TimeOfDayCardComponent;
    }());
    TimeOfDayCardComponent.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: TimeOfDayCardComponent, deps: [], target: i0__namespace.ɵɵFactoryTarget.Component });
    TimeOfDayCardComponent.ɵcmp = i0__namespace.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: TimeOfDayCardComponent, selector: "iot-time-of-day-card", inputs: { title: "title", outerTitle: "outerTitle", subject: "subject", data: "data", min: "min" }, usesOnChanges: true, ngImport: i0__namespace, template: "\n    <iot-dashboard-card\n      class=\"time-of-day\"\n      [colspan]=\"1\"\n      [outerTitle]=\"outerTitle\">\n      <div  iot-card-header padding=\"true\">{{ title | translate}}</div>\n      <div  iot-card-content padding=\"true\">\n        <div class=\"ga-time-of-day-chart\" #thisDiv>\n          <svg width=\"272\" height=\"404\">\n            <g class=\"header\"></g>\n            <g class=\"grid-container\" transform=\"translate(0, 0)\">\n              <g class=\"x-axis\" transform=\"translate(0, 322)\" fill=\"none\" font-size=\"10\" font-family=\"sans-serif\"\n                 text-anchor=\"middle\">\n                <path class=\"domain\" stroke=\"currentColor\" d=\"M0.5,6V0.5H232.5V6\"></path>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(16.57142857142856,0)\">\n                  <line stroke=\"currentColor\" y2=\"6\"></line>\n                  <text fill=\"currentColor\" y=\"9\" dy=\"0.71em\" transform=\"translate(-13.571428571428573, 0)\"\n                        style=\"text-anchor: start;\">{{today[6]}}\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(49.71428571428571,0)\">\n                  <line stroke=\"currentColor\" y2=\"6\"></line>\n                  <text fill=\"currentColor\" y=\"9\" dy=\"0.71em\" transform=\"translate(-13.571428571428573, 0)\"\n                        style=\"text-anchor: start;\">{{today[5]}}\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(82.85714285714285,0)\">\n                  <line stroke=\"currentColor\" y2=\"6\"></line>\n                  <text fill=\"currentColor\" y=\"9\" dy=\"0.71em\" transform=\"translate(-13.571428571428573, 0)\"\n                        style=\"text-anchor: start;\">{{today[4]}}\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(116,0)\">\n                  <line stroke=\"currentColor\" y2=\"6\"></line>\n                  <text fill=\"currentColor\" y=\"9\" dy=\"0.71em\" transform=\"translate(-13.571428571428573, 0)\"\n                        style=\"text-anchor: start;\">{{today[3]}}\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(149.14285714285714,0)\">\n                  <line stroke=\"currentColor\" y2=\"6\"></line>\n                  <text fill=\"currentColor\" y=\"9\" dy=\"0.71em\" transform=\"translate(-13.571428571428573, 0)\"\n                        style=\"text-anchor: start;\">{{today[2]}}\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(182.2857142857143,0)\">\n                  <line stroke=\"currentColor\" y2=\"6\"></line>\n                  <text fill=\"currentColor\" y=\"9\" dy=\"0.71em\" transform=\"translate(-13.571428571428573, 0)\"\n                        style=\"text-anchor: start;\">{{today[1]}}\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(215.42857142857147,0)\">\n                  <line stroke=\"currentColor\" y2=\"6\"></line>\n                  <text fill=\"currentColor\" y=\"9\" dy=\"0.71em\" transform=\"translate(-13.571428571428573, 0)\"\n                        style=\"text-anchor: start;\">{{today[0]}}\n                  </text>\n                </g>\n              </g>\n              <g class=\"y-axis\" transform=\"translate(232, 0)\" fill=\"none\" font-size=\"10\" font-family=\"sans-serif\"\n                 text-anchor=\"start\">\n                <path class=\"domain\" stroke=\"currentColor\" d=\"M6,0.5H0.5V324.5H6\"></path>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(0, 6.75)\">\n                  <line stroke=\"currentColor\" x2=\"6\"></line>\n                  <text fill=\"currentColor\" x=\"9\" dy=\"0.32em\" transform=\"translate(30, 0)\"\n                        style=\"text-anchor: end;\">12a.m.\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(0, 33.75)\">\n                  <line stroke=\"currentColor\" x2=\"6\"></line>\n                  <text fill=\"currentColor\" x=\"9\" dy=\"0.32em\" transform=\"translate(30, 0)\"\n                        style=\"text-anchor: end;\">2a.m.\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(0, 60.75)\">\n                  <line stroke=\"currentColor\" x2=\"6\"></line>\n                  <text fill=\"currentColor\" x=\"9\" dy=\"0.32em\" transform=\"translate(30, 0)\"\n                        style=\"text-anchor: end;\">4a.m.\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(0, 87.75)\">\n                  <line stroke=\"currentColor\" x2=\"6\"></line>\n                  <text fill=\"currentColor\" x=\"9\" dy=\"0.32em\" transform=\"translate(30, 0)\"\n                        style=\"text-anchor: end;\">6a.m.\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(0, 114.75)\">\n                  <line stroke=\"currentColor\" x2=\"6\"></line>\n                  <text fill=\"currentColor\" x=\"9\" dy=\"0.32em\" transform=\"translate(30, 0)\"\n                        style=\"text-anchor: end;\">8a.m.\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(0, 141.75)\">\n                  <line stroke=\"currentColor\" x2=\"6\"></line>\n                  <text fill=\"currentColor\" x=\"9\" dy=\"0.32em\" transform=\"translate(30, 0)\"\n                        style=\"text-anchor: end;\">10a.m.\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(0, 168.75)\">\n                  <line stroke=\"currentColor\" x2=\"6\"></line>\n                  <text fill=\"currentColor\" x=\"9\" dy=\"0.32em\" transform=\"translate(30, 0)\"\n                        style=\"text-anchor: end;\">12p.m.\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(0, 195.75)\">\n                  <line stroke=\"currentColor\" x2=\"6\"></line>\n                  <text fill=\"currentColor\" x=\"9\" dy=\"0.32em\" transform=\"translate(30, 0)\"\n                        style=\"text-anchor: end;\">2p.m.\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(0, 222.75)\">\n                  <line stroke=\"currentColor\" x2=\"6\"></line>\n                  <text fill=\"currentColor\" x=\"9\" dy=\"0.32em\" transform=\"translate(30, 0)\"\n                        style=\"text-anchor: end;\">4p.m.\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(0, 249.75)\">\n                  <line stroke=\"currentColor\" x2=\"6\"></line>\n                  <text fill=\"currentColor\" x=\"9\" dy=\"0.32em\" transform=\"translate(30, 0)\"\n                        style=\"text-anchor: end;\">6p.m.\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(0, 276.75)\">\n                  <line stroke=\"currentColor\" x2=\"6\"></line>\n                  <text fill=\"currentColor\" x=\"9\" dy=\"0.32em\" transform=\"translate(30, 0)\"\n                        style=\"text-anchor: end;\">8p.m.\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(0, 303.75)\">\n                  <line stroke=\"currentColor\" x2=\"6\"></line>\n                  <text fill=\"currentColor\" x=\"9\" dy=\"0.32em\" transform=\"translate(30, 0)\"\n                        style=\"text-anchor: end;\">10p.m.\n                  </text>\n                </g>\n              </g>\n              <g class=\"row \" *ngFor=\"let item of [].constructor(24); let i = index;\">\n                <g [ngClass]=\"getClass(i, w)\"\n                   *ngFor=\"let week of [].constructor(7); let w = index;\">\n                  <rect\n                    [matTooltip]=\"getTooltip(i,w)\"\n                    class=\"rect\" width=\"30.14285659790039\"\n                    height=\"10.5\" [attr.x]=\"1.5+(w * 33.15)\"\n                    [attr.y]=\"1.5+ (i * 13.5)\"\n                  ></rect>\n                </g>\n\n              </g>\n\n            </g>\n            <g class=\"legend-group\" transform=\"translate(0, 359)\">\n              <rect height=\"10\" width=\"55\" x=\"0\" y=\"0\" class=\"legend-block blue-1\"></rect>\n              <rect height=\"10\" width=\"55\" x=\"58\" y=\"0\" class=\"legend-block blue-2\"></rect>\n              <rect height=\"10\" width=\"55\" x=\"116\" y=\"0\" class=\"legend-block blue-3\"></rect>\n              <rect height=\"10\" width=\"55\" x=\"174\" y=\"0\" class=\"legend-block blue-4\"></rect>\n              <text x=\"0\" y=\"25\" class=\"legend-text\" style=\"text-anchor: start;\">{{val1}}</text>\n              <text x=\"58\" y=\"25\" class=\"legend-text\" style=\"text-anchor: middle;\">{{val2}}</text>\n              <text x=\"116\" y=\"25\" class=\"legend-text\" style=\"text-anchor: middle;\">{{val3}}</text>\n              <text x=\"174\" y=\"25\" class=\"legend-text\" style=\"text-anchor: middle;\">{{val4}}</text>\n              <text x=\"232\" y=\"25\" class=\"legend-text\" style=\"text-anchor: end;\">{{val5}}</text>\n            </g>\n          </svg>\n        </div>\n\n      </div>\n      <div  iot-card-footer><ng-content select=\"[iot-card-footer]\"></ng-content></div>\n    </iot-dashboard-card>\n  ", isInline: true, components: [{ type: DashboardCardComponent, selector: "iot-dashboard-card", inputs: ["colspan", "showHeader", "outerTitle", "innerTitle", "showTitle", "height", "width"] }], directives: [{ type: IotCardHeader, selector: "[iot-card-header]", inputs: ["padding"] }, { type: IotCardContent, selector: "[iot-card-content]", inputs: ["padding"] }, { type: i4__namespace$2.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { type: i4__namespace$2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i3__namespace.MatTooltip, selector: "[matTooltip]", exportAs: ["matTooltip"] }, { type: IotCardFooter, selector: "[iot-card-footer]", inputs: ["padding"] }], pipes: { "translate": i5__namespace.TranslatePipe } });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: TimeOfDayCardComponent, decorators: [{
                type: i0.Component,
                args: [{
                        selector: 'iot-time-of-day-card',
                        template: "\n    <iot-dashboard-card\n      class=\"time-of-day\"\n      [colspan]=\"1\"\n      [outerTitle]=\"outerTitle\">\n      <div  iot-card-header padding=\"true\">{{ title | translate}}</div>\n      <div  iot-card-content padding=\"true\">\n        <div class=\"ga-time-of-day-chart\" #thisDiv>\n          <svg width=\"272\" height=\"404\">\n            <g class=\"header\"></g>\n            <g class=\"grid-container\" transform=\"translate(0, 0)\">\n              <g class=\"x-axis\" transform=\"translate(0, 322)\" fill=\"none\" font-size=\"10\" font-family=\"sans-serif\"\n                 text-anchor=\"middle\">\n                <path class=\"domain\" stroke=\"currentColor\" d=\"M0.5,6V0.5H232.5V6\"></path>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(16.57142857142856,0)\">\n                  <line stroke=\"currentColor\" y2=\"6\"></line>\n                  <text fill=\"currentColor\" y=\"9\" dy=\"0.71em\" transform=\"translate(-13.571428571428573, 0)\"\n                        style=\"text-anchor: start;\">{{today[6]}}\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(49.71428571428571,0)\">\n                  <line stroke=\"currentColor\" y2=\"6\"></line>\n                  <text fill=\"currentColor\" y=\"9\" dy=\"0.71em\" transform=\"translate(-13.571428571428573, 0)\"\n                        style=\"text-anchor: start;\">{{today[5]}}\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(82.85714285714285,0)\">\n                  <line stroke=\"currentColor\" y2=\"6\"></line>\n                  <text fill=\"currentColor\" y=\"9\" dy=\"0.71em\" transform=\"translate(-13.571428571428573, 0)\"\n                        style=\"text-anchor: start;\">{{today[4]}}\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(116,0)\">\n                  <line stroke=\"currentColor\" y2=\"6\"></line>\n                  <text fill=\"currentColor\" y=\"9\" dy=\"0.71em\" transform=\"translate(-13.571428571428573, 0)\"\n                        style=\"text-anchor: start;\">{{today[3]}}\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(149.14285714285714,0)\">\n                  <line stroke=\"currentColor\" y2=\"6\"></line>\n                  <text fill=\"currentColor\" y=\"9\" dy=\"0.71em\" transform=\"translate(-13.571428571428573, 0)\"\n                        style=\"text-anchor: start;\">{{today[2]}}\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(182.2857142857143,0)\">\n                  <line stroke=\"currentColor\" y2=\"6\"></line>\n                  <text fill=\"currentColor\" y=\"9\" dy=\"0.71em\" transform=\"translate(-13.571428571428573, 0)\"\n                        style=\"text-anchor: start;\">{{today[1]}}\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(215.42857142857147,0)\">\n                  <line stroke=\"currentColor\" y2=\"6\"></line>\n                  <text fill=\"currentColor\" y=\"9\" dy=\"0.71em\" transform=\"translate(-13.571428571428573, 0)\"\n                        style=\"text-anchor: start;\">{{today[0]}}\n                  </text>\n                </g>\n              </g>\n              <g class=\"y-axis\" transform=\"translate(232, 0)\" fill=\"none\" font-size=\"10\" font-family=\"sans-serif\"\n                 text-anchor=\"start\">\n                <path class=\"domain\" stroke=\"currentColor\" d=\"M6,0.5H0.5V324.5H6\"></path>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(0, 6.75)\">\n                  <line stroke=\"currentColor\" x2=\"6\"></line>\n                  <text fill=\"currentColor\" x=\"9\" dy=\"0.32em\" transform=\"translate(30, 0)\"\n                        style=\"text-anchor: end;\">12a.m.\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(0, 33.75)\">\n                  <line stroke=\"currentColor\" x2=\"6\"></line>\n                  <text fill=\"currentColor\" x=\"9\" dy=\"0.32em\" transform=\"translate(30, 0)\"\n                        style=\"text-anchor: end;\">2a.m.\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(0, 60.75)\">\n                  <line stroke=\"currentColor\" x2=\"6\"></line>\n                  <text fill=\"currentColor\" x=\"9\" dy=\"0.32em\" transform=\"translate(30, 0)\"\n                        style=\"text-anchor: end;\">4a.m.\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(0, 87.75)\">\n                  <line stroke=\"currentColor\" x2=\"6\"></line>\n                  <text fill=\"currentColor\" x=\"9\" dy=\"0.32em\" transform=\"translate(30, 0)\"\n                        style=\"text-anchor: end;\">6a.m.\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(0, 114.75)\">\n                  <line stroke=\"currentColor\" x2=\"6\"></line>\n                  <text fill=\"currentColor\" x=\"9\" dy=\"0.32em\" transform=\"translate(30, 0)\"\n                        style=\"text-anchor: end;\">8a.m.\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(0, 141.75)\">\n                  <line stroke=\"currentColor\" x2=\"6\"></line>\n                  <text fill=\"currentColor\" x=\"9\" dy=\"0.32em\" transform=\"translate(30, 0)\"\n                        style=\"text-anchor: end;\">10a.m.\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(0, 168.75)\">\n                  <line stroke=\"currentColor\" x2=\"6\"></line>\n                  <text fill=\"currentColor\" x=\"9\" dy=\"0.32em\" transform=\"translate(30, 0)\"\n                        style=\"text-anchor: end;\">12p.m.\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(0, 195.75)\">\n                  <line stroke=\"currentColor\" x2=\"6\"></line>\n                  <text fill=\"currentColor\" x=\"9\" dy=\"0.32em\" transform=\"translate(30, 0)\"\n                        style=\"text-anchor: end;\">2p.m.\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(0, 222.75)\">\n                  <line stroke=\"currentColor\" x2=\"6\"></line>\n                  <text fill=\"currentColor\" x=\"9\" dy=\"0.32em\" transform=\"translate(30, 0)\"\n                        style=\"text-anchor: end;\">4p.m.\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(0, 249.75)\">\n                  <line stroke=\"currentColor\" x2=\"6\"></line>\n                  <text fill=\"currentColor\" x=\"9\" dy=\"0.32em\" transform=\"translate(30, 0)\"\n                        style=\"text-anchor: end;\">6p.m.\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(0, 276.75)\">\n                  <line stroke=\"currentColor\" x2=\"6\"></line>\n                  <text fill=\"currentColor\" x=\"9\" dy=\"0.32em\" transform=\"translate(30, 0)\"\n                        style=\"text-anchor: end;\">8p.m.\n                  </text>\n                </g>\n                <g class=\"tick\" opacity=\"1\" transform=\"translate(0, 303.75)\">\n                  <line stroke=\"currentColor\" x2=\"6\"></line>\n                  <text fill=\"currentColor\" x=\"9\" dy=\"0.32em\" transform=\"translate(30, 0)\"\n                        style=\"text-anchor: end;\">10p.m.\n                  </text>\n                </g>\n              </g>\n              <g class=\"row \" *ngFor=\"let item of [].constructor(24); let i = index;\">\n                <g [ngClass]=\"getClass(i, w)\"\n                   *ngFor=\"let week of [].constructor(7); let w = index;\">\n                  <rect\n                    [matTooltip]=\"getTooltip(i,w)\"\n                    class=\"rect\" width=\"30.14285659790039\"\n                    height=\"10.5\" [attr.x]=\"1.5+(w * 33.15)\"\n                    [attr.y]=\"1.5+ (i * 13.5)\"\n                  ></rect>\n                </g>\n\n              </g>\n\n            </g>\n            <g class=\"legend-group\" transform=\"translate(0, 359)\">\n              <rect height=\"10\" width=\"55\" x=\"0\" y=\"0\" class=\"legend-block blue-1\"></rect>\n              <rect height=\"10\" width=\"55\" x=\"58\" y=\"0\" class=\"legend-block blue-2\"></rect>\n              <rect height=\"10\" width=\"55\" x=\"116\" y=\"0\" class=\"legend-block blue-3\"></rect>\n              <rect height=\"10\" width=\"55\" x=\"174\" y=\"0\" class=\"legend-block blue-4\"></rect>\n              <text x=\"0\" y=\"25\" class=\"legend-text\" style=\"text-anchor: start;\">{{val1}}</text>\n              <text x=\"58\" y=\"25\" class=\"legend-text\" style=\"text-anchor: middle;\">{{val2}}</text>\n              <text x=\"116\" y=\"25\" class=\"legend-text\" style=\"text-anchor: middle;\">{{val3}}</text>\n              <text x=\"174\" y=\"25\" class=\"legend-text\" style=\"text-anchor: middle;\">{{val4}}</text>\n              <text x=\"232\" y=\"25\" class=\"legend-text\" style=\"text-anchor: end;\">{{val5}}</text>\n            </g>\n          </svg>\n        </div>\n\n      </div>\n      <div  iot-card-footer><ng-content select=\"[iot-card-footer]\"></ng-content></div>\n    </iot-dashboard-card>\n  "
                    }]
            }], ctorParameters: function () { return []; }, propDecorators: { title: [{
                    type: i0.Input
                }], outerTitle: [{
                    type: i0.Input
                }], subject: [{
                    type: i0.Input
                }], data: [{
                    type: i0.Input
                }], min: [{
                    type: i0.Input
                }] } });

    var oneday = 24 * 3600000;
    var oneMonth = 31 * 24 * 3600000;
    var KeyValueCardComponent = /** @class */ (function () {
        function KeyValueCardComponent() {
            this.click = new i0.EventEmitter();
            this.hover = new i0.EventEmitter();
            this.unHover = new i0.EventEmitter();
            this.displayedColumns = ['label', 'value'];
        }
        KeyValueCardComponent.prototype.ngOnInit = function () {
        };
        KeyValueCardComponent.prototype.getShowShortTime = function (entries) {
            if (!entries) {
                return false;
            }
            if (!entries.value) {
                return false;
            }
            if (entries.value === 'undefined') {
                return false;
            }
            if (entries.value < (new Date().getTime() - oneday)) {
                return false;
            }
            return true;
        };
        KeyValueCardComponent.prototype.getShowLongTime = function (entries) {
            if (!entries)
                return false;
            if (!entries.value)
                return false;
            if (entries.value === 'undefined')
                return false;
            if (entries.value < (new Date().getTime() - oneMonth)) {
                return false;
            }
            if (entries.value > (new Date().getTime() - oneday))
                return false;
            return true;
        };
        KeyValueCardComponent.prototype.getShowVeryLongTime = function (entries) {
            if (!entries)
                return false;
            if (!entries.value)
                return false;
            if (entries.value === 'undefined')
                return false;
            if (entries.value > (new Date().getTime() - oneMonth))
                return false;
            return true;
        };
        KeyValueCardComponent.prototype.getNoTime = function (entries) {
            if (!entries)
                return true;
            if (!entries.value)
                return true;
            if (entries.value == 0)
                return true;
            return false;
        };
        return KeyValueCardComponent;
    }());
    KeyValueCardComponent.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: KeyValueCardComponent, deps: [], target: i0__namespace.ɵɵFactoryTarget.Component });
    KeyValueCardComponent.ɵcmp = i0__namespace.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: KeyValueCardComponent, selector: "iot-key-value-card", inputs: { outerTitle: "outerTitle", title: "title", keyValueData: "keyValueData" }, outputs: { click: "click", hover: "hover", unHover: "unHover" }, ngImport: i0__namespace, template: "\n    <iot-dashboard-card\n      [colspan]=\"1\"\n      [outerTitle]=\"outerTitle\">\n      <div *ngIf=\"title\" iot-card-header padding=\"true\">{{title |translate}}</div>\n      <div  iot-card-content >\n          <table mat-table [dataSource]=\"keyValueData\" style=\"width:100%\">\n\n            <ng-container matColumnDef=\"label\">\n              <th mat-header-cell *matHeaderCellDef> Item</th>\n              <td\n                (click)=\"click.emit(entries)\"\n                (mouseenter)=\"hover.emit(entries)\"\n                (mouseleave)=\"unHover.emit()\"\n                class=\"text \" [ngClass]=\"{warnrow: entries.error}\"\n                  mat-cell *matCellDef=\"let entries\"> {{entries.label|translate}}</td>\n            </ng-container>\n\n\n            <ng-container matColumnDef=\"value\">\n              <th mat-header-cell *matHeaderCellDef> Cost</th>\n              <td class=\"value text warnrow\" [ngClass]=\"{warnrow: entries.error}\"  mat-cell *matCellDef=\"let entries\">\n\n                <div *ngIf=\"!!entries.value && entries.value > 0 && entries.type =='date' && getShowShortTime(entries)\">{{entries.value |date:'H:mm'}}</div>\n                <div *ngIf=\"!!entries.value && entries.value > 0 && entries.type =='date' && getShowLongTime(entries)\">{{entries.value |date:'MMM d, H:mm' }}</div>\n                <div *ngIf=\"!!entries.value && entries.value > 0 && entries.type =='date' && getShowVeryLongTime(entries)\">{{entries.value |date:'shortDate'}}</div>\n\n                <div *ngIf=\"entries.type =='date' && getNoTime(entries)\"> --</div>\n                <div *ngIf=\"entries.type =='string'\">{{entries.value }}</div>\n                <div *ngIf=\"entries.type =='link'\"><a [href]=\"entries.link\"> {{entries.value }}</a></div>\n                <div *ngIf=\"entries.type =='boolean' && entries.value\">{{entries.trueValue | translate}}</div>\n                <div *ngIf=\"entries.type =='boolean' && !entries.value\">{{entries.falseValue | translate}}</div>\n                <div *ngIf=\"entries.type =='booleanIcon' && entries.value\">\n                  <mat-icon>{{entries.trueValue }}</mat-icon>\n                </div>\n                <div *ngIf=\"entries.type =='booleanIcon' && !entries.value\">\n                  <mat-icon>{{entries.falseValue }}</mat-icon>\n                </div>\n                <div *ngIf=\"entries.type =='array'\">{{entries.values[entries.value] }}</div>\n                <div *ngIf=\"entries.type =='number'\">{{entries.value | number: entries.digitsInfo || '1.0-0'}}{{entries.unit }}</div>\n              </td>\n\n            </ng-container>\n            <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n          </table>\n      </div>\n    </iot-dashboard-card>\n  ", isInline: true, components: [{ type: DashboardCardComponent, selector: "iot-dashboard-card", inputs: ["colspan", "showHeader", "outerTitle", "innerTitle", "showTitle", "height", "width"] }, { type: i2__namespace$3.MatTable, selector: "mat-table, table[mat-table]", exportAs: ["matTable"] }, { type: i2__namespace$1.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }, { type: i2__namespace$3.MatRow, selector: "mat-row, tr[mat-row]", exportAs: ["matRow"] }], directives: [{ type: i4__namespace$2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: IotCardHeader, selector: "[iot-card-header]", inputs: ["padding"] }, { type: IotCardContent, selector: "[iot-card-content]", inputs: ["padding"] }, { type: i2__namespace$3.MatColumnDef, selector: "[matColumnDef]", inputs: ["sticky", "matColumnDef"] }, { type: i2__namespace$3.MatHeaderCellDef, selector: "[matHeaderCellDef]" }, { type: i2__namespace$3.MatHeaderCell, selector: "mat-header-cell, th[mat-header-cell]" }, { type: i2__namespace$3.MatCellDef, selector: "[matCellDef]" }, { type: i2__namespace$3.MatCell, selector: "mat-cell, td[mat-cell]" }, { type: i4__namespace$2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i2__namespace$3.MatRowDef, selector: "[matRowDef]", inputs: ["matRowDefColumns", "matRowDefWhen"] }], pipes: { "translate": i5__namespace.TranslatePipe, "date": i4__namespace$2.DatePipe, "number": i4__namespace$2.DecimalPipe } });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: KeyValueCardComponent, decorators: [{
                type: i0.Component,
                args: [{
                        selector: 'iot-key-value-card',
                        template: "\n    <iot-dashboard-card\n      [colspan]=\"1\"\n      [outerTitle]=\"outerTitle\">\n      <div *ngIf=\"title\" iot-card-header padding=\"true\">{{title |translate}}</div>\n      <div  iot-card-content >\n          <table mat-table [dataSource]=\"keyValueData\" style=\"width:100%\">\n\n            <ng-container matColumnDef=\"label\">\n              <th mat-header-cell *matHeaderCellDef> Item</th>\n              <td\n                (click)=\"click.emit(entries)\"\n                (mouseenter)=\"hover.emit(entries)\"\n                (mouseleave)=\"unHover.emit()\"\n                class=\"text \" [ngClass]=\"{warnrow: entries.error}\"\n                  mat-cell *matCellDef=\"let entries\"> {{entries.label|translate}}</td>\n            </ng-container>\n\n\n            <ng-container matColumnDef=\"value\">\n              <th mat-header-cell *matHeaderCellDef> Cost</th>\n              <td class=\"value text warnrow\" [ngClass]=\"{warnrow: entries.error}\"  mat-cell *matCellDef=\"let entries\">\n\n                <div *ngIf=\"!!entries.value && entries.value > 0 && entries.type =='date' && getShowShortTime(entries)\">{{entries.value |date:'H:mm'}}</div>\n                <div *ngIf=\"!!entries.value && entries.value > 0 && entries.type =='date' && getShowLongTime(entries)\">{{entries.value |date:'MMM d, H:mm' }}</div>\n                <div *ngIf=\"!!entries.value && entries.value > 0 && entries.type =='date' && getShowVeryLongTime(entries)\">{{entries.value |date:'shortDate'}}</div>\n\n                <div *ngIf=\"entries.type =='date' && getNoTime(entries)\"> --</div>\n                <div *ngIf=\"entries.type =='string'\">{{entries.value }}</div>\n                <div *ngIf=\"entries.type =='link'\"><a [href]=\"entries.link\"> {{entries.value }}</a></div>\n                <div *ngIf=\"entries.type =='boolean' && entries.value\">{{entries.trueValue | translate}}</div>\n                <div *ngIf=\"entries.type =='boolean' && !entries.value\">{{entries.falseValue | translate}}</div>\n                <div *ngIf=\"entries.type =='booleanIcon' && entries.value\">\n                  <mat-icon>{{entries.trueValue }}</mat-icon>\n                </div>\n                <div *ngIf=\"entries.type =='booleanIcon' && !entries.value\">\n                  <mat-icon>{{entries.falseValue }}</mat-icon>\n                </div>\n                <div *ngIf=\"entries.type =='array'\">{{entries.values[entries.value] }}</div>\n                <div *ngIf=\"entries.type =='number'\">{{entries.value | number: entries.digitsInfo || '1.0-0'}}{{entries.unit }}</div>\n              </td>\n\n            </ng-container>\n            <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n          </table>\n      </div>\n    </iot-dashboard-card>\n  "
                    }]
            }], ctorParameters: function () { return []; }, propDecorators: { outerTitle: [{
                    type: i0.Input
                }], title: [{
                    type: i0.Input
                }], keyValueData: [{
                    type: i0.Input
                }], click: [{
                    type: i0.Output
                }], hover: [{
                    type: i0.Output
                }], unHover: [{
                    type: i0.Output
                }] } });

    var PageHeaderComponent = /** @class */ (function () {
        function PageHeaderComponent() {
            this.back = new i0.EventEmitter();
        }
        PageHeaderComponent.prototype.ngOnInit = function () {
        };
        return PageHeaderComponent;
    }());
    PageHeaderComponent.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: PageHeaderComponent, deps: [], target: i0__namespace.ɵɵFactoryTarget.Component });
    PageHeaderComponent.ɵcmp = i0__namespace.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: PageHeaderComponent, selector: "iot-page-header", inputs: { title: "title", subTitle: "subTitle", routerLink: "routerLink", backLabel: "backLabel" }, outputs: { back: "back" }, ngImport: i0__namespace, template: "\n    <div class=\"header d-flex flex-column align-items-start\">\n      <a\n        *ngIf=\"routerLink && backLabel\"\n        mat-button [routerLink]=\"routerLink\">\n        <mat-icon>keyboard_backspace</mat-icon>\n        {{backLabel | translate }} </a>\n      <a\n        *ngIf=\"!routerLink && backLabel\"\n        mat-button (click)=\"back.emit()\" >\n        <mat-icon>keyboard_backspace</mat-icon>\n        {{backLabel | translate }} </a>\n      <div class=\"d-flex flex-row w-100\">\n        <div class=\"header-text flex-grow-1\">{{title | translate}} </div>\n\n        <ng-content select=\"[button3]\"></ng-content>\n        <ng-content select=\"[button2]\"></ng-content>\n        <ng-content select=\"[button1]\"></ng-content>\n      </div>\n      <div\n        *ngIf=\"subTitle\"\n        class=\"header-sub-text\">{{subTitle | translate}} </div>\n    </div>\n  ", isInline: true, components: [{ type: i1__namespace.MatAnchor, selector: "a[mat-button], a[mat-raised-button], a[mat-icon-button], a[mat-fab],             a[mat-mini-fab], a[mat-stroked-button], a[mat-flat-button]", inputs: ["disabled", "disableRipple", "color", "tabIndex"], exportAs: ["matButton", "matAnchor"] }, { type: i2__namespace$1.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }], directives: [{ type: i4__namespace$2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i1__namespace$2.RouterLinkWithHref, selector: "a[routerLink],area[routerLink]", inputs: ["routerLink", "target", "queryParams", "fragment", "queryParamsHandling", "preserveFragment", "skipLocationChange", "replaceUrl", "state", "relativeTo"] }], pipes: { "translate": i5__namespace.TranslatePipe } });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: PageHeaderComponent, decorators: [{
                type: i0.Component,
                args: [{
                        selector: 'iot-page-header',
                        template: "\n    <div class=\"header d-flex flex-column align-items-start\">\n      <a\n        *ngIf=\"routerLink && backLabel\"\n        mat-button [routerLink]=\"routerLink\">\n        <mat-icon>keyboard_backspace</mat-icon>\n        {{backLabel | translate }} </a>\n      <a\n        *ngIf=\"!routerLink && backLabel\"\n        mat-button (click)=\"back.emit()\" >\n        <mat-icon>keyboard_backspace</mat-icon>\n        {{backLabel | translate }} </a>\n      <div class=\"d-flex flex-row w-100\">\n        <div class=\"header-text flex-grow-1\">{{title | translate}} </div>\n\n        <ng-content select=\"[button3]\"></ng-content>\n        <ng-content select=\"[button2]\"></ng-content>\n        <ng-content select=\"[button1]\"></ng-content>\n      </div>\n      <div\n        *ngIf=\"subTitle\"\n        class=\"header-sub-text\">{{subTitle | translate}} </div>\n    </div>\n  "
                    }]
            }], ctorParameters: function () { return []; }, propDecorators: { title: [{
                    type: i0.Input
                }], subTitle: [{
                    type: i0.Input
                }], routerLink: [{
                    type: i0.Input
                }], backLabel: [{
                    type: i0.Input
                }], back: [{
                    type: i0.Output
                }] } });

    var CustomerSelectModalComponent = /** @class */ (function () {
        function CustomerSelectModalComponent(dialogRef, data) {
            this.dialogRef = dialogRef;
            this.data = data;
            this.customerlist = data.customers;
        }
        CustomerSelectModalComponent.prototype.onNoClick = function () {
            this.dialogRef.close();
        };
        CustomerSelectModalComponent.prototype.ngOnInit = function () {
        };
        return CustomerSelectModalComponent;
    }());
    CustomerSelectModalComponent.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: CustomerSelectModalComponent, deps: [{ token: i1__namespace$4.MatDialogRef }, { token: i1$4.MAT_DIALOG_DATA }], target: i0__namespace.ɵɵFactoryTarget.Component });
    CustomerSelectModalComponent.ɵcmp = i0__namespace.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: CustomerSelectModalComponent, selector: "iot-customer-select-modal", ngImport: i0__namespace, template: "\n    <div class=\"cust-header\">\n      <h1 mat-dialog-title>{{'DISTRIBUTOR.SELECT_CUST' }}</h1>\n      <div class=\"cust-button-control\" >\n        <button\n          *ngIf=\"!data['mobile'] \"\n          mat-button\n\n        >\n          <mat-icon>create_new_folder</mat-icon>\n          {{'NEW_DIST' }}</button>\n        <button\n          *ngIf=\"!data['mobile']\"\n          mat-button\n        >\n\n          <mat-icon>create_new_folder</mat-icon>\n\n          {{'NEW_CUST' }}</button>\n\n\n      </div>\n\n\n    </div>\n    {{customerlist|async|json}}\n    <div mat-dialog-actions class=\"cust-actions\">\n      <button mat-button (click)=\"onNoClick()\">{{'CONFIG.CANCEL'}}</button>\n      <button mat-button cdkFocusInitial\n\n      >{{'CONFIG.OPEN'}}</button>\n    </div>\n  ", isInline: true, styles: ["\n    .cust-header {\n      padding-top: 16px;\n      display: flex;\n      flex: 1;\n      justify-content: space-between;\n      margin-top: -8px;\n      min-height: 25px;\n    }\n\n    tbody a:hover.cust-name-link {\n      border-bottom-color: #3367d6;\n      color: #3367d6;\n    }\n\n    .cust-button-control {\n      margin-top: -15px;\n      display: flex;\n      flex-wrap: wrap;\n      flex-direction: row-reverse;\n      margin-left: 15px;\n    }\n\n    .cust-actions {\n      text-transform: capitalize;\n    }\n  "], components: [{ type: i1__namespace.MatButton, selector: "button[mat-button], button[mat-raised-button], button[mat-icon-button],             button[mat-fab], button[mat-mini-fab], button[mat-stroked-button],             button[mat-flat-button]", inputs: ["disabled", "disableRipple", "color"], exportAs: ["matButton"] }, { type: i2__namespace$1.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }], directives: [{ type: i1__namespace$4.MatDialogTitle, selector: "[mat-dialog-title], [matDialogTitle]", inputs: ["id"], exportAs: ["matDialogTitle"] }, { type: i4__namespace$2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i1__namespace$4.MatDialogActions, selector: "[mat-dialog-actions], mat-dialog-actions, [matDialogActions]" }], pipes: { "json": i4__namespace$2.JsonPipe, "async": i4__namespace$2.AsyncPipe } });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: CustomerSelectModalComponent, decorators: [{
                type: i0.Component,
                args: [{
                        selector: 'iot-customer-select-modal',
                        template: "\n    <div class=\"cust-header\">\n      <h1 mat-dialog-title>{{'DISTRIBUTOR.SELECT_CUST' }}</h1>\n      <div class=\"cust-button-control\" >\n        <button\n          *ngIf=\"!data['mobile'] \"\n          mat-button\n\n        >\n          <mat-icon>create_new_folder</mat-icon>\n          {{'NEW_DIST' }}</button>\n        <button\n          *ngIf=\"!data['mobile']\"\n          mat-button\n        >\n\n          <mat-icon>create_new_folder</mat-icon>\n\n          {{'NEW_CUST' }}</button>\n\n\n      </div>\n\n\n    </div>\n    {{customerlist|async|json}}\n    <div mat-dialog-actions class=\"cust-actions\">\n      <button mat-button (click)=\"onNoClick()\">{{'CONFIG.CANCEL'}}</button>\n      <button mat-button cdkFocusInitial\n\n      >{{'CONFIG.OPEN'}}</button>\n    </div>\n  ",
                        styles: ["\n    .cust-header {\n      padding-top: 16px;\n      display: flex;\n      flex: 1;\n      justify-content: space-between;\n      margin-top: -8px;\n      min-height: 25px;\n    }\n\n    tbody a:hover.cust-name-link {\n      border-bottom-color: #3367d6;\n      color: #3367d6;\n    }\n\n    .cust-button-control {\n      margin-top: -15px;\n      display: flex;\n      flex-wrap: wrap;\n      flex-direction: row-reverse;\n      margin-left: 15px;\n    }\n\n    .cust-actions {\n      text-transform: capitalize;\n    }\n  "
                        ]
                    }]
            }], ctorParameters: function () {
            return [{ type: i1__namespace$4.MatDialogRef }, { type: undefined, decorators: [{
                            type: i0.Inject,
                            args: [i1$4.MAT_DIALOG_DATA]
                        }] }];
        } });

    var DynamicFormWidth = /** @class */ (function () {
        function DynamicFormWidth(mediaMatcher) {
            this.mediaMatcher = mediaMatcher;
            this.label = "test";
            this.offset = 0;
            this.isBig = true;
            var mediaQueryList = mediaMatcher.matchMedia('(min-width: 1px)');
        }
        Object.defineProperty(DynamicFormWidth.prototype, "template", {
            get: function () {
                return null;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(DynamicFormWidth.prototype, "pristine", {
            get: function () {
                return true;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(DynamicFormWidth.prototype, "key", {
            get: function () {
                return '';
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(DynamicFormWidth.prototype, "newValue", {
            get: function () {
                return '';
            },
            enumerable: false,
            configurable: true
        });
        DynamicFormWidth.prototype.ngOnInit = function () {
            var _this = this;
            // this.offset.subscribe(o => {
            if (this.mediaQueryList) {
                if (this.mediaQueryList.removeEventListener) {
                    this.mediaQueryList.removeEventListener("change", this.eventListener);
                }
                else {
                    this.mediaQueryList.removeListener(this.eventListener);
                }
            }
            this.mediaQueryList = this.mediaMatcher.matchMedia('(min-width: ' + (700 + this.offset) + 'px)');
            this.eventListener = function (e) {
                _this.isBig = e.matches;
            };
            this.isBig = this.mediaQueryList.matches;
            if (this.mediaQueryList.addEventListener) {
                this.mediaQueryList.addEventListener("change", this.eventListener);
            }
            else {
                this.mediaQueryList.addListener(this.eventListener);
            }
            // });
        };
        return DynamicFormWidth;
    }());
    DynamicFormWidth.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: DynamicFormWidth, deps: [{ token: i1__namespace$5.MediaMatcher }], target: i0__namespace.ɵɵFactoryTarget.Directive });
    DynamicFormWidth.ɵdir = i0__namespace.ɵɵngDeclareDirective({ minVersion: "12.0.0", version: "12.2.6", type: DynamicFormWidth, selector: "[test]", inputs: { label: "label" }, ngImport: i0__namespace });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: DynamicFormWidth, decorators: [{
                type: i0.Directive,
                args: [{ selector: '[test]' }]
            }], ctorParameters: function () { return [{ type: i1__namespace$5.MediaMatcher }]; }, propDecorators: { label: [{
                    type: i0.Input
                }] } });

    var IotFormComponent = /** @class */ (function () {
        function IotFormComponent() {
            this.readonly = false;
            this.emitAll = false;
            this.alwaysShowDiscard = false;
            this.saveButtonText = "CONFIG.SAVE";
            this.statesDescription = {
                1: { text: '', loading: false, check: false },
                2: { text: 'CONFIG.SUBMITTING', loading: true, check: false },
                3: { text: 'CONFIG.CHANGES_SAVED', loading: false, check: true },
            };
            this.save = new i0.EventEmitter();
            this.discard = new i0.EventEmitter();
            this.active = false;
            this.tabs = [];
            this.formSubscription = new rxjs.Subscription();
        }
        IotFormComponent.prototype.ngAfterContentInit = function () {
            var _this = this;
            setTimeout(function () {
                _this.inputTabs.forEach(function (tab) {
                    if (tab.form) {
                        _this.formSubscription.add(tab.form.valueChanges.subscribe(function (formChange) {
                            _this.pristineCheck();
                        }));
                    }
                });
                _this.tabs = _this.inputTabs.toArray();
            }, 0);
        };
        IotFormComponent.prototype.pristineCheck = function () {
            var pristine = true;
            this.inputTabs.forEach(function (tab) {
                pristine = pristine && tab.pristine;
            });
            this.active = !pristine;
        };
        IotFormComponent.prototype._save = function () {
            var _this = this;
            var toReturn = {};
            this.inputTabs.forEach(function (tab) {
                var attr = tab.key;
                if (attr != '') {
                    if ((!tab.pristine) || _this.emitAll) {
                        toReturn[attr] = tab.newValue;
                    }
                }
            });
            this.save.emit(toReturn);
        };
        return IotFormComponent;
    }());
    IotFormComponent.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotFormComponent, deps: [], target: i0__namespace.ɵɵFactoryTarget.Component });
    IotFormComponent.ɵcmp = i0__namespace.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotFormComponent, selector: "iot-form", inputs: { readonly: "readonly", state: "state", emitAll: "emitAll", alwaysShowDiscard: "alwaysShowDiscard", saveButtonText: "saveButtonText", statesDescription: "statesDescription", active: "active" }, outputs: { save: "save", discard: "discard" }, queries: [{ propertyName: "inputTabs", predicate: DynamicFormWidth, descendants: true }], ngImport: i0__namespace, template: "\n    <div\n      class=\"iot-drawer-content\">\n      <ng-content select=\"[page-header]\"></ng-content>\n      <ng-content select=\"[below-header]\"></ng-content>\n      <div *ngFor=\"let tab of tabs; let i = index\" class=\"fields-container\"\n           role=\"tabpanel\"\n           attr.aria-labelledby=\"{{i}}-tab\">\n        <ng-container *ngTemplateOutlet=\"tab.template\"></ng-container>\n      </div>\n    </div>\n\n    <div class=\"outer-save-bar\" *ngIf=\"!readonly\">\n      <div class=\"button-row\">\n        <button *ngIf=\"alwaysShowDiscard || active\"\n                (click)=\"discard.emit()\"\n                mat-button>{{'CONFIG.CANCEL'|translate}}\n        </button>\n        <button [disabled]=\"!active\"\n                (click)=\"_save()\"\n                mat-raised-button color=\"primary\">{{saveButtonText |translate}}\n        </button>\n\n      </div>\n\n      <div  *ngIf=\"saveButtonText != ''\" class=\"bottom-font\">\n        <div class=\"message-outer\" *ngIf=\"statesDescription && statesDescription[state]\">\n          <mat-spinner\n            *ngIf=\"statesDescription[state].loading\"\n            class=\"icon\"\n            [diameter]=\"20\"\n          ></mat-spinner>\n          <mat-icon\n            *ngIf=\"statesDescription[state].check\"\n            class=\"icon\">check_circle_outline\n          </mat-icon>\n          <div class=\"message\">{{statesDescription[state].text |translate}} </div>\n        </div>\n      </div>\n    </div>\n  ", isInline: true, styles: ["\n      .fields-container .outer-margin{\n        padding-right: 24px;\n        padding-top: 16px;\n      }\n\n      .fields-container .outer-margin.nopaddingtop {\n        padding-top: 0px;\n      }\n\n      .outer-save-bar {\n        z-index: 3;\n        background: #fff;\n        bottom: 0;\n        box-shadow: 0 -5px 5px -5px #999;\n\n        left: 0;\n        position: fixed;\n        right: 0;\n        /*height: 36px;*/\n        display: flex;\n        flex-direction: row-reverse;\n        justify-content: space-between;\n\n        padding: 16px;\n      }\n\n      .button-row {\n\n      }\n\n      .icon {\n        margin-right: 8px;\n      }\n\n      .message-outer {\n        display: flex;\n        align-items: center;\n      }\n\n      .bottom-font {\n        color: #3c4043;\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 14px;\n        font-weight: 400;\n        letter-spacing: .0142857143em;\n        line-height: 1.25rem;\n      }\n\n      .message {\n        line-height: 20px;\n        height: 20px;\n      }\n    "], components: [{ type: i1__namespace.MatButton, selector: "button[mat-button], button[mat-raised-button], button[mat-icon-button],             button[mat-fab], button[mat-mini-fab], button[mat-stroked-button],             button[mat-flat-button]", inputs: ["disabled", "disableRipple", "color"], exportAs: ["matButton"] }, { type: i2__namespace$4.MatSpinner, selector: "mat-spinner", inputs: ["color"] }, { type: i2__namespace$1.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }], directives: [{ type: i4__namespace$2.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { type: i4__namespace$2.NgTemplateOutlet, selector: "[ngTemplateOutlet]", inputs: ["ngTemplateOutletContext", "ngTemplateOutlet"] }, { type: i4__namespace$2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }], pipes: { "translate": i5__namespace.TranslatePipe }, encapsulation: i0__namespace.ViewEncapsulation.None });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotFormComponent, decorators: [{
                type: i0.Component,
                args: [{
                        selector: 'iot-form',
                        template: "\n    <div\n      class=\"iot-drawer-content\">\n      <ng-content select=\"[page-header]\"></ng-content>\n      <ng-content select=\"[below-header]\"></ng-content>\n      <div *ngFor=\"let tab of tabs; let i = index\" class=\"fields-container\"\n           role=\"tabpanel\"\n           attr.aria-labelledby=\"{{i}}-tab\">\n        <ng-container *ngTemplateOutlet=\"tab.template\"></ng-container>\n      </div>\n    </div>\n\n    <div class=\"outer-save-bar\" *ngIf=\"!readonly\">\n      <div class=\"button-row\">\n        <button *ngIf=\"alwaysShowDiscard || active\"\n                (click)=\"discard.emit()\"\n                mat-button>{{'CONFIG.CANCEL'|translate}}\n        </button>\n        <button [disabled]=\"!active\"\n                (click)=\"_save()\"\n                mat-raised-button color=\"primary\">{{saveButtonText |translate}}\n        </button>\n\n      </div>\n\n      <div  *ngIf=\"saveButtonText != ''\" class=\"bottom-font\">\n        <div class=\"message-outer\" *ngIf=\"statesDescription && statesDescription[state]\">\n          <mat-spinner\n            *ngIf=\"statesDescription[state].loading\"\n            class=\"icon\"\n            [diameter]=\"20\"\n          ></mat-spinner>\n          <mat-icon\n            *ngIf=\"statesDescription[state].check\"\n            class=\"icon\">check_circle_outline\n          </mat-icon>\n          <div class=\"message\">{{statesDescription[state].text |translate}} </div>\n        </div>\n      </div>\n    </div>\n  ",
                        styles: [
                            "\n      .fields-container .outer-margin{\n        padding-right: 24px;\n        padding-top: 16px;\n      }\n\n      .fields-container .outer-margin.nopaddingtop {\n        padding-top: 0px;\n      }\n\n      .outer-save-bar {\n        z-index: 3;\n        background: #fff;\n        bottom: 0;\n        box-shadow: 0 -5px 5px -5px #999;\n\n        left: 0;\n        position: fixed;\n        right: 0;\n        /*height: 36px;*/\n        display: flex;\n        flex-direction: row-reverse;\n        justify-content: space-between;\n\n        padding: 16px;\n      }\n\n      .button-row {\n\n      }\n\n      .icon {\n        margin-right: 8px;\n      }\n\n      .message-outer {\n        display: flex;\n        align-items: center;\n      }\n\n      .bottom-font {\n        color: #3c4043;\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 14px;\n        font-weight: 400;\n        letter-spacing: .0142857143em;\n        line-height: 1.25rem;\n      }\n\n      .message {\n        line-height: 20px;\n        height: 20px;\n      }\n    "
                        ],
                        encapsulation: i0.ViewEncapsulation.None
                    }]
            }], ctorParameters: function () { return []; }, propDecorators: { readonly: [{
                    type: i0.Input
                }], state: [{
                    type: i0.Input
                }], emitAll: [{
                    type: i0.Input
                }], alwaysShowDiscard: [{
                    type: i0.Input
                }], saveButtonText: [{
                    type: i0.Input
                }], statesDescription: [{
                    type: i0.Input
                }], save: [{
                    type: i0.Output
                }], discard: [{
                    type: i0.Output
                }], inputTabs: [{
                    type: i0.ContentChildren,
                    args: [DynamicFormWidth, { descendants: true }]
                }], active: [{
                    type: i0.Input
                }] } });

    var IotInputTextComponent = /** @class */ (function (_super) {
        __extends(IotInputTextComponent, _super);
        function IotInputTextComponent() {
            var _this = _super.apply(this, __spreadArray([], __read(arguments))) || this;
            _this.form = new i4$3.FormGroup({
                inputId: new i4$3.FormControl(''),
            });
            _this.attribute = 'todo';
            _this.value = '';
            _this.type = 'text';
            _this.disabled = false;
            _this.onChange = new i0.EventEmitter();
            return _this;
        }
        Object.defineProperty(IotInputTextComponent.prototype, "template", {
            get: function () {
                return this._template;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(IotInputTextComponent.prototype, "pristine", {
            get: function () {
                return this.form.value.inputId === this.value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(IotInputTextComponent.prototype, "key", {
            get: function () {
                return this.attribute;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(IotInputTextComponent.prototype, "newValue", {
            get: function () {
                return this.form.value.inputId;
            },
            enumerable: false,
            configurable: true
        });
        IotInputTextComponent.prototype.ngOnInit = function () {
            _super.prototype.ngOnInit.call(this);
            this.form = new i4$3.FormGroup({
                inputId: new i4$3.FormControl({ value: this.value, disabled: this.disabled }),
            });
            // if (this.disabled) {
            //
            // }
        };
        IotInputTextComponent.prototype.ngOnChanges = function (changes) {
            if (changes.value && !!this.value) {
                this.form.setValue({ inputId: this.value });
            }
        };
        return IotInputTextComponent;
    }(DynamicFormWidth));
    IotInputTextComponent.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotInputTextComponent, deps: null, target: i0__namespace.ɵɵFactoryTarget.Component });
    IotInputTextComponent.ɵcmp = i0__namespace.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotInputTextComponent, selector: "iot-input-text", inputs: { label: "label", attribute: "attribute", description: "description", value: "value", type: "type", disabled: "disabled" }, outputs: { onChange: "onChange" }, providers: [{ provide: DynamicFormWidth, useExisting: i0.forwardRef(function () { return IotInputTextComponent; }) }], viewQueries: [{ propertyName: "_template", first: true, predicate: i0.TemplateRef, descendants: true }], usesInheritance: true, usesOnChanges: true, ngImport: i0__namespace, template: "\n    <ng-template>\n        <div class=\"outer-margin\">\n            <div class=\"outeroneline\" [ngClass]=\"{'outersmall':!isBig}\">\n                <div class=\"label-outer\" [ngClass]=\"{'label-outer-thin':!isBig}\"\n                >\n                    <div class=\"label\">{{label | translate}} </div>\n                </div>\n\n                <form [formGroup]=\"form\" class=\"content\">\n                  <div\n                    *ngIf=\"!!description\"\n                    class=\"checkbox-content-description\">{{description | translate}}</div>\n                    <div class=\"form-content\">\n\n                        <input class=\"input-class\" matInput [type]=\"type\"\n                               formControlName=\"inputId\"\n                               (ngModelChange)=\"onChange.emit($event)\"\n\n                               >\n                        <span class=\"border-class\"></span>\n                    </div>\n                </form>\n\n\n            </div>\n\n        </div>\n    </ng-template>\n\n    ", isInline: true, styles: ["\n        .outer-margin {\n            /*margin-left: -5px;*/\n\n            padding-right: 50px;\n        }\n\n        .label-outer.label-outer-thin {\n            min-height: 40px;\n        }\n\n        .checkbox-content-description {\n          font-family: Roboto, Arial, sans-serif;\n          font-size: 12px;\n          font-weight: 400;\n          letter-spacing: .025em;\n          line-height: 16px;\n          color: #5f6368;\n          margin-bottom: 13px;\n          margin-top: 2px;\n        }\n\n        .label-outer {\n            flex-basis: 33%;\n            min-height: 64px;\n            min-width: 220px;\n            display: block;\n            padding-top: 13px;\n            color: #3c4043;\n        }\n\n        .label {\n\n            font-family: Roboto, Arial, sans-serif;\n            font-size: 14px;\n            font-weight: 400;\n            height: 20px;\n            color: rgb(60, 64, 67);\n        }\n\n        .outeroneline {\n            display: flex;\n            max-width: 900px;\n        }\n\n\n        .outersmall {\n            flex-direction: column;\n            padding-bottom: 8px;\n        }\n\n\n        .content {\n            flex-direction: column;\n            flex-basis: 67%;\n            min-width: 280px;\n        }\n\n        .form-content {\n\n            width: 100%;\n            height: 46px;\n            padding: 0 16px;\n            border: 1px solid #dadce0;\n            border-radius: 4px;\n            border-top-left-radius: 4px;\n            border-top-right-radius: 4px;\n            border-top-right-radius: 4px;\n            border-bottom-right-radius: 4px;\n            border-bottom-right-radius: 4px;\n            border-bottom-left-radius: 4px;\n            pointer-events: none;\n        }\n\n\n        .form-content:hover {\n            border: 2px solid black;\n        }\n\n        .input-class {\n            height: 100%;\n            width: 100%;\n            display: flex;\n            border: none !important;\n            background-color: transparent;\n            pointer-events: auto;\n            font-family: Roboto, Arial, sans-serif;\n            font-size: 1rem;\n            font-weight: 400;\n            letter-spacing: .00625em;\n        }\n\n        .input-class:focus {\n            outline: none;\n        }\n    "], directives: [{ type: i4__namespace$2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i4__namespace$3.ɵNgNoValidate, selector: "form:not([ngNoForm]):not([ngNativeValidate])" }, { type: i4__namespace$3.NgControlStatusGroup, selector: "[formGroupName],[formArrayName],[ngModelGroup],[formGroup],form:not([ngNoForm]),[ngForm]" }, { type: i4__namespace$3.FormGroupDirective, selector: "[formGroup]", inputs: ["formGroup"], outputs: ["ngSubmit"], exportAs: ["ngForm"] }, { type: i4__namespace$2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i3__namespace$1.MatInput, selector: "input[matInput], textarea[matInput], select[matNativeControl],      input[matNativeControl], textarea[matNativeControl]", inputs: ["id", "disabled", "required", "type", "value", "readonly", "placeholder", "errorStateMatcher", "aria-describedby"], exportAs: ["matInput"] }, { type: i4__namespace$3.DefaultValueAccessor, selector: "input:not([type=checkbox])[formControlName],textarea[formControlName],input:not([type=checkbox])[formControl],textarea[formControl],input:not([type=checkbox])[ngModel],textarea[ngModel],[ngDefaultControl]" }, { type: i4__namespace$3.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { type: i4__namespace$3.FormControlName, selector: "[formControlName]", inputs: ["disabled", "formControlName", "ngModel"], outputs: ["ngModelChange"] }], pipes: { "translate": i5__namespace.TranslatePipe } });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotInputTextComponent, decorators: [{
                type: i0.Component,
                args: [{
                        selector: 'iot-input-text',
                        template: "\n    <ng-template>\n        <div class=\"outer-margin\">\n            <div class=\"outeroneline\" [ngClass]=\"{'outersmall':!isBig}\">\n                <div class=\"label-outer\" [ngClass]=\"{'label-outer-thin':!isBig}\"\n                >\n                    <div class=\"label\">{{label | translate}} </div>\n                </div>\n\n                <form [formGroup]=\"form\" class=\"content\">\n                  <div\n                    *ngIf=\"!!description\"\n                    class=\"checkbox-content-description\">{{description | translate}}</div>\n                    <div class=\"form-content\">\n\n                        <input class=\"input-class\" matInput [type]=\"type\"\n                               formControlName=\"inputId\"\n                               (ngModelChange)=\"onChange.emit($event)\"\n\n                               >\n                        <span class=\"border-class\"></span>\n                    </div>\n                </form>\n\n\n            </div>\n\n        </div>\n    </ng-template>\n\n    ",
                        styles: ["\n        .outer-margin {\n            /*margin-left: -5px;*/\n\n            padding-right: 50px;\n        }\n\n        .label-outer.label-outer-thin {\n            min-height: 40px;\n        }\n\n        .checkbox-content-description {\n          font-family: Roboto, Arial, sans-serif;\n          font-size: 12px;\n          font-weight: 400;\n          letter-spacing: .025em;\n          line-height: 16px;\n          color: #5f6368;\n          margin-bottom: 13px;\n          margin-top: 2px;\n        }\n\n        .label-outer {\n            flex-basis: 33%;\n            min-height: 64px;\n            min-width: 220px;\n            display: block;\n            padding-top: 13px;\n            color: #3c4043;\n        }\n\n        .label {\n\n            font-family: Roboto, Arial, sans-serif;\n            font-size: 14px;\n            font-weight: 400;\n            height: 20px;\n            color: rgb(60, 64, 67);\n        }\n\n        .outeroneline {\n            display: flex;\n            max-width: 900px;\n        }\n\n\n        .outersmall {\n            flex-direction: column;\n            padding-bottom: 8px;\n        }\n\n\n        .content {\n            flex-direction: column;\n            flex-basis: 67%;\n            min-width: 280px;\n        }\n\n        .form-content {\n\n            width: 100%;\n            height: 46px;\n            padding: 0 16px;\n            border: 1px solid #dadce0;\n            border-radius: 4px;\n            border-top-left-radius: 4px;\n            border-top-right-radius: 4px;\n            border-top-right-radius: 4px;\n            border-bottom-right-radius: 4px;\n            border-bottom-right-radius: 4px;\n            border-bottom-left-radius: 4px;\n            pointer-events: none;\n        }\n\n\n        .form-content:hover {\n            border: 2px solid black;\n        }\n\n        .input-class {\n            height: 100%;\n            width: 100%;\n            display: flex;\n            border: none !important;\n            background-color: transparent;\n            pointer-events: auto;\n            font-family: Roboto, Arial, sans-serif;\n            font-size: 1rem;\n            font-weight: 400;\n            letter-spacing: .00625em;\n        }\n\n        .input-class:focus {\n            outline: none;\n        }\n    "],
                        providers: [{ provide: DynamicFormWidth, useExisting: i0.forwardRef(function () { return IotInputTextComponent; }) }]
                    }]
            }], propDecorators: { _template: [{
                    type: i0.ViewChild,
                    args: [i0.TemplateRef]
                }], label: [{
                    type: i0.Input
                }], attribute: [{
                    type: i0.Input
                }], description: [{
                    type: i0.Input
                }], value: [{
                    type: i0.Input
                }], type: [{
                    type: i0.Input
                }], disabled: [{
                    type: i0.Input
                }], onChange: [{
                    type: i0.Output
                }] } });

    var IotInputHorizontalLineComponent = /** @class */ (function (_super) {
        __extends(IotInputHorizontalLineComponent, _super);
        function IotInputHorizontalLineComponent() {
            var _this = _super.apply(this, __spreadArray([], __read(arguments))) || this;
            _this.value = "none";
            _this.label = "none";
            _this.disabled = false;
            _this.onChange = new i0.EventEmitter();
            return _this;
        }
        Object.defineProperty(IotInputHorizontalLineComponent.prototype, "template", {
            get: function () {
                return this._template;
            },
            enumerable: false,
            configurable: true
        });
        IotInputHorizontalLineComponent.prototype.ngOnInit = function () {
            // this.template = this.t;
            _super.prototype.ngOnInit.call(this);
        };
        return IotInputHorizontalLineComponent;
    }(DynamicFormWidth));
    IotInputHorizontalLineComponent.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotInputHorizontalLineComponent, deps: null, target: i0__namespace.ɵɵFactoryTarget.Component });
    IotInputHorizontalLineComponent.ɵcmp = i0__namespace.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotInputHorizontalLineComponent, selector: "iot-horizontal-line", inputs: { value: "value", label: "label", disabled: "disabled" }, outputs: { onChange: "onChange" }, providers: [{ provide: DynamicFormWidth, useExisting: i0.forwardRef(function () { return IotInputHorizontalLineComponent; }) }], viewQueries: [{ propertyName: "_template", first: true, predicate: i0.TemplateRef, descendants: true }], usesInheritance: true, ngImport: i0__namespace, template: "\n\n    <ng-template>\n      <div *ngIf=\"!isBig\" class=\"content horizontal-line\">\n      </div>\n\n      <div *ngIf=\"isBig\" class=\"horizontal-line big-line\">\n      </div>\n    </ng-template>\n\n  ", isInline: true, styles: ["\n    .horizontal-line {\n      border-bottom: 1px solid #dadce0;\n      margin-right: 17px;\n      margin-bottom: 10px;\n      min-width: 395px;\n    }\n\n    .big-line {\n      max-width: 928px;\n    }\n\n  "], directives: [{ type: i4__namespace$2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }] });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotInputHorizontalLineComponent, decorators: [{
                type: i0.Component,
                args: [{
                        selector: 'iot-horizontal-line',
                        template: "\n\n    <ng-template>\n      <div *ngIf=\"!isBig\" class=\"content horizontal-line\">\n      </div>\n\n      <div *ngIf=\"isBig\" class=\"horizontal-line big-line\">\n      </div>\n    </ng-template>\n\n  ",
                        styles: ["\n    .horizontal-line {\n      border-bottom: 1px solid #dadce0;\n      margin-right: 17px;\n      margin-bottom: 10px;\n      min-width: 395px;\n    }\n\n    .big-line {\n      max-width: 928px;\n    }\n\n  "],
                        providers: [{ provide: DynamicFormWidth, useExisting: i0.forwardRef(function () { return IotInputHorizontalLineComponent; }) }]
                    }]
            }], propDecorators: { _template: [{
                    type: i0.ViewChild,
                    args: [i0.TemplateRef]
                }], value: [{
                    type: i0.Input
                }], label: [{
                    type: i0.Input
                }], disabled: [{
                    type: i0.Input
                }], onChange: [{
                    type: i0.Output
                }] } });

    var IotInputTextareaComponent = /** @class */ (function (_super) {
        __extends(IotInputTextareaComponent, _super);
        function IotInputTextareaComponent() {
            var _this = _super.apply(this, __spreadArray([], __read(arguments))) || this;
            _this.form = new i4$3.FormGroup({
                text: new i4$3.FormControl(''),
            });
            _this.attribute = 'todo';
            _this.rows = 20;
            _this.disabled = false;
            _this.onChange = new i0.EventEmitter();
            return _this;
        }
        Object.defineProperty(IotInputTextareaComponent.prototype, "template", {
            get: function () {
                return this._template;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(IotInputTextareaComponent.prototype, "pristine", {
            get: function () {
                return this.form.value.text === this.value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(IotInputTextareaComponent.prototype, "key", {
            get: function () {
                return this.attribute;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(IotInputTextareaComponent.prototype, "newValue", {
            get: function () {
                return this.form.value.text;
            },
            enumerable: false,
            configurable: true
        });
        IotInputTextareaComponent.prototype.ngOnInit = function () {
            _super.prototype.ngOnInit.call(this);
            if (this.disabled) {
                this.form = new i4$3.FormGroup({
                    text: new i4$3.FormControl({ value: '', disabled: this.disabled }),
                });
            }
        };
        return IotInputTextareaComponent;
    }(DynamicFormWidth));
    IotInputTextareaComponent.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotInputTextareaComponent, deps: null, target: i0__namespace.ɵɵFactoryTarget.Component });
    IotInputTextareaComponent.ɵcmp = i0__namespace.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotInputTextareaComponent, selector: "iot-input-textarea", inputs: { label: "label", attribute: "attribute", value: "value", placeholder: "placeholder", rows: "rows", disabled: "disabled" }, outputs: { onChange: "onChange" }, providers: [{ provide: DynamicFormWidth, useExisting: i0.forwardRef(function () { return IotInputTextareaComponent; }) }], viewQueries: [{ propertyName: "_template", first: true, predicate: i0.TemplateRef, descendants: true }], usesInheritance: true, ngImport: i0__namespace, template: "\n    <ng-template>\n      <div class=\"outer-margin\">\n        <div class=\"outeroneline\" [ngClass]=\"{'outersmall':!isBig}\">\n          <div class=\"label-outer\" [ngClass]=\"{'label-outer-thin':!isBig}\"\n          >\n            <div class=\"label\">{{label | translate}} </div>\n          </div>\n\n          <form [formGroup]=\"form\" class=\"content\">\n\n            <textarea class=\"input-class\" matInput type=\"text\"\n                      formControlName=\"text\"\n                      (ngModelChange)=\"onChange.emit($event)\"\n                      [placeholder]=\"placeholder\"\n                      [disabled]=\"disabled\"\n                      [rows]=\"rows\"\n                      [ngModel]=\"value\"></textarea>\n\n\n          </form>\n\n\n        </div>\n\n      </div>\n    </ng-template>\n  ", isInline: true, styles: ["\n    .outer-margin {\n      padding-right: 50px;\n    }\n\n    .label-outer.label-outer-thin {\n      min-height: 40px;\n    }\n\n    .label-outer {\n      flex-basis: 33%;\n      min-height: 64px;\n      min-width: 220px;\n      display: block;\n      padding-top: 13px;\n      color: #3c4043;\n    }\n\n    .label {\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 14px;\n      font-weight: 400;\n      height: 20px;\n      color: rgb(60, 64, 67);\n    }\n\n    .outeroneline {\n      display: flex;\n      max-width: 900px;\n    }\n\n    .outersmall {\n      flex-direction: column;\n      padding-bottom: 8px;\n    }\n\n    .content {\n      flex-direction: column;\n      flex-basis: 67%;\n      min-width: 280px;\n      padding-bottom: 20px;\n    }\n\n    .input-class {\n      height: 100%;\n      width: 100%;\n      display: flex;\n      border: none !important;\n      background-color: transparent;\n      pointer-events: auto;\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 1rem;\n      font-weight: 400;\n      letter-spacing: .00625em;\n    }\n\n    .input-class:focus {\n      outline: none;\n    }\n  "], directives: [{ type: i4__namespace$2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i4__namespace$3.ɵNgNoValidate, selector: "form:not([ngNoForm]):not([ngNativeValidate])" }, { type: i4__namespace$3.NgControlStatusGroup, selector: "[formGroupName],[formArrayName],[ngModelGroup],[formGroup],form:not([ngNoForm]),[ngForm]" }, { type: i4__namespace$3.FormGroupDirective, selector: "[formGroup]", inputs: ["formGroup"], outputs: ["ngSubmit"], exportAs: ["ngForm"] }, { type: i3__namespace$1.MatInput, selector: "input[matInput], textarea[matInput], select[matNativeControl],      input[matNativeControl], textarea[matNativeControl]", inputs: ["id", "disabled", "required", "type", "value", "readonly", "placeholder", "errorStateMatcher", "aria-describedby"], exportAs: ["matInput"] }, { type: i4__namespace$3.DefaultValueAccessor, selector: "input:not([type=checkbox])[formControlName],textarea[formControlName],input:not([type=checkbox])[formControl],textarea[formControl],input:not([type=checkbox])[ngModel],textarea[ngModel],[ngDefaultControl]" }, { type: i4__namespace$3.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { type: i4__namespace$3.FormControlName, selector: "[formControlName]", inputs: ["disabled", "formControlName", "ngModel"], outputs: ["ngModelChange"] }], pipes: { "translate": i5__namespace.TranslatePipe } });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotInputTextareaComponent, decorators: [{
                type: i0.Component,
                args: [{
                        selector: 'iot-input-textarea',
                        template: "\n    <ng-template>\n      <div class=\"outer-margin\">\n        <div class=\"outeroneline\" [ngClass]=\"{'outersmall':!isBig}\">\n          <div class=\"label-outer\" [ngClass]=\"{'label-outer-thin':!isBig}\"\n          >\n            <div class=\"label\">{{label | translate}} </div>\n          </div>\n\n          <form [formGroup]=\"form\" class=\"content\">\n\n            <textarea class=\"input-class\" matInput type=\"text\"\n                      formControlName=\"text\"\n                      (ngModelChange)=\"onChange.emit($event)\"\n                      [placeholder]=\"placeholder\"\n                      [disabled]=\"disabled\"\n                      [rows]=\"rows\"\n                      [ngModel]=\"value\"></textarea>\n\n\n          </form>\n\n\n        </div>\n\n      </div>\n    </ng-template>\n  ",
                        styles: ["\n    .outer-margin {\n      padding-right: 50px;\n    }\n\n    .label-outer.label-outer-thin {\n      min-height: 40px;\n    }\n\n    .label-outer {\n      flex-basis: 33%;\n      min-height: 64px;\n      min-width: 220px;\n      display: block;\n      padding-top: 13px;\n      color: #3c4043;\n    }\n\n    .label {\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 14px;\n      font-weight: 400;\n      height: 20px;\n      color: rgb(60, 64, 67);\n    }\n\n    .outeroneline {\n      display: flex;\n      max-width: 900px;\n    }\n\n    .outersmall {\n      flex-direction: column;\n      padding-bottom: 8px;\n    }\n\n    .content {\n      flex-direction: column;\n      flex-basis: 67%;\n      min-width: 280px;\n      padding-bottom: 20px;\n    }\n\n    .input-class {\n      height: 100%;\n      width: 100%;\n      display: flex;\n      border: none !important;\n      background-color: transparent;\n      pointer-events: auto;\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 1rem;\n      font-weight: 400;\n      letter-spacing: .00625em;\n    }\n\n    .input-class:focus {\n      outline: none;\n    }\n  "],
                        providers: [{ provide: DynamicFormWidth, useExisting: i0.forwardRef(function () { return IotInputTextareaComponent; }) }]
                    }]
            }], propDecorators: { _template: [{
                    type: i0.ViewChild,
                    args: [i0.TemplateRef]
                }], label: [{
                    type: i0.Input
                }], attribute: [{
                    type: i0.Input
                }], value: [{
                    type: i0.Input
                }], placeholder: [{
                    type: i0.Input
                }], rows: [{
                    type: i0.Input
                }], disabled: [{
                    type: i0.Input
                }], onChange: [{
                    type: i0.Output
                }] } });

    var IotInputSelectValueComponent = /** @class */ (function (_super) {
        __extends(IotInputSelectValueComponent, _super);
        function IotInputSelectValueComponent() {
            var _this = _super.apply(this, __spreadArray([], __read(arguments))) || this;
            _this.form = new i4$3.FormGroup({
                selectId: new i4$3.FormControl(''),
            });
            _this.attribute = 'todo';
            _this.description = 'explain this field';
            _this.disabled = false;
            _this.onChange = new i0.EventEmitter();
            return _this;
        }
        Object.defineProperty(IotInputSelectValueComponent.prototype, "template", {
            get: function () {
                return this._template;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(IotInputSelectValueComponent.prototype, "pristine", {
            get: function () {
                return this.form.value.selectId === this.value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(IotInputSelectValueComponent.prototype, "key", {
            get: function () {
                return this.attribute;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(IotInputSelectValueComponent.prototype, "newValue", {
            get: function () {
                return this.form.value.selectId;
            },
            enumerable: false,
            configurable: true
        });
        IotInputSelectValueComponent.prototype.change = function (event) {
            this.onChange.emit(event);
        };
        IotInputSelectValueComponent.prototype.ngOnInit = function () {
            _super.prototype.ngOnInit.call(this);
            if (this.disabled) {
                this.form = new i4$3.FormGroup({
                    selectId: new i4$3.FormControl({ value: '', disabled: this.disabled }),
                });
            }
            if (this.value) {
                this.form.setValue({ selectId: this.value });
            }
        };
        return IotInputSelectValueComponent;
    }(DynamicFormWidth));
    IotInputSelectValueComponent.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotInputSelectValueComponent, deps: null, target: i0__namespace.ɵɵFactoryTarget.Component });
    IotInputSelectValueComponent.ɵcmp = i0__namespace.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotInputSelectValueComponent, selector: "iot-input-select-values", inputs: { label: "label", attribute: "attribute", description: "description", value: "value", separator: "separator", values: "values", disabled: "disabled" }, outputs: { onChange: "onChange" }, providers: [{ provide: DynamicFormWidth, useExisting: i0.forwardRef(function () { return IotInputSelectValueComponent; }) }], viewQueries: [{ propertyName: "_template", first: true, predicate: i0.TemplateRef, descendants: true }], usesInheritance: true, ngImport: i0__namespace, template: "\n    <ng-template>\n      <div class=\"outer-margin\">\n        <div\n          *ngIf=\"label && label !== ''\"\n          class=\"outer\" [ngClass]=\"{'outersmall':!isBig}\">\n          <div class=\"label-outer\" [ngClass]=\"{'label-outer-thin':!isBig}\"\n          >\n            <div class=\"label\">{{label | translate}}</div>\n\n          </div>\n\n          <form [formGroup]=\"form\" class=\"content\">\n            <div class=\"checkbox-content\">\n              <div class=\"checkbox-content-description\">{{description | translate}}</div>\n              <mat-select\n                formControlName=\"selectId\"\n                (ngModelChange)=\"change($event)\">\n\n                <mat-option *ngFor=\"let v of values\" [value]=\"v.id\">{{v.name}} </mat-option>\n              </mat-select>\n            </div>\n            <div *ngIf=\"separator\" class=\"separator\"></div>\n          </form>\n\n\n        </div>\n\n      </div>\n    </ng-template>\n  ", isInline: true, styles: ["\n      .separator {\n        border-bottom: 1px solid #dadce0;\n        padding-top: 16px;\n      }\n\n      .checkbox-content-description {\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 12px;\n        font-weight: 400;\n        letter-spacing: .025em;\n        line-height: 16px;\n        color: #5f6368;\n        margin-bottom: 13px;\n        margin-top: 2px;\n      }\n\n      .outer-margin {\n        margin-left: 0px;\n        padding-right: 50px;\n      }\n\n      .label-outer.label-outer-thin {\n        min-height: 40px;\n      }\n\n      .label-outer {\n        flex-basis: 33%;\n        min-height: 64px;\n        min-width: 220px;\n        display: block;\n        padding-top: 13px;\n        color: #3c4043;\n      }\n\n      .label {\n\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 14px;\n        font-weight: 400;\n        height: 20px;\n        color: rgb(60, 64, 67);\n      }\n\n      .outer {\n        display: flex;\n        max-width: 900px;\n        margin-bottom: 16px;\n      }\n\n      .outersmall {\n        flex-direction: column;\n        padding-bottom: 8px;\n      }\n\n      .content {\n        flex-direction: column;\n        flex-basis: 67%;\n        min-width: 280px;\n      }\n\n      .checkbox-content {\n        display: flex;\n        flex-direction: column;\n        color: #3c4043;\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 14px;\n        font-weight: 400;\n        letter-spacing: 0.2px;\n        line-height: 20px;\n      }\n    "], components: [{ type: i1__namespace$6.MatSelect, selector: "mat-select", inputs: ["disabled", "disableRipple", "tabIndex"], exportAs: ["matSelect"] }, { type: i4__namespace$1.MatOption, selector: "mat-option", exportAs: ["matOption"] }], directives: [{ type: i4__namespace$2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i4__namespace$2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i4__namespace$3.ɵNgNoValidate, selector: "form:not([ngNoForm]):not([ngNativeValidate])" }, { type: i4__namespace$3.NgControlStatusGroup, selector: "[formGroupName],[formArrayName],[ngModelGroup],[formGroup],form:not([ngNoForm]),[ngForm]" }, { type: i4__namespace$3.FormGroupDirective, selector: "[formGroup]", inputs: ["formGroup"], outputs: ["ngSubmit"], exportAs: ["ngForm"] }, { type: i4__namespace$3.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { type: i4__namespace$3.FormControlName, selector: "[formControlName]", inputs: ["disabled", "formControlName", "ngModel"], outputs: ["ngModelChange"] }, { type: i4__namespace$2.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }], pipes: { "translate": i5__namespace.TranslatePipe } });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotInputSelectValueComponent, decorators: [{
                type: i0.Component,
                args: [{
                        selector: 'iot-input-select-values',
                        template: "\n    <ng-template>\n      <div class=\"outer-margin\">\n        <div\n          *ngIf=\"label && label !== ''\"\n          class=\"outer\" [ngClass]=\"{'outersmall':!isBig}\">\n          <div class=\"label-outer\" [ngClass]=\"{'label-outer-thin':!isBig}\"\n          >\n            <div class=\"label\">{{label | translate}}</div>\n\n          </div>\n\n          <form [formGroup]=\"form\" class=\"content\">\n            <div class=\"checkbox-content\">\n              <div class=\"checkbox-content-description\">{{description | translate}}</div>\n              <mat-select\n                formControlName=\"selectId\"\n                (ngModelChange)=\"change($event)\">\n\n                <mat-option *ngFor=\"let v of values\" [value]=\"v.id\">{{v.name}} </mat-option>\n              </mat-select>\n            </div>\n            <div *ngIf=\"separator\" class=\"separator\"></div>\n          </form>\n\n\n        </div>\n\n      </div>\n    </ng-template>\n  ",
                        styles: [
                            "\n      .separator {\n        border-bottom: 1px solid #dadce0;\n        padding-top: 16px;\n      }\n\n      .checkbox-content-description {\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 12px;\n        font-weight: 400;\n        letter-spacing: .025em;\n        line-height: 16px;\n        color: #5f6368;\n        margin-bottom: 13px;\n        margin-top: 2px;\n      }\n\n      .outer-margin {\n        margin-left: 0px;\n        padding-right: 50px;\n      }\n\n      .label-outer.label-outer-thin {\n        min-height: 40px;\n      }\n\n      .label-outer {\n        flex-basis: 33%;\n        min-height: 64px;\n        min-width: 220px;\n        display: block;\n        padding-top: 13px;\n        color: #3c4043;\n      }\n\n      .label {\n\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 14px;\n        font-weight: 400;\n        height: 20px;\n        color: rgb(60, 64, 67);\n      }\n\n      .outer {\n        display: flex;\n        max-width: 900px;\n        margin-bottom: 16px;\n      }\n\n      .outersmall {\n        flex-direction: column;\n        padding-bottom: 8px;\n      }\n\n      .content {\n        flex-direction: column;\n        flex-basis: 67%;\n        min-width: 280px;\n      }\n\n      .checkbox-content {\n        display: flex;\n        flex-direction: column;\n        color: #3c4043;\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 14px;\n        font-weight: 400;\n        letter-spacing: 0.2px;\n        line-height: 20px;\n      }\n    "
                        ],
                        providers: [{ provide: DynamicFormWidth, useExisting: i0.forwardRef(function () { return IotInputSelectValueComponent; }) }]
                    }]
            }], propDecorators: { _template: [{
                    type: i0.ViewChild,
                    args: [i0.TemplateRef]
                }], label: [{
                    type: i0.Input
                }], attribute: [{
                    type: i0.Input
                }], description: [{
                    type: i0.Input
                }], value: [{
                    type: i0.Input
                }], separator: [{
                    type: i0.Input
                }], values: [{
                    type: i0.Input
                }], disabled: [{
                    type: i0.Input
                }], onChange: [{
                    type: i0.Output
                }] } });

    var IotDatePickerComponent = /** @class */ (function (_super) {
        __extends(IotDatePickerComponent, _super);
        function IotDatePickerComponent() {
            var _this = _super.apply(this, __spreadArray([], __read(arguments))) || this;
            _this.timestampInSec = false;
            _this.form = new i4$3.FormGroup({
                dateId: new i4$3.FormControl(''),
            });
            _this.attribute = 'todo';
            _this.description = 'explain this field';
            _this.type = 'text';
            _this.disabled = false;
            _this.onChange = new i0.EventEmitter();
            return _this;
        }
        Object.defineProperty(IotDatePickerComponent.prototype, "template", {
            get: function () {
                return this._template;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(IotDatePickerComponent.prototype, "pristine", {
            get: function () {
                return this.form.value.dateId === this.value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(IotDatePickerComponent.prototype, "key", {
            get: function () {
                return this.attribute;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(IotDatePickerComponent.prototype, "newValue", {
            get: function () {
                if (this.timestampInSec) {
                    if (this.form.value.dateId.unix) {
                        return {
                            seconds: this.form.value.dateId.unix()
                        };
                    }
                    else {
                        return {
                            seconds: this.form.value.dateId.getTime() / 1000
                        };
                    }
                }
                return this.form.value.dateId;
            },
            enumerable: false,
            configurable: true
        });
        IotDatePickerComponent.prototype.ngOnInit = function () {
            _super.prototype.ngOnInit.call(this);
            if (this.value && this.value.seconds) {
                this.value = new Date(this.value.seconds * 1000);
                this.timestampInSec = true;
            }
            this.form.setValue({ dateId: this.value });
            if (this.disabled) {
                this.form = new i4$3.FormGroup({
                    dateId: new i4$3.FormControl({ value: '', disabled: this.disabled }),
                });
            }
        };
        return IotDatePickerComponent;
    }(DynamicFormWidth));
    IotDatePickerComponent.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotDatePickerComponent, deps: null, target: i0__namespace.ɵɵFactoryTarget.Component });
    IotDatePickerComponent.ɵcmp = i0__namespace.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotDatePickerComponent, selector: "iot-date-picker", inputs: { label: "label", attribute: "attribute", description: "description", value: "value", type: "type", disabled: "disabled" }, outputs: { onChange: "onChange" }, providers: [{ provide: DynamicFormWidth, useExisting: i0.forwardRef(function () { return IotDatePickerComponent; }) }], viewQueries: [{ propertyName: "_template", first: true, predicate: i0.TemplateRef, descendants: true }], usesInheritance: true, ngImport: i0__namespace, template: "\n    <ng-template>\n      <div class=\"outer-margin\">\n        <div\n          class=\"outeroneline\"\n          [ngClass]=\"{'outersmall':!isBig}\">\n          <div\n            class=\"label-outer\"\n            [ngClass]=\"{'label-outer-thin':!isBig}\"\n          >\n            <div class=\"label\">{{label | translate}} </div>\n          </div>\n          <form [formGroup]=\"form\" class=\"content\">\n\n            <mat-form-field appearance=\"outline\" class=\"w-100\">\n              <mat-label>{{description | translate}}</mat-label>\n              <input\n                formControlName=\"dateId\"\n                (ngModelChange)=\"onChange.emit($event)\"\n                matInput [matDatepicker]=\"picker\">\n              <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\n              <mat-datepicker #picker></mat-datepicker>\n            </mat-form-field>\n            <span class=\"border-class\"></span>\n          </form>\n        </div>\n      </div>\n    </ng-template>\n\n  ", isInline: true, styles: ["\n    .outer-margin {\n      /*margin-left: -5px;*/\n\n      padding-right: 50px;\n    }\n\n    .label-outer.label-outer-thin {\n      min-height: 40px;\n    }\n\n    .label-outer {\n      flex-basis: 33%;\n      min-height: 64px;\n      min-width: 220px;\n      display: block;\n      padding-top: 13px;\n      color: #3c4043;\n    }\n\n    .label {\n\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 14px;\n      font-weight: 400;\n      height: 20px;\n      color: rgb(60, 64, 67);\n    }\n\n    .outeroneline {\n      display: flex;\n      max-width: 900px;\n    }\n\n\n    .outersmall {\n      flex-direction: column;\n      padding-bottom: 8px;\n    }\n\n\n    .content {\n      flex-direction: column;\n      flex-basis: 67%;\n      min-width: 280px;\n    }\n\n    .form-content {\n\n      width: 100%;\n      height: 46px;\n      padding: 0 16px;\n      border: 1px solid #dadce0;\n      border-radius: 4px;\n      border-top-left-radius: 4px;\n      border-top-right-radius: 4px;\n      border-top-right-radius: 4px;\n      border-bottom-right-radius: 4px;\n      border-bottom-right-radius: 4px;\n      border-bottom-left-radius: 4px;\n      pointer-events: none;\n    }\n\n\n    .form-content:hover {\n      border: 2px solid black;\n    }\n\n    .input-class {\n      height: 100%;\n      width: 100%;\n      display: flex;\n      border: none !important;\n      background-color: transparent;\n      pointer-events: auto;\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 1rem;\n      font-weight: 400;\n      letter-spacing: .00625em;\n    }\n\n    .input-class:focus {\n      outline: none;\n    }\n  "], components: [{ type: i1__namespace$7.MatFormField, selector: "mat-form-field", inputs: ["color", "floatLabel", "appearance", "hideRequiredMarker", "hintLabel"], exportAs: ["matFormField"] }, { type: i2__namespace$5.MatDatepickerToggle, selector: "mat-datepicker-toggle", inputs: ["tabIndex", "disabled", "for", "aria-label", "disableRipple"], exportAs: ["matDatepickerToggle"] }, { type: i2__namespace$5.MatDatepicker, selector: "mat-datepicker", exportAs: ["matDatepicker"] }], directives: [{ type: i4__namespace$2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i4__namespace$3.ɵNgNoValidate, selector: "form:not([ngNoForm]):not([ngNativeValidate])" }, { type: i4__namespace$3.NgControlStatusGroup, selector: "[formGroupName],[formArrayName],[ngModelGroup],[formGroup],form:not([ngNoForm]),[ngForm]" }, { type: i4__namespace$3.FormGroupDirective, selector: "[formGroup]", inputs: ["formGroup"], outputs: ["ngSubmit"], exportAs: ["ngForm"] }, { type: i1__namespace$7.MatLabel, selector: "mat-label" }, { type: i4__namespace$3.DefaultValueAccessor, selector: "input:not([type=checkbox])[formControlName],textarea[formControlName],input:not([type=checkbox])[formControl],textarea[formControl],input:not([type=checkbox])[ngModel],textarea[ngModel],[ngDefaultControl]" }, { type: i3__namespace$1.MatInput, selector: "input[matInput], textarea[matInput], select[matNativeControl],      input[matNativeControl], textarea[matNativeControl]", inputs: ["id", "disabled", "required", "type", "value", "readonly", "placeholder", "errorStateMatcher", "aria-describedby"], exportAs: ["matInput"] }, { type: i2__namespace$5.MatDatepickerInput, selector: "input[matDatepicker]", inputs: ["matDatepicker", "min", "max", "matDatepickerFilter"], exportAs: ["matDatepickerInput"] }, { type: i4__namespace$3.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { type: i4__namespace$3.FormControlName, selector: "[formControlName]", inputs: ["disabled", "formControlName", "ngModel"], outputs: ["ngModelChange"] }, { type: i1__namespace$7.MatSuffix, selector: "[matSuffix]" }], pipes: { "translate": i5__namespace.TranslatePipe } });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotDatePickerComponent, decorators: [{
                type: i0.Component,
                args: [{
                        selector: 'iot-date-picker',
                        template: "\n    <ng-template>\n      <div class=\"outer-margin\">\n        <div\n          class=\"outeroneline\"\n          [ngClass]=\"{'outersmall':!isBig}\">\n          <div\n            class=\"label-outer\"\n            [ngClass]=\"{'label-outer-thin':!isBig}\"\n          >\n            <div class=\"label\">{{label | translate}} </div>\n          </div>\n          <form [formGroup]=\"form\" class=\"content\">\n\n            <mat-form-field appearance=\"outline\" class=\"w-100\">\n              <mat-label>{{description | translate}}</mat-label>\n              <input\n                formControlName=\"dateId\"\n                (ngModelChange)=\"onChange.emit($event)\"\n                matInput [matDatepicker]=\"picker\">\n              <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\n              <mat-datepicker #picker></mat-datepicker>\n            </mat-form-field>\n            <span class=\"border-class\"></span>\n          </form>\n        </div>\n      </div>\n    </ng-template>\n\n  ",
                        styles: ["\n    .outer-margin {\n      /*margin-left: -5px;*/\n\n      padding-right: 50px;\n    }\n\n    .label-outer.label-outer-thin {\n      min-height: 40px;\n    }\n\n    .label-outer {\n      flex-basis: 33%;\n      min-height: 64px;\n      min-width: 220px;\n      display: block;\n      padding-top: 13px;\n      color: #3c4043;\n    }\n\n    .label {\n\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 14px;\n      font-weight: 400;\n      height: 20px;\n      color: rgb(60, 64, 67);\n    }\n\n    .outeroneline {\n      display: flex;\n      max-width: 900px;\n    }\n\n\n    .outersmall {\n      flex-direction: column;\n      padding-bottom: 8px;\n    }\n\n\n    .content {\n      flex-direction: column;\n      flex-basis: 67%;\n      min-width: 280px;\n    }\n\n    .form-content {\n\n      width: 100%;\n      height: 46px;\n      padding: 0 16px;\n      border: 1px solid #dadce0;\n      border-radius: 4px;\n      border-top-left-radius: 4px;\n      border-top-right-radius: 4px;\n      border-top-right-radius: 4px;\n      border-bottom-right-radius: 4px;\n      border-bottom-right-radius: 4px;\n      border-bottom-left-radius: 4px;\n      pointer-events: none;\n    }\n\n\n    .form-content:hover {\n      border: 2px solid black;\n    }\n\n    .input-class {\n      height: 100%;\n      width: 100%;\n      display: flex;\n      border: none !important;\n      background-color: transparent;\n      pointer-events: auto;\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 1rem;\n      font-weight: 400;\n      letter-spacing: .00625em;\n    }\n\n    .input-class:focus {\n      outline: none;\n    }\n  "],
                        providers: [{ provide: DynamicFormWidth, useExisting: i0.forwardRef(function () { return IotDatePickerComponent; }) }]
                    }]
            }], propDecorators: { _template: [{
                    type: i0.ViewChild,
                    args: [i0.TemplateRef]
                }], label: [{
                    type: i0.Input
                }], attribute: [{
                    type: i0.Input
                }], description: [{
                    type: i0.Input
                }], value: [{
                    type: i0.Input
                }], type: [{
                    type: i0.Input
                }], disabled: [{
                    type: i0.Input
                }], onChange: [{
                    type: i0.Output
                }] } });

    var IotDefineIdentifierComponent = /** @class */ (function (_super) {
        __extends(IotDefineIdentifierComponent, _super);
        function IotDefineIdentifierComponent() {
            var _this = _super.apply(this, __spreadArray([], __read(arguments))) || this;
            _this.prefix = [
                {
                    'nid': 'smaxx',
                    'name': 'S)maxx'
                },
                {
                    'nid': 'iot',
                    'name': 'Io-Things'
                },
            ];
            _this.form = new i4$3.FormGroup({
                value: new i4$3.FormControl(''),
            });
            _this.attribute = 'todo';
            _this.description = 'explain this field';
            _this.disabled = false;
            _this.new = '';
            _this.thirpartySelection = 'smaxx';
            return _this;
        }
        Object.defineProperty(IotDefineIdentifierComponent.prototype, "template", {
            get: function () {
                return this._template;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(IotDefineIdentifierComponent.prototype, "pristine", {
            get: function () {
                return JSON.stringify(this.value) === JSON.stringify(this._value);
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(IotDefineIdentifierComponent.prototype, "key", {
            get: function () {
                return this.attribute;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(IotDefineIdentifierComponent.prototype, "newValue", {
            get: function () {
                return this._value;
            },
            enumerable: false,
            configurable: true
        });
        IotDefineIdentifierComponent.prototype.change = function (event) {
        };
        IotDefineIdentifierComponent.prototype.ngOnInit = function () {
            _super.prototype.ngOnInit.call(this);
            this._value = this.value;
            if (this.disabled) {
                // this.form = new FormGroup({
                //   selectId: new FormControl({value: '', disabled: this.disabled}),
                // });
            }
            this.form.setValue({ value: this.value });
        };
        IotDefineIdentifierComponent.prototype.addIdentifier = function () {
            this._value = __spreadArray(__spreadArray([], __read(this._value)), ["urn:" + this.thirpartySelection + ":" + this.new]);
            this.form.setValue({ value: __spreadArray(__spreadArray([], __read(this.value)), ["urn:" + this.thirpartySelection + ":" + this.new]) });
        };
        IotDefineIdentifierComponent.prototype.remove = function (id) {
            this._value = this._value.filter(function (urn) { return urn !== id; });
            this.form.setValue({ value: this._value });
        };
        return IotDefineIdentifierComponent;
    }(DynamicFormWidth));
    IotDefineIdentifierComponent.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotDefineIdentifierComponent, deps: null, target: i0__namespace.ɵɵFactoryTarget.Component });
    IotDefineIdentifierComponent.ɵcmp = i0__namespace.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotDefineIdentifierComponent, selector: "iot-define-identifier", inputs: { prefix: "prefix", label: "label", attribute: "attribute", description: "description", value: "value", separator: "separator", disabled: "disabled" }, providers: [{ provide: DynamicFormWidth, useExisting: i0.forwardRef(function () { return IotDefineIdentifierComponent; }) }], viewQueries: [{ propertyName: "_template", first: true, predicate: i0.TemplateRef, descendants: true }], usesInheritance: true, ngImport: i0__namespace, template: "\n    <ng-template>\n      <div class=\"outer-margin\">\n        <div class=\"outer\" [ngClass]=\"{'outersmall':!isBig}\">\n          <div class=\"label-outer\" [ngClass]=\"{'label-outer-thin':!isBig}\"\n          >\n            <div class=\"label\">{{label | translate}}</div>\n          </div>\n\n          <div class=\"content\">\n            <div class=\"checkbox-content\">\n              <div class=\"checkbox-content-description\">{{description | translate}}</div>\n\n              <div *ngFor=\"let id of _value\">\n                <div>\n                  <mat-form-field appearance=\"outline\" class=\"w-100\">\n                    <input class=\"input-class\" matInput type=\"text\"\n                           [disabled]=\"true\"\n                           [ngModel]=\"id\">\n                    <span class=\"border-class\"></span>\n                    <button\n                      (click)=\"remove(id)\"\n                      matSuffix mat-icon-button\n                      color=\"primary\" aria-label=\"Example icon button with a menu icon\">\n                      <mat-icon>delete</mat-icon>\n                    </button>\n\n                  </mat-form-field>\n                </div>\n              </div>\n\n              <div class=\"d-flex flex-row w-100\">\n\n                <div class=\"select-button mr-2\">\n                  <mat-form-field appearance=\"outline\">\n                    <mat-label>Third party</mat-label>\n\n                    <mat-select\n                      [(ngModel)]=\"thirpartySelection\"\n\n                    >\n                      <mat-option\n\n                        *ngFor=\"let p of prefix\" [value]=\"p.nid\">{{p.name}} </mat-option>\n                    </mat-select>\n                  </mat-form-field>\n                </div>\n                <div class=\"flex-grow-1\">\n                  <mat-form-field appearance=\"outline\" class=\"w-100\">\n                    <input class=\"input-class\" matInput type=\"text\"\n                           (keyup.enter)=\"addIdentifier()\"\n                           [disabled]=\"false\"\n                           [(ngModel)]=\"new\">\n                    <button\n                      (click)=\"addIdentifier()\"\n                      matSuffix mat-icon-button\n                      color=\"primary\" aria-label=\"Example icon button with a menu icon\">\n                      <mat-icon>addv</mat-icon>\n                    </button>\n                  </mat-form-field>\n                </div>\n              </div>\n\n            </div>\n            <div *ngIf=\"separator\" class=\"separator\"></div>\n          </div>\n\n\n        </div>\n      </div>\n    </ng-template>\n  ", isInline: true, styles: ["\n      .separator {\n        border-bottom: 1px solid #dadce0;\n        padding-top: 16px;\n      }\n\n      .checkbox-content-description {\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 12px;\n        font-weight: 400;\n        letter-spacing: .025em;\n        line-height: 16px;\n        color: #5f6368;\n        margin-bottom: 13px;\n        margin-top: 2px;\n      }\n\n      .outer-margin {\n        margin-left: 0px;\n        padding-right: 50px;\n      }\n\n      .label-outer.label-outer-thin {\n        min-height: 40px;\n      }\n\n      .label-outer {\n        flex-basis: 33%;\n        min-height: 64px;\n        min-width: 220px;\n        display: block;\n        padding-top: 13px;\n        color: #3c4043;\n      }\n\n      .label {\n\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 14px;\n        font-weight: 400;\n        height: 20px;\n        color: rgb(60, 64, 67);\n      }\n\n      .outer {\n        display: flex;\n        max-width: 900px;\n        margin-bottom: 16px;\n      }\n\n      .outersmall {\n        flex-direction: column;\n        padding-bottom: 8px;\n      }\n\n      .content {\n        flex-direction: column;\n        flex-basis: 67%;\n        min-width: 280px;\n      }\n\n      .checkbox-content {\n        display: flex;\n        flex-direction: column;\n        color: #3c4043;\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 14px;\n        font-weight: 400;\n        letter-spacing: 0.2px;\n        line-height: 20px;\n      }\n    "], components: [{ type: i1__namespace$7.MatFormField, selector: "mat-form-field", inputs: ["color", "floatLabel", "appearance", "hideRequiredMarker", "hintLabel"], exportAs: ["matFormField"] }, { type: i1__namespace.MatButton, selector: "button[mat-button], button[mat-raised-button], button[mat-icon-button],             button[mat-fab], button[mat-mini-fab], button[mat-stroked-button],             button[mat-flat-button]", inputs: ["disabled", "disableRipple", "color"], exportAs: ["matButton"] }, { type: i2__namespace$1.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }, { type: i1__namespace$6.MatSelect, selector: "mat-select", inputs: ["disabled", "disableRipple", "tabIndex"], exportAs: ["matSelect"] }, { type: i4__namespace$1.MatOption, selector: "mat-option", exportAs: ["matOption"] }], directives: [{ type: i4__namespace$2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i4__namespace$2.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { type: i3__namespace$1.MatInput, selector: "input[matInput], textarea[matInput], select[matNativeControl],      input[matNativeControl], textarea[matNativeControl]", inputs: ["id", "disabled", "required", "type", "value", "readonly", "placeholder", "errorStateMatcher", "aria-describedby"], exportAs: ["matInput"] }, { type: i4__namespace$3.DefaultValueAccessor, selector: "input:not([type=checkbox])[formControlName],textarea[formControlName],input:not([type=checkbox])[formControl],textarea[formControl],input:not([type=checkbox])[ngModel],textarea[ngModel],[ngDefaultControl]" }, { type: i4__namespace$3.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { type: i4__namespace$3.NgModel, selector: "[ngModel]:not([formControlName]):not([formControl])", inputs: ["name", "disabled", "ngModel", "ngModelOptions"], outputs: ["ngModelChange"], exportAs: ["ngModel"] }, { type: i1__namespace$7.MatSuffix, selector: "[matSuffix]" }, { type: i1__namespace$7.MatLabel, selector: "mat-label" }, { type: i4__namespace$2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }], pipes: { "translate": i5__namespace.TranslatePipe } });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotDefineIdentifierComponent, decorators: [{
                type: i0.Component,
                args: [{
                        selector: 'iot-define-identifier',
                        template: "\n    <ng-template>\n      <div class=\"outer-margin\">\n        <div class=\"outer\" [ngClass]=\"{'outersmall':!isBig}\">\n          <div class=\"label-outer\" [ngClass]=\"{'label-outer-thin':!isBig}\"\n          >\n            <div class=\"label\">{{label | translate}}</div>\n          </div>\n\n          <div class=\"content\">\n            <div class=\"checkbox-content\">\n              <div class=\"checkbox-content-description\">{{description | translate}}</div>\n\n              <div *ngFor=\"let id of _value\">\n                <div>\n                  <mat-form-field appearance=\"outline\" class=\"w-100\">\n                    <input class=\"input-class\" matInput type=\"text\"\n                           [disabled]=\"true\"\n                           [ngModel]=\"id\">\n                    <span class=\"border-class\"></span>\n                    <button\n                      (click)=\"remove(id)\"\n                      matSuffix mat-icon-button\n                      color=\"primary\" aria-label=\"Example icon button with a menu icon\">\n                      <mat-icon>delete</mat-icon>\n                    </button>\n\n                  </mat-form-field>\n                </div>\n              </div>\n\n              <div class=\"d-flex flex-row w-100\">\n\n                <div class=\"select-button mr-2\">\n                  <mat-form-field appearance=\"outline\">\n                    <mat-label>Third party</mat-label>\n\n                    <mat-select\n                      [(ngModel)]=\"thirpartySelection\"\n\n                    >\n                      <mat-option\n\n                        *ngFor=\"let p of prefix\" [value]=\"p.nid\">{{p.name}} </mat-option>\n                    </mat-select>\n                  </mat-form-field>\n                </div>\n                <div class=\"flex-grow-1\">\n                  <mat-form-field appearance=\"outline\" class=\"w-100\">\n                    <input class=\"input-class\" matInput type=\"text\"\n                           (keyup.enter)=\"addIdentifier()\"\n                           [disabled]=\"false\"\n                           [(ngModel)]=\"new\">\n                    <button\n                      (click)=\"addIdentifier()\"\n                      matSuffix mat-icon-button\n                      color=\"primary\" aria-label=\"Example icon button with a menu icon\">\n                      <mat-icon>addv</mat-icon>\n                    </button>\n                  </mat-form-field>\n                </div>\n              </div>\n\n            </div>\n            <div *ngIf=\"separator\" class=\"separator\"></div>\n          </div>\n\n\n        </div>\n      </div>\n    </ng-template>\n  ",
                        styles: [
                            "\n      .separator {\n        border-bottom: 1px solid #dadce0;\n        padding-top: 16px;\n      }\n\n      .checkbox-content-description {\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 12px;\n        font-weight: 400;\n        letter-spacing: .025em;\n        line-height: 16px;\n        color: #5f6368;\n        margin-bottom: 13px;\n        margin-top: 2px;\n      }\n\n      .outer-margin {\n        margin-left: 0px;\n        padding-right: 50px;\n      }\n\n      .label-outer.label-outer-thin {\n        min-height: 40px;\n      }\n\n      .label-outer {\n        flex-basis: 33%;\n        min-height: 64px;\n        min-width: 220px;\n        display: block;\n        padding-top: 13px;\n        color: #3c4043;\n      }\n\n      .label {\n\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 14px;\n        font-weight: 400;\n        height: 20px;\n        color: rgb(60, 64, 67);\n      }\n\n      .outer {\n        display: flex;\n        max-width: 900px;\n        margin-bottom: 16px;\n      }\n\n      .outersmall {\n        flex-direction: column;\n        padding-bottom: 8px;\n      }\n\n      .content {\n        flex-direction: column;\n        flex-basis: 67%;\n        min-width: 280px;\n      }\n\n      .checkbox-content {\n        display: flex;\n        flex-direction: column;\n        color: #3c4043;\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 14px;\n        font-weight: 400;\n        letter-spacing: 0.2px;\n        line-height: 20px;\n      }\n    "
                        ],
                        providers: [{ provide: DynamicFormWidth, useExisting: i0.forwardRef(function () { return IotDefineIdentifierComponent; }) }]
                    }]
            }], propDecorators: { _template: [{
                    type: i0.ViewChild,
                    args: [i0.TemplateRef]
                }], prefix: [{
                    type: i0.Input
                }], label: [{
                    type: i0.Input
                }], attribute: [{
                    type: i0.Input
                }], description: [{
                    type: i0.Input
                }], value: [{
                    type: i0.Input
                }], separator: [{
                    type: i0.Input
                }], disabled: [{
                    type: i0.Input
                }] } });

    var IotInputPlaceholderComponent = /** @class */ (function (_super) {
        __extends(IotInputPlaceholderComponent, _super);
        function IotInputPlaceholderComponent() {
            var _this = _super.apply(this, __spreadArray([], __read(arguments))) || this;
            _this.fullWidth = false;
            _this.attribute = 'todo';
            _this.type = 'text';
            _this.disabled = false;
            _this.onChange = new i0.EventEmitter();
            return _this;
        }
        Object.defineProperty(IotInputPlaceholderComponent.prototype, "template", {
            get: function () {
                return this._template;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(IotInputPlaceholderComponent.prototype, "pristine", {
            get: function () {
                // return this.form.value.inputId === this.value;
                return true;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(IotInputPlaceholderComponent.prototype, "key", {
            get: function () {
                return '';
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(IotInputPlaceholderComponent.prototype, "newValue", {
            get: function () {
                return '';
            },
            enumerable: false,
            configurable: true
        });
        IotInputPlaceholderComponent.prototype.ngOnInit = function () {
            _super.prototype.ngOnInit.call(this);
            if (this.disabled) {
                // this.form = new FormGroup({
                //   inputId: new FormControl({value:'', disabled: this.disabled}),
                // });
            }
        };
        return IotInputPlaceholderComponent;
    }(DynamicFormWidth));
    IotInputPlaceholderComponent.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotInputPlaceholderComponent, deps: null, target: i0__namespace.ɵɵFactoryTarget.Component });
    IotInputPlaceholderComponent.ɵcmp = i0__namespace.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotInputPlaceholderComponent, selector: "iot-input-placeholder", inputs: { label: "label", fullWidth: "fullWidth", attribute: "attribute", value: "value", type: "type", disabled: "disabled" }, outputs: { onChange: "onChange" }, providers: [{ provide: DynamicFormWidth, useExisting: i0.forwardRef(function () { return IotInputPlaceholderComponent; }) }], viewQueries: [{ propertyName: "_template", first: true, predicate: i0.TemplateRef, descendants: true }], usesInheritance: true, ngImport: i0__namespace, template: "\n    <ng-template>\n      <div *ngIf=\"!disabled && fullWidth\">\n        <ng-content select=\"[full-width]\"></ng-content>\n      </div>\n      <div *ngIf=\"!disabled && !fullWidth\" class=\"outer-margin\">\n            <div class=\"outeroneline\" [ngClass]=\"{'outersmall':!isBig}\">\n                <div class=\"label-outer\" [ngClass]=\"{'label-outer-thin':!isBig}\"\n                >\n                    <div class=\"label\">{{label | translate}} </div>\n                </div>\n\n                <div class=\"content\">\n                    <ng-content select=\"[field-positioned]\"></ng-content>\n\n                </div>\n\n\n            </div>\n\n        </div>\n    </ng-template>\n\n    ", isInline: true, styles: ["\n        .outer-margin {\n            padding-right: 50px;\n        }\n\n        .label-outer.label-outer-thin {\n            min-height: 40px;\n        }\n\n        .label-outer {\n            flex-basis: 33%;\n            min-height: 64px;\n            min-width: 220px;\n            display: block;\n            padding-top: 13px;\n            color: #3c4043;\n        }\n\n        .label {\n\n            font-family: Roboto, Arial, sans-serif;\n            font-size: 14px;\n            font-weight: 400;\n            height: 20px;\n            color: rgb(60, 64, 67);\n        }\n\n        .outeroneline {\n            display: flex;\n            max-width: 900px;\n        }\n\n\n        .outersmall {\n            flex-direction: column;\n            padding-bottom: 8px;\n        }\n\n\n        .content {\n            flex-direction: column;\n            flex-basis: 67%;\n            min-width: 280px;\n        }\n\n\n\n\n    "], directives: [{ type: i4__namespace$2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i4__namespace$2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }], pipes: { "translate": i5__namespace.TranslatePipe } });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotInputPlaceholderComponent, decorators: [{
                type: i0.Component,
                args: [{
                        selector: 'iot-input-placeholder',
                        template: "\n    <ng-template>\n      <div *ngIf=\"!disabled && fullWidth\">\n        <ng-content select=\"[full-width]\"></ng-content>\n      </div>\n      <div *ngIf=\"!disabled && !fullWidth\" class=\"outer-margin\">\n            <div class=\"outeroneline\" [ngClass]=\"{'outersmall':!isBig}\">\n                <div class=\"label-outer\" [ngClass]=\"{'label-outer-thin':!isBig}\"\n                >\n                    <div class=\"label\">{{label | translate}} </div>\n                </div>\n\n                <div class=\"content\">\n                    <ng-content select=\"[field-positioned]\"></ng-content>\n\n                </div>\n\n\n            </div>\n\n        </div>\n    </ng-template>\n\n    ",
                        styles: ["\n        .outer-margin {\n            padding-right: 50px;\n        }\n\n        .label-outer.label-outer-thin {\n            min-height: 40px;\n        }\n\n        .label-outer {\n            flex-basis: 33%;\n            min-height: 64px;\n            min-width: 220px;\n            display: block;\n            padding-top: 13px;\n            color: #3c4043;\n        }\n\n        .label {\n\n            font-family: Roboto, Arial, sans-serif;\n            font-size: 14px;\n            font-weight: 400;\n            height: 20px;\n            color: rgb(60, 64, 67);\n        }\n\n        .outeroneline {\n            display: flex;\n            max-width: 900px;\n        }\n\n\n        .outersmall {\n            flex-direction: column;\n            padding-bottom: 8px;\n        }\n\n\n        .content {\n            flex-direction: column;\n            flex-basis: 67%;\n            min-width: 280px;\n        }\n\n\n\n\n    "],
                        providers: [{ provide: DynamicFormWidth, useExisting: i0.forwardRef(function () { return IotInputPlaceholderComponent; }) }]
                    }]
            }], propDecorators: { _template: [{
                    type: i0.ViewChild,
                    args: [i0.TemplateRef]
                }], label: [{
                    type: i0.Input
                }], fullWidth: [{
                    type: i0.Input
                }], attribute: [{
                    type: i0.Input
                }], value: [{
                    type: i0.Input
                }], type: [{
                    type: i0.Input
                }], disabled: [{
                    type: i0.Input
                }], onChange: [{
                    type: i0.Output
                }] } });

    var IotInputLocationComponent = /** @class */ (function (_super) {
        __extends(IotInputLocationComponent, _super);
        function IotInputLocationComponent() {
            var _this = _super.apply(this, __spreadArray([], __read(arguments))) || this;
            _this.zoom = 15;
            _this.form = new i4$3.FormGroup({
                inputId: new i4$3.FormControl(''),
            });
            _this.attribute = 'todo';
            _this.type = 'text';
            _this.disabled = false;
            _this.onChange = new i0.EventEmitter();
            return _this;
        }
        Object.defineProperty(IotInputLocationComponent.prototype, "template", {
            get: function () {
                return this._template;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(IotInputLocationComponent.prototype, "pristine", {
            get: function () {
                return this.lat == this.latCopy && this.lng == this.lngCopy && this.zoom == this.zoomCopy;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(IotInputLocationComponent.prototype, "key", {
            get: function () {
                return this.attribute;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(IotInputLocationComponent.prototype, "newValue", {
            get: function () {
                return {
                    latitude: this.latCopy,
                    longitude: this.lngCopy,
                    zoom: this.zoomCopy
                };
            },
            enumerable: false,
            configurable: true
        });
        IotInputLocationComponent.prototype.markerChange = function (event) {
            this.latCopy = event.lat || event.coords.lat;
            this.lngCopy = event.lng || event.coords.lng;
            this.form.setValue({ inputId: '' + this.lat });
            this.onChange.emit(event.coords || event);
        };
        IotInputLocationComponent.prototype.zoomChange = function (event) {
            this.zoomCopy = event;
            this.form.setValue({ inputId: '' + event });
        };
        IotInputLocationComponent.prototype.ngOnInit = function () {
            _super.prototype.ngOnInit.call(this);
            this.latCopy = this.lat;
            this.lngCopy = this.lng;
            this.zoomCopy = this.zoom;
            if (this.disabled) {
                this.form = new i4$3.FormGroup({
                    inputId: new i4$3.FormControl({ value: '' + this.lat, disabled: this.disabled }),
                });
            }
        };
        return IotInputLocationComponent;
    }(DynamicFormWidth));
    IotInputLocationComponent.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotInputLocationComponent, deps: null, target: i0__namespace.ɵɵFactoryTarget.Component });
    IotInputLocationComponent.ɵcmp = i0__namespace.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotInputLocationComponent, selector: "iot-input-location", inputs: { zoom: "zoom", lat: "lat", lng: "lng", label: "label", attribute: "attribute", value: "value", type: "type", disabled: "disabled" }, outputs: { onChange: "onChange" }, providers: [{ provide: DynamicFormWidth, useExisting: i0.forwardRef(function () { return IotInputLocationComponent; }) }], viewQueries: [{ propertyName: "_template", first: true, predicate: i0.TemplateRef, descendants: true }], usesInheritance: true, ngImport: i0__namespace, template: "\n    <ng-template>\n        <div class=\"outer-margin\">\n            <div class=\"outeroneline\" [ngClass]=\"{'outersmall':!isBig}\">\n                <div class=\"label-outer\" [ngClass]=\"{'label-outer-thin':!isBig}\"\n                >\n                    <div class=\"label\">{{label | translate}} </div>\n                </div>\n\n              <div class=\"content \">\n                <agm-map\n                  class=\"input-map\"\n                  [zoom]=\"zoom\"\n                  [mapTypeId]=\"'roadmap'\"\n                  (zoomChange)=\"zoomChange($event)\"\n                  (centerChange)=\"markerChange($event)\"\n                  [latitude]=\"lat\" [longitude]=\"lng\">\n\n                  <agm-marker\n                              [markerDraggable]=\"true\"\n                              (dragEnd)=\"markerChange($event)\"\n                              [latitude]=\"latCopy\"\n                              [longitude]=\"lngCopy\">\n                  </agm-marker>\n                </agm-map>\n\n              </div>\n\n\n\n            </div>\n\n        </div>\n    </ng-template>\n\n    ", isInline: true, styles: ["\n    agm-map {\n      height: 200px;\n    }\n        .outer-margin {\n            /*margin-left: -5px;*/\n\n            padding-right: 50px;\n        }\n\n        .label-outer.label-outer-thin {\n            min-height: 40px;\n        }\n\n        .label-outer {\n            flex-basis: 33%;\n            min-height: 64px;\n            min-width: 220px;\n            display: block;\n            padding-top: 13px;\n            color: #3c4043;\n        }\n\n        .label {\n\n            font-family: Roboto, Arial, sans-serif;\n            font-size: 14px;\n            font-weight: 400;\n            height: 20px;\n            color: rgb(60, 64, 67);\n        }\n\n        .outeroneline {\n            display: flex;\n            max-width: 900px;\n        }\n\n\n        .outersmall {\n            flex-direction: column;\n            padding-bottom: 8px;\n        }\n\n\n        .content {\n            flex-direction: column;\n            flex-basis: 67%;\n            min-width: 280px;\n          border: 1px solid #dadce0;\n          border-radius: 4px;\n        }\n\n        .form-content > div {\n\n\n          border: 2px solid red;\n          border-radius: 25px;\n\n        }\n\n\n\n    .input-map {\n\n      height: 300px;\n      width: 100%;\n      display: flex;\n      border: none !important;\n      background-color: transparent;\n      pointer-events: auto;\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 1rem;\n      font-weight: 400;\n      letter-spacing: .00625em;\n    }\n    "], components: [{ type: i1__namespace$8.AgmMap, selector: "agm-map", inputs: ["longitude", "latitude", "zoom", "mapDraggable", "disableDoubleClickZoom", "disableDefaultUI", "scrollwheel", "keyboardShortcuts", "styles", "usePanning", "fitBounds", "scaleControl", "mapTypeControl", "panControl", "rotateControl", "fullscreenControl", "mapTypeId", "clickableIcons", "showDefaultInfoWindow", "gestureHandling", "tilt", "minZoom", "maxZoom", "controlSize", "backgroundColor", "draggableCursor", "draggingCursor", "zoomControl", "zoomControlOptions", "streetViewControl", "streetViewControlOptions", "fitBoundsPadding", "scaleControlOptions", "mapTypeControlOptions", "panControlOptions", "rotateControlOptions", "fullscreenControlOptions", "restriction"], outputs: ["mapClick", "mapRightClick", "mapDblClick", "centerChange", "boundsChange", "mapTypeIdChange", "idle", "zoomChange", "mapReady", "tilesLoaded"] }], directives: [{ type: i4__namespace$2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i1__namespace$8.AgmMarker, selector: "agm-marker", inputs: ["latitude", "longitude", "title", "label", "markerDraggable", "iconUrl", "openInfoWindow", "opacity", "visible", "zIndex", "animation", "markerClickable"], outputs: ["markerClick", "dragStart", "drag", "dragEnd", "mouseOver", "mouseOut", "animationChange", "markerDblClick", "markerRightClick"] }], pipes: { "translate": i5__namespace.TranslatePipe } });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotInputLocationComponent, decorators: [{
                type: i0.Component,
                args: [{
                        selector: 'iot-input-location',
                        template: "\n    <ng-template>\n        <div class=\"outer-margin\">\n            <div class=\"outeroneline\" [ngClass]=\"{'outersmall':!isBig}\">\n                <div class=\"label-outer\" [ngClass]=\"{'label-outer-thin':!isBig}\"\n                >\n                    <div class=\"label\">{{label | translate}} </div>\n                </div>\n\n              <div class=\"content \">\n                <agm-map\n                  class=\"input-map\"\n                  [zoom]=\"zoom\"\n                  [mapTypeId]=\"'roadmap'\"\n                  (zoomChange)=\"zoomChange($event)\"\n                  (centerChange)=\"markerChange($event)\"\n                  [latitude]=\"lat\" [longitude]=\"lng\">\n\n                  <agm-marker\n                              [markerDraggable]=\"true\"\n                              (dragEnd)=\"markerChange($event)\"\n                              [latitude]=\"latCopy\"\n                              [longitude]=\"lngCopy\">\n                  </agm-marker>\n                </agm-map>\n\n              </div>\n\n\n\n            </div>\n\n        </div>\n    </ng-template>\n\n    ",
                        styles: ["\n    agm-map {\n      height: 200px;\n    }\n        .outer-margin {\n            /*margin-left: -5px;*/\n\n            padding-right: 50px;\n        }\n\n        .label-outer.label-outer-thin {\n            min-height: 40px;\n        }\n\n        .label-outer {\n            flex-basis: 33%;\n            min-height: 64px;\n            min-width: 220px;\n            display: block;\n            padding-top: 13px;\n            color: #3c4043;\n        }\n\n        .label {\n\n            font-family: Roboto, Arial, sans-serif;\n            font-size: 14px;\n            font-weight: 400;\n            height: 20px;\n            color: rgb(60, 64, 67);\n        }\n\n        .outeroneline {\n            display: flex;\n            max-width: 900px;\n        }\n\n\n        .outersmall {\n            flex-direction: column;\n            padding-bottom: 8px;\n        }\n\n\n        .content {\n            flex-direction: column;\n            flex-basis: 67%;\n            min-width: 280px;\n          border: 1px solid #dadce0;\n          border-radius: 4px;\n        }\n\n        .form-content > div {\n\n\n          border: 2px solid red;\n          border-radius: 25px;\n\n        }\n\n\n\n    .input-map {\n\n      height: 300px;\n      width: 100%;\n      display: flex;\n      border: none !important;\n      background-color: transparent;\n      pointer-events: auto;\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 1rem;\n      font-weight: 400;\n      letter-spacing: .00625em;\n    }\n    "],
                        providers: [{ provide: DynamicFormWidth, useExisting: i0.forwardRef(function () { return IotInputLocationComponent; }) }]
                    }]
            }], propDecorators: { _template: [{
                    type: i0.ViewChild,
                    args: [i0.TemplateRef]
                }], zoom: [{
                    type: i0.Input
                }], lat: [{
                    type: i0.Input
                }], lng: [{
                    type: i0.Input
                }], label: [{
                    type: i0.Input
                }], attribute: [{
                    type: i0.Input
                }], value: [{
                    type: i0.Input
                }], type: [{
                    type: i0.Input
                }], disabled: [{
                    type: i0.Input
                }], onChange: [{
                    type: i0.Output
                }] } });

    var IotInputCheckboxComponent = /** @class */ (function (_super) {
        __extends(IotInputCheckboxComponent, _super);
        function IotInputCheckboxComponent() {
            var _this = _super.apply(this, __spreadArray([], __read(arguments))) || this;
            _this.form = new i4$3.FormGroup({
                checkboxControl: new i4$3.FormControl(false),
            });
            _this.leftLabel = '';
            _this.attribute = 'todo';
            _this.description = 'explain this field';
            _this.disabled = false;
            _this.inverse = false;
            _this.onChange = new i0.EventEmitter();
            return _this;
        }
        Object.defineProperty(IotInputCheckboxComponent.prototype, "template", {
            get: function () {
                return this._template;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(IotInputCheckboxComponent.prototype, "pristine", {
            get: function () {
                if (this.inverse) {
                    return (!!this.form.value.checkboxControl) === !this.value;
                }
                return (!!this.form.value.checkboxControl) === this.value;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(IotInputCheckboxComponent.prototype, "key", {
            get: function () {
                return this.attribute;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(IotInputCheckboxComponent.prototype, "newValue", {
            get: function () {
                console.log('checkbox value is ', this.attribute, this.form.value.checkboxControl);
                if (this.inverse) {
                    return !this.form.value.checkboxControl;
                }
                return !!this.form.value.checkboxControl;
            },
            enumerable: false,
            configurable: true
        });
        IotInputCheckboxComponent.prototype.change = function (event) {
            this.onChange.emit(event);
        };
        IotInputCheckboxComponent.prototype.ngOnInit = function () {
            _super.prototype.ngOnInit.call(this);
            if (this.disabled) {
                this.form = new i4$3.FormGroup({
                    checkboxControl: new i4$3.FormControl({ value: '', disabled: this.disabled }),
                });
            }
            if (this.inverse) {
                this.form.setValue({ checkboxControl: !this.value });
            }
            else {
                this.form.setValue({ checkboxControl: this.value });
            }
        };
        IotInputCheckboxComponent.prototype.ngOnChanges = function (changes) {
            if (changes.value) {
                if (this.inverse) {
                    this.form.setValue({ checkboxControl: !this.value });
                }
                else {
                    this.form.setValue({ checkboxControl: !!this.value });
                }
            }
        };
        return IotInputCheckboxComponent;
    }(DynamicFormWidth));
    IotInputCheckboxComponent.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotInputCheckboxComponent, deps: null, target: i0__namespace.ɵɵFactoryTarget.Component });
    IotInputCheckboxComponent.ɵcmp = i0__namespace.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotInputCheckboxComponent, selector: "iot-input-checkbox", inputs: { leftLabel: "leftLabel", label: "label", attribute: "attribute", description: "description", value: "value", separator: "separator", disabled: "disabled", inverse: "inverse" }, outputs: { onChange: "onChange" }, providers: [{ provide: DynamicFormWidth, useExisting: i0.forwardRef(function () { return IotInputCheckboxComponent; }) }], viewQueries: [{ propertyName: "_template", first: true, predicate: i0.TemplateRef, descendants: true }], usesInheritance: true, usesOnChanges: true, ngImport: i0__namespace, template: "\n    <ng-template>\n      <div\n        [ngClass]=\"{'outer-margin':true, 'nopaddingtop' : leftLabel === ''}\"\n        >\n        <div\n\n          class=\"outer\" [ngClass]=\"{'outersmall':!isBig}\">\n          <div\n            class=\"label-outer\" [ngClass]=\"{'label-outer-thin':!isBig}\"\n          >\n            <div class=\"label\">{{leftLabel | translate}}</div>\n          </div>\n\n          <form [formGroup]=\"form\" class=\"content\">\n            <div class=\"checkbox-content\">\n              <div\n                *ngIf=\"description !== ''\"\n                class=\"checkbox-content-description\">{{description | translate}}</div>\n\n              <mat-checkbox\n                formControlName=\"checkboxControl\"\n                            (ngModelChange)=\"change($event)\">\n                {{label | translate}}\n              </mat-checkbox>\n            </div>\n            <div *ngIf=\"separator\" class=\"separator\"></div>\n          </form>\n\n\n        </div>\n\n      </div>\n    </ng-template>\n  ", isInline: true, styles: ["\n      .separator {\n        border-bottom: 1px solid #dadce0;\n        padding-top: 16px;\n      }\n\n      .checkbox-content-description {\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 12px;\n        font-weight: 400;\n        letter-spacing: .025em;\n        line-height: 16px;\n        color: #5f6368;\n        margin-bottom: 13px;\n        margin-top: 2px;\n      }\n\n      .outer-margin {\n        margin-left: 0px;\n        padding-right: 50px;\n      }\n\n      .label-outer.label-outer-thin {\n        min-height: 40px;\n      }\n\n      .label-outer {\n        flex-basis: 33%;\n        min-height: 64px;\n        min-width: 220px;\n        display: block;\n        padding-top: 13px;\n        color: #3c4043;\n      }\n\n      .label {\n\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 14px;\n        font-weight: 400;\n        height: 20px;\n        color: rgb(60, 64, 67);\n      }\n\n      .outer {\n        display: flex;\n        max-width: 900px;\n        margin-bottom: 16px;\n      }\n\n      .outersmall {\n        flex-direction: column;\n        padding-bottom: 8px;\n      }\n\n      .content {\n        flex-direction: column;\n        flex-basis: 67%;\n        min-width: 280px;\n      }\n\n      .checkbox-content {\n        display: flex;\n        flex-direction: column;\n        color: #3c4043;\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 14px;\n        font-weight: 400;\n        letter-spacing: 0.2px;\n        line-height: 20px;\n      }\n    "], components: [{ type: i1__namespace$9.MatCheckbox, selector: "mat-checkbox", inputs: ["disableRipple", "color", "tabIndex", "aria-label", "aria-labelledby", "id", "labelPosition", "name", "required", "checked", "disabled", "indeterminate", "aria-describedby", "value"], outputs: ["change", "indeterminateChange"], exportAs: ["matCheckbox"] }], directives: [{ type: i4__namespace$2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i4__namespace$3.ɵNgNoValidate, selector: "form:not([ngNoForm]):not([ngNativeValidate])" }, { type: i4__namespace$3.NgControlStatusGroup, selector: "[formGroupName],[formArrayName],[ngModelGroup],[formGroup],form:not([ngNoForm]),[ngForm]" }, { type: i4__namespace$3.FormGroupDirective, selector: "[formGroup]", inputs: ["formGroup"], outputs: ["ngSubmit"], exportAs: ["ngForm"] }, { type: i4__namespace$2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i4__namespace$3.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { type: i4__namespace$3.FormControlName, selector: "[formControlName]", inputs: ["disabled", "formControlName", "ngModel"], outputs: ["ngModelChange"] }], pipes: { "translate": i5__namespace.TranslatePipe } });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotInputCheckboxComponent, decorators: [{
                type: i0.Component,
                args: [{
                        selector: 'iot-input-checkbox',
                        template: "\n    <ng-template>\n      <div\n        [ngClass]=\"{'outer-margin':true, 'nopaddingtop' : leftLabel === ''}\"\n        >\n        <div\n\n          class=\"outer\" [ngClass]=\"{'outersmall':!isBig}\">\n          <div\n            class=\"label-outer\" [ngClass]=\"{'label-outer-thin':!isBig}\"\n          >\n            <div class=\"label\">{{leftLabel | translate}}</div>\n          </div>\n\n          <form [formGroup]=\"form\" class=\"content\">\n            <div class=\"checkbox-content\">\n              <div\n                *ngIf=\"description !== ''\"\n                class=\"checkbox-content-description\">{{description | translate}}</div>\n\n              <mat-checkbox\n                formControlName=\"checkboxControl\"\n                            (ngModelChange)=\"change($event)\">\n                {{label | translate}}\n              </mat-checkbox>\n            </div>\n            <div *ngIf=\"separator\" class=\"separator\"></div>\n          </form>\n\n\n        </div>\n\n      </div>\n    </ng-template>\n  ",
                        styles: [
                            "\n      .separator {\n        border-bottom: 1px solid #dadce0;\n        padding-top: 16px;\n      }\n\n      .checkbox-content-description {\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 12px;\n        font-weight: 400;\n        letter-spacing: .025em;\n        line-height: 16px;\n        color: #5f6368;\n        margin-bottom: 13px;\n        margin-top: 2px;\n      }\n\n      .outer-margin {\n        margin-left: 0px;\n        padding-right: 50px;\n      }\n\n      .label-outer.label-outer-thin {\n        min-height: 40px;\n      }\n\n      .label-outer {\n        flex-basis: 33%;\n        min-height: 64px;\n        min-width: 220px;\n        display: block;\n        padding-top: 13px;\n        color: #3c4043;\n      }\n\n      .label {\n\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 14px;\n        font-weight: 400;\n        height: 20px;\n        color: rgb(60, 64, 67);\n      }\n\n      .outer {\n        display: flex;\n        max-width: 900px;\n        margin-bottom: 16px;\n      }\n\n      .outersmall {\n        flex-direction: column;\n        padding-bottom: 8px;\n      }\n\n      .content {\n        flex-direction: column;\n        flex-basis: 67%;\n        min-width: 280px;\n      }\n\n      .checkbox-content {\n        display: flex;\n        flex-direction: column;\n        color: #3c4043;\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 14px;\n        font-weight: 400;\n        letter-spacing: 0.2px;\n        line-height: 20px;\n      }\n    "
                        ],
                        providers: [{ provide: DynamicFormWidth, useExisting: i0.forwardRef(function () { return IotInputCheckboxComponent; }) }]
                    }]
            }], propDecorators: { _template: [{
                    type: i0.ViewChild,
                    args: [i0.TemplateRef]
                }], leftLabel: [{
                    type: i0.Input
                }], label: [{
                    type: i0.Input
                }], attribute: [{
                    type: i0.Input
                }], description: [{
                    type: i0.Input
                }], value: [{
                    type: i0.Input
                }], separator: [{
                    type: i0.Input
                }], disabled: [{
                    type: i0.Input
                }], inverse: [{
                    type: i0.Input
                }], onChange: [{
                    type: i0.Output
                }] } });

    var IotInputLabelsComponent = /** @class */ (function (_super) {
        __extends(IotInputLabelsComponent, _super);
        function IotInputLabelsComponent() {
            var _this = _super.apply(this, __spreadArray([], __read(arguments))) || this;
            _this.separatorKeysCodes = [keycodes.ENTER, keycodes.COMMA];
            _this.form = new i4$3.FormGroup({
                inputId: new i4$3.FormControl(''),
            });
            _this.attribute = 'todo';
            _this.value = [];
            _this.copyArray = [];
            _this.type = 'text';
            _this.disabled = false;
            _this.onChange = new i0.EventEmitter();
            return _this;
        }
        Object.defineProperty(IotInputLabelsComponent.prototype, "template", {
            get: function () {
                return this._template;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(IotInputLabelsComponent.prototype, "pristine", {
            get: function () {
                return JSON.stringify(this.value) == JSON.stringify(this.copyArray);
                // return this.value.toString() === this.copyArray.toString();
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(IotInputLabelsComponent.prototype, "key", {
            get: function () {
                return this.attribute;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(IotInputLabelsComponent.prototype, "newValue", {
            get: function () {
                return this.copyArray;
            },
            enumerable: false,
            configurable: true
        });
        IotInputLabelsComponent.prototype.remove = function (fruit) {
            var index = this.copyArray.indexOf(fruit);
            if (index >= 0) {
                this.copyArray.splice(index, 1);
            }
            this.onChange.emit(this.copyArray);
        };
        IotInputLabelsComponent.prototype.add = function (event) {
            var value = (event.value || '').trim();
            if (value) {
                this.copyArray.push(value);
            }
            this.form.setValue({ inputId: null });
            this.onChange.emit(this.copyArray);
        };
        IotInputLabelsComponent.prototype.ngOnInit = function () {
            _super.prototype.ngOnInit.call(this);
            this.form = new i4$3.FormGroup({
                inputId: new i4$3.FormControl({ value: null, disabled: this.disabled }),
            });
        };
        IotInputLabelsComponent.prototype.ngOnChanges = function (changes) {
            if (changes.value && !!this.value) {
                this.copyArray = __spreadArray([], __read(this.value));
            }
        };
        return IotInputLabelsComponent;
    }(DynamicFormWidth));
    IotInputLabelsComponent.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotInputLabelsComponent, deps: null, target: i0__namespace.ɵɵFactoryTarget.Component });
    IotInputLabelsComponent.ɵcmp = i0__namespace.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotInputLabelsComponent, selector: "iot-input-labels", inputs: { label: "label", attribute: "attribute", description: "description", value: "value", type: "type", disabled: "disabled" }, outputs: { onChange: "onChange" }, providers: [{ provide: DynamicFormWidth, useExisting: i0.forwardRef(function () { return IotInputLabelsComponent; }) }], viewQueries: [{ propertyName: "_template", first: true, predicate: i0.TemplateRef, descendants: true }, { propertyName: "fruitInput", first: true, predicate: ["labelInput"], descendants: true }], usesInheritance: true, usesOnChanges: true, ngImport: i0__namespace, template: "\n    <ng-template>\n      <div class=\"outer-margin\">\n        <div class=\"outeroneline\" [ngClass]=\"{'outersmall':!isBig}\">\n          <div class=\"label-outer\" [ngClass]=\"{'label-outer-thin':!isBig}\"\n          >\n            <div class=\"label\">{{label | translate}} </div>\n          </div>\n\n          <form [formGroup]=\"form\" class=\"content\">\n            <div\n              *ngIf=\"!!description\"\n              class=\"checkbox-content-description\">{{description | translate}}</div>\n\n            <div class=\"form-label\">\n              <mat-chip-list #chipList aria-label=\"Fruit selection\">\n                <mat-chip\n                  *ngFor=\"let label of copyArray\"\n                  [selectable]=\"true\"\n                  [removable]=\"true\"\n                  (removed)=\"remove(label)\">\n                  {{label}}\n                  <mat-icon matChipRemove>cancel</mat-icon>\n                </mat-chip>\n                <input\n                  class=\"label-input\"\n                  [placeholder]=\"'DEVICE.NEW_LABEL' | translate\"\n                  #labelInput\n                  formControlName=\"inputId\"\n                  [matChipInputFor]=\"chipList\"\n                  [matChipInputSeparatorKeyCodes]=\"separatorKeysCodes\"\n                  (matChipInputTokenEnd)=\"add($event)\">\n              </mat-chip-list>\n            </div>\n            <!--                    <div class=\"form-content\">-->\n\n            <!--                        <input class=\"input-class\" matInput [type]=\"type\"-->\n            <!--                               formControlName=\"inputId\"-->\n            <!--                               (ngModelChange)=\"onChange.emit($event)\"-->\n\n            <!--                               >-->\n            <!--                        <span class=\"border-class\"></span>-->\n            <!--                    </div>-->\n          </form>\n\n\n        </div>\n\n      </div>\n    </ng-template>\n\n  ", isInline: true, styles: ["\n    input.label-input {\n      background: transparent;\n      border-width: 0;\n    }\n\n    .outer-margin {\n      /*margin-left: -5px;*/\n\n      padding-right: 50px;\n    }\n\n    .label-outer.label-outer-thin {\n      min-height: 40px;\n    }\n\n    .checkbox-content-description {\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 12px;\n      font-weight: 400;\n      letter-spacing: .025em;\n      line-height: 16px;\n      color: #5f6368;\n      margin-bottom: 13px;\n      margin-top: 2px;\n    }\n\n    .label-outer {\n      flex-basis: 33%;\n      min-height: 64px;\n      min-width: 220px;\n      display: block;\n      padding-top: 13px;\n      color: #3c4043;\n    }\n\n    .label {\n\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 14px;\n      font-weight: 400;\n      height: 20px;\n      color: rgb(60, 64, 67);\n    }\n\n    .outeroneline {\n      display: flex;\n      max-width: 900px;\n    }\n\n\n    .outersmall {\n      flex-direction: column;\n      padding-bottom: 8px;\n    }\n\n\n    .content {\n      flex-direction: column;\n      flex-basis: 67%;\n      min-width: 280px;\n    }\n\n    .form-content {\n\n      width: 100%;\n      height: 46px;\n      padding: 0 16px;\n      border: 1px solid #dadce0;\n      border-radius: 4px;\n      border-top-left-radius: 4px;\n      border-top-right-radius: 4px;\n      border-top-right-radius: 4px;\n      border-bottom-right-radius: 4px;\n      border-bottom-right-radius: 4px;\n      border-bottom-left-radius: 4px;\n      pointer-events: none;\n    }\n\n\n    .form-content:hover {\n      border: 2px solid black;\n    }\n\n    .input-class {\n      height: 100%;\n      width: 100%;\n      display: flex;\n      border: none !important;\n      background-color: transparent;\n      pointer-events: auto;\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 1rem;\n      font-weight: 400;\n      letter-spacing: .00625em;\n    }\n\n    .input-class:focus {\n      outline: none;\n    }\n  "], components: [{ type: i1__namespace$a.MatChipList, selector: "mat-chip-list", inputs: ["aria-orientation", "multiple", "compareWith", "value", "required", "placeholder", "disabled", "selectable", "tabIndex", "errorStateMatcher"], outputs: ["change", "valueChange"], exportAs: ["matChipList"] }, { type: i2__namespace$1.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }], directives: [{ type: i4__namespace$2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i4__namespace$3.ɵNgNoValidate, selector: "form:not([ngNoForm]):not([ngNativeValidate])" }, { type: i4__namespace$3.NgControlStatusGroup, selector: "[formGroupName],[formArrayName],[ngModelGroup],[formGroup],form:not([ngNoForm]),[ngForm]" }, { type: i4__namespace$3.FormGroupDirective, selector: "[formGroup]", inputs: ["formGroup"], outputs: ["ngSubmit"], exportAs: ["ngForm"] }, { type: i4__namespace$2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i4__namespace$2.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { type: i1__namespace$a.MatChip, selector: "mat-basic-chip, [mat-basic-chip], mat-chip, [mat-chip]", inputs: ["color", "disableRipple", "tabIndex", "selected", "value", "selectable", "disabled", "removable"], outputs: ["selectionChange", "destroyed", "removed"], exportAs: ["matChip"] }, { type: i1__namespace$a.MatChipRemove, selector: "[matChipRemove]" }, { type: i4__namespace$3.DefaultValueAccessor, selector: "input:not([type=checkbox])[formControlName],textarea[formControlName],input:not([type=checkbox])[formControl],textarea[formControl],input:not([type=checkbox])[ngModel],textarea[ngModel],[ngDefaultControl]" }, { type: i1__namespace$a.MatChipInput, selector: "input[matChipInputFor]", inputs: ["matChipInputSeparatorKeyCodes", "placeholder", "id", "matChipInputFor", "matChipInputAddOnBlur", "disabled"], outputs: ["matChipInputTokenEnd"], exportAs: ["matChipInput", "matChipInputFor"] }, { type: i4__namespace$3.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { type: i4__namespace$3.FormControlName, selector: "[formControlName]", inputs: ["disabled", "formControlName", "ngModel"], outputs: ["ngModelChange"] }], pipes: { "translate": i5__namespace.TranslatePipe } });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotInputLabelsComponent, decorators: [{
                type: i0.Component,
                args: [{
                        selector: 'iot-input-labels',
                        template: "\n    <ng-template>\n      <div class=\"outer-margin\">\n        <div class=\"outeroneline\" [ngClass]=\"{'outersmall':!isBig}\">\n          <div class=\"label-outer\" [ngClass]=\"{'label-outer-thin':!isBig}\"\n          >\n            <div class=\"label\">{{label | translate}} </div>\n          </div>\n\n          <form [formGroup]=\"form\" class=\"content\">\n            <div\n              *ngIf=\"!!description\"\n              class=\"checkbox-content-description\">{{description | translate}}</div>\n\n            <div class=\"form-label\">\n              <mat-chip-list #chipList aria-label=\"Fruit selection\">\n                <mat-chip\n                  *ngFor=\"let label of copyArray\"\n                  [selectable]=\"true\"\n                  [removable]=\"true\"\n                  (removed)=\"remove(label)\">\n                  {{label}}\n                  <mat-icon matChipRemove>cancel</mat-icon>\n                </mat-chip>\n                <input\n                  class=\"label-input\"\n                  [placeholder]=\"'DEVICE.NEW_LABEL' | translate\"\n                  #labelInput\n                  formControlName=\"inputId\"\n                  [matChipInputFor]=\"chipList\"\n                  [matChipInputSeparatorKeyCodes]=\"separatorKeysCodes\"\n                  (matChipInputTokenEnd)=\"add($event)\">\n              </mat-chip-list>\n            </div>\n            <!--                    <div class=\"form-content\">-->\n\n            <!--                        <input class=\"input-class\" matInput [type]=\"type\"-->\n            <!--                               formControlName=\"inputId\"-->\n            <!--                               (ngModelChange)=\"onChange.emit($event)\"-->\n\n            <!--                               >-->\n            <!--                        <span class=\"border-class\"></span>-->\n            <!--                    </div>-->\n          </form>\n\n\n        </div>\n\n      </div>\n    </ng-template>\n\n  ",
                        styles: ["\n    input.label-input {\n      background: transparent;\n      border-width: 0;\n    }\n\n    .outer-margin {\n      /*margin-left: -5px;*/\n\n      padding-right: 50px;\n    }\n\n    .label-outer.label-outer-thin {\n      min-height: 40px;\n    }\n\n    .checkbox-content-description {\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 12px;\n      font-weight: 400;\n      letter-spacing: .025em;\n      line-height: 16px;\n      color: #5f6368;\n      margin-bottom: 13px;\n      margin-top: 2px;\n    }\n\n    .label-outer {\n      flex-basis: 33%;\n      min-height: 64px;\n      min-width: 220px;\n      display: block;\n      padding-top: 13px;\n      color: #3c4043;\n    }\n\n    .label {\n\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 14px;\n      font-weight: 400;\n      height: 20px;\n      color: rgb(60, 64, 67);\n    }\n\n    .outeroneline {\n      display: flex;\n      max-width: 900px;\n    }\n\n\n    .outersmall {\n      flex-direction: column;\n      padding-bottom: 8px;\n    }\n\n\n    .content {\n      flex-direction: column;\n      flex-basis: 67%;\n      min-width: 280px;\n    }\n\n    .form-content {\n\n      width: 100%;\n      height: 46px;\n      padding: 0 16px;\n      border: 1px solid #dadce0;\n      border-radius: 4px;\n      border-top-left-radius: 4px;\n      border-top-right-radius: 4px;\n      border-top-right-radius: 4px;\n      border-bottom-right-radius: 4px;\n      border-bottom-right-radius: 4px;\n      border-bottom-left-radius: 4px;\n      pointer-events: none;\n    }\n\n\n    .form-content:hover {\n      border: 2px solid black;\n    }\n\n    .input-class {\n      height: 100%;\n      width: 100%;\n      display: flex;\n      border: none !important;\n      background-color: transparent;\n      pointer-events: auto;\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 1rem;\n      font-weight: 400;\n      letter-spacing: .00625em;\n    }\n\n    .input-class:focus {\n      outline: none;\n    }\n  "],
                        providers: [{ provide: DynamicFormWidth, useExisting: i0.forwardRef(function () { return IotInputLabelsComponent; }) }]
                    }]
            }], propDecorators: { _template: [{
                    type: i0.ViewChild,
                    args: [i0.TemplateRef]
                }], fruitInput: [{
                    type: i0.ViewChild,
                    args: ['labelInput']
                }], label: [{
                    type: i0.Input
                }], attribute: [{
                    type: i0.Input
                }], description: [{
                    type: i0.Input
                }], value: [{
                    type: i0.Input
                }], type: [{
                    type: i0.Input
                }], disabled: [{
                    type: i0.Input
                }], onChange: [{
                    type: i0.Output
                }] } });

    var IotUiLibModule = /** @class */ (function () {
        function IotUiLibModule() {
        }
        return IotUiLibModule;
    }());
    IotUiLibModule.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotUiLibModule, deps: [], target: i0__namespace.ɵɵFactoryTarget.NgModule });
    IotUiLibModule.ɵmod = i0__namespace.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotUiLibModule, declarations: [SideNavBarComponent,
            TopNavBarComponent,
            AccountMenuDisplayButtonComponent,
            LanguageSelectorComponent,
            CustomerSelectorComponent,
            CustomerSelectorButtonComponent,
            SideNavBarComponent,
            SingleListEntryComponent,
            CompositeListEntryComponent,
            CustomerSelectModalComponent,
            DashboardGridComponent,
            DashboardCardComponent,
            DashboardCard,
            IotCardFooter, IotCardHeader, IotCardContent,
            DashboardMediaQueryComponent,
            TimeOfDayCardComponent,
            KeyValueCardComponent,
            PageHeaderComponent,
            IotFormComponent,
            IotInputTextComponent,
            IotInputHorizontalLineComponent,
            IotInputTextareaComponent,
            IotInputSelectValueComponent,
            IotInputPlaceholderComponent,
            IotDatePickerComponent,
            IotDefineIdentifierComponent,
            IotInputLocationComponent,
            IotInputCheckboxComponent,
            IotInputLabelsComponent,
            DynamicFormWidth], imports: [i4$2.CommonModule,
            i1.MatButtonModule,
            i2$1.MatIconModule,
            i4.MatMenuModule,
            i3.MatTooltipModule,
            i2.MatCardModule,
            i2$2.MatSidenavModule,
            i1$2.RouterModule,
            i1$1.MatListModule,
            i1$4.MatDialogModule, i5__namespace.TranslateModule, i1$3.MatGridListModule,
            i2$3.MatTableModule,
            i2$4.MatProgressSpinnerModule,
            i4$3.FormsModule,
            i4$3.ReactiveFormsModule,
            i1$6.MatSelectModule,
            i2$5.MatDatepickerModule,
            i3$1.MatInputModule,
            i1$8.AgmCoreModule,
            i1$9.MatCheckboxModule,
            i1$a.MatChipsModule], exports: [SideNavBarComponent,
            TopNavBarComponent,
            AccountMenuDisplayButtonComponent,
            LanguageSelectorComponent,
            CustomerSelectorComponent,
            CustomerSelectorButtonComponent,
            DashboardGridComponent,
            DashboardCard,
            IotCardFooter, IotCardHeader, IotCardContent,
            DashboardCardComponent,
            TimeOfDayCardComponent,
            KeyValueCardComponent,
            PageHeaderComponent,
            IotFormComponent,
            IotInputTextComponent,
            IotInputHorizontalLineComponent,
            IotInputTextareaComponent,
            IotInputSelectValueComponent,
            IotInputPlaceholderComponent,
            IotDefineIdentifierComponent,
            IotDatePickerComponent,
            IotInputLocationComponent,
            IotInputCheckboxComponent,
            IotInputLabelsComponent,
            DynamicFormWidth] });
    IotUiLibModule.ɵinj = i0__namespace.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotUiLibModule, imports: [[
                i4$2.CommonModule,
                i1.MatButtonModule,
                i2$1.MatIconModule,
                i4.MatMenuModule,
                i3.MatTooltipModule,
                i2.MatCardModule,
                i2$2.MatSidenavModule,
                i1$2.RouterModule,
                i1$1.MatListModule,
                i1$4.MatDialogModule,
                i5.TranslateModule.forRoot(),
                i1$3.MatGridListModule,
                i2$3.MatTableModule,
                i2$4.MatProgressSpinnerModule,
                i4$3.FormsModule,
                i4$3.ReactiveFormsModule,
                i1$6.MatSelectModule,
                i2$5.MatDatepickerModule,
                i3$1.MatInputModule,
                i1$8.AgmCoreModule,
                i1$9.MatCheckboxModule,
                i1$a.MatChipsModule
            ]] });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0__namespace, type: IotUiLibModule, decorators: [{
                type: i0.NgModule,
                args: [{
                        declarations: [
                            SideNavBarComponent,
                            TopNavBarComponent,
                            AccountMenuDisplayButtonComponent,
                            LanguageSelectorComponent,
                            CustomerSelectorComponent,
                            CustomerSelectorButtonComponent,
                            SideNavBarComponent,
                            SingleListEntryComponent,
                            CompositeListEntryComponent,
                            CustomerSelectModalComponent,
                            DashboardGridComponent,
                            DashboardCardComponent,
                            DashboardCard,
                            IotCardFooter, IotCardHeader, IotCardContent,
                            DashboardMediaQueryComponent,
                            TimeOfDayCardComponent,
                            KeyValueCardComponent,
                            PageHeaderComponent,
                            IotFormComponent,
                            IotInputTextComponent,
                            IotInputHorizontalLineComponent,
                            IotInputTextareaComponent,
                            IotInputSelectValueComponent,
                            IotInputPlaceholderComponent,
                            IotDatePickerComponent,
                            IotDefineIdentifierComponent,
                            IotInputLocationComponent,
                            IotInputCheckboxComponent,
                            IotInputLabelsComponent,
                            DynamicFormWidth
                        ],
                        exports: [
                            SideNavBarComponent,
                            TopNavBarComponent,
                            AccountMenuDisplayButtonComponent,
                            LanguageSelectorComponent,
                            CustomerSelectorComponent,
                            CustomerSelectorButtonComponent,
                            DashboardGridComponent,
                            DashboardCard,
                            IotCardFooter, IotCardHeader, IotCardContent,
                            DashboardCardComponent,
                            TimeOfDayCardComponent,
                            KeyValueCardComponent,
                            PageHeaderComponent,
                            IotFormComponent,
                            IotInputTextComponent,
                            IotInputHorizontalLineComponent,
                            IotInputTextareaComponent,
                            IotInputSelectValueComponent,
                            IotInputPlaceholderComponent,
                            IotDefineIdentifierComponent,
                            IotDatePickerComponent,
                            IotInputLocationComponent,
                            IotInputCheckboxComponent,
                            IotInputLabelsComponent,
                            DynamicFormWidth
                        ],
                        imports: [
                            i4$2.CommonModule,
                            i1.MatButtonModule,
                            i2$1.MatIconModule,
                            i4.MatMenuModule,
                            i3.MatTooltipModule,
                            i2.MatCardModule,
                            i2$2.MatSidenavModule,
                            i1$2.RouterModule,
                            i1$1.MatListModule,
                            i1$4.MatDialogModule,
                            i5.TranslateModule.forRoot(),
                            i1$3.MatGridListModule,
                            i2$3.MatTableModule,
                            i2$4.MatProgressSpinnerModule,
                            i4$3.FormsModule,
                            i4$3.ReactiveFormsModule,
                            i1$6.MatSelectModule,
                            i2$5.MatDatepickerModule,
                            i3$1.MatInputModule,
                            i1$8.AgmCoreModule,
                            i1$9.MatCheckboxModule,
                            i1$a.MatChipsModule
                        ]
                    }]
            }] });

    /*
     * Public API Surface of iot-ui-lib
     */

    /**
     * Generated bundle index. Do not edit.
     */

    exports.AccountMenuDisplayButtonComponent = AccountMenuDisplayButtonComponent;
    exports.CustomerSelectorButtonComponent = CustomerSelectorButtonComponent;
    exports.CustomerSelectorComponent = CustomerSelectorComponent;
    exports.DashboardCard = DashboardCard;
    exports.DashboardCardComponent = DashboardCardComponent;
    exports.DashboardGridComponent = DashboardGridComponent;
    exports.DynamicFormWidth = DynamicFormWidth;
    exports.IotCardContent = IotCardContent;
    exports.IotCardFooter = IotCardFooter;
    exports.IotCardHeader = IotCardHeader;
    exports.IotDatePickerComponent = IotDatePickerComponent;
    exports.IotDefineIdentifierComponent = IotDefineIdentifierComponent;
    exports.IotFormComponent = IotFormComponent;
    exports.IotInputCheckboxComponent = IotInputCheckboxComponent;
    exports.IotInputHorizontalLineComponent = IotInputHorizontalLineComponent;
    exports.IotInputLabelsComponent = IotInputLabelsComponent;
    exports.IotInputLocationComponent = IotInputLocationComponent;
    exports.IotInputPlaceholderComponent = IotInputPlaceholderComponent;
    exports.IotInputSelectValueComponent = IotInputSelectValueComponent;
    exports.IotInputTextComponent = IotInputTextComponent;
    exports.IotInputTextareaComponent = IotInputTextareaComponent;
    exports.IotUiLibModule = IotUiLibModule;
    exports.IotUiLibService = IotUiLibService;
    exports.KeyValueCardComponent = KeyValueCardComponent;
    exports.LanguageSelectorComponent = LanguageSelectorComponent;
    exports.PageHeaderComponent = PageHeaderComponent;
    exports.SideNavBarComponent = SideNavBarComponent;
    exports.TimeOfDayCardComponent = TimeOfDayCardComponent;
    exports.TopNavBarComponent = TopNavBarComponent;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=iot-ui-lib.umd.js.map
