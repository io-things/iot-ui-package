import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/dialog";
import * as i2 from "@angular/material/button";
import * as i3 from "@angular/material/icon";
import * as i4 from "@angular/common";
export class CustomerSelectModalComponent {
    constructor(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.customerlist = data.customers;
    }
    onNoClick() {
        this.dialogRef.close();
    }
    ngOnInit() {
    }
}
CustomerSelectModalComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: CustomerSelectModalComponent, deps: [{ token: i1.MatDialogRef }, { token: MAT_DIALOG_DATA }], target: i0.ɵɵFactoryTarget.Component });
CustomerSelectModalComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: CustomerSelectModalComponent, selector: "iot-customer-select-modal", ngImport: i0, template: `
    <div class="cust-header">
      <h1 mat-dialog-title>{{'DISTRIBUTOR.SELECT_CUST' }}</h1>
      <div class="cust-button-control" >
        <button
          *ngIf="!data['mobile'] "
          mat-button

        >
          <mat-icon>create_new_folder</mat-icon>
          {{'NEW_DIST' }}</button>
        <button
          *ngIf="!data['mobile']"
          mat-button
        >

          <mat-icon>create_new_folder</mat-icon>

          {{'NEW_CUST' }}</button>


      </div>


    </div>
    {{customerlist|async|json}}
    <div mat-dialog-actions class="cust-actions">
      <button mat-button (click)="onNoClick()">{{'CONFIG.CANCEL'}}</button>
      <button mat-button cdkFocusInitial

      >{{'CONFIG.OPEN'}}</button>
    </div>
  `, isInline: true, styles: ["\n    .cust-header {\n      padding-top: 16px;\n      display: flex;\n      flex: 1;\n      justify-content: space-between;\n      margin-top: -8px;\n      min-height: 25px;\n    }\n\n    tbody a:hover.cust-name-link {\n      border-bottom-color: #3367d6;\n      color: #3367d6;\n    }\n\n    .cust-button-control {\n      margin-top: -15px;\n      display: flex;\n      flex-wrap: wrap;\n      flex-direction: row-reverse;\n      margin-left: 15px;\n    }\n\n    .cust-actions {\n      text-transform: capitalize;\n    }\n  "], components: [{ type: i2.MatButton, selector: "button[mat-button], button[mat-raised-button], button[mat-icon-button],             button[mat-fab], button[mat-mini-fab], button[mat-stroked-button],             button[mat-flat-button]", inputs: ["disabled", "disableRipple", "color"], exportAs: ["matButton"] }, { type: i3.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }], directives: [{ type: i1.MatDialogTitle, selector: "[mat-dialog-title], [matDialogTitle]", inputs: ["id"], exportAs: ["matDialogTitle"] }, { type: i4.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i1.MatDialogActions, selector: "[mat-dialog-actions], mat-dialog-actions, [matDialogActions]" }], pipes: { "json": i4.JsonPipe, "async": i4.AsyncPipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: CustomerSelectModalComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-customer-select-modal',
                    template: `
    <div class="cust-header">
      <h1 mat-dialog-title>{{'DISTRIBUTOR.SELECT_CUST' }}</h1>
      <div class="cust-button-control" >
        <button
          *ngIf="!data['mobile'] "
          mat-button

        >
          <mat-icon>create_new_folder</mat-icon>
          {{'NEW_DIST' }}</button>
        <button
          *ngIf="!data['mobile']"
          mat-button
        >

          <mat-icon>create_new_folder</mat-icon>

          {{'NEW_CUST' }}</button>


      </div>


    </div>
    {{customerlist|async|json}}
    <div mat-dialog-actions class="cust-actions">
      <button mat-button (click)="onNoClick()">{{'CONFIG.CANCEL'}}</button>
      <button mat-button cdkFocusInitial

      >{{'CONFIG.OPEN'}}</button>
    </div>
  `,
                    styles: [`
    .cust-header {
      padding-top: 16px;
      display: flex;
      flex: 1;
      justify-content: space-between;
      margin-top: -8px;
      min-height: 25px;
    }

    tbody a:hover.cust-name-link {
      border-bottom-color: #3367d6;
      color: #3367d6;
    }

    .cust-button-control {
      margin-top: -15px;
      display: flex;
      flex-wrap: wrap;
      flex-direction: row-reverse;
      margin-left: 15px;
    }

    .cust-actions {
      text-transform: capitalize;
    }
  `
                    ]
                }]
        }], ctorParameters: function () { return [{ type: i1.MatDialogRef }, { type: undefined, decorators: [{
                    type: Inject,
                    args: [MAT_DIALOG_DATA]
                }] }]; } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VzdG9tZXItc2VsZWN0LW1vZGFsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2lvdC11aS1saWIvc3JjL2xpYi9tb2RhbC9jdXN0b21lci1zZWxlY3QtbW9kYWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUUsTUFBTSxFQUF1QyxNQUFNLGVBQWUsQ0FBQztBQUN0RixPQUFPLEVBQUMsZUFBZSxFQUFlLE1BQU0sMEJBQTBCLENBQUM7Ozs7OztBQWtFdkUsTUFBTSxPQUFPLDRCQUE0QjtJQUl2QyxZQUNTLFNBQXFELEVBQzVCLElBQVM7UUFEbEMsY0FBUyxHQUFULFNBQVMsQ0FBNEM7UUFDNUIsU0FBSSxHQUFKLElBQUksQ0FBSztRQUN6QyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUE7SUFDcEMsQ0FBQztJQUVELFNBQVM7UUFDUCxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ3pCLENBQUM7SUFFRCxRQUFRO0lBQ1IsQ0FBQzs7eUhBZlUsNEJBQTRCLDhDQU03QixlQUFlOzZHQU5kLDRCQUE0QixpRUE5RDdCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQWdDVDsyRkE4QlUsNEJBQTRCO2tCQWhFeEMsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsMkJBQTJCO29CQUNyQyxRQUFRLEVBQUU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0dBZ0NUO29CQUNELE1BQU0sRUFBRSxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQTBCUjtxQkFDQTtpQkFDRjs7MEJBT0ksTUFBTTsyQkFBQyxlQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIEluamVjdCwgT25EZXN0cm95LCBPbkluaXQsIFZpZXdFbmNhcHN1bGF0aW9ufSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7TUFUX0RJQUxPR19EQVRBLCBNYXREaWFsb2dSZWZ9IGZyb20gXCJAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2dcIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnaW90LWN1c3RvbWVyLXNlbGVjdC1tb2RhbCcsXG4gIHRlbXBsYXRlOiBgXG4gICAgPGRpdiBjbGFzcz1cImN1c3QtaGVhZGVyXCI+XG4gICAgICA8aDEgbWF0LWRpYWxvZy10aXRsZT57eydESVNUUklCVVRPUi5TRUxFQ1RfQ1VTVCcgfX08L2gxPlxuICAgICAgPGRpdiBjbGFzcz1cImN1c3QtYnV0dG9uLWNvbnRyb2xcIiA+XG4gICAgICAgIDxidXR0b25cbiAgICAgICAgICAqbmdJZj1cIiFkYXRhWydtb2JpbGUnXSBcIlxuICAgICAgICAgIG1hdC1idXR0b25cblxuICAgICAgICA+XG4gICAgICAgICAgPG1hdC1pY29uPmNyZWF0ZV9uZXdfZm9sZGVyPC9tYXQtaWNvbj5cbiAgICAgICAgICB7eydORVdfRElTVCcgfX08L2J1dHRvbj5cbiAgICAgICAgPGJ1dHRvblxuICAgICAgICAgICpuZ0lmPVwiIWRhdGFbJ21vYmlsZSddXCJcbiAgICAgICAgICBtYXQtYnV0dG9uXG4gICAgICAgID5cblxuICAgICAgICAgIDxtYXQtaWNvbj5jcmVhdGVfbmV3X2ZvbGRlcjwvbWF0LWljb24+XG5cbiAgICAgICAgICB7eydORVdfQ1VTVCcgfX08L2J1dHRvbj5cblxuXG4gICAgICA8L2Rpdj5cblxuXG4gICAgPC9kaXY+XG4gICAge3tjdXN0b21lcmxpc3R8YXN5bmN8anNvbn19XG4gICAgPGRpdiBtYXQtZGlhbG9nLWFjdGlvbnMgY2xhc3M9XCJjdXN0LWFjdGlvbnNcIj5cbiAgICAgIDxidXR0b24gbWF0LWJ1dHRvbiAoY2xpY2spPVwib25Ob0NsaWNrKClcIj57eydDT05GSUcuQ0FOQ0VMJ319PC9idXR0b24+XG4gICAgICA8YnV0dG9uIG1hdC1idXR0b24gY2RrRm9jdXNJbml0aWFsXG5cbiAgICAgID57eydDT05GSUcuT1BFTid9fTwvYnV0dG9uPlxuICAgIDwvZGl2PlxuICBgLFxuICBzdHlsZXM6IFtgXG4gICAgLmN1c3QtaGVhZGVyIHtcbiAgICAgIHBhZGRpbmctdG9wOiAxNnB4O1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGZsZXg6IDE7XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICBtYXJnaW4tdG9wOiAtOHB4O1xuICAgICAgbWluLWhlaWdodDogMjVweDtcbiAgICB9XG5cbiAgICB0Ym9keSBhOmhvdmVyLmN1c3QtbmFtZS1saW5rIHtcbiAgICAgIGJvcmRlci1ib3R0b20tY29sb3I6ICMzMzY3ZDY7XG4gICAgICBjb2xvcjogIzMzNjdkNjtcbiAgICB9XG5cbiAgICAuY3VzdC1idXR0b24tY29udHJvbCB7XG4gICAgICBtYXJnaW4tdG9wOiAtMTVweDtcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICBmbGV4LXdyYXA6IHdyYXA7XG4gICAgICBmbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XG4gICAgICBtYXJnaW4tbGVmdDogMTVweDtcbiAgICB9XG5cbiAgICAuY3VzdC1hY3Rpb25zIHtcbiAgICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICAgIH1cbiAgYFxuICBdXG59KVxuZXhwb3J0IGNsYXNzIEN1c3RvbWVyU2VsZWN0TW9kYWxDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIHB1YmxpYyBjdXN0b21lcmxpc3Q7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHVibGljIGRpYWxvZ1JlZjogTWF0RGlhbG9nUmVmPEN1c3RvbWVyU2VsZWN0TW9kYWxDb21wb25lbnQ+LFxuICAgIEBJbmplY3QoTUFUX0RJQUxPR19EQVRBKSBwdWJsaWMgZGF0YTogYW55KSB7XG4gICAgdGhpcy5jdXN0b21lcmxpc3QgPSBkYXRhLmN1c3RvbWVyc1xuICB9XG5cbiAgb25Ob0NsaWNrKCk6IHZvaWQge1xuICAgIHRoaXMuZGlhbG9nUmVmLmNsb3NlKCk7XG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgfVxuXG59XG4iXX0=