import { Component, Input } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/menu";
import * as i2 from "@angular/material/card";
import * as i3 from "@angular/material/tooltip";
export class AccountMenuDisplayButtonComponent {
    constructor() {
        this.avatar = '--';
    }
    ngOnInit() {
    }
    ngOnChanges(changes) {
        if (changes.user) {
            this.avatar = '';
            (this.user.displayName || '-').split(' ')
                .filter(w => w != '').forEach(word => this.avatar += word[0]);
        }
    }
}
AccountMenuDisplayButtonComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: AccountMenuDisplayButtonComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
AccountMenuDisplayButtonComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: AccountMenuDisplayButtonComponent, selector: "iot-account-menu-display-button", inputs: { user: "user" }, usesOnChanges: true, ngImport: i0, template: `
<!--    <button mat-button [matMenuTriggerFor]="aboveMenu">Above</button>-->
<!--    <mat-menu #aboveMenu="matMenu" yPosition="above">-->
<!--      <button mat-menu-item>Item 1</button>-->
<!--      <button mat-menu-item>Item 2</button>-->
<!--    </mat-menu>-->
<!--    -->
    <a class="outer" [matMenuTriggerFor]="accountMenu" matTooltip="Account">

      <div class="avatar">
          {{avatar}}
      </div>
      <mat-menu #accountMenu="matMenu" yPosition="below" xPosition="before" >
        <mat-card  class="mat-elevation-z0">
          <mat-card-header>
            <div class="avatar" mat-card-avatar>
              {{avatar}}
            </div>

            <mat-card-title> {{user.displayName}}</mat-card-title>
            <mat-card-subtitle>{{user.email}}</mat-card-subtitle>
          </mat-card-header>

          <mat-card-actions>
            <ng-content select="[button1]"></ng-content>
            <ng-content select="[button2]"></ng-content>
<!--            <button mat-button >{{'HOME.SIGNOUT'}}</button>-->

          </mat-card-actions>
        </mat-card>
      </mat-menu>
    </a>

  `, isInline: true, styles: ["\n      .outer {\n        position: relative;\n      }\n\n      .avatar {\n        border-radius: 50%;\n        display: flex;\n        justify-content: center;\n        align-items: center;\n        color: rgba(0, 0, 0, 0.3);\n\n        right: 0px;\n        width: 40px;\n        height: 40px;\n        background: #E4E9EB 0% 0% no-repeat padding-box;\n        opacity: 1;\n      }\n    "], components: [{ type: i1.MatMenu, selector: "mat-menu", exportAs: ["matMenu"] }, { type: i2.MatCard, selector: "mat-card", exportAs: ["matCard"] }, { type: i2.MatCardHeader, selector: "mat-card-header" }], directives: [{ type: i3.MatTooltip, selector: "[matTooltip]", exportAs: ["matTooltip"] }, { type: i1.MatMenuTrigger, selector: "[mat-menu-trigger-for], [matMenuTriggerFor]", exportAs: ["matMenuTrigger"] }, { type: i2.MatCardAvatar, selector: "[mat-card-avatar], [matCardAvatar]" }, { type: i2.MatCardTitle, selector: "mat-card-title, [mat-card-title], [matCardTitle]" }, { type: i2.MatCardSubtitle, selector: "mat-card-subtitle, [mat-card-subtitle], [matCardSubtitle]" }, { type: i2.MatCardActions, selector: "mat-card-actions", inputs: ["align"], exportAs: ["matCardActions"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: AccountMenuDisplayButtonComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-account-menu-display-button',
                    template: `
<!--    <button mat-button [matMenuTriggerFor]="aboveMenu">Above</button>-->
<!--    <mat-menu #aboveMenu="matMenu" yPosition="above">-->
<!--      <button mat-menu-item>Item 1</button>-->
<!--      <button mat-menu-item>Item 2</button>-->
<!--    </mat-menu>-->
<!--    -->
    <a class="outer" [matMenuTriggerFor]="accountMenu" matTooltip="Account">

      <div class="avatar">
          {{avatar}}
      </div>
      <mat-menu #accountMenu="matMenu" yPosition="below" xPosition="before" >
        <mat-card  class="mat-elevation-z0">
          <mat-card-header>
            <div class="avatar" mat-card-avatar>
              {{avatar}}
            </div>

            <mat-card-title> {{user.displayName}}</mat-card-title>
            <mat-card-subtitle>{{user.email}}</mat-card-subtitle>
          </mat-card-header>

          <mat-card-actions>
            <ng-content select="[button1]"></ng-content>
            <ng-content select="[button2]"></ng-content>
<!--            <button mat-button >{{'HOME.SIGNOUT'}}</button>-->

          </mat-card-actions>
        </mat-card>
      </mat-menu>
    </a>

  `,
                    styles: [
                        `
      .outer {
        position: relative;
      }

      .avatar {
        border-radius: 50%;
        display: flex;
        justify-content: center;
        align-items: center;
        color: rgba(0, 0, 0, 0.3);

        right: 0px;
        width: 40px;
        height: 40px;
        background: #E4E9EB 0% 0% no-repeat padding-box;
        opacity: 1;
      }
    `
                    ]
                }]
        }], ctorParameters: function () { return []; }, propDecorators: { user: [{
                type: Input
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3VudC1tZW51LWRpc3BsYXktYnV0dG9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2lvdC11aS1saWIvc3JjL2xpYi9hY2NvdW50L2FjY291bnQtbWVudS1kaXNwbGF5LWJ1dHRvbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFDLFNBQVMsRUFBRSxLQUFLLEVBQW1DLE1BQU0sZUFBZSxDQUFDOzs7OztBQTREakYsTUFBTSxPQUFPLGlDQUFpQztJQUs1QztRQUZBLFdBQU0sR0FBRSxJQUFJLENBQUM7SUFFRyxDQUFDO0lBRWpCLFFBQVE7SUFDUixDQUFDO0lBRUQsV0FBVyxDQUFDLE9BQXNCO1FBQ2hDLElBQUksT0FBTyxDQUFDLElBQUksRUFBRTtZQUNoQixJQUFJLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQztZQUNqQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxJQUFJLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7aUJBQ3RDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO1NBQ2hFO0lBQ0gsQ0FBQzs7OEhBaEJVLGlDQUFpQztrSEFBakMsaUNBQWlDLHNIQXhEbEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQWlDVDsyRkF1QlUsaUNBQWlDO2tCQTFEN0MsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsaUNBQWlDO29CQUMzQyxRQUFRLEVBQUU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQWlDVDtvQkFDRCxNQUFNLEVBQUU7d0JBQ047Ozs7Ozs7Ozs7Ozs7Ozs7OztLQWtCQztxQkFDRjtpQkFDRjswRUFHVSxJQUFJO3NCQUFaLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXQsIE9uQ2hhbmdlcywgT25Jbml0LCBTaW1wbGVDaGFuZ2VzfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnaW90LWFjY291bnQtbWVudS1kaXNwbGF5LWJ1dHRvbicsXG4gIHRlbXBsYXRlOiBgXG48IS0tICAgIDxidXR0b24gbWF0LWJ1dHRvbiBbbWF0TWVudVRyaWdnZXJGb3JdPVwiYWJvdmVNZW51XCI+QWJvdmU8L2J1dHRvbj4tLT5cbjwhLS0gICAgPG1hdC1tZW51ICNhYm92ZU1lbnU9XCJtYXRNZW51XCIgeVBvc2l0aW9uPVwiYWJvdmVcIj4tLT5cbjwhLS0gICAgICA8YnV0dG9uIG1hdC1tZW51LWl0ZW0+SXRlbSAxPC9idXR0b24+LS0+XG48IS0tICAgICAgPGJ1dHRvbiBtYXQtbWVudS1pdGVtPkl0ZW0gMjwvYnV0dG9uPi0tPlxuPCEtLSAgICA8L21hdC1tZW51Pi0tPlxuPCEtLSAgICAtLT5cbiAgICA8YSBjbGFzcz1cIm91dGVyXCIgW21hdE1lbnVUcmlnZ2VyRm9yXT1cImFjY291bnRNZW51XCIgbWF0VG9vbHRpcD1cIkFjY291bnRcIj5cblxuICAgICAgPGRpdiBjbGFzcz1cImF2YXRhclwiPlxuICAgICAgICAgIHt7YXZhdGFyfX1cbiAgICAgIDwvZGl2PlxuICAgICAgPG1hdC1tZW51ICNhY2NvdW50TWVudT1cIm1hdE1lbnVcIiB5UG9zaXRpb249XCJiZWxvd1wiIHhQb3NpdGlvbj1cImJlZm9yZVwiID5cbiAgICAgICAgPG1hdC1jYXJkICBjbGFzcz1cIm1hdC1lbGV2YXRpb24tejBcIj5cbiAgICAgICAgICA8bWF0LWNhcmQtaGVhZGVyPlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImF2YXRhclwiIG1hdC1jYXJkLWF2YXRhcj5cbiAgICAgICAgICAgICAge3thdmF0YXJ9fVxuICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgIDxtYXQtY2FyZC10aXRsZT4ge3t1c2VyLmRpc3BsYXlOYW1lfX08L21hdC1jYXJkLXRpdGxlPlxuICAgICAgICAgICAgPG1hdC1jYXJkLXN1YnRpdGxlPnt7dXNlci5lbWFpbH19PC9tYXQtY2FyZC1zdWJ0aXRsZT5cbiAgICAgICAgICA8L21hdC1jYXJkLWhlYWRlcj5cblxuICAgICAgICAgIDxtYXQtY2FyZC1hY3Rpb25zPlxuICAgICAgICAgICAgPG5nLWNvbnRlbnQgc2VsZWN0PVwiW2J1dHRvbjFdXCI+PC9uZy1jb250ZW50PlxuICAgICAgICAgICAgPG5nLWNvbnRlbnQgc2VsZWN0PVwiW2J1dHRvbjJdXCI+PC9uZy1jb250ZW50PlxuPCEtLSAgICAgICAgICAgIDxidXR0b24gbWF0LWJ1dHRvbiA+e3snSE9NRS5TSUdOT1VUJ319PC9idXR0b24+LS0+XG5cbiAgICAgICAgICA8L21hdC1jYXJkLWFjdGlvbnM+XG4gICAgICAgIDwvbWF0LWNhcmQ+XG4gICAgICA8L21hdC1tZW51PlxuICAgIDwvYT5cblxuICBgLFxuICBzdHlsZXM6IFtcbiAgICBgXG4gICAgICAub3V0ZXIge1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICB9XG5cbiAgICAgIC5hdmF0YXIge1xuICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBjb2xvcjogcmdiYSgwLCAwLCAwLCAwLjMpO1xuXG4gICAgICAgIHJpZ2h0OiAwcHg7XG4gICAgICAgIHdpZHRoOiA0MHB4O1xuICAgICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgICAgIGJhY2tncm91bmQ6ICNFNEU5RUIgMCUgMCUgbm8tcmVwZWF0IHBhZGRpbmctYm94O1xuICAgICAgICBvcGFjaXR5OiAxO1xuICAgICAgfVxuICAgIGBcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBBY2NvdW50TWVudURpc3BsYXlCdXR0b25Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsT25DaGFuZ2VzIHtcblxuICBASW5wdXQoKSB1c2VyOiB7ZGlzcGxheU5hbWU6IHN0cmluZzsgZW1haWw6c3RyaW5nfTtcbiAgYXZhdGFyPSAnLS0nO1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gIH1cblxuICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKTogdm9pZCB7XG4gICAgaWYgKGNoYW5nZXMudXNlcikge1xuICAgICAgdGhpcy5hdmF0YXIgPSAnJztcbiAgICAgICh0aGlzLnVzZXIuZGlzcGxheU5hbWUgfHwgJy0nKS5zcGxpdCgnICcpXG4gICAgICAgIC5maWx0ZXIodyA9PiB3ICE9ICcnKS5mb3JFYWNoKHdvcmQgPT4gdGhpcy5hdmF0YXIgKz0gd29yZFswXSlcbiAgICB9XG4gIH1cblxufVxuIl19