/*
 * Copyright (C) Io-Things, BV - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Stefaan Ternier <stefaan@io-things.eu>, 1/6/2020
 */
import { Component, ContentChild, Directive, Input, TemplateRef, ViewEncapsulation } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
import * as i2 from "@ngx-translate/core";
export class IotCardHeader {
    constructor(elRef, renderer) {
        this.elRef = elRef;
        this.renderer = renderer;
        this.padding = false;
        this.renderer.addClass(this.elRef.nativeElement, 'box-sizing');
        this.renderer.setStyle(this.elRef.nativeElement, 'height', `56px`);
    }
    ngOnInit() {
        if (this.padding) {
            this.renderer.setStyle(this.elRef.nativeElement, 'padding', `16px 16px 0 16px`);
        }
    }
}
IotCardHeader.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotCardHeader, deps: [{ token: i0.ElementRef }, { token: i0.Renderer2 }], target: i0.ɵɵFactoryTarget.Directive });
IotCardHeader.ɵdir = i0.ɵɵngDeclareDirective({ minVersion: "12.0.0", version: "12.2.6", type: IotCardHeader, selector: "[iot-card-header]", inputs: { padding: "padding" }, ngImport: i0 });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotCardHeader, decorators: [{
            type: Directive,
            args: [{ selector: '[iot-card-header]' }]
        }], ctorParameters: function () { return [{ type: i0.ElementRef }, { type: i0.Renderer2 }]; }, propDecorators: { padding: [{
                type: Input
            }] } });
export class IotCardContent {
    constructor(elRef, renderer) {
        this.elRef = elRef;
        this.renderer = renderer;
        this.padding = false;
        this.renderer.addClass(this.elRef.nativeElement, 'box-sizing');
        this.renderer.setStyle(this.elRef.nativeElement, 'height', `100%`);
    }
    ngOnInit() {
        if (this.padding) {
            this.renderer.setStyle(this.elRef.nativeElement, 'padding', `0 16px 0px 16px`);
        }
    }
}
IotCardContent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotCardContent, deps: [{ token: i0.ElementRef }, { token: i0.Renderer2 }], target: i0.ɵɵFactoryTarget.Directive });
IotCardContent.ɵdir = i0.ɵɵngDeclareDirective({ minVersion: "12.0.0", version: "12.2.6", type: IotCardContent, selector: "[iot-card-content]", inputs: { padding: "padding" }, ngImport: i0 });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotCardContent, decorators: [{
            type: Directive,
            args: [{ selector: '[iot-card-content]' }]
        }], ctorParameters: function () { return [{ type: i0.ElementRef }, { type: i0.Renderer2 }]; }, propDecorators: { padding: [{
                type: Input
            }] } });
export class IotCardFooter {
    constructor(elRef, renderer) {
        this.elRef = elRef;
        this.renderer = renderer;
        this.padding = false;
        this.renderer.addClass(this.elRef.nativeElement, 'box-sizing');
        this.renderer.setStyle(this.elRef.nativeElement, 'height', `40px`);
    }
    ngOnInit() {
        if (this.padding) {
            this.renderer.setStyle(this.elRef.nativeElement, 'padding', `0 16px 16px 16px`);
        }
    }
}
IotCardFooter.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotCardFooter, deps: [{ token: i0.ElementRef }, { token: i0.Renderer2 }], target: i0.ɵɵFactoryTarget.Directive });
IotCardFooter.ɵdir = i0.ɵɵngDeclareDirective({ minVersion: "12.0.0", version: "12.2.6", type: IotCardFooter, selector: "[iot-card-footer]", inputs: { padding: "padding" }, ngImport: i0 });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotCardFooter, decorators: [{
            type: Directive,
            args: [{ selector: '[iot-card-footer]' }]
        }], ctorParameters: function () { return [{ type: i0.ElementRef }, { type: i0.Renderer2 }]; }, propDecorators: { padding: [{
                type: Input
            }] } });
export class DashboardCardComponent {
    constructor() {
        this.colspan = 1;
        this.showHeader = true;
        this.showTitle = true;
        this.height = "500px";
        this.width = "320px";
        this.cardClass = {
            'iotcol-1': false,
            'iotcol-2': false,
            'iotcol-3': false,
            'iotcol-4': false,
        };
    }
    ngOnInit() {
        this.cardClass['iotcol-' + this.colspan] = true;
    }
    ngOnChanges(changes) {
        if (changes.colspan) {
            this.cardClass['iotcol-' + changes.colspan.previousValue] = false;
            this.cardClass['iotcol-' + changes.colspan.currentValue] = true;
        }
    }
}
DashboardCardComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: DashboardCardComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
DashboardCardComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: DashboardCardComponent, selector: "iot-dashboard-card", inputs: { colspan: "colspan", showHeader: "showHeader", outerTitle: "outerTitle", innerTitle: "innerTitle", showTitle: "showTitle", height: "height", width: "width" }, queries: [{ propertyName: "template", first: true, predicate: TemplateRef, descendants: true }], usesOnChanges: true, ngImport: i0, template: `
    <div class="outer-card">

      <div class="iotcard">
        <div
          class="iot-card-outer-title" [ngStyle]="{'width':width}">
          <div *ngIf="outerTitle">{{outerTitle | translate}}</div>
          <div *ngIf="!outerTitle">&nbsp;</div>
        </div>
        <div [ngClass]="cardClass" class="iot-inner-card iot-card-scss" [ngStyle]="{'height':height}">

          <div class="custheader iot-card-title">
            <ng-content select="[iot-card-header]"></ng-content>
          </div>
          <div class="custmiddle">
            <ng-content select="[iot-card-content]"></ng-content>
          </div>
          <div class="custfooter">
            <ng-content select="[iot-card-footer]"></ng-content>
          </div>
        </div>
      </div>
    </div>`, isInline: true, styles: ["\n\n\n    .custheader {\n      -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */\n      -moz-box-sizing: border-box; /* Firefox, other Gecko */\n      box-sizing: border-box;\n    }\n\n    .custmiddle {\n      flex-grow: 1;\n    }\n\n    .custfooter {\n\n    }\n  "], directives: [{ type: i1.NgStyle, selector: "[ngStyle]", inputs: ["ngStyle"] }, { type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }], pipes: { "translate": i2.TranslatePipe }, encapsulation: i0.ViewEncapsulation.None });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: DashboardCardComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-dashboard-card',
                    template: `
    <div class="outer-card">

      <div class="iotcard">
        <div
          class="iot-card-outer-title" [ngStyle]="{'width':width}">
          <div *ngIf="outerTitle">{{outerTitle | translate}}</div>
          <div *ngIf="!outerTitle">&nbsp;</div>
        </div>
        <div [ngClass]="cardClass" class="iot-inner-card iot-card-scss" [ngStyle]="{'height':height}">

          <div class="custheader iot-card-title">
            <ng-content select="[iot-card-header]"></ng-content>
          </div>
          <div class="custmiddle">
            <ng-content select="[iot-card-content]"></ng-content>
          </div>
          <div class="custfooter">
            <ng-content select="[iot-card-footer]"></ng-content>
          </div>
        </div>
      </div>
    </div>`,
                    encapsulation: ViewEncapsulation.None,
                    styles: [`


    .custheader {
      -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
      -moz-box-sizing: border-box; /* Firefox, other Gecko */
      box-sizing: border-box;
    }

    .custmiddle {
      flex-grow: 1;
    }

    .custfooter {

    }
  `]
                }]
        }], ctorParameters: function () { return []; }, propDecorators: { template: [{
                type: ContentChild,
                args: [TemplateRef]
            }], colspan: [{
                type: Input
            }], showHeader: [{
                type: Input
            }], outerTitle: [{
                type: Input
            }], innerTitle: [{
                type: Input
            }], showTitle: [{
                type: Input
            }], height: [{
                type: Input
            }], width: [{
                type: Input
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGFzaGJvYXJkLWNhcmQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvaW90LXVpLWxpYi9zcmMvbGliL2Rhc2hib2FyZC9kYXNoYm9hcmQtY2FyZC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7O0dBS0c7QUFFSCxPQUFPLEVBQ0wsU0FBUyxFQUNULFlBQVksRUFBRSxTQUFTLEVBQ3ZCLEtBQUssRUFJTCxXQUFXLEVBQ1gsaUJBQWlCLEVBQ2xCLE1BQU0sZUFBZSxDQUFDOzs7O0FBR3ZCLE1BQU0sT0FBTyxhQUFhO0lBSXhCLFlBQ1UsS0FBaUIsRUFDakIsUUFBbUI7UUFEbkIsVUFBSyxHQUFMLEtBQUssQ0FBWTtRQUNqQixhQUFRLEdBQVIsUUFBUSxDQUFXO1FBSnBCLFlBQU8sR0FBRyxLQUFLLENBQUM7UUFLdkIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQ3BCLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxFQUFFLFlBQVksQ0FBQyxDQUFDO1FBQzFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUNwQixJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsRUFDeEIsUUFBUSxFQUNSLE1BQU0sQ0FBQyxDQUFDO0lBRVosQ0FBQztJQUdELFFBQVE7UUFDTixJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDaEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQ3BCLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxFQUN4QixTQUFTLEVBQ1Qsa0JBQWtCLENBQUMsQ0FBQztTQUN2QjtJQUNILENBQUM7OzBHQXhCVSxhQUFhOzhGQUFiLGFBQWE7MkZBQWIsYUFBYTtrQkFEekIsU0FBUzttQkFBQyxFQUFDLFFBQVEsRUFBRSxtQkFBbUIsRUFBQzt5SEFHL0IsT0FBTztzQkFBZixLQUFLOztBQTBCUixNQUFNLE9BQU8sY0FBYztJQUd6QixZQUNVLEtBQWlCLEVBQ2pCLFFBQW1CO1FBRG5CLFVBQUssR0FBTCxLQUFLLENBQVk7UUFDakIsYUFBUSxHQUFSLFFBQVEsQ0FBVztRQUpwQixZQUFPLEdBQUcsS0FBSyxDQUFDO1FBS3ZCLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUNwQixJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsRUFBRSxZQUFZLENBQUMsQ0FBQztRQUMxQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FDcEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLEVBQ3hCLFFBQVEsRUFDUixNQUFNLENBQUMsQ0FBQztJQUNaLENBQUM7SUFFRCxRQUFRO1FBQ04sSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ2hCLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUNwQixJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsRUFDeEIsU0FBUyxFQUNULGlCQUFpQixDQUFDLENBQUM7U0FDdEI7SUFDSCxDQUFDOzsyR0FyQlUsY0FBYzsrRkFBZCxjQUFjOzJGQUFkLGNBQWM7a0JBRDFCLFNBQVM7bUJBQUMsRUFBQyxRQUFRLEVBQUUsb0JBQW9CLEVBQUM7eUhBRWhDLE9BQU87c0JBQWYsS0FBSzs7QUF3QlIsTUFBTSxPQUFPLGFBQWE7SUFJeEIsWUFDVSxLQUFpQixFQUNqQixRQUFtQjtRQURuQixVQUFLLEdBQUwsS0FBSyxDQUFZO1FBQ2pCLGFBQVEsR0FBUixRQUFRLENBQVc7UUFKcEIsWUFBTyxHQUFHLEtBQUssQ0FBQztRQUt2QixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FDcEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLEVBQUUsWUFBWSxDQUFDLENBQUM7UUFDMUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQ3BCLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxFQUN4QixRQUFRLEVBQ1IsTUFBTSxDQUFDLENBQUM7SUFFWixDQUFDO0lBRUQsUUFBUTtRQUNOLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNoQixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FDcEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLEVBQ3hCLFNBQVMsRUFDVCxrQkFBa0IsQ0FBQyxDQUFDO1NBQ3ZCO0lBQ0gsQ0FBQzs7MEdBdkJVLGFBQWE7OEZBQWIsYUFBYTsyRkFBYixhQUFhO2tCQUR6QixTQUFTO21CQUFDLEVBQUMsUUFBUSxFQUFFLG1CQUFtQixFQUFDO3lIQUcvQixPQUFPO3NCQUFmLEtBQUs7O0FBcUVSLE1BQU0sT0FBTyxzQkFBc0I7SUFtQmpDO1FBZlMsWUFBTyxHQUFHLENBQUMsQ0FBQztRQUNaLGVBQVUsR0FBRyxJQUFJLENBQUM7UUFHbEIsY0FBUyxHQUFHLElBQUksQ0FBQztRQUNqQixXQUFNLEdBQUcsT0FBTyxDQUFDO1FBQ2pCLFVBQUssR0FBRyxPQUFPLENBQUM7UUFFekIsY0FBUyxHQUFHO1lBQ1YsVUFBVSxFQUFFLEtBQUs7WUFDakIsVUFBVSxFQUFFLEtBQUs7WUFDakIsVUFBVSxFQUFFLEtBQUs7WUFDakIsVUFBVSxFQUFFLEtBQUs7U0FDbEIsQ0FBQztJQUdGLENBQUM7SUFFRCxRQUFRO1FBQ04sSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLElBQUksQ0FBQztJQUVsRCxDQUFDO0lBRUQsV0FBVyxDQUFDLE9BQXNCO1FBQ2hDLElBQUksT0FBTyxDQUFDLE9BQU8sRUFBRTtZQUNuQixJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxHQUFHLEtBQUssQ0FBQztZQUNsRSxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxHQUFHLElBQUksQ0FBQztTQUNqRTtJQUNILENBQUM7O21IQWhDVSxzQkFBc0I7dUdBQXRCLHNCQUFzQix3UUFFbkIsV0FBVyxxRUE1Q2Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7V0FzQkQ7MkZBb0JFLHNCQUFzQjtrQkE1Q2xDLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtvQkFDOUIsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1dBc0JEO29CQUNULGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJO29CQUNyQyxNQUFNLEVBQUUsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7OztHQWdCUixDQUFDO2lCQUNIOzBFQUc0QixRQUFRO3NCQUFsQyxZQUFZO3VCQUFDLFdBQVc7Z0JBRWhCLE9BQU87c0JBQWYsS0FBSztnQkFDRyxVQUFVO3NCQUFsQixLQUFLO2dCQUNHLFVBQVU7c0JBQWxCLEtBQUs7Z0JBQ0csVUFBVTtzQkFBbEIsS0FBSztnQkFDRyxTQUFTO3NCQUFqQixLQUFLO2dCQUNHLE1BQU07c0JBQWQsS0FBSztnQkFDRyxLQUFLO3NCQUFiLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxuICogQ29weXJpZ2h0IChDKSBJby1UaGluZ3MsIEJWIC0gQWxsIFJpZ2h0cyBSZXNlcnZlZFxuICogVW5hdXRob3JpemVkIGNvcHlpbmcgb2YgdGhpcyBmaWxlLCB2aWEgYW55IG1lZGl1bSBpcyBzdHJpY3RseSBwcm9oaWJpdGVkXG4gKiBQcm9wcmlldGFyeSBhbmQgY29uZmlkZW50aWFsXG4gKiBXcml0dGVuIGJ5IFN0ZWZhYW4gVGVybmllciA8c3RlZmFhbkBpby10aGluZ3MuZXU+LCAxLzYvMjAyMFxuICovXG5cbmltcG9ydCB7XG4gIENvbXBvbmVudCxcbiAgQ29udGVudENoaWxkLCBEaXJlY3RpdmUsIEVsZW1lbnRSZWYsXG4gIElucHV0LFxuICBPbkNoYW5nZXMsXG4gIE9uSW5pdCwgUmVuZGVyZXIyLFxuICBTaW1wbGVDaGFuZ2VzLFxuICBUZW1wbGF0ZVJlZixcbiAgVmlld0VuY2Fwc3VsYXRpb25cbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBEaXJlY3RpdmUoe3NlbGVjdG9yOiAnW2lvdC1jYXJkLWhlYWRlcl0nfSlcbmV4cG9ydCBjbGFzcyBJb3RDYXJkSGVhZGVyIGltcGxlbWVudHMgT25Jbml0IHtcblxuICBASW5wdXQoKSBwYWRkaW5nID0gZmFsc2U7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBlbFJlZjogRWxlbWVudFJlZixcbiAgICBwcml2YXRlIHJlbmRlcmVyOiBSZW5kZXJlcjIpIHtcbiAgICB0aGlzLnJlbmRlcmVyLmFkZENsYXNzKFxuICAgICAgdGhpcy5lbFJlZi5uYXRpdmVFbGVtZW50LCAnYm94LXNpemluZycpO1xuICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUoXG4gICAgICB0aGlzLmVsUmVmLm5hdGl2ZUVsZW1lbnQsXG4gICAgICAnaGVpZ2h0JyxcbiAgICAgIGA1NnB4YCk7XG5cbiAgfVxuXG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgaWYgKHRoaXMucGFkZGluZykge1xuICAgICAgdGhpcy5yZW5kZXJlci5zZXRTdHlsZShcbiAgICAgICAgdGhpcy5lbFJlZi5uYXRpdmVFbGVtZW50LFxuICAgICAgICAncGFkZGluZycsXG4gICAgICAgIGAxNnB4IDE2cHggMCAxNnB4YCk7XG4gICAgfVxuICB9XG59XG5cbkBEaXJlY3RpdmUoe3NlbGVjdG9yOiAnW2lvdC1jYXJkLWNvbnRlbnRdJ30pXG5leHBvcnQgY2xhc3MgSW90Q2FyZENvbnRlbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBASW5wdXQoKSBwYWRkaW5nID0gZmFsc2U7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBlbFJlZjogRWxlbWVudFJlZixcbiAgICBwcml2YXRlIHJlbmRlcmVyOiBSZW5kZXJlcjIpIHtcbiAgICB0aGlzLnJlbmRlcmVyLmFkZENsYXNzKFxuICAgICAgdGhpcy5lbFJlZi5uYXRpdmVFbGVtZW50LCAnYm94LXNpemluZycpO1xuICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUoXG4gICAgICB0aGlzLmVsUmVmLm5hdGl2ZUVsZW1lbnQsXG4gICAgICAnaGVpZ2h0JyxcbiAgICAgIGAxMDAlYCk7XG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5wYWRkaW5nKSB7XG4gICAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKFxuICAgICAgICB0aGlzLmVsUmVmLm5hdGl2ZUVsZW1lbnQsXG4gICAgICAgICdwYWRkaW5nJyxcbiAgICAgICAgYDAgMTZweCAwcHggMTZweGApO1xuICAgIH1cbiAgfVxufVxuXG5ARGlyZWN0aXZlKHtzZWxlY3RvcjogJ1tpb3QtY2FyZC1mb290ZXJdJ30pXG5leHBvcnQgY2xhc3MgSW90Q2FyZEZvb3RlciBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgQElucHV0KCkgcGFkZGluZyA9IGZhbHNlO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgZWxSZWY6IEVsZW1lbnRSZWYsXG4gICAgcHJpdmF0ZSByZW5kZXJlcjogUmVuZGVyZXIyKSB7XG4gICAgdGhpcy5yZW5kZXJlci5hZGRDbGFzcyhcbiAgICAgIHRoaXMuZWxSZWYubmF0aXZlRWxlbWVudCwgJ2JveC1zaXppbmcnKTtcbiAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKFxuICAgICAgdGhpcy5lbFJlZi5uYXRpdmVFbGVtZW50LFxuICAgICAgJ2hlaWdodCcsXG4gICAgICBgNDBweGApO1xuXG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5wYWRkaW5nKSB7XG4gICAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKFxuICAgICAgICB0aGlzLmVsUmVmLm5hdGl2ZUVsZW1lbnQsXG4gICAgICAgICdwYWRkaW5nJyxcbiAgICAgICAgYDAgMTZweCAxNnB4IDE2cHhgKTtcbiAgICB9XG4gIH1cbn1cblxuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdpb3QtZGFzaGJvYXJkLWNhcmQnLFxuICB0ZW1wbGF0ZTogYFxuICAgIDxkaXYgY2xhc3M9XCJvdXRlci1jYXJkXCI+XG5cbiAgICAgIDxkaXYgY2xhc3M9XCJpb3RjYXJkXCI+XG4gICAgICAgIDxkaXZcbiAgICAgICAgICBjbGFzcz1cImlvdC1jYXJkLW91dGVyLXRpdGxlXCIgW25nU3R5bGVdPVwieyd3aWR0aCc6d2lkdGh9XCI+XG4gICAgICAgICAgPGRpdiAqbmdJZj1cIm91dGVyVGl0bGVcIj57e291dGVyVGl0bGUgfCB0cmFuc2xhdGV9fTwvZGl2PlxuICAgICAgICAgIDxkaXYgKm5nSWY9XCIhb3V0ZXJUaXRsZVwiPiZuYnNwOzwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBbbmdDbGFzc109XCJjYXJkQ2xhc3NcIiBjbGFzcz1cImlvdC1pbm5lci1jYXJkIGlvdC1jYXJkLXNjc3NcIiBbbmdTdHlsZV09XCJ7J2hlaWdodCc6aGVpZ2h0fVwiPlxuXG4gICAgICAgICAgPGRpdiBjbGFzcz1cImN1c3RoZWFkZXIgaW90LWNhcmQtdGl0bGVcIj5cbiAgICAgICAgICAgIDxuZy1jb250ZW50IHNlbGVjdD1cIltpb3QtY2FyZC1oZWFkZXJdXCI+PC9uZy1jb250ZW50PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJjdXN0bWlkZGxlXCI+XG4gICAgICAgICAgICA8bmctY29udGVudCBzZWxlY3Q9XCJbaW90LWNhcmQtY29udGVudF1cIj48L25nLWNvbnRlbnQ+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzcz1cImN1c3Rmb290ZXJcIj5cbiAgICAgICAgICAgIDxuZy1jb250ZW50IHNlbGVjdD1cIltpb3QtY2FyZC1mb290ZXJdXCI+PC9uZy1jb250ZW50PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PmAsXG4gIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmUsXG4gIHN0eWxlczogW2BcblxuXG4gICAgLmN1c3RoZWFkZXIge1xuICAgICAgLXdlYmtpdC1ib3gtc2l6aW5nOiBib3JkZXItYm94OyAvKiBTYWZhcmkvQ2hyb21lLCBvdGhlciBXZWJLaXQgKi9cbiAgICAgIC1tb3otYm94LXNpemluZzogYm9yZGVyLWJveDsgLyogRmlyZWZveCwgb3RoZXIgR2Vja28gKi9cbiAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgfVxuXG4gICAgLmN1c3RtaWRkbGUge1xuICAgICAgZmxleC1ncm93OiAxO1xuICAgIH1cblxuICAgIC5jdXN0Zm9vdGVyIHtcblxuICAgIH1cbiAgYF1cbn0pXG5leHBvcnQgY2xhc3MgRGFzaGJvYXJkQ2FyZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25DaGFuZ2VzIHtcblxuICBAQ29udGVudENoaWxkKFRlbXBsYXRlUmVmKSB0ZW1wbGF0ZTogVGVtcGxhdGVSZWY8YW55PjtcblxuICBASW5wdXQoKSBjb2xzcGFuID0gMTtcbiAgQElucHV0KCkgc2hvd0hlYWRlciA9IHRydWU7XG4gIEBJbnB1dCgpIG91dGVyVGl0bGU7XG4gIEBJbnB1dCgpIGlubmVyVGl0bGU7XG4gIEBJbnB1dCgpIHNob3dUaXRsZSA9IHRydWU7XG4gIEBJbnB1dCgpIGhlaWdodCA9IFwiNTAwcHhcIjtcbiAgQElucHV0KCkgd2lkdGggPSBcIjMyMHB4XCI7XG5cbiAgY2FyZENsYXNzID0ge1xuICAgICdpb3Rjb2wtMSc6IGZhbHNlLFxuICAgICdpb3Rjb2wtMic6IGZhbHNlLFxuICAgICdpb3Rjb2wtMyc6IGZhbHNlLFxuICAgICdpb3Rjb2wtNCc6IGZhbHNlLFxuICB9O1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgdGhpcy5jYXJkQ2xhc3NbJ2lvdGNvbC0nICsgdGhpcy5jb2xzcGFuXSA9IHRydWU7XG5cbiAgfVxuXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpOiB2b2lkIHtcbiAgICBpZiAoY2hhbmdlcy5jb2xzcGFuKSB7XG4gICAgICB0aGlzLmNhcmRDbGFzc1snaW90Y29sLScgKyBjaGFuZ2VzLmNvbHNwYW4ucHJldmlvdXNWYWx1ZV0gPSBmYWxzZTtcbiAgICAgIHRoaXMuY2FyZENsYXNzWydpb3Rjb2wtJyArIGNoYW5nZXMuY29sc3Bhbi5jdXJyZW50VmFsdWVdID0gdHJ1ZTtcbiAgICB9XG4gIH1cblxufVxuIl19