import { Component, ContentChildren, Directive, Input } from '@angular/core';
import { DashboardMediaQueryComponent } from "./dashboard-media-query.component";
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/grid-list";
import * as i2 from "@angular/common";
export class DashboardCard {
    constructor(elementRef, templateRef) {
        this.elementRef = elementRef;
        this.templateRef = templateRef;
        this.cols = [0, 1, 2, 3, 4];
    }
}
DashboardCard.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: DashboardCard, deps: [{ token: i0.ElementRef }, { token: i0.TemplateRef }], target: i0.ɵɵFactoryTarget.Directive });
DashboardCard.ɵdir = i0.ɵɵngDeclareDirective({ minVersion: "12.0.0", version: "12.2.6", type: DashboardCard, selector: "[iotCard]", inputs: { cardId: "cardId", colspan: "colspan" }, ngImport: i0 });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: DashboardCard, decorators: [{
            type: Directive,
            args: [{ selector: '[iotCard]' }]
        }], ctorParameters: function () { return [{ type: i0.ElementRef }, { type: i0.TemplateRef }]; }, propDecorators: { cardId: [{
                type: Input
            }], colspan: [{
                type: Input
            }] } });
export class DashboardGridComponent extends DashboardMediaQueryComponent {
    constructor() {
        super(...arguments);
        this.rowHeight = '55px';
        this._col = [[], [], [], [], []];
        this._templateMap = {};
        this._cardMap = {};
        this.row = [1, 2];
    }
    ngAfterContentInit() {
        this.topLevelPanes.forEach(card => {
            this._templateMap[card.cardId] = card.templateRef;
            this._cardMap[card.cardId] = card;
            if (card.colspan && card.colspan.indexOf(',') != -1) {
                let i = 1;
                card.colspan.split(',').forEach((nAsString) => {
                    this._cardMap[card.cardId].cols[i++] = Number.parseInt(nAsString, 10);
                });
            }
            else if (card.colspan) {
                const defaultColspan = Number.parseInt(card.colspan, 10);
                this._cardMap[card.cardId].cols[1] = defaultColspan;
                this._cardMap[card.cardId].cols[2] = defaultColspan;
                this._cardMap[card.cardId].cols[3] = defaultColspan;
                this._cardMap[card.cardId].cols[4] = defaultColspan;
            }
        });
    }
    ngOnInit() {
        if (!this.col1) {
            this._col[1] = [];
        }
        else {
            this._col[1] = this.col1.split(",");
        }
        if (!this.col2) {
            this._col[2] = [];
        }
        else {
            this._col[2] = this.col2.split(",");
        }
        if (!this.col3) {
            this._col[3] = [];
        }
        else {
            this._col[3] = this.col3.split(",");
        }
        if (!this.col4) {
            this._col[4] = [];
        }
        else {
            this._col[4] = this.col4.split(",");
        }
    }
}
DashboardGridComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: DashboardGridComponent, deps: null, target: i0.ɵɵFactoryTarget.Component });
DashboardGridComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: DashboardGridComponent, selector: "iot-dashboard", inputs: { rowHeight: "rowHeight", col1: "col1", col2: "col2", col3: "col3", col4: "col4" }, queries: [{ propertyName: "topLevelPanes", predicate: DashboardCard }], usesInheritance: true, ngImport: i0, template: `
    <div class="centered-content">
      <mat-grid-list
        [cols]="cols" [rowHeight]="rowHeight" [ngStyle]="absoluteStyle">

        <mat-grid-tile
          *ngFor="let key of _col[cols]"
          [rowspan]="10"

          [colspan]="_cardMap[key].cols[cols] "
        >

          <ng-container [ngTemplateOutlet]="(_templateMap[key] )"
                        [ngTemplateOutletContext]="{
                        columnsAvailable: _cardMap[key].cols[cols] }"
          >
<!--            _cardMap[key].colspan ?  (_cardMap[key].colspan < 0? cols + _cardMap[key].colspan : _cardMap[key].colspan  ) : cols-->
          </ng-container>
        </mat-grid-tile>
      </mat-grid-list>


      <!--            <div style="width: 100px">test</div>-->
    </div>
  `, isInline: true, components: [{ type: i1.MatGridList, selector: "mat-grid-list", inputs: ["cols", "gutterSize", "rowHeight"], exportAs: ["matGridList"] }, { type: i1.MatGridTile, selector: "mat-grid-tile", inputs: ["rowspan", "colspan"], exportAs: ["matGridTile"] }], directives: [{ type: i2.NgStyle, selector: "[ngStyle]", inputs: ["ngStyle"] }, { type: i2.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { type: i2.NgTemplateOutlet, selector: "[ngTemplateOutlet]", inputs: ["ngTemplateOutletContext", "ngTemplateOutlet"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: DashboardGridComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-dashboard',
                    template: `
    <div class="centered-content">
      <mat-grid-list
        [cols]="cols" [rowHeight]="rowHeight" [ngStyle]="absoluteStyle">

        <mat-grid-tile
          *ngFor="let key of _col[cols]"
          [rowspan]="10"

          [colspan]="_cardMap[key].cols[cols] "
        >

          <ng-container [ngTemplateOutlet]="(_templateMap[key] )"
                        [ngTemplateOutletContext]="{
                        columnsAvailable: _cardMap[key].cols[cols] }"
          >
<!--            _cardMap[key].colspan ?  (_cardMap[key].colspan < 0? cols + _cardMap[key].colspan : _cardMap[key].colspan  ) : cols-->
          </ng-container>
        </mat-grid-tile>
      </mat-grid-list>


      <!--            <div style="width: 100px">test</div>-->
    </div>
  `
                }]
        }], propDecorators: { topLevelPanes: [{
                type: ContentChildren,
                args: [DashboardCard]
            }], rowHeight: [{
                type: Input
            }], col1: [{
                type: Input
            }], col2: [{
                type: Input
            }], col3: [{
                type: Input
            }], col4: [{
                type: Input
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGFzaGJvYXJkLWdyaWQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvaW90LXVpLWxpYi9zcmMvbGliL2Rhc2hib2FyZC9kYXNoYm9hcmQtZ3JpZC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUVMLFNBQVMsRUFDVCxlQUFlLEVBQ2YsU0FBUyxFQUVULEtBQUssRUFHTixNQUFNLGVBQWUsQ0FBQztBQUV2QixPQUFPLEVBQUMsNEJBQTRCLEVBQUMsTUFBTSxtQ0FBbUMsQ0FBQzs7OztBQUkvRSxNQUFNLE9BQU8sYUFBYTtJQU14QixZQUFvQixVQUFzQixFQUFTLFdBQTZCO1FBQTVELGVBQVUsR0FBVixVQUFVLENBQVk7UUFBUyxnQkFBVyxHQUFYLFdBQVcsQ0FBa0I7UUFGaEYsU0FBSSxHQUFHLENBQUMsQ0FBQyxFQUFDLENBQUMsRUFBQyxDQUFDLEVBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxDQUFDO0lBRWdFLENBQUM7OzBHQU56RSxhQUFhOzhGQUFiLGFBQWE7MkZBQWIsYUFBYTtrQkFEekIsU0FBUzttQkFBQyxFQUFDLFFBQVEsRUFBRSxXQUFXLEVBQUM7MkhBRXZCLE1BQU07c0JBQWQsS0FBSztnQkFDRyxPQUFPO3NCQUFmLEtBQUs7O0FBb0NSLE1BQU0sT0FBTyxzQkFBdUIsU0FBUSw0QkFBNEI7SUE1QnhFOztRQStCVyxjQUFTLEdBQUcsTUFBTSxDQUFDO1FBTTVCLFNBQUksR0FBRSxDQUFDLEVBQUUsRUFBQyxFQUFFLEVBQUMsRUFBRSxFQUFDLEVBQUUsRUFBQyxFQUFFLENBQUMsQ0FBQztRQUV2QixpQkFBWSxHQUF3QyxFQUFFLENBQUM7UUFDdkQsYUFBUSxHQUFxQyxFQUFFLENBQUM7UUFJaEQsUUFBRyxHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0tBbURkO0lBL0NDLGtCQUFrQjtRQUNoQixJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUNoQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFBO1lBQ2pELElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQztZQUNsQyxJQUFJLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUU7Z0JBQ25ELElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDVixJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBRTtvQkFDNUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQ3hFLENBQUMsQ0FBQyxDQUFBO2FBQ0g7aUJBQU0sSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO2dCQUN2QixNQUFNLGNBQWMsR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQ3pELElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxjQUFjLENBQUM7Z0JBQ3BELElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxjQUFjLENBQUM7Z0JBQ3BELElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxjQUFjLENBQUM7Z0JBQ3BELElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxjQUFjLENBQUM7YUFDckQ7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUVMLENBQUM7SUFFRCxRQUFRO1FBQ04sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFDZCxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztTQUNuQjthQUFNO1lBQ0wsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUNyQztRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQ2QsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7U0FDbkI7YUFBTTtZQUNMLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDckM7UUFFRCxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRTtZQUVkLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDO1NBQ25CO2FBQU07WUFDTCxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQ3JDO1FBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFFZCxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztTQUNuQjthQUFNO1lBQ0wsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUNyQztJQUNILENBQUM7O21IQWpFVSxzQkFBc0I7dUdBQXRCLHNCQUFzQiwrS0FFaEIsYUFBYSxvREE1QnBCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7R0F3QlQ7MkZBRVUsc0JBQXNCO2tCQTVCbEMsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsZUFBZTtvQkFDekIsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7R0F3QlQ7aUJBQ0Y7OEJBR2lDLGFBQWE7c0JBQTVDLGVBQWU7dUJBQUMsYUFBYTtnQkFDckIsU0FBUztzQkFBakIsS0FBSztnQkFDRyxJQUFJO3NCQUFaLEtBQUs7Z0JBQ0csSUFBSTtzQkFBWixLQUFLO2dCQUNHLElBQUk7c0JBQVosS0FBSztnQkFDRyxJQUFJO3NCQUFaLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuICBBZnRlckNvbnRlbnRJbml0LFxuICBDb21wb25lbnQsXG4gIENvbnRlbnRDaGlsZHJlbixcbiAgRGlyZWN0aXZlLFxuICBFbGVtZW50UmVmLFxuICBJbnB1dCwgT25Jbml0LFxuICBRdWVyeUxpc3QsXG4gIFRlbXBsYXRlUmVmXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0ICogYXMgcXVlcnlzdHJpbmcgZnJvbSBcInF1ZXJ5c3RyaW5nXCI7XG5pbXBvcnQge0Rhc2hib2FyZE1lZGlhUXVlcnlDb21wb25lbnR9IGZyb20gXCIuL2Rhc2hib2FyZC1tZWRpYS1xdWVyeS5jb21wb25lbnRcIjtcbmltcG9ydCB7TWF0RGlhbG9nfSBmcm9tIFwiQGFuZ3VsYXIvbWF0ZXJpYWwvZGlhbG9nXCI7XG5cbkBEaXJlY3RpdmUoe3NlbGVjdG9yOiAnW2lvdENhcmRdJ30pXG5leHBvcnQgY2xhc3MgRGFzaGJvYXJkQ2FyZCB7XG4gIEBJbnB1dCgpIGNhcmRJZCE6IHN0cmluZztcbiAgQElucHV0KCkgY29sc3BhbjogYW55O1xuXG4gIGNvbHMgPSBbMCwxLDIsMyw0XTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGVsZW1lbnRSZWY6IEVsZW1lbnRSZWYsIHB1YmxpYyB0ZW1wbGF0ZVJlZjogVGVtcGxhdGVSZWY8YW55Pikge31cblxufVxuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdpb3QtZGFzaGJvYXJkJyxcbiAgdGVtcGxhdGU6IGBcbiAgICA8ZGl2IGNsYXNzPVwiY2VudGVyZWQtY29udGVudFwiPlxuICAgICAgPG1hdC1ncmlkLWxpc3RcbiAgICAgICAgW2NvbHNdPVwiY29sc1wiIFtyb3dIZWlnaHRdPVwicm93SGVpZ2h0XCIgW25nU3R5bGVdPVwiYWJzb2x1dGVTdHlsZVwiPlxuXG4gICAgICAgIDxtYXQtZ3JpZC10aWxlXG4gICAgICAgICAgKm5nRm9yPVwibGV0IGtleSBvZiBfY29sW2NvbHNdXCJcbiAgICAgICAgICBbcm93c3Bhbl09XCIxMFwiXG5cbiAgICAgICAgICBbY29sc3Bhbl09XCJfY2FyZE1hcFtrZXldLmNvbHNbY29sc10gXCJcbiAgICAgICAgPlxuXG4gICAgICAgICAgPG5nLWNvbnRhaW5lciBbbmdUZW1wbGF0ZU91dGxldF09XCIoX3RlbXBsYXRlTWFwW2tleV0gKVwiXG4gICAgICAgICAgICAgICAgICAgICAgICBbbmdUZW1wbGF0ZU91dGxldENvbnRleHRdPVwie1xuICAgICAgICAgICAgICAgICAgICAgICAgY29sdW1uc0F2YWlsYWJsZTogX2NhcmRNYXBba2V5XS5jb2xzW2NvbHNdIH1cIlxuICAgICAgICAgID5cbjwhLS0gICAgICAgICAgICBfY2FyZE1hcFtrZXldLmNvbHNwYW4gPyAgKF9jYXJkTWFwW2tleV0uY29sc3BhbiA8IDA/IGNvbHMgKyBfY2FyZE1hcFtrZXldLmNvbHNwYW4gOiBfY2FyZE1hcFtrZXldLmNvbHNwYW4gICkgOiBjb2xzLS0+XG4gICAgICAgICAgPC9uZy1jb250YWluZXI+XG4gICAgICAgIDwvbWF0LWdyaWQtdGlsZT5cbiAgICAgIDwvbWF0LWdyaWQtbGlzdD5cblxuXG4gICAgICA8IS0tICAgICAgICAgICAgPGRpdiBzdHlsZT1cIndpZHRoOiAxMDBweFwiPnRlc3Q8L2Rpdj4tLT5cbiAgICA8L2Rpdj5cbiAgYFxufSlcbmV4cG9ydCBjbGFzcyBEYXNoYm9hcmRHcmlkQ29tcG9uZW50IGV4dGVuZHMgRGFzaGJvYXJkTWVkaWFRdWVyeUNvbXBvbmVudCBpbXBsZW1lbnRzIEFmdGVyQ29udGVudEluaXQsIE9uSW5pdHtcblxuICBAQ29udGVudENoaWxkcmVuKERhc2hib2FyZENhcmQpIHRvcExldmVsUGFuZXMhOiBRdWVyeUxpc3Q8RGFzaGJvYXJkQ2FyZD47XG4gIEBJbnB1dCgpIHJvd0hlaWdodCA9ICc1NXB4JztcbiAgQElucHV0KCkgY29sMTogc3RyaW5nO1xuICBASW5wdXQoKSBjb2wyOiBzdHJpbmc7XG4gIEBJbnB1dCgpIGNvbDM6IHN0cmluZztcbiAgQElucHV0KCkgY29sNDogc3RyaW5nO1xuXG4gIF9jb2w9IFtbXSxbXSxbXSxbXSxbXV07XG5cbiAgX3RlbXBsYXRlTWFwOiB7IFtrZXk6IHN0cmluZ106IFRlbXBsYXRlUmVmPGFueT4gfSA9IHt9O1xuICBfY2FyZE1hcDogeyBba2V5OiBzdHJpbmddOiBEYXNoYm9hcmRDYXJkIH0gPSB7fTtcblxuXG5cbiAgcm93ID0gWzEsIDJdO1xuXG5cblxuICBuZ0FmdGVyQ29udGVudEluaXQoKTogdm9pZCB7XG4gICAgdGhpcy50b3BMZXZlbFBhbmVzLmZvckVhY2goY2FyZCA9PiB7XG4gICAgICB0aGlzLl90ZW1wbGF0ZU1hcFtjYXJkLmNhcmRJZF0gPSBjYXJkLnRlbXBsYXRlUmVmXG4gICAgICB0aGlzLl9jYXJkTWFwW2NhcmQuY2FyZElkXSA9IGNhcmQ7XG4gICAgICBpZiAoY2FyZC5jb2xzcGFuICYmIGNhcmQuY29sc3Bhbi5pbmRleE9mKCcsJykgIT0gLTEpIHtcbiAgICAgICAgbGV0IGkgPSAxO1xuICAgICAgICBjYXJkLmNvbHNwYW4uc3BsaXQoJywnKS5mb3JFYWNoKChuQXNTdHJpbmcpID0+e1xuICAgICAgICAgIHRoaXMuX2NhcmRNYXBbY2FyZC5jYXJkSWRdLmNvbHNbaSsrXSA9IE51bWJlci5wYXJzZUludChuQXNTdHJpbmcsIDEwKTtcbiAgICAgICAgfSlcbiAgICAgIH0gZWxzZSBpZiAoY2FyZC5jb2xzcGFuKSB7XG4gICAgICAgIGNvbnN0IGRlZmF1bHRDb2xzcGFuID0gTnVtYmVyLnBhcnNlSW50KGNhcmQuY29sc3BhbiwgMTApO1xuICAgICAgICB0aGlzLl9jYXJkTWFwW2NhcmQuY2FyZElkXS5jb2xzWzFdID0gZGVmYXVsdENvbHNwYW47XG4gICAgICAgIHRoaXMuX2NhcmRNYXBbY2FyZC5jYXJkSWRdLmNvbHNbMl0gPSBkZWZhdWx0Q29sc3BhbjtcbiAgICAgICAgdGhpcy5fY2FyZE1hcFtjYXJkLmNhcmRJZF0uY29sc1szXSA9IGRlZmF1bHRDb2xzcGFuO1xuICAgICAgICB0aGlzLl9jYXJkTWFwW2NhcmQuY2FyZElkXS5jb2xzWzRdID0gZGVmYXVsdENvbHNwYW47XG4gICAgICB9XG4gICAgfSk7XG5cbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIGlmICghdGhpcy5jb2wxKSB7XG4gICAgICB0aGlzLl9jb2xbMV0gPSBbXTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5fY29sWzFdID0gdGhpcy5jb2wxLnNwbGl0KFwiLFwiKTtcbiAgICB9XG4gICAgaWYgKCF0aGlzLmNvbDIpIHtcbiAgICAgIHRoaXMuX2NvbFsyXSA9IFtdO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLl9jb2xbMl0gPSB0aGlzLmNvbDIuc3BsaXQoXCIsXCIpO1xuICAgIH1cblxuICAgIGlmICghdGhpcy5jb2wzKSB7XG5cbiAgICAgIHRoaXMuX2NvbFszXSA9IFtdO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLl9jb2xbM10gPSB0aGlzLmNvbDMuc3BsaXQoXCIsXCIpO1xuICAgIH1cblxuICAgIGlmICghdGhpcy5jb2w0KSB7XG5cbiAgICAgIHRoaXMuX2NvbFs0XSA9IFtdO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLl9jb2xbNF0gPSB0aGlzLmNvbDQuc3BsaXQoXCIsXCIpO1xuICAgIH1cbiAgfVxuXG59XG4iXX0=