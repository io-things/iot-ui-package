import { Component, Input } from '@angular/core';
import * as i0 from "@angular/core";
export class DashboardMediaQueryComponent {
    constructor() {
        this.offsetSize = 192;
        this.small = matchMedia(`(max-width: 700px)`);
        this.medium = matchMedia(`(max-width: 1000px)`);
        this.large = matchMedia(`(min-width: 1700px)`);
        this.absoluteStyle = {
            'width': "400px"
        };
    }
    setMediaQueries() {
        if (!!this.smallEventListener) {
            if (this.small.removeEventListener) {
                this.small.removeEventListener("change", this.smallEventListener);
            }
            else {
                this.small.removeListener(this.smallEventListener);
            }
        }
        if (!!this.mediumEventListener) {
            if (this.medium.removeEventListener) {
                this.medium.removeEventListener("change", this.mediumEventListener);
            }
            else {
                this.medium.removeListener(this.mediumEventListener);
            }
        }
        if (!!this.bigEventListener) {
            if (this.large.removeEventListener) {
                this.large.removeEventListener("change", this.bigEventListener);
            }
            else {
                this.large.removeListener(this.bigEventListener);
            }
        }
        const smallOffset = this.offsetSize + 656 + 26;
        const mediumOffset = this.offsetSize + 656 + 26 + 16 + 320;
        const bigOffset = mediumOffset + 16 + 320;
        this.small = matchMedia(`(max-width: ${smallOffset}px)`);
        this.medium = matchMedia(`(max-width: ${mediumOffset}px)`);
        this.large = matchMedia(`(min-width: ${bigOffset}px)`);
        this.smallEventListener = (e) => {
            if (e.matches) {
                this.match1Columns();
            }
            else {
                if (this.medium.matches) {
                    this.match2Columns();
                }
                else if (this.large.matches) {
                    this.match4Columns();
                }
                else {
                    // console.log(`not small, not wide` + this.large.media)
                    this.match3Columns();
                }
                // console.log(`This is a not small screen — more than ${smallOffset} wide.`+e.media)
            }
        };
        if (!!this.smallEventListener) {
            // console.log("before adding ", this.smallEventListener);
            if (this.small.addEventListener) {
                this.small.addEventListener("change", this.smallEventListener);
            }
            else {
                this.small.addListener(this.smallEventListener);
            }
        }
        this.mediumEventListener = (e) => {
            // console.log("medium");
            if (e.matches) {
                if (this.small.matches) {
                    this.match1Columns();
                }
                else {
                    this.match2Columns();
                }
            }
            else {
                if (this.large.matches) {
                    this.match4Columns();
                }
                else {
                    // console.log(`not small, not wide` + this.large.media)
                    this.match3Columns();
                }
            }
        };
        if (!!this.mediumEventListener) {
            // console.log("before adding ", this.smallEventListener);
            if (this.medium.addEventListener) {
                this.medium.addEventListener("change", this.mediumEventListener);
            }
            else {
                this.medium.addListener(this.mediumEventListener);
            }
        }
        // console.log("---- smallE ", this.smallEventListener);
        this.bigEventListener = (e) => {
            // console.log("big");
            if (e.matches) {
                // console.log(`exact wide match`+ e.media)
                if (this.large.matches) {
                    this.match4Columns();
                }
                else {
                    this.match3Columns();
                }
            }
            else {
                if (this.small.matches) {
                    // console.log(`not wide, is small`+ this.small.media)
                    this.match1Columns();
                }
                else if (this.medium.matches) {
                    this.match2Columns();
                }
                else {
                    // console.log(`not wide, not small`+ this.small.media)
                    this.match3Columns();
                }
            }
        };
        if (!!this.bigEventListener) {
            if (this.large.addEventListener) {
                this.large.addEventListener("change", this.bigEventListener);
            }
            else {
                this.large.addListener(this.bigEventListener);
            }
        }
        if (this.small.matches) {
            this.match1Columns();
        }
        else if (this.medium.matches) {
            this.match2Columns();
        }
        else if (this.large.matches) {
            this.match4Columns();
        }
        else {
            this.match3Columns();
        }
    }
    match1Columns() {
        this.cols = 1;
        this.absoluteStyle['width'] = '331px';
    }
    match2Columns() {
        this.cols = 2;
        this.absoluteStyle['width'] = '667px';
    }
    match3Columns() {
        this.cols = 3;
        this.absoluteStyle['width'] = '1003px';
    }
    match4Columns() {
        this.cols = 4;
        this.absoluteStyle['width'] = '1339px';
    }
    ngOnInit() {
        this.setMediaQueries();
    }
    ngOnChanges(changes) {
        this.setMediaQueries();
    }
}
DashboardMediaQueryComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: DashboardMediaQueryComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
DashboardMediaQueryComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: DashboardMediaQueryComponent, selector: "app-dashboard-grid", inputs: { offsetSize: "offsetSize" }, usesOnChanges: true, ngImport: i0, template: '', isInline: true });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: DashboardMediaQueryComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'app-dashboard-grid',
                    template: '',
                    styles: []
                }]
        }], ctorParameters: function () { return []; }, propDecorators: { offsetSize: [{
                type: Input
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGFzaGJvYXJkLW1lZGlhLXF1ZXJ5LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2lvdC11aS1saWIvc3JjL2xpYi9kYXNoYm9hcmQvZGFzaGJvYXJkLW1lZGlhLXF1ZXJ5LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBRUwsU0FBUyxFQUVULEtBQUssRUFLTixNQUFNLGVBQWUsQ0FBQzs7QUFPdkIsTUFBTSxPQUFPLDRCQUE0QjtJQWlCdkM7UUFoQlMsZUFBVSxHQUFHLEdBQUcsQ0FBQztRQUNsQixVQUFLLEdBQW1CLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1FBQ3pELFdBQU0sR0FBbUIsVUFBVSxDQUFDLHFCQUFxQixDQUFDLENBQUM7UUFDM0QsVUFBSyxHQUFtQixVQUFVLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUVsRSxrQkFBYSxHQUFHO1lBQ2QsT0FBTyxFQUFFLE9BQU87U0FDakIsQ0FBQztJQVVGLENBQUM7SUFJTyxlQUFlO1FBQ3JCLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRTtZQUM3QixJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsbUJBQW1CLEVBQUU7Z0JBQ2xDLElBQUksQ0FBQyxLQUFLLENBQUMsbUJBQW1CLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO2FBQ25FO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFBO2FBQ25EO1NBQ0Y7UUFDRCxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEVBQUU7WUFDOUIsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLG1CQUFtQixFQUFFO2dCQUNuQyxJQUFJLENBQUMsTUFBTSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQzthQUNyRTtpQkFBTTtnQkFDTCxJQUFJLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQTthQUNyRDtTQUNGO1FBQ0QsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFO1lBQzNCLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxtQkFBbUIsRUFBRTtnQkFDbEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7YUFDakU7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUE7YUFDakQ7U0FDRjtRQUNELE1BQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxVQUFVLEdBQUcsR0FBRyxHQUFHLEVBQUUsQ0FBQztRQUMvQyxNQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsVUFBVSxHQUFHLEdBQUcsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEdBQUcsQ0FBQztRQUMzRCxNQUFNLFNBQVMsR0FBRyxZQUFZLEdBQUcsRUFBRSxHQUFHLEdBQUcsQ0FBQztRQUUxQyxJQUFJLENBQUMsS0FBSyxHQUFHLFVBQVUsQ0FBQyxlQUFlLFdBQVcsS0FBSyxDQUFDLENBQUM7UUFDekQsSUFBSSxDQUFDLE1BQU0sR0FBRyxVQUFVLENBQUMsZUFBZSxZQUFZLEtBQUssQ0FBQyxDQUFDO1FBQzNELElBQUksQ0FBQyxLQUFLLEdBQUcsVUFBVSxDQUFDLGVBQWUsU0FBUyxLQUFLLENBQUMsQ0FBQztRQUd2RCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxDQUFDLEVBQUUsRUFBRTtZQUM5QixJQUFJLENBQUMsQ0FBQyxPQUFPLEVBQUU7Z0JBQ2IsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO2FBQ3RCO2lCQUFNO2dCQUNMLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUU7b0JBQ3ZCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQTtpQkFDckI7cUJBQU0sSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRTtvQkFDN0IsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFBO2lCQUNyQjtxQkFBTTtvQkFDTCx3REFBd0Q7b0JBQ3hELElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztpQkFDdEI7Z0JBQ0QscUZBQXFGO2FBQ3RGO1FBQ0gsQ0FBQyxDQUFDO1FBQ0YsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFFO1lBQzdCLDBEQUEwRDtZQUMxRCxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLEVBQUU7Z0JBQy9CLElBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO2FBQ2hFO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO2FBQ2pEO1NBRUY7UUFFRCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsQ0FBQyxDQUFDLEVBQUUsRUFBRTtZQUMvQix5QkFBeUI7WUFDekIsSUFBSSxDQUFDLENBQUMsT0FBTyxFQUFFO2dCQUNiLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUU7b0JBQ3RCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztpQkFDdEI7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO2lCQUN0QjthQUNGO2lCQUFNO2dCQUNMLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUU7b0JBQ3RCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQTtpQkFDckI7cUJBQU07b0JBQ0wsd0RBQXdEO29CQUN4RCxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7aUJBQ3RCO2FBQ0Y7UUFDSCxDQUFDLENBQUM7UUFDRixJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEVBQUU7WUFDOUIsMERBQTBEO1lBQzFELElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDaEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7YUFDbEU7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7YUFDbkQ7U0FFRjtRQUVELHdEQUF3RDtRQUV4RCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxDQUFDLEVBQUUsRUFBRTtZQUM1QixzQkFBc0I7WUFDdEIsSUFBSSxDQUFDLENBQUMsT0FBTyxFQUFFO2dCQUNiLDJDQUEyQztnQkFDM0MsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRTtvQkFDdEIsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFBO2lCQUNyQjtxQkFBTTtvQkFFTCxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7aUJBQ3RCO2FBQ0Y7aUJBQU07Z0JBQ0wsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRTtvQkFDdEIsc0RBQXNEO29CQUN0RCxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUE7aUJBQ3JCO3FCQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUU7b0JBQzlCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQTtpQkFDckI7cUJBQU07b0JBQ0wsdURBQXVEO29CQUN2RCxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7aUJBQ3RCO2FBQ0Y7UUFDSCxDQUFDLENBQUM7UUFDRixJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7WUFDM0IsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixFQUFFO2dCQUMvQixJQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQzthQUM5RDtpQkFBTTtnQkFDTCxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQzthQUMvQztTQUVGO1FBRUQsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRTtZQUV0QixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUE7U0FDckI7YUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFO1lBQzlCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQTtTQUNyQjthQUFNLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUU7WUFDN0IsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFBO1NBQ3JCO2FBQU07WUFDTCxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUE7U0FDckI7SUFDSCxDQUFDO0lBRU8sYUFBYTtRQUNuQixJQUFJLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQztRQUNkLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLEdBQUcsT0FBTyxDQUFDO0lBQ3hDLENBQUM7SUFFTyxhQUFhO1FBQ25CLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDO1FBRWQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsR0FBRyxPQUFPLENBQUM7SUFDeEMsQ0FBQztJQUVPLGFBQWE7UUFDbkIsSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUM7UUFFZCxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxHQUFHLFFBQVEsQ0FBQztJQUN6QyxDQUFDO0lBRU8sYUFBYTtRQUNuQixJQUFJLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQztRQUVkLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLEdBQUcsUUFBUSxDQUFDO0lBQ3pDLENBQUM7SUFFRCxRQUFRO1FBQ04sSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO0lBQ3pCLENBQUM7SUFFRCxXQUFXLENBQUMsT0FBc0I7UUFDaEMsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO0lBQ3pCLENBQUM7O3lIQW5MVSw0QkFBNEI7NkdBQTVCLDRCQUE0QixxSEFIN0IsRUFBRTsyRkFHRCw0QkFBNEI7a0JBTHhDLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtvQkFDOUIsUUFBUSxFQUFFLEVBQUU7b0JBQ1osTUFBTSxFQUFFLEVBQUU7aUJBQ1g7MEVBRVUsVUFBVTtzQkFBbEIsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XG4gIEFmdGVyQ29udGVudEluaXQsXG4gIENvbXBvbmVudCxcbiAgQ29udGVudENoaWxkcmVuLFxuICBJbnB1dCxcbiAgT25DaGFuZ2VzLFxuICBPbkluaXQsXG4gIFF1ZXJ5TGlzdCwgU2ltcGxlQ2hhbmdlcyxcbiAgVGVtcGxhdGVSZWZcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2FwcC1kYXNoYm9hcmQtZ3JpZCcsXG4gIHRlbXBsYXRlOiAnJyxcbiAgc3R5bGVzOiBbXVxufSlcbmV4cG9ydCBjbGFzcyBEYXNoYm9hcmRNZWRpYVF1ZXJ5Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkNoYW5nZXMge1xuICBASW5wdXQoKSBvZmZzZXRTaXplID0gMTkyO1xuICBwcml2YXRlIHNtYWxsOiBNZWRpYVF1ZXJ5TGlzdCA9IG1hdGNoTWVkaWEoYChtYXgtd2lkdGg6IDcwMHB4KWApO1xuICBwcml2YXRlIG1lZGl1bTogTWVkaWFRdWVyeUxpc3QgPSBtYXRjaE1lZGlhKGAobWF4LXdpZHRoOiAxMDAwcHgpYCk7XG4gIHByaXZhdGUgbGFyZ2U6IE1lZGlhUXVlcnlMaXN0ID0gbWF0Y2hNZWRpYShgKG1pbi13aWR0aDogMTcwMHB4KWApO1xuXG4gIGFic29sdXRlU3R5bGUgPSB7XG4gICAgJ3dpZHRoJzogXCI0MDBweFwiXG4gIH07XG5cbiAgY29sczogbnVtYmVyO1xuXG5cbiAgcHJpdmF0ZSBzbWFsbEV2ZW50TGlzdGVuZXI7XG4gIHByaXZhdGUgbWVkaXVtRXZlbnRMaXN0ZW5lcjtcbiAgcHJpdmF0ZSBiaWdFdmVudExpc3RlbmVyO1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICB9XG5cblxuXG4gIHByaXZhdGUgc2V0TWVkaWFRdWVyaWVzKCkge1xuICAgIGlmICghIXRoaXMuc21hbGxFdmVudExpc3RlbmVyKSB7XG4gICAgICBpZiAodGhpcy5zbWFsbC5yZW1vdmVFdmVudExpc3RlbmVyKSB7XG4gICAgICAgIHRoaXMuc21hbGwucmVtb3ZlRXZlbnRMaXN0ZW5lcihcImNoYW5nZVwiLCB0aGlzLnNtYWxsRXZlbnRMaXN0ZW5lcik7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLnNtYWxsLnJlbW92ZUxpc3RlbmVyKHRoaXMuc21hbGxFdmVudExpc3RlbmVyKVxuICAgICAgfVxuICAgIH1cbiAgICBpZiAoISF0aGlzLm1lZGl1bUV2ZW50TGlzdGVuZXIpIHtcbiAgICAgIGlmICh0aGlzLm1lZGl1bS5yZW1vdmVFdmVudExpc3RlbmVyKSB7XG4gICAgICAgIHRoaXMubWVkaXVtLnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJjaGFuZ2VcIiwgdGhpcy5tZWRpdW1FdmVudExpc3RlbmVyKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMubWVkaXVtLnJlbW92ZUxpc3RlbmVyKHRoaXMubWVkaXVtRXZlbnRMaXN0ZW5lcilcbiAgICAgIH1cbiAgICB9XG4gICAgaWYgKCEhdGhpcy5iaWdFdmVudExpc3RlbmVyKSB7XG4gICAgICBpZiAodGhpcy5sYXJnZS5yZW1vdmVFdmVudExpc3RlbmVyKSB7XG4gICAgICAgIHRoaXMubGFyZ2UucmVtb3ZlRXZlbnRMaXN0ZW5lcihcImNoYW5nZVwiLCB0aGlzLmJpZ0V2ZW50TGlzdGVuZXIpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5sYXJnZS5yZW1vdmVMaXN0ZW5lcih0aGlzLmJpZ0V2ZW50TGlzdGVuZXIpXG4gICAgICB9XG4gICAgfVxuICAgIGNvbnN0IHNtYWxsT2Zmc2V0ID0gdGhpcy5vZmZzZXRTaXplICsgNjU2ICsgMjY7XG4gICAgY29uc3QgbWVkaXVtT2Zmc2V0ID0gdGhpcy5vZmZzZXRTaXplICsgNjU2ICsgMjYgKyAxNiArIDMyMDtcbiAgICBjb25zdCBiaWdPZmZzZXQgPSBtZWRpdW1PZmZzZXQgKyAxNiArIDMyMDtcblxuICAgIHRoaXMuc21hbGwgPSBtYXRjaE1lZGlhKGAobWF4LXdpZHRoOiAke3NtYWxsT2Zmc2V0fXB4KWApO1xuICAgIHRoaXMubWVkaXVtID0gbWF0Y2hNZWRpYShgKG1heC13aWR0aDogJHttZWRpdW1PZmZzZXR9cHgpYCk7XG4gICAgdGhpcy5sYXJnZSA9IG1hdGNoTWVkaWEoYChtaW4td2lkdGg6ICR7YmlnT2Zmc2V0fXB4KWApO1xuXG5cbiAgICB0aGlzLnNtYWxsRXZlbnRMaXN0ZW5lciA9IChlKSA9PiB7XG4gICAgICBpZiAoZS5tYXRjaGVzKSB7XG4gICAgICAgIHRoaXMubWF0Y2gxQ29sdW1ucygpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgaWYgKHRoaXMubWVkaXVtLm1hdGNoZXMpIHtcbiAgICAgICAgICB0aGlzLm1hdGNoMkNvbHVtbnMoKVxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMubGFyZ2UubWF0Y2hlcykge1xuICAgICAgICAgIHRoaXMubWF0Y2g0Q29sdW1ucygpXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgLy8gY29uc29sZS5sb2coYG5vdCBzbWFsbCwgbm90IHdpZGVgICsgdGhpcy5sYXJnZS5tZWRpYSlcbiAgICAgICAgICB0aGlzLm1hdGNoM0NvbHVtbnMoKTtcbiAgICAgICAgfVxuICAgICAgICAvLyBjb25zb2xlLmxvZyhgVGhpcyBpcyBhIG5vdCBzbWFsbCBzY3JlZW4g4oCUIG1vcmUgdGhhbiAke3NtYWxsT2Zmc2V0fSB3aWRlLmArZS5tZWRpYSlcbiAgICAgIH1cbiAgICB9O1xuICAgIGlmICghIXRoaXMuc21hbGxFdmVudExpc3RlbmVyKSB7XG4gICAgICAvLyBjb25zb2xlLmxvZyhcImJlZm9yZSBhZGRpbmcgXCIsIHRoaXMuc21hbGxFdmVudExpc3RlbmVyKTtcbiAgICAgIGlmICh0aGlzLnNtYWxsLmFkZEV2ZW50TGlzdGVuZXIpIHtcbiAgICAgICAgdGhpcy5zbWFsbC5hZGRFdmVudExpc3RlbmVyKFwiY2hhbmdlXCIsIHRoaXMuc21hbGxFdmVudExpc3RlbmVyKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuc21hbGwuYWRkTGlzdGVuZXIodGhpcy5zbWFsbEV2ZW50TGlzdGVuZXIpO1xuICAgICAgfVxuXG4gICAgfVxuXG4gICAgdGhpcy5tZWRpdW1FdmVudExpc3RlbmVyID0gKGUpID0+IHtcbiAgICAgIC8vIGNvbnNvbGUubG9nKFwibWVkaXVtXCIpO1xuICAgICAgaWYgKGUubWF0Y2hlcykge1xuICAgICAgICBpZiAodGhpcy5zbWFsbC5tYXRjaGVzKSB7XG4gICAgICAgICAgdGhpcy5tYXRjaDFDb2x1bW5zKCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdGhpcy5tYXRjaDJDb2x1bW5zKCk7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGlmICh0aGlzLmxhcmdlLm1hdGNoZXMpIHtcbiAgICAgICAgICB0aGlzLm1hdGNoNENvbHVtbnMoKVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIC8vIGNvbnNvbGUubG9nKGBub3Qgc21hbGwsIG5vdCB3aWRlYCArIHRoaXMubGFyZ2UubWVkaWEpXG4gICAgICAgICAgdGhpcy5tYXRjaDNDb2x1bW5zKCk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9O1xuICAgIGlmICghIXRoaXMubWVkaXVtRXZlbnRMaXN0ZW5lcikge1xuICAgICAgLy8gY29uc29sZS5sb2coXCJiZWZvcmUgYWRkaW5nIFwiLCB0aGlzLnNtYWxsRXZlbnRMaXN0ZW5lcik7XG4gICAgICBpZiAodGhpcy5tZWRpdW0uYWRkRXZlbnRMaXN0ZW5lcikge1xuICAgICAgICB0aGlzLm1lZGl1bS5hZGRFdmVudExpc3RlbmVyKFwiY2hhbmdlXCIsIHRoaXMubWVkaXVtRXZlbnRMaXN0ZW5lcik7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLm1lZGl1bS5hZGRMaXN0ZW5lcih0aGlzLm1lZGl1bUV2ZW50TGlzdGVuZXIpO1xuICAgICAgfVxuXG4gICAgfVxuXG4gICAgLy8gY29uc29sZS5sb2coXCItLS0tIHNtYWxsRSBcIiwgdGhpcy5zbWFsbEV2ZW50TGlzdGVuZXIpO1xuXG4gICAgdGhpcy5iaWdFdmVudExpc3RlbmVyID0gKGUpID0+IHtcbiAgICAgIC8vIGNvbnNvbGUubG9nKFwiYmlnXCIpO1xuICAgICAgaWYgKGUubWF0Y2hlcykge1xuICAgICAgICAvLyBjb25zb2xlLmxvZyhgZXhhY3Qgd2lkZSBtYXRjaGArIGUubWVkaWEpXG4gICAgICAgIGlmICh0aGlzLmxhcmdlLm1hdGNoZXMpIHtcbiAgICAgICAgICB0aGlzLm1hdGNoNENvbHVtbnMoKVxuICAgICAgICB9IGVsc2Uge1xuXG4gICAgICAgICAgdGhpcy5tYXRjaDNDb2x1bW5zKCk7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGlmICh0aGlzLnNtYWxsLm1hdGNoZXMpIHtcbiAgICAgICAgICAvLyBjb25zb2xlLmxvZyhgbm90IHdpZGUsIGlzIHNtYWxsYCsgdGhpcy5zbWFsbC5tZWRpYSlcbiAgICAgICAgICB0aGlzLm1hdGNoMUNvbHVtbnMoKVxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMubWVkaXVtLm1hdGNoZXMpIHtcbiAgICAgICAgICB0aGlzLm1hdGNoMkNvbHVtbnMoKVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIC8vIGNvbnNvbGUubG9nKGBub3Qgd2lkZSwgbm90IHNtYWxsYCsgdGhpcy5zbWFsbC5tZWRpYSlcbiAgICAgICAgICB0aGlzLm1hdGNoM0NvbHVtbnMoKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH07XG4gICAgaWYgKCEhdGhpcy5iaWdFdmVudExpc3RlbmVyKSB7XG4gICAgICBpZiAodGhpcy5sYXJnZS5hZGRFdmVudExpc3RlbmVyKSB7XG4gICAgICAgIHRoaXMubGFyZ2UuYWRkRXZlbnRMaXN0ZW5lcihcImNoYW5nZVwiLCB0aGlzLmJpZ0V2ZW50TGlzdGVuZXIpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5sYXJnZS5hZGRMaXN0ZW5lcih0aGlzLmJpZ0V2ZW50TGlzdGVuZXIpO1xuICAgICAgfVxuXG4gICAgfVxuXG4gICAgaWYgKHRoaXMuc21hbGwubWF0Y2hlcykge1xuXG4gICAgICB0aGlzLm1hdGNoMUNvbHVtbnMoKVxuICAgIH0gZWxzZSBpZiAodGhpcy5tZWRpdW0ubWF0Y2hlcykge1xuICAgICAgdGhpcy5tYXRjaDJDb2x1bW5zKClcbiAgICB9IGVsc2UgaWYgKHRoaXMubGFyZ2UubWF0Y2hlcykge1xuICAgICAgdGhpcy5tYXRjaDRDb2x1bW5zKClcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5tYXRjaDNDb2x1bW5zKClcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIG1hdGNoMUNvbHVtbnMoKSB7XG4gICAgdGhpcy5jb2xzID0gMTtcbiAgICB0aGlzLmFic29sdXRlU3R5bGVbJ3dpZHRoJ10gPSAnMzMxcHgnO1xuICB9XG5cbiAgcHJpdmF0ZSBtYXRjaDJDb2x1bW5zKCkge1xuICAgIHRoaXMuY29scyA9IDI7XG5cbiAgICB0aGlzLmFic29sdXRlU3R5bGVbJ3dpZHRoJ10gPSAnNjY3cHgnO1xuICB9XG5cbiAgcHJpdmF0ZSBtYXRjaDNDb2x1bW5zKCkge1xuICAgIHRoaXMuY29scyA9IDM7XG5cbiAgICB0aGlzLmFic29sdXRlU3R5bGVbJ3dpZHRoJ10gPSAnMTAwM3B4JztcbiAgfVxuXG4gIHByaXZhdGUgbWF0Y2g0Q29sdW1ucygpIHtcbiAgICB0aGlzLmNvbHMgPSA0O1xuXG4gICAgdGhpcy5hYnNvbHV0ZVN0eWxlWyd3aWR0aCddID0gJzEzMzlweCc7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLnNldE1lZGlhUXVlcmllcygpO1xuICB9XG5cbiAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcyk6IHZvaWQge1xuICAgIHRoaXMuc2V0TWVkaWFRdWVyaWVzKCk7XG4gIH1cblxufVxuIl19