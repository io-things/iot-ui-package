import { Component, EventEmitter, Input, Output } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@ngx-translate/core";
import * as i2 from "@angular/material/button";
import * as i3 from "@angular/material/icon";
import * as i4 from "@angular/material/menu";
import * as i5 from "@angular/material/tooltip";
import * as i6 from "@angular/common";
export class LanguageSelectorComponent {
    constructor(translate) {
        this.translate = translate;
        this.lang = 'en';
        this.langList = [];
        this.setLang = new EventEmitter();
        translate.setDefaultLang(this.lang);
    }
    ngOnInit() {
    }
    selectLang(langKey) {
        this.translate.use(langKey);
        this.setLang.emit(langKey);
    }
    ngOnChanges(changes) {
        if (changes.lang) {
            this.translate.setDefaultLang(changes.lang.currentValue);
        }
    }
}
LanguageSelectorComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: LanguageSelectorComponent, deps: [{ token: i1.TranslateService }], target: i0.ɵɵFactoryTarget.Component });
LanguageSelectorComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: LanguageSelectorComponent, selector: "iot-language-selector", inputs: { lang: "lang", langList: "langList" }, outputs: { setLang: "setLang" }, usesOnChanges: true, ngImport: i0, template: `
    <button mat-icon-button
            [mat-menu-trigger-for]="themeMenu"
            matTooltip="Select a language!"
            tabindex="-1">
      <mat-icon>language</mat-icon>
    </button>


    <mat-menu #themeMenu="matMenu" x-position="before">
      <button
        *ngFor="let langEntry of langList"
        mat-menu-item
        (click)="selectLang(langEntry.key)">
        {{langEntry.name | translate}}
      </button>
      <!--      <button mat-menu-item-->
      <!--              (click)="selectLang('nl')">-->
      <!--        {{'CONFIG.LANG.NL'}}-->
      <!--      </button>-->
    </mat-menu>
  `, isInline: true, components: [{ type: i2.MatButton, selector: "button[mat-button], button[mat-raised-button], button[mat-icon-button],             button[mat-fab], button[mat-mini-fab], button[mat-stroked-button],             button[mat-flat-button]", inputs: ["disabled", "disableRipple", "color"], exportAs: ["matButton"] }, { type: i3.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }, { type: i4.MatMenu, selector: "mat-menu", exportAs: ["matMenu"] }, { type: i4.MatMenuItem, selector: "[mat-menu-item]", inputs: ["disabled", "disableRipple", "role"], exportAs: ["matMenuItem"] }], directives: [{ type: i5.MatTooltip, selector: "[matTooltip]", exportAs: ["matTooltip"] }, { type: i4.MatMenuTrigger, selector: "[mat-menu-trigger-for], [matMenuTriggerFor]", exportAs: ["matMenuTrigger"] }, { type: i6.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }], pipes: { "translate": i1.TranslatePipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: LanguageSelectorComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-language-selector',
                    template: `
    <button mat-icon-button
            [mat-menu-trigger-for]="themeMenu"
            matTooltip="Select a language!"
            tabindex="-1">
      <mat-icon>language</mat-icon>
    </button>


    <mat-menu #themeMenu="matMenu" x-position="before">
      <button
        *ngFor="let langEntry of langList"
        mat-menu-item
        (click)="selectLang(langEntry.key)">
        {{langEntry.name | translate}}
      </button>
      <!--      <button mat-menu-item-->
      <!--              (click)="selectLang('nl')">-->
      <!--        {{'CONFIG.LANG.NL'}}-->
      <!--      </button>-->
    </mat-menu>
  `,
                    styles: []
                }]
        }], ctorParameters: function () { return [{ type: i1.TranslateService }]; }, propDecorators: { lang: [{
                type: Input
            }], langList: [{
                type: Input
            }], setLang: [{
                type: Output
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGFuZ3VhZ2Utc2VsZWN0b3IuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvaW90LXVpLWxpYi9zcmMvbGliL2xhbmcvbGFuZ3VhZ2Utc2VsZWN0b3IuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBcUIsTUFBTSxFQUFnQixNQUFNLGVBQWUsQ0FBQzs7Ozs7Ozs7QUE2QnZHLE1BQU0sT0FBTyx5QkFBeUI7SUFLcEMsWUFBb0IsU0FBMkI7UUFBM0IsY0FBUyxHQUFULFNBQVMsQ0FBa0I7UUFKdEMsU0FBSSxHQUFXLElBQUksQ0FBQztRQUNwQixhQUFRLEdBQW9DLEVBQUUsQ0FBQztRQUM5QyxZQUFPLEdBQUcsSUFBSSxZQUFZLEVBQVUsQ0FBQztRQUc3QyxTQUFTLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN0QyxDQUFDO0lBRUQsUUFBUTtJQUNSLENBQUM7SUFFRCxVQUFVLENBQUMsT0FBZTtRQUV4QixJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUM1QixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUM3QixDQUFDO0lBRUQsV0FBVyxDQUFDLE9BQXNCO1FBQ2hDLElBQUksT0FBTyxDQUFDLElBQUksRUFBRTtZQUNoQixJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1NBQzFEO0lBQ0gsQ0FBQzs7c0hBdEJVLHlCQUF5QjswR0FBekIseUJBQXlCLG1LQXhCMUI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQXFCVDsyRkFHVSx5QkFBeUI7a0JBMUJyQyxTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSx1QkFBdUI7b0JBQ2pDLFFBQVEsRUFBRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0dBcUJUO29CQUNELE1BQU0sRUFBRSxFQUFFO2lCQUNYO3VHQUVVLElBQUk7c0JBQVosS0FBSztnQkFDRyxRQUFRO3NCQUFoQixLQUFLO2dCQUNJLE9BQU87c0JBQWhCLE1BQU0iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25DaGFuZ2VzLCBPbkluaXQsIE91dHB1dCwgU2ltcGxlQ2hhbmdlc30gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge1RyYW5zbGF0ZVNlcnZpY2V9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdpb3QtbGFuZ3VhZ2Utc2VsZWN0b3InLFxuICB0ZW1wbGF0ZTogYFxuICAgIDxidXR0b24gbWF0LWljb24tYnV0dG9uXG4gICAgICAgICAgICBbbWF0LW1lbnUtdHJpZ2dlci1mb3JdPVwidGhlbWVNZW51XCJcbiAgICAgICAgICAgIG1hdFRvb2x0aXA9XCJTZWxlY3QgYSBsYW5ndWFnZSFcIlxuICAgICAgICAgICAgdGFiaW5kZXg9XCItMVwiPlxuICAgICAgPG1hdC1pY29uPmxhbmd1YWdlPC9tYXQtaWNvbj5cbiAgICA8L2J1dHRvbj5cblxuXG4gICAgPG1hdC1tZW51ICN0aGVtZU1lbnU9XCJtYXRNZW51XCIgeC1wb3NpdGlvbj1cImJlZm9yZVwiPlxuICAgICAgPGJ1dHRvblxuICAgICAgICAqbmdGb3I9XCJsZXQgbGFuZ0VudHJ5IG9mIGxhbmdMaXN0XCJcbiAgICAgICAgbWF0LW1lbnUtaXRlbVxuICAgICAgICAoY2xpY2spPVwic2VsZWN0TGFuZyhsYW5nRW50cnkua2V5KVwiPlxuICAgICAgICB7e2xhbmdFbnRyeS5uYW1lIHwgdHJhbnNsYXRlfX1cbiAgICAgIDwvYnV0dG9uPlxuICAgICAgPCEtLSAgICAgIDxidXR0b24gbWF0LW1lbnUtaXRlbS0tPlxuICAgICAgPCEtLSAgICAgICAgICAgICAgKGNsaWNrKT1cInNlbGVjdExhbmcoJ25sJylcIj4tLT5cbiAgICAgIDwhLS0gICAgICAgIHt7J0NPTkZJRy5MQU5HLk5MJ319LS0+XG4gICAgICA8IS0tICAgICAgPC9idXR0b24+LS0+XG4gICAgPC9tYXQtbWVudT5cbiAgYCxcbiAgc3R5bGVzOiBbXVxufSlcbmV4cG9ydCBjbGFzcyBMYW5ndWFnZVNlbGVjdG9yQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkNoYW5nZXMge1xuICBASW5wdXQoKSBsYW5nOiBzdHJpbmcgPSAnZW4nO1xuICBASW5wdXQoKSBsYW5nTGlzdDogeyBrZXk6IHN0cmluZywgbmFtZTogc3RyaW5nIH1bXSA9IFtdO1xuICBAT3V0cHV0KCkgc2V0TGFuZyA9IG5ldyBFdmVudEVtaXR0ZXI8c3RyaW5nPigpO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgdHJhbnNsYXRlOiBUcmFuc2xhdGVTZXJ2aWNlKSB7XG4gICAgdHJhbnNsYXRlLnNldERlZmF1bHRMYW5nKHRoaXMubGFuZyk7XG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgfVxuXG4gIHNlbGVjdExhbmcobGFuZ0tleTogc3RyaW5nKSB7XG5cbiAgICB0aGlzLnRyYW5zbGF0ZS51c2UobGFuZ0tleSk7XG4gICAgdGhpcy5zZXRMYW5nLmVtaXQobGFuZ0tleSk7XG4gIH1cblxuICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKTogdm9pZCB7XG4gICAgaWYgKGNoYW5nZXMubGFuZykge1xuICAgICAgdGhpcy50cmFuc2xhdGUuc2V0RGVmYXVsdExhbmcoY2hhbmdlcy5sYW5nLmN1cnJlbnRWYWx1ZSk7XG4gICAgfVxuICB9XG59XG4iXX0=