import { Component, Input } from "@angular/core";
import * as moment from 'moment';
import * as i0 from "@angular/core";
import * as i1 from "../dashboard/dashboard-card.component";
import * as i2 from "@angular/common";
import * as i3 from "@angular/material/tooltip";
import * as i4 from "@ngx-translate/core";
export class TimeOfDayCardComponent {
    constructor() {
        this.title = '';
        this.subject = "PIR Bewegingen";
        this.min = 10;
        this.highest = 5;
        this.today = ['', '', '', '', '', '', ''];
    }
    ngOnInit() {
        let now = moment();
        for (let i = 0; i < 7; i++) {
            this.today[i] = now.format("dd");
            now = now.subtract(1, 'days');
        }
    }
    getTooltip(hour, week) {
        return this.data[week][hour] + " " + this.subject;
    }
    getClass(hour, week) {
        let classRet = { 'cell': true };
        if (!this.data) {
            classRet['grey-1'] = true;
        }
        else {
            if (this.data[week][hour] == 0) {
                classRet['grey-1'] = true;
            }
            else if (this.data[week][hour] < this.val2) {
                classRet['blue-1'] = true;
            }
            else if (this.data[week][hour] < this.val3) {
                classRet['blue-2'] = true;
            }
            else if (this.data[week][hour] < this.val4) {
                classRet['blue-3'] = true;
            }
            else if (this.data[week][hour] <= this.val5) {
                classRet['blue-4'] = true;
            }
        }
        return classRet;
    }
    ngOnChanges(changes) {
        this.lowest = null;
        this.highest = null;
        if (this.data != null) {
            this.data.map(row => row.map(cell => {
                if (cell != 0) {
                    if (this.lowest == null)
                        this.lowest = cell;
                    this.lowest = Math.min(this.lowest, cell);
                    this.highest = Math.max(this.highest, cell);
                }
            }));
        }
        if (this.lowest == null) {
            this.lowest = 1;
        }
        this.lowest = Math.floor(this.lowest);
        this.highest = Math.ceil(this.highest);
        this.highest = Math.max(this.highest, this.min);
        if (this.lowest > 10) {
            this.lowest = Math.floor(this.lowest / 10) * 10;
        }
        const diff = this.highest - this.lowest;
        let step = Math.ceil(diff / 4);
        if (step > 10) {
            step = Math.ceil(step / 10) * 10;
        }
        if (step === 0) {
            step = 1;
        }
        this.val1 = this.lowest;
        this.val2 = this.val1 + step;
        this.val3 = this.val2 + step;
        this.val4 = this.val3 + step;
        this.val5 = this.val4 + step;
    }
    showTooltip(evt, test) {
    }
}
TimeOfDayCardComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: TimeOfDayCardComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
TimeOfDayCardComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: TimeOfDayCardComponent, selector: "iot-time-of-day-card", inputs: { title: "title", outerTitle: "outerTitle", subject: "subject", data: "data", min: "min" }, usesOnChanges: true, ngImport: i0, template: `
    <iot-dashboard-card
      class="time-of-day"
      [colspan]="1"
      [outerTitle]="outerTitle">
      <div  iot-card-header padding="true">{{ title | translate}}</div>
      <div  iot-card-content padding="true">
        <div class="ga-time-of-day-chart" #thisDiv>
          <svg width="272" height="404">
            <g class="header"></g>
            <g class="grid-container" transform="translate(0, 0)">
              <g class="x-axis" transform="translate(0, 322)" fill="none" font-size="10" font-family="sans-serif"
                 text-anchor="middle">
                <path class="domain" stroke="currentColor" d="M0.5,6V0.5H232.5V6"></path>
                <g class="tick" opacity="1" transform="translate(16.57142857142856,0)">
                  <line stroke="currentColor" y2="6"></line>
                  <text fill="currentColor" y="9" dy="0.71em" transform="translate(-13.571428571428573, 0)"
                        style="text-anchor: start;">{{today[6]}}
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(49.71428571428571,0)">
                  <line stroke="currentColor" y2="6"></line>
                  <text fill="currentColor" y="9" dy="0.71em" transform="translate(-13.571428571428573, 0)"
                        style="text-anchor: start;">{{today[5]}}
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(82.85714285714285,0)">
                  <line stroke="currentColor" y2="6"></line>
                  <text fill="currentColor" y="9" dy="0.71em" transform="translate(-13.571428571428573, 0)"
                        style="text-anchor: start;">{{today[4]}}
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(116,0)">
                  <line stroke="currentColor" y2="6"></line>
                  <text fill="currentColor" y="9" dy="0.71em" transform="translate(-13.571428571428573, 0)"
                        style="text-anchor: start;">{{today[3]}}
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(149.14285714285714,0)">
                  <line stroke="currentColor" y2="6"></line>
                  <text fill="currentColor" y="9" dy="0.71em" transform="translate(-13.571428571428573, 0)"
                        style="text-anchor: start;">{{today[2]}}
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(182.2857142857143,0)">
                  <line stroke="currentColor" y2="6"></line>
                  <text fill="currentColor" y="9" dy="0.71em" transform="translate(-13.571428571428573, 0)"
                        style="text-anchor: start;">{{today[1]}}
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(215.42857142857147,0)">
                  <line stroke="currentColor" y2="6"></line>
                  <text fill="currentColor" y="9" dy="0.71em" transform="translate(-13.571428571428573, 0)"
                        style="text-anchor: start;">{{today[0]}}
                  </text>
                </g>
              </g>
              <g class="y-axis" transform="translate(232, 0)" fill="none" font-size="10" font-family="sans-serif"
                 text-anchor="start">
                <path class="domain" stroke="currentColor" d="M6,0.5H0.5V324.5H6"></path>
                <g class="tick" opacity="1" transform="translate(0, 6.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">12a.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 33.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">2a.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 60.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">4a.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 87.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">6a.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 114.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">8a.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 141.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">10a.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 168.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">12p.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 195.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">2p.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 222.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">4p.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 249.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">6p.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 276.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">8p.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 303.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">10p.m.
                  </text>
                </g>
              </g>
              <g class="row " *ngFor="let item of [].constructor(24); let i = index;">
                <g [ngClass]="getClass(i, w)"
                   *ngFor="let week of [].constructor(7); let w = index;">
                  <rect
                    [matTooltip]="getTooltip(i,w)"
                    class="rect" width="30.14285659790039"
                    height="10.5" [attr.x]="1.5+(w * 33.15)"
                    [attr.y]="1.5+ (i * 13.5)"
                  ></rect>
                </g>

              </g>

            </g>
            <g class="legend-group" transform="translate(0, 359)">
              <rect height="10" width="55" x="0" y="0" class="legend-block blue-1"></rect>
              <rect height="10" width="55" x="58" y="0" class="legend-block blue-2"></rect>
              <rect height="10" width="55" x="116" y="0" class="legend-block blue-3"></rect>
              <rect height="10" width="55" x="174" y="0" class="legend-block blue-4"></rect>
              <text x="0" y="25" class="legend-text" style="text-anchor: start;">{{val1}}</text>
              <text x="58" y="25" class="legend-text" style="text-anchor: middle;">{{val2}}</text>
              <text x="116" y="25" class="legend-text" style="text-anchor: middle;">{{val3}}</text>
              <text x="174" y="25" class="legend-text" style="text-anchor: middle;">{{val4}}</text>
              <text x="232" y="25" class="legend-text" style="text-anchor: end;">{{val5}}</text>
            </g>
          </svg>
        </div>

      </div>
      <div  iot-card-footer><ng-content select="[iot-card-footer]"></ng-content></div>
    </iot-dashboard-card>
  `, isInline: true, components: [{ type: i1.DashboardCardComponent, selector: "iot-dashboard-card", inputs: ["colspan", "showHeader", "outerTitle", "innerTitle", "showTitle", "height", "width"] }], directives: [{ type: i1.IotCardHeader, selector: "[iot-card-header]", inputs: ["padding"] }, { type: i1.IotCardContent, selector: "[iot-card-content]", inputs: ["padding"] }, { type: i2.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { type: i2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i3.MatTooltip, selector: "[matTooltip]", exportAs: ["matTooltip"] }, { type: i1.IotCardFooter, selector: "[iot-card-footer]", inputs: ["padding"] }], pipes: { "translate": i4.TranslatePipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: TimeOfDayCardComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-time-of-day-card',
                    template: `
    <iot-dashboard-card
      class="time-of-day"
      [colspan]="1"
      [outerTitle]="outerTitle">
      <div  iot-card-header padding="true">{{ title | translate}}</div>
      <div  iot-card-content padding="true">
        <div class="ga-time-of-day-chart" #thisDiv>
          <svg width="272" height="404">
            <g class="header"></g>
            <g class="grid-container" transform="translate(0, 0)">
              <g class="x-axis" transform="translate(0, 322)" fill="none" font-size="10" font-family="sans-serif"
                 text-anchor="middle">
                <path class="domain" stroke="currentColor" d="M0.5,6V0.5H232.5V6"></path>
                <g class="tick" opacity="1" transform="translate(16.57142857142856,0)">
                  <line stroke="currentColor" y2="6"></line>
                  <text fill="currentColor" y="9" dy="0.71em" transform="translate(-13.571428571428573, 0)"
                        style="text-anchor: start;">{{today[6]}}
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(49.71428571428571,0)">
                  <line stroke="currentColor" y2="6"></line>
                  <text fill="currentColor" y="9" dy="0.71em" transform="translate(-13.571428571428573, 0)"
                        style="text-anchor: start;">{{today[5]}}
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(82.85714285714285,0)">
                  <line stroke="currentColor" y2="6"></line>
                  <text fill="currentColor" y="9" dy="0.71em" transform="translate(-13.571428571428573, 0)"
                        style="text-anchor: start;">{{today[4]}}
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(116,0)">
                  <line stroke="currentColor" y2="6"></line>
                  <text fill="currentColor" y="9" dy="0.71em" transform="translate(-13.571428571428573, 0)"
                        style="text-anchor: start;">{{today[3]}}
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(149.14285714285714,0)">
                  <line stroke="currentColor" y2="6"></line>
                  <text fill="currentColor" y="9" dy="0.71em" transform="translate(-13.571428571428573, 0)"
                        style="text-anchor: start;">{{today[2]}}
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(182.2857142857143,0)">
                  <line stroke="currentColor" y2="6"></line>
                  <text fill="currentColor" y="9" dy="0.71em" transform="translate(-13.571428571428573, 0)"
                        style="text-anchor: start;">{{today[1]}}
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(215.42857142857147,0)">
                  <line stroke="currentColor" y2="6"></line>
                  <text fill="currentColor" y="9" dy="0.71em" transform="translate(-13.571428571428573, 0)"
                        style="text-anchor: start;">{{today[0]}}
                  </text>
                </g>
              </g>
              <g class="y-axis" transform="translate(232, 0)" fill="none" font-size="10" font-family="sans-serif"
                 text-anchor="start">
                <path class="domain" stroke="currentColor" d="M6,0.5H0.5V324.5H6"></path>
                <g class="tick" opacity="1" transform="translate(0, 6.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">12a.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 33.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">2a.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 60.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">4a.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 87.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">6a.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 114.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">8a.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 141.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">10a.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 168.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">12p.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 195.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">2p.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 222.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">4p.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 249.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">6p.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 276.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">8p.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 303.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">10p.m.
                  </text>
                </g>
              </g>
              <g class="row " *ngFor="let item of [].constructor(24); let i = index;">
                <g [ngClass]="getClass(i, w)"
                   *ngFor="let week of [].constructor(7); let w = index;">
                  <rect
                    [matTooltip]="getTooltip(i,w)"
                    class="rect" width="30.14285659790039"
                    height="10.5" [attr.x]="1.5+(w * 33.15)"
                    [attr.y]="1.5+ (i * 13.5)"
                  ></rect>
                </g>

              </g>

            </g>
            <g class="legend-group" transform="translate(0, 359)">
              <rect height="10" width="55" x="0" y="0" class="legend-block blue-1"></rect>
              <rect height="10" width="55" x="58" y="0" class="legend-block blue-2"></rect>
              <rect height="10" width="55" x="116" y="0" class="legend-block blue-3"></rect>
              <rect height="10" width="55" x="174" y="0" class="legend-block blue-4"></rect>
              <text x="0" y="25" class="legend-text" style="text-anchor: start;">{{val1}}</text>
              <text x="58" y="25" class="legend-text" style="text-anchor: middle;">{{val2}}</text>
              <text x="116" y="25" class="legend-text" style="text-anchor: middle;">{{val3}}</text>
              <text x="174" y="25" class="legend-text" style="text-anchor: middle;">{{val4}}</text>
              <text x="232" y="25" class="legend-text" style="text-anchor: end;">{{val5}}</text>
            </g>
          </svg>
        </div>

      </div>
      <div  iot-card-footer><ng-content select="[iot-card-footer]"></ng-content></div>
    </iot-dashboard-card>
  `
                }]
        }], ctorParameters: function () { return []; }, propDecorators: { title: [{
                type: Input
            }], outerTitle: [{
                type: Input
            }], subject: [{
                type: Input
            }], data: [{
                type: Input
            }], min: [{
                type: Input
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGltZS1vZi1kYXktY2FyZC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9pb3QtdWktbGliL3NyYy9saWIvY2FyZHMvdGltZS1vZi1kYXktY2FyZC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUNMLFNBQVMsRUFFVCxLQUFLLEVBTU4sTUFBTSxlQUFlLENBQUM7QUFDdkIsT0FBTyxLQUFLLE1BQU0sTUFBTSxRQUFRLENBQUM7Ozs7OztBQTBLakMsTUFBTSxPQUFPLHNCQUFzQjtJQWlCakM7UUFmUyxVQUFLLEdBQUcsRUFBRSxDQUFDO1FBRVgsWUFBTyxHQUFHLGdCQUFnQixDQUFDO1FBRTNCLFFBQUcsR0FBRyxFQUFFLENBQUM7UUFHbEIsWUFBTyxHQUFHLENBQUMsQ0FBQztRQVlaLFVBQUssR0FBYSxDQUFDLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBRi9DLENBQUM7SUFJRCxRQUFRO1FBR04sSUFBSSxHQUFHLEdBQUcsTUFBTSxFQUFFLENBQUM7UUFDbkIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUMxQixJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDakMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1NBQy9CO0lBQ0gsQ0FBQztJQUVELFVBQVUsQ0FBQyxJQUFZLEVBQUUsSUFBWTtRQUNuQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7SUFDcEQsQ0FBQztJQUVELFFBQVEsQ0FBQyxJQUFZLEVBQUUsSUFBWTtRQUNqQyxJQUFJLFFBQVEsR0FBRyxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUMsQ0FBQTtRQUM3QixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRTtZQUNkLFFBQVEsQ0FBQyxRQUFRLENBQUMsR0FBRyxJQUFJLENBQUM7U0FDM0I7YUFBTTtZQUNMLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQzlCLFFBQVEsQ0FBQyxRQUFRLENBQUMsR0FBRyxJQUFJLENBQUM7YUFDM0I7aUJBQU0sSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLEVBQUU7Z0JBQzVDLFFBQVEsQ0FBQyxRQUFRLENBQUMsR0FBRyxJQUFJLENBQUM7YUFDM0I7aUJBQU0sSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLEVBQUU7Z0JBQzVDLFFBQVEsQ0FBQyxRQUFRLENBQUMsR0FBRyxJQUFJLENBQUM7YUFDM0I7aUJBQU0sSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLEVBQUU7Z0JBQzVDLFFBQVEsQ0FBQyxRQUFRLENBQUMsR0FBRyxJQUFJLENBQUM7YUFDM0I7aUJBQU0sSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUU7Z0JBQzdDLFFBQVEsQ0FBQyxRQUFRLENBQUMsR0FBRyxJQUFJLENBQUM7YUFDM0I7U0FDRjtRQUNELE9BQU8sUUFBUSxDQUFDO0lBQ2xCLENBQUM7SUFFRCxXQUFXLENBQUMsT0FBc0I7UUFFaEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDbkIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDcEIsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksRUFBRTtZQUNyQixJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ2xDLElBQUksSUFBSSxJQUFJLENBQUMsRUFBRTtvQkFDYixJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSTt3QkFBRSxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztvQkFDNUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7b0JBQzFDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDO2lCQUM3QztZQUVILENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDTDtRQUVELElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLEVBQUU7WUFDdkIsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7U0FDakI7UUFFRCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRXRDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDdkMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBRWhELElBQUksSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLEVBQUU7WUFDcEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFBO1NBQ2hEO1FBQ0QsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3hDLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQy9CLElBQUksSUFBSSxHQUFHLEVBQUUsRUFBRTtZQUNiLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUE7U0FDakM7UUFDRCxJQUFJLElBQUksS0FBSyxDQUFDLEVBQUU7WUFDZCxJQUFJLEdBQUcsQ0FBQyxDQUFDO1NBQ1Y7UUFDRCxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDeEIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztRQUM3QixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQzdCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFDN0IsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztJQUUvQixDQUFDO0lBR0QsV0FBVyxDQUFDLEdBQVEsRUFBRSxJQUFTO0lBRS9CLENBQUM7O21IQXZHVSxzQkFBc0I7dUdBQXRCLHNCQUFzQixxTEF0S3ZCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQW9LVDsyRkFFVSxzQkFBc0I7a0JBeEtsQyxTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxzQkFBc0I7b0JBQ2hDLFFBQVEsRUFBRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7R0FvS1Q7aUJBQ0Y7MEVBR1UsS0FBSztzQkFBYixLQUFLO2dCQUNHLFVBQVU7c0JBQWxCLEtBQUs7Z0JBQ0csT0FBTztzQkFBZixLQUFLO2dCQUNHLElBQUk7c0JBQVosS0FBSztnQkFDRyxHQUFHO3NCQUFYLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuICBDb21wb25lbnQsXG4gIENvbnRlbnRDaGlsZCxcbiAgSW5wdXQsXG4gIE9uQ2hhbmdlcyxcbiAgT25Jbml0LFxuICBTaW1wbGVDaGFuZ2VzLFxuICBUZW1wbGF0ZVJlZixcbiAgVmlld0VuY2Fwc3VsYXRpb25cbn0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCAqIGFzIG1vbWVudCBmcm9tICdtb21lbnQnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdpb3QtdGltZS1vZi1kYXktY2FyZCcsXG4gIHRlbXBsYXRlOiBgXG4gICAgPGlvdC1kYXNoYm9hcmQtY2FyZFxuICAgICAgY2xhc3M9XCJ0aW1lLW9mLWRheVwiXG4gICAgICBbY29sc3Bhbl09XCIxXCJcbiAgICAgIFtvdXRlclRpdGxlXT1cIm91dGVyVGl0bGVcIj5cbiAgICAgIDxkaXYgIGlvdC1jYXJkLWhlYWRlciBwYWRkaW5nPVwidHJ1ZVwiPnt7IHRpdGxlIHwgdHJhbnNsYXRlfX08L2Rpdj5cbiAgICAgIDxkaXYgIGlvdC1jYXJkLWNvbnRlbnQgcGFkZGluZz1cInRydWVcIj5cbiAgICAgICAgPGRpdiBjbGFzcz1cImdhLXRpbWUtb2YtZGF5LWNoYXJ0XCIgI3RoaXNEaXY+XG4gICAgICAgICAgPHN2ZyB3aWR0aD1cIjI3MlwiIGhlaWdodD1cIjQwNFwiPlxuICAgICAgICAgICAgPGcgY2xhc3M9XCJoZWFkZXJcIj48L2c+XG4gICAgICAgICAgICA8ZyBjbGFzcz1cImdyaWQtY29udGFpbmVyXCIgdHJhbnNmb3JtPVwidHJhbnNsYXRlKDAsIDApXCI+XG4gICAgICAgICAgICAgIDxnIGNsYXNzPVwieC1heGlzXCIgdHJhbnNmb3JtPVwidHJhbnNsYXRlKDAsIDMyMilcIiBmaWxsPVwibm9uZVwiIGZvbnQtc2l6ZT1cIjEwXCIgZm9udC1mYW1pbHk9XCJzYW5zLXNlcmlmXCJcbiAgICAgICAgICAgICAgICAgdGV4dC1hbmNob3I9XCJtaWRkbGVcIj5cbiAgICAgICAgICAgICAgICA8cGF0aCBjbGFzcz1cImRvbWFpblwiIHN0cm9rZT1cImN1cnJlbnRDb2xvclwiIGQ9XCJNMC41LDZWMC41SDIzMi41VjZcIj48L3BhdGg+XG4gICAgICAgICAgICAgICAgPGcgY2xhc3M9XCJ0aWNrXCIgb3BhY2l0eT1cIjFcIiB0cmFuc2Zvcm09XCJ0cmFuc2xhdGUoMTYuNTcxNDI4NTcxNDI4NTYsMClcIj5cbiAgICAgICAgICAgICAgICAgIDxsaW5lIHN0cm9rZT1cImN1cnJlbnRDb2xvclwiIHkyPVwiNlwiPjwvbGluZT5cbiAgICAgICAgICAgICAgICAgIDx0ZXh0IGZpbGw9XCJjdXJyZW50Q29sb3JcIiB5PVwiOVwiIGR5PVwiMC43MWVtXCIgdHJhbnNmb3JtPVwidHJhbnNsYXRlKC0xMy41NzE0Mjg1NzE0Mjg1NzMsIDApXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlPVwidGV4dC1hbmNob3I6IHN0YXJ0O1wiPnt7dG9kYXlbNl19fVxuICAgICAgICAgICAgICAgICAgPC90ZXh0PlxuICAgICAgICAgICAgICAgIDwvZz5cbiAgICAgICAgICAgICAgICA8ZyBjbGFzcz1cInRpY2tcIiBvcGFjaXR5PVwiMVwiIHRyYW5zZm9ybT1cInRyYW5zbGF0ZSg0OS43MTQyODU3MTQyODU3MSwwKVwiPlxuICAgICAgICAgICAgICAgICAgPGxpbmUgc3Ryb2tlPVwiY3VycmVudENvbG9yXCIgeTI9XCI2XCI+PC9saW5lPlxuICAgICAgICAgICAgICAgICAgPHRleHQgZmlsbD1cImN1cnJlbnRDb2xvclwiIHk9XCI5XCIgZHk9XCIwLjcxZW1cIiB0cmFuc2Zvcm09XCJ0cmFuc2xhdGUoLTEzLjU3MTQyODU3MTQyODU3MywgMClcIlxuICAgICAgICAgICAgICAgICAgICAgICAgc3R5bGU9XCJ0ZXh0LWFuY2hvcjogc3RhcnQ7XCI+e3t0b2RheVs1XX19XG4gICAgICAgICAgICAgICAgICA8L3RleHQ+XG4gICAgICAgICAgICAgICAgPC9nPlxuICAgICAgICAgICAgICAgIDxnIGNsYXNzPVwidGlja1wiIG9wYWNpdHk9XCIxXCIgdHJhbnNmb3JtPVwidHJhbnNsYXRlKDgyLjg1NzE0Mjg1NzE0Mjg1LDApXCI+XG4gICAgICAgICAgICAgICAgICA8bGluZSBzdHJva2U9XCJjdXJyZW50Q29sb3JcIiB5Mj1cIjZcIj48L2xpbmU+XG4gICAgICAgICAgICAgICAgICA8dGV4dCBmaWxsPVwiY3VycmVudENvbG9yXCIgeT1cIjlcIiBkeT1cIjAuNzFlbVwiIHRyYW5zZm9ybT1cInRyYW5zbGF0ZSgtMTMuNTcxNDI4NTcxNDI4NTczLCAwKVwiXG4gICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT1cInRleHQtYW5jaG9yOiBzdGFydDtcIj57e3RvZGF5WzRdfX1cbiAgICAgICAgICAgICAgICAgIDwvdGV4dD5cbiAgICAgICAgICAgICAgICA8L2c+XG4gICAgICAgICAgICAgICAgPGcgY2xhc3M9XCJ0aWNrXCIgb3BhY2l0eT1cIjFcIiB0cmFuc2Zvcm09XCJ0cmFuc2xhdGUoMTE2LDApXCI+XG4gICAgICAgICAgICAgICAgICA8bGluZSBzdHJva2U9XCJjdXJyZW50Q29sb3JcIiB5Mj1cIjZcIj48L2xpbmU+XG4gICAgICAgICAgICAgICAgICA8dGV4dCBmaWxsPVwiY3VycmVudENvbG9yXCIgeT1cIjlcIiBkeT1cIjAuNzFlbVwiIHRyYW5zZm9ybT1cInRyYW5zbGF0ZSgtMTMuNTcxNDI4NTcxNDI4NTczLCAwKVwiXG4gICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT1cInRleHQtYW5jaG9yOiBzdGFydDtcIj57e3RvZGF5WzNdfX1cbiAgICAgICAgICAgICAgICAgIDwvdGV4dD5cbiAgICAgICAgICAgICAgICA8L2c+XG4gICAgICAgICAgICAgICAgPGcgY2xhc3M9XCJ0aWNrXCIgb3BhY2l0eT1cIjFcIiB0cmFuc2Zvcm09XCJ0cmFuc2xhdGUoMTQ5LjE0Mjg1NzE0Mjg1NzE0LDApXCI+XG4gICAgICAgICAgICAgICAgICA8bGluZSBzdHJva2U9XCJjdXJyZW50Q29sb3JcIiB5Mj1cIjZcIj48L2xpbmU+XG4gICAgICAgICAgICAgICAgICA8dGV4dCBmaWxsPVwiY3VycmVudENvbG9yXCIgeT1cIjlcIiBkeT1cIjAuNzFlbVwiIHRyYW5zZm9ybT1cInRyYW5zbGF0ZSgtMTMuNTcxNDI4NTcxNDI4NTczLCAwKVwiXG4gICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT1cInRleHQtYW5jaG9yOiBzdGFydDtcIj57e3RvZGF5WzJdfX1cbiAgICAgICAgICAgICAgICAgIDwvdGV4dD5cbiAgICAgICAgICAgICAgICA8L2c+XG4gICAgICAgICAgICAgICAgPGcgY2xhc3M9XCJ0aWNrXCIgb3BhY2l0eT1cIjFcIiB0cmFuc2Zvcm09XCJ0cmFuc2xhdGUoMTgyLjI4NTcxNDI4NTcxNDMsMClcIj5cbiAgICAgICAgICAgICAgICAgIDxsaW5lIHN0cm9rZT1cImN1cnJlbnRDb2xvclwiIHkyPVwiNlwiPjwvbGluZT5cbiAgICAgICAgICAgICAgICAgIDx0ZXh0IGZpbGw9XCJjdXJyZW50Q29sb3JcIiB5PVwiOVwiIGR5PVwiMC43MWVtXCIgdHJhbnNmb3JtPVwidHJhbnNsYXRlKC0xMy41NzE0Mjg1NzE0Mjg1NzMsIDApXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlPVwidGV4dC1hbmNob3I6IHN0YXJ0O1wiPnt7dG9kYXlbMV19fVxuICAgICAgICAgICAgICAgICAgPC90ZXh0PlxuICAgICAgICAgICAgICAgIDwvZz5cbiAgICAgICAgICAgICAgICA8ZyBjbGFzcz1cInRpY2tcIiBvcGFjaXR5PVwiMVwiIHRyYW5zZm9ybT1cInRyYW5zbGF0ZSgyMTUuNDI4NTcxNDI4NTcxNDcsMClcIj5cbiAgICAgICAgICAgICAgICAgIDxsaW5lIHN0cm9rZT1cImN1cnJlbnRDb2xvclwiIHkyPVwiNlwiPjwvbGluZT5cbiAgICAgICAgICAgICAgICAgIDx0ZXh0IGZpbGw9XCJjdXJyZW50Q29sb3JcIiB5PVwiOVwiIGR5PVwiMC43MWVtXCIgdHJhbnNmb3JtPVwidHJhbnNsYXRlKC0xMy41NzE0Mjg1NzE0Mjg1NzMsIDApXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlPVwidGV4dC1hbmNob3I6IHN0YXJ0O1wiPnt7dG9kYXlbMF19fVxuICAgICAgICAgICAgICAgICAgPC90ZXh0PlxuICAgICAgICAgICAgICAgIDwvZz5cbiAgICAgICAgICAgICAgPC9nPlxuICAgICAgICAgICAgICA8ZyBjbGFzcz1cInktYXhpc1wiIHRyYW5zZm9ybT1cInRyYW5zbGF0ZSgyMzIsIDApXCIgZmlsbD1cIm5vbmVcIiBmb250LXNpemU9XCIxMFwiIGZvbnQtZmFtaWx5PVwic2Fucy1zZXJpZlwiXG4gICAgICAgICAgICAgICAgIHRleHQtYW5jaG9yPVwic3RhcnRcIj5cbiAgICAgICAgICAgICAgICA8cGF0aCBjbGFzcz1cImRvbWFpblwiIHN0cm9rZT1cImN1cnJlbnRDb2xvclwiIGQ9XCJNNiwwLjVIMC41VjMyNC41SDZcIj48L3BhdGg+XG4gICAgICAgICAgICAgICAgPGcgY2xhc3M9XCJ0aWNrXCIgb3BhY2l0eT1cIjFcIiB0cmFuc2Zvcm09XCJ0cmFuc2xhdGUoMCwgNi43NSlcIj5cbiAgICAgICAgICAgICAgICAgIDxsaW5lIHN0cm9rZT1cImN1cnJlbnRDb2xvclwiIHgyPVwiNlwiPjwvbGluZT5cbiAgICAgICAgICAgICAgICAgIDx0ZXh0IGZpbGw9XCJjdXJyZW50Q29sb3JcIiB4PVwiOVwiIGR5PVwiMC4zMmVtXCIgdHJhbnNmb3JtPVwidHJhbnNsYXRlKDMwLCAwKVwiXG4gICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT1cInRleHQtYW5jaG9yOiBlbmQ7XCI+MTJhLm0uXG4gICAgICAgICAgICAgICAgICA8L3RleHQ+XG4gICAgICAgICAgICAgICAgPC9nPlxuICAgICAgICAgICAgICAgIDxnIGNsYXNzPVwidGlja1wiIG9wYWNpdHk9XCIxXCIgdHJhbnNmb3JtPVwidHJhbnNsYXRlKDAsIDMzLjc1KVwiPlxuICAgICAgICAgICAgICAgICAgPGxpbmUgc3Ryb2tlPVwiY3VycmVudENvbG9yXCIgeDI9XCI2XCI+PC9saW5lPlxuICAgICAgICAgICAgICAgICAgPHRleHQgZmlsbD1cImN1cnJlbnRDb2xvclwiIHg9XCI5XCIgZHk9XCIwLjMyZW1cIiB0cmFuc2Zvcm09XCJ0cmFuc2xhdGUoMzAsIDApXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlPVwidGV4dC1hbmNob3I6IGVuZDtcIj4yYS5tLlxuICAgICAgICAgICAgICAgICAgPC90ZXh0PlxuICAgICAgICAgICAgICAgIDwvZz5cbiAgICAgICAgICAgICAgICA8ZyBjbGFzcz1cInRpY2tcIiBvcGFjaXR5PVwiMVwiIHRyYW5zZm9ybT1cInRyYW5zbGF0ZSgwLCA2MC43NSlcIj5cbiAgICAgICAgICAgICAgICAgIDxsaW5lIHN0cm9rZT1cImN1cnJlbnRDb2xvclwiIHgyPVwiNlwiPjwvbGluZT5cbiAgICAgICAgICAgICAgICAgIDx0ZXh0IGZpbGw9XCJjdXJyZW50Q29sb3JcIiB4PVwiOVwiIGR5PVwiMC4zMmVtXCIgdHJhbnNmb3JtPVwidHJhbnNsYXRlKDMwLCAwKVwiXG4gICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT1cInRleHQtYW5jaG9yOiBlbmQ7XCI+NGEubS5cbiAgICAgICAgICAgICAgICAgIDwvdGV4dD5cbiAgICAgICAgICAgICAgICA8L2c+XG4gICAgICAgICAgICAgICAgPGcgY2xhc3M9XCJ0aWNrXCIgb3BhY2l0eT1cIjFcIiB0cmFuc2Zvcm09XCJ0cmFuc2xhdGUoMCwgODcuNzUpXCI+XG4gICAgICAgICAgICAgICAgICA8bGluZSBzdHJva2U9XCJjdXJyZW50Q29sb3JcIiB4Mj1cIjZcIj48L2xpbmU+XG4gICAgICAgICAgICAgICAgICA8dGV4dCBmaWxsPVwiY3VycmVudENvbG9yXCIgeD1cIjlcIiBkeT1cIjAuMzJlbVwiIHRyYW5zZm9ybT1cInRyYW5zbGF0ZSgzMCwgMClcIlxuICAgICAgICAgICAgICAgICAgICAgICAgc3R5bGU9XCJ0ZXh0LWFuY2hvcjogZW5kO1wiPjZhLm0uXG4gICAgICAgICAgICAgICAgICA8L3RleHQ+XG4gICAgICAgICAgICAgICAgPC9nPlxuICAgICAgICAgICAgICAgIDxnIGNsYXNzPVwidGlja1wiIG9wYWNpdHk9XCIxXCIgdHJhbnNmb3JtPVwidHJhbnNsYXRlKDAsIDExNC43NSlcIj5cbiAgICAgICAgICAgICAgICAgIDxsaW5lIHN0cm9rZT1cImN1cnJlbnRDb2xvclwiIHgyPVwiNlwiPjwvbGluZT5cbiAgICAgICAgICAgICAgICAgIDx0ZXh0IGZpbGw9XCJjdXJyZW50Q29sb3JcIiB4PVwiOVwiIGR5PVwiMC4zMmVtXCIgdHJhbnNmb3JtPVwidHJhbnNsYXRlKDMwLCAwKVwiXG4gICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT1cInRleHQtYW5jaG9yOiBlbmQ7XCI+OGEubS5cbiAgICAgICAgICAgICAgICAgIDwvdGV4dD5cbiAgICAgICAgICAgICAgICA8L2c+XG4gICAgICAgICAgICAgICAgPGcgY2xhc3M9XCJ0aWNrXCIgb3BhY2l0eT1cIjFcIiB0cmFuc2Zvcm09XCJ0cmFuc2xhdGUoMCwgMTQxLjc1KVwiPlxuICAgICAgICAgICAgICAgICAgPGxpbmUgc3Ryb2tlPVwiY3VycmVudENvbG9yXCIgeDI9XCI2XCI+PC9saW5lPlxuICAgICAgICAgICAgICAgICAgPHRleHQgZmlsbD1cImN1cnJlbnRDb2xvclwiIHg9XCI5XCIgZHk9XCIwLjMyZW1cIiB0cmFuc2Zvcm09XCJ0cmFuc2xhdGUoMzAsIDApXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlPVwidGV4dC1hbmNob3I6IGVuZDtcIj4xMGEubS5cbiAgICAgICAgICAgICAgICAgIDwvdGV4dD5cbiAgICAgICAgICAgICAgICA8L2c+XG4gICAgICAgICAgICAgICAgPGcgY2xhc3M9XCJ0aWNrXCIgb3BhY2l0eT1cIjFcIiB0cmFuc2Zvcm09XCJ0cmFuc2xhdGUoMCwgMTY4Ljc1KVwiPlxuICAgICAgICAgICAgICAgICAgPGxpbmUgc3Ryb2tlPVwiY3VycmVudENvbG9yXCIgeDI9XCI2XCI+PC9saW5lPlxuICAgICAgICAgICAgICAgICAgPHRleHQgZmlsbD1cImN1cnJlbnRDb2xvclwiIHg9XCI5XCIgZHk9XCIwLjMyZW1cIiB0cmFuc2Zvcm09XCJ0cmFuc2xhdGUoMzAsIDApXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlPVwidGV4dC1hbmNob3I6IGVuZDtcIj4xMnAubS5cbiAgICAgICAgICAgICAgICAgIDwvdGV4dD5cbiAgICAgICAgICAgICAgICA8L2c+XG4gICAgICAgICAgICAgICAgPGcgY2xhc3M9XCJ0aWNrXCIgb3BhY2l0eT1cIjFcIiB0cmFuc2Zvcm09XCJ0cmFuc2xhdGUoMCwgMTk1Ljc1KVwiPlxuICAgICAgICAgICAgICAgICAgPGxpbmUgc3Ryb2tlPVwiY3VycmVudENvbG9yXCIgeDI9XCI2XCI+PC9saW5lPlxuICAgICAgICAgICAgICAgICAgPHRleHQgZmlsbD1cImN1cnJlbnRDb2xvclwiIHg9XCI5XCIgZHk9XCIwLjMyZW1cIiB0cmFuc2Zvcm09XCJ0cmFuc2xhdGUoMzAsIDApXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlPVwidGV4dC1hbmNob3I6IGVuZDtcIj4ycC5tLlxuICAgICAgICAgICAgICAgICAgPC90ZXh0PlxuICAgICAgICAgICAgICAgIDwvZz5cbiAgICAgICAgICAgICAgICA8ZyBjbGFzcz1cInRpY2tcIiBvcGFjaXR5PVwiMVwiIHRyYW5zZm9ybT1cInRyYW5zbGF0ZSgwLCAyMjIuNzUpXCI+XG4gICAgICAgICAgICAgICAgICA8bGluZSBzdHJva2U9XCJjdXJyZW50Q29sb3JcIiB4Mj1cIjZcIj48L2xpbmU+XG4gICAgICAgICAgICAgICAgICA8dGV4dCBmaWxsPVwiY3VycmVudENvbG9yXCIgeD1cIjlcIiBkeT1cIjAuMzJlbVwiIHRyYW5zZm9ybT1cInRyYW5zbGF0ZSgzMCwgMClcIlxuICAgICAgICAgICAgICAgICAgICAgICAgc3R5bGU9XCJ0ZXh0LWFuY2hvcjogZW5kO1wiPjRwLm0uXG4gICAgICAgICAgICAgICAgICA8L3RleHQ+XG4gICAgICAgICAgICAgICAgPC9nPlxuICAgICAgICAgICAgICAgIDxnIGNsYXNzPVwidGlja1wiIG9wYWNpdHk9XCIxXCIgdHJhbnNmb3JtPVwidHJhbnNsYXRlKDAsIDI0OS43NSlcIj5cbiAgICAgICAgICAgICAgICAgIDxsaW5lIHN0cm9rZT1cImN1cnJlbnRDb2xvclwiIHgyPVwiNlwiPjwvbGluZT5cbiAgICAgICAgICAgICAgICAgIDx0ZXh0IGZpbGw9XCJjdXJyZW50Q29sb3JcIiB4PVwiOVwiIGR5PVwiMC4zMmVtXCIgdHJhbnNmb3JtPVwidHJhbnNsYXRlKDMwLCAwKVwiXG4gICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT1cInRleHQtYW5jaG9yOiBlbmQ7XCI+NnAubS5cbiAgICAgICAgICAgICAgICAgIDwvdGV4dD5cbiAgICAgICAgICAgICAgICA8L2c+XG4gICAgICAgICAgICAgICAgPGcgY2xhc3M9XCJ0aWNrXCIgb3BhY2l0eT1cIjFcIiB0cmFuc2Zvcm09XCJ0cmFuc2xhdGUoMCwgMjc2Ljc1KVwiPlxuICAgICAgICAgICAgICAgICAgPGxpbmUgc3Ryb2tlPVwiY3VycmVudENvbG9yXCIgeDI9XCI2XCI+PC9saW5lPlxuICAgICAgICAgICAgICAgICAgPHRleHQgZmlsbD1cImN1cnJlbnRDb2xvclwiIHg9XCI5XCIgZHk9XCIwLjMyZW1cIiB0cmFuc2Zvcm09XCJ0cmFuc2xhdGUoMzAsIDApXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlPVwidGV4dC1hbmNob3I6IGVuZDtcIj44cC5tLlxuICAgICAgICAgICAgICAgICAgPC90ZXh0PlxuICAgICAgICAgICAgICAgIDwvZz5cbiAgICAgICAgICAgICAgICA8ZyBjbGFzcz1cInRpY2tcIiBvcGFjaXR5PVwiMVwiIHRyYW5zZm9ybT1cInRyYW5zbGF0ZSgwLCAzMDMuNzUpXCI+XG4gICAgICAgICAgICAgICAgICA8bGluZSBzdHJva2U9XCJjdXJyZW50Q29sb3JcIiB4Mj1cIjZcIj48L2xpbmU+XG4gICAgICAgICAgICAgICAgICA8dGV4dCBmaWxsPVwiY3VycmVudENvbG9yXCIgeD1cIjlcIiBkeT1cIjAuMzJlbVwiIHRyYW5zZm9ybT1cInRyYW5zbGF0ZSgzMCwgMClcIlxuICAgICAgICAgICAgICAgICAgICAgICAgc3R5bGU9XCJ0ZXh0LWFuY2hvcjogZW5kO1wiPjEwcC5tLlxuICAgICAgICAgICAgICAgICAgPC90ZXh0PlxuICAgICAgICAgICAgICAgIDwvZz5cbiAgICAgICAgICAgICAgPC9nPlxuICAgICAgICAgICAgICA8ZyBjbGFzcz1cInJvdyBcIiAqbmdGb3I9XCJsZXQgaXRlbSBvZiBbXS5jb25zdHJ1Y3RvcigyNCk7IGxldCBpID0gaW5kZXg7XCI+XG4gICAgICAgICAgICAgICAgPGcgW25nQ2xhc3NdPVwiZ2V0Q2xhc3MoaSwgdylcIlxuICAgICAgICAgICAgICAgICAgICpuZ0Zvcj1cImxldCB3ZWVrIG9mIFtdLmNvbnN0cnVjdG9yKDcpOyBsZXQgdyA9IGluZGV4O1wiPlxuICAgICAgICAgICAgICAgICAgPHJlY3RcbiAgICAgICAgICAgICAgICAgICAgW21hdFRvb2x0aXBdPVwiZ2V0VG9vbHRpcChpLHcpXCJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3M9XCJyZWN0XCIgd2lkdGg9XCIzMC4xNDI4NTY1OTc5MDAzOVwiXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodD1cIjEwLjVcIiBbYXR0ci54XT1cIjEuNSsodyAqIDMzLjE1KVwiXG4gICAgICAgICAgICAgICAgICAgIFthdHRyLnldPVwiMS41KyAoaSAqIDEzLjUpXCJcbiAgICAgICAgICAgICAgICAgID48L3JlY3Q+XG4gICAgICAgICAgICAgICAgPC9nPlxuXG4gICAgICAgICAgICAgIDwvZz5cblxuICAgICAgICAgICAgPC9nPlxuICAgICAgICAgICAgPGcgY2xhc3M9XCJsZWdlbmQtZ3JvdXBcIiB0cmFuc2Zvcm09XCJ0cmFuc2xhdGUoMCwgMzU5KVwiPlxuICAgICAgICAgICAgICA8cmVjdCBoZWlnaHQ9XCIxMFwiIHdpZHRoPVwiNTVcIiB4PVwiMFwiIHk9XCIwXCIgY2xhc3M9XCJsZWdlbmQtYmxvY2sgYmx1ZS0xXCI+PC9yZWN0PlxuICAgICAgICAgICAgICA8cmVjdCBoZWlnaHQ9XCIxMFwiIHdpZHRoPVwiNTVcIiB4PVwiNThcIiB5PVwiMFwiIGNsYXNzPVwibGVnZW5kLWJsb2NrIGJsdWUtMlwiPjwvcmVjdD5cbiAgICAgICAgICAgICAgPHJlY3QgaGVpZ2h0PVwiMTBcIiB3aWR0aD1cIjU1XCIgeD1cIjExNlwiIHk9XCIwXCIgY2xhc3M9XCJsZWdlbmQtYmxvY2sgYmx1ZS0zXCI+PC9yZWN0PlxuICAgICAgICAgICAgICA8cmVjdCBoZWlnaHQ9XCIxMFwiIHdpZHRoPVwiNTVcIiB4PVwiMTc0XCIgeT1cIjBcIiBjbGFzcz1cImxlZ2VuZC1ibG9jayBibHVlLTRcIj48L3JlY3Q+XG4gICAgICAgICAgICAgIDx0ZXh0IHg9XCIwXCIgeT1cIjI1XCIgY2xhc3M9XCJsZWdlbmQtdGV4dFwiIHN0eWxlPVwidGV4dC1hbmNob3I6IHN0YXJ0O1wiPnt7dmFsMX19PC90ZXh0PlxuICAgICAgICAgICAgICA8dGV4dCB4PVwiNThcIiB5PVwiMjVcIiBjbGFzcz1cImxlZ2VuZC10ZXh0XCIgc3R5bGU9XCJ0ZXh0LWFuY2hvcjogbWlkZGxlO1wiPnt7dmFsMn19PC90ZXh0PlxuICAgICAgICAgICAgICA8dGV4dCB4PVwiMTE2XCIgeT1cIjI1XCIgY2xhc3M9XCJsZWdlbmQtdGV4dFwiIHN0eWxlPVwidGV4dC1hbmNob3I6IG1pZGRsZTtcIj57e3ZhbDN9fTwvdGV4dD5cbiAgICAgICAgICAgICAgPHRleHQgeD1cIjE3NFwiIHk9XCIyNVwiIGNsYXNzPVwibGVnZW5kLXRleHRcIiBzdHlsZT1cInRleHQtYW5jaG9yOiBtaWRkbGU7XCI+e3t2YWw0fX08L3RleHQ+XG4gICAgICAgICAgICAgIDx0ZXh0IHg9XCIyMzJcIiB5PVwiMjVcIiBjbGFzcz1cImxlZ2VuZC10ZXh0XCIgc3R5bGU9XCJ0ZXh0LWFuY2hvcjogZW5kO1wiPnt7dmFsNX19PC90ZXh0PlxuICAgICAgICAgICAgPC9nPlxuICAgICAgICAgIDwvc3ZnPlxuICAgICAgICA8L2Rpdj5cblxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2ICBpb3QtY2FyZC1mb290ZXI+PG5nLWNvbnRlbnQgc2VsZWN0PVwiW2lvdC1jYXJkLWZvb3Rlcl1cIj48L25nLWNvbnRlbnQ+PC9kaXY+XG4gICAgPC9pb3QtZGFzaGJvYXJkLWNhcmQ+XG4gIGBcbn0pXG5leHBvcnQgY2xhc3MgVGltZU9mRGF5Q2FyZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25DaGFuZ2VzIHtcblxuICBASW5wdXQoKSB0aXRsZSA9ICcnO1xuICBASW5wdXQoKSBvdXRlclRpdGxlO1xuICBASW5wdXQoKSBzdWJqZWN0ID0gXCJQSVIgQmV3ZWdpbmdlblwiO1xuICBASW5wdXQoKSBkYXRhOiBudW1iZXJbXVtdOyAvL251bWJlcltdW107XG4gIEBJbnB1dCgpIG1pbiA9IDEwO1xuXG4gIGxvd2VzdDogbnVtYmVyO1xuICBoaWdoZXN0ID0gNTtcblxuICB2YWwxOiBudW1iZXI7XG4gIHZhbDI6IG51bWJlcjtcbiAgdmFsMzogbnVtYmVyO1xuICB2YWw0OiBudW1iZXI7XG4gIHZhbDU6IG51bWJlcjtcblxuICBjb25zdHJ1Y3RvcigpIHtcblxuICB9XG5cbiAgdG9kYXk6IHN0cmluZ1tdID0gWycnLCAnJywgJycsICcnLCAnJywgJycsICcnXTtcblxuICBuZ09uSW5pdCgpIHtcblxuXG4gICAgbGV0IG5vdyA9IG1vbWVudCgpO1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgNzsgaSsrKSB7XG4gICAgICB0aGlzLnRvZGF5W2ldID0gbm93LmZvcm1hdChcImRkXCIpO1xuICAgICAgbm93ID0gbm93LnN1YnRyYWN0KDEsICdkYXlzJyk7XG4gICAgfVxuICB9XG5cbiAgZ2V0VG9vbHRpcChob3VyOiBudW1iZXIsIHdlZWs6IG51bWJlcikge1xuICAgIHJldHVybiB0aGlzLmRhdGFbd2Vla11baG91cl0gKyBcIiBcIiArIHRoaXMuc3ViamVjdDtcbiAgfVxuXG4gIGdldENsYXNzKGhvdXI6IG51bWJlciwgd2VlazogbnVtYmVyKSB7XG4gICAgbGV0IGNsYXNzUmV0ID0geydjZWxsJzogdHJ1ZX1cbiAgICBpZiAoIXRoaXMuZGF0YSkge1xuICAgICAgY2xhc3NSZXRbJ2dyZXktMSddID0gdHJ1ZTtcbiAgICB9IGVsc2Uge1xuICAgICAgaWYgKHRoaXMuZGF0YVt3ZWVrXVtob3VyXSA9PSAwKSB7XG4gICAgICAgIGNsYXNzUmV0WydncmV5LTEnXSA9IHRydWU7XG4gICAgICB9IGVsc2UgaWYgKHRoaXMuZGF0YVt3ZWVrXVtob3VyXSA8IHRoaXMudmFsMikge1xuICAgICAgICBjbGFzc1JldFsnYmx1ZS0xJ10gPSB0cnVlO1xuICAgICAgfSBlbHNlIGlmICh0aGlzLmRhdGFbd2Vla11baG91cl0gPCB0aGlzLnZhbDMpIHtcbiAgICAgICAgY2xhc3NSZXRbJ2JsdWUtMiddID0gdHJ1ZTtcbiAgICAgIH0gZWxzZSBpZiAodGhpcy5kYXRhW3dlZWtdW2hvdXJdIDwgdGhpcy52YWw0KSB7XG4gICAgICAgIGNsYXNzUmV0WydibHVlLTMnXSA9IHRydWU7XG4gICAgICB9IGVsc2UgaWYgKHRoaXMuZGF0YVt3ZWVrXVtob3VyXSA8PSB0aGlzLnZhbDUpIHtcbiAgICAgICAgY2xhc3NSZXRbJ2JsdWUtNCddID0gdHJ1ZTtcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIGNsYXNzUmV0O1xuICB9XG5cbiAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcyk6IHZvaWQge1xuXG4gICAgdGhpcy5sb3dlc3QgPSBudWxsO1xuICAgIHRoaXMuaGlnaGVzdCA9IG51bGw7XG4gICAgaWYgKHRoaXMuZGF0YSAhPSBudWxsKSB7XG4gICAgICB0aGlzLmRhdGEubWFwKHJvdyA9PiByb3cubWFwKGNlbGwgPT4ge1xuICAgICAgICBpZiAoY2VsbCAhPSAwKSB7XG4gICAgICAgICAgaWYgKHRoaXMubG93ZXN0ID09IG51bGwpIHRoaXMubG93ZXN0ID0gY2VsbDtcbiAgICAgICAgICB0aGlzLmxvd2VzdCA9IE1hdGgubWluKHRoaXMubG93ZXN0LCBjZWxsKTtcbiAgICAgICAgICB0aGlzLmhpZ2hlc3QgPSBNYXRoLm1heCh0aGlzLmhpZ2hlc3QsIGNlbGwpO1xuICAgICAgICB9XG5cbiAgICAgIH0pKTtcbiAgICB9XG5cbiAgICBpZiAodGhpcy5sb3dlc3QgPT0gbnVsbCkge1xuICAgICAgdGhpcy5sb3dlc3QgPSAxO1xuICAgIH1cblxuICAgIHRoaXMubG93ZXN0ID0gTWF0aC5mbG9vcih0aGlzLmxvd2VzdCk7XG5cbiAgICB0aGlzLmhpZ2hlc3QgPSBNYXRoLmNlaWwodGhpcy5oaWdoZXN0KTtcbiAgICB0aGlzLmhpZ2hlc3QgPSBNYXRoLm1heCh0aGlzLmhpZ2hlc3QsIHRoaXMubWluKTtcblxuICAgIGlmICh0aGlzLmxvd2VzdCA+IDEwKSB7XG4gICAgICB0aGlzLmxvd2VzdCA9IE1hdGguZmxvb3IodGhpcy5sb3dlc3QgLyAxMCkgKiAxMFxuICAgIH1cbiAgICBjb25zdCBkaWZmID0gdGhpcy5oaWdoZXN0IC0gdGhpcy5sb3dlc3Q7XG4gICAgbGV0IHN0ZXAgPSBNYXRoLmNlaWwoZGlmZiAvIDQpO1xuICAgIGlmIChzdGVwID4gMTApIHtcbiAgICAgIHN0ZXAgPSBNYXRoLmNlaWwoc3RlcCAvIDEwKSAqIDEwXG4gICAgfVxuICAgIGlmIChzdGVwID09PSAwKSB7XG4gICAgICBzdGVwID0gMTtcbiAgICB9XG4gICAgdGhpcy52YWwxID0gdGhpcy5sb3dlc3Q7XG4gICAgdGhpcy52YWwyID0gdGhpcy52YWwxICsgc3RlcDtcbiAgICB0aGlzLnZhbDMgPSB0aGlzLnZhbDIgKyBzdGVwO1xuICAgIHRoaXMudmFsNCA9IHRoaXMudmFsMyArIHN0ZXA7XG4gICAgdGhpcy52YWw1ID0gdGhpcy52YWw0ICsgc3RlcDtcblxuICB9XG5cblxuICBzaG93VG9vbHRpcChldnQ6IGFueSwgdGVzdDogYW55KSB7XG5cbiAgfVxuXG59XG4iXX0=