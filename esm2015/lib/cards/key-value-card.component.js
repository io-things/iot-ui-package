import { Component, EventEmitter, Input, Output } from "@angular/core";
import * as i0 from "@angular/core";
import * as i1 from "../dashboard/dashboard-card.component";
import * as i2 from "@angular/material/table";
import * as i3 from "@angular/material/icon";
import * as i4 from "@angular/common";
import * as i5 from "@ngx-translate/core";
const oneday = 24 * 3600000;
const oneMonth = 31 * 24 * 3600000;
export class KeyValueCardComponent {
    constructor() {
        this.click = new EventEmitter();
        this.hover = new EventEmitter();
        this.unHover = new EventEmitter();
        this.displayedColumns = ['label', 'value'];
    }
    ngOnInit() {
    }
    getShowShortTime(entries) {
        if (!entries) {
            return false;
        }
        if (!entries.value) {
            return false;
        }
        if (entries.value === 'undefined') {
            return false;
        }
        if (entries.value < (new Date().getTime() - oneday)) {
            return false;
        }
        return true;
    }
    getShowLongTime(entries) {
        if (!entries)
            return false;
        if (!entries.value)
            return false;
        if (entries.value === 'undefined')
            return false;
        if (entries.value < (new Date().getTime() - oneMonth)) {
            return false;
        }
        if (entries.value > (new Date().getTime() - oneday))
            return false;
        return true;
    }
    getShowVeryLongTime(entries) {
        if (!entries)
            return false;
        if (!entries.value)
            return false;
        if (entries.value === 'undefined')
            return false;
        if (entries.value > (new Date().getTime() - oneMonth))
            return false;
        return true;
    }
    getNoTime(entries) {
        if (!entries)
            return true;
        if (!entries.value)
            return true;
        if (entries.value == 0)
            return true;
        return false;
    }
}
KeyValueCardComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: KeyValueCardComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
KeyValueCardComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: KeyValueCardComponent, selector: "iot-key-value-card", inputs: { outerTitle: "outerTitle", title: "title", keyValueData: "keyValueData" }, outputs: { click: "click", hover: "hover", unHover: "unHover" }, ngImport: i0, template: `
    <iot-dashboard-card
      [colspan]="1"
      [outerTitle]="outerTitle">
      <div *ngIf="title" iot-card-header padding="true">{{title |translate}}</div>
      <div  iot-card-content >
          <table mat-table [dataSource]="keyValueData" style="width:100%">

            <ng-container matColumnDef="label">
              <th mat-header-cell *matHeaderCellDef> Item</th>
              <td
                (click)="click.emit(entries)"
                (mouseenter)="hover.emit(entries)"
                (mouseleave)="unHover.emit()"
                class="text " [ngClass]="{warnrow: entries.error}"
                  mat-cell *matCellDef="let entries"> {{entries.label|translate}}</td>
            </ng-container>


            <ng-container matColumnDef="value">
              <th mat-header-cell *matHeaderCellDef> Cost</th>
              <td class="value text warnrow" [ngClass]="{warnrow: entries.error}"  mat-cell *matCellDef="let entries">

                <div *ngIf="!!entries.value && entries.value > 0 && entries.type =='date' && getShowShortTime(entries)">{{entries.value |date:'H:mm'}}</div>
                <div *ngIf="!!entries.value && entries.value > 0 && entries.type =='date' && getShowLongTime(entries)">{{entries.value |date:'MMM d, H:mm' }}</div>
                <div *ngIf="!!entries.value && entries.value > 0 && entries.type =='date' && getShowVeryLongTime(entries)">{{entries.value |date:'shortDate'}}</div>

                <div *ngIf="entries.type =='date' && getNoTime(entries)"> --</div>
                <div *ngIf="entries.type =='string'">{{entries.value }}</div>
                <div *ngIf="entries.type =='link'"><a [href]="entries.link"> {{entries.value }}</a></div>
                <div *ngIf="entries.type =='boolean' && entries.value">{{entries.trueValue | translate}}</div>
                <div *ngIf="entries.type =='boolean' && !entries.value">{{entries.falseValue | translate}}</div>
                <div *ngIf="entries.type =='booleanIcon' && entries.value">
                  <mat-icon>{{entries.trueValue }}</mat-icon>
                </div>
                <div *ngIf="entries.type =='booleanIcon' && !entries.value">
                  <mat-icon>{{entries.falseValue }}</mat-icon>
                </div>
                <div *ngIf="entries.type =='array'">{{entries.values[entries.value] }}</div>
                <div *ngIf="entries.type =='number'">{{entries.value | number: entries.digitsInfo || '1.0-0'}}{{entries.unit }}</div>
              </td>

            </ng-container>
            <tr mat-row *matRowDef="let row; columns: displayedColumns;"></tr>
          </table>
      </div>
    </iot-dashboard-card>
  `, isInline: true, components: [{ type: i1.DashboardCardComponent, selector: "iot-dashboard-card", inputs: ["colspan", "showHeader", "outerTitle", "innerTitle", "showTitle", "height", "width"] }, { type: i2.MatTable, selector: "mat-table, table[mat-table]", exportAs: ["matTable"] }, { type: i3.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }, { type: i2.MatRow, selector: "mat-row, tr[mat-row]", exportAs: ["matRow"] }], directives: [{ type: i4.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i1.IotCardHeader, selector: "[iot-card-header]", inputs: ["padding"] }, { type: i1.IotCardContent, selector: "[iot-card-content]", inputs: ["padding"] }, { type: i2.MatColumnDef, selector: "[matColumnDef]", inputs: ["sticky", "matColumnDef"] }, { type: i2.MatHeaderCellDef, selector: "[matHeaderCellDef]" }, { type: i2.MatHeaderCell, selector: "mat-header-cell, th[mat-header-cell]" }, { type: i2.MatCellDef, selector: "[matCellDef]" }, { type: i2.MatCell, selector: "mat-cell, td[mat-cell]" }, { type: i4.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i2.MatRowDef, selector: "[matRowDef]", inputs: ["matRowDefColumns", "matRowDefWhen"] }], pipes: { "translate": i5.TranslatePipe, "date": i4.DatePipe, "number": i4.DecimalPipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: KeyValueCardComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-key-value-card',
                    template: `
    <iot-dashboard-card
      [colspan]="1"
      [outerTitle]="outerTitle">
      <div *ngIf="title" iot-card-header padding="true">{{title |translate}}</div>
      <div  iot-card-content >
          <table mat-table [dataSource]="keyValueData" style="width:100%">

            <ng-container matColumnDef="label">
              <th mat-header-cell *matHeaderCellDef> Item</th>
              <td
                (click)="click.emit(entries)"
                (mouseenter)="hover.emit(entries)"
                (mouseleave)="unHover.emit()"
                class="text " [ngClass]="{warnrow: entries.error}"
                  mat-cell *matCellDef="let entries"> {{entries.label|translate}}</td>
            </ng-container>


            <ng-container matColumnDef="value">
              <th mat-header-cell *matHeaderCellDef> Cost</th>
              <td class="value text warnrow" [ngClass]="{warnrow: entries.error}"  mat-cell *matCellDef="let entries">

                <div *ngIf="!!entries.value && entries.value > 0 && entries.type =='date' && getShowShortTime(entries)">{{entries.value |date:'H:mm'}}</div>
                <div *ngIf="!!entries.value && entries.value > 0 && entries.type =='date' && getShowLongTime(entries)">{{entries.value |date:'MMM d, H:mm' }}</div>
                <div *ngIf="!!entries.value && entries.value > 0 && entries.type =='date' && getShowVeryLongTime(entries)">{{entries.value |date:'shortDate'}}</div>

                <div *ngIf="entries.type =='date' && getNoTime(entries)"> --</div>
                <div *ngIf="entries.type =='string'">{{entries.value }}</div>
                <div *ngIf="entries.type =='link'"><a [href]="entries.link"> {{entries.value }}</a></div>
                <div *ngIf="entries.type =='boolean' && entries.value">{{entries.trueValue | translate}}</div>
                <div *ngIf="entries.type =='boolean' && !entries.value">{{entries.falseValue | translate}}</div>
                <div *ngIf="entries.type =='booleanIcon' && entries.value">
                  <mat-icon>{{entries.trueValue }}</mat-icon>
                </div>
                <div *ngIf="entries.type =='booleanIcon' && !entries.value">
                  <mat-icon>{{entries.falseValue }}</mat-icon>
                </div>
                <div *ngIf="entries.type =='array'">{{entries.values[entries.value] }}</div>
                <div *ngIf="entries.type =='number'">{{entries.value | number: entries.digitsInfo || '1.0-0'}}{{entries.unit }}</div>
              </td>

            </ng-container>
            <tr mat-row *matRowDef="let row; columns: displayedColumns;"></tr>
          </table>
      </div>
    </iot-dashboard-card>
  `
                }]
        }], ctorParameters: function () { return []; }, propDecorators: { outerTitle: [{
                type: Input
            }], title: [{
                type: Input
            }], keyValueData: [{
                type: Input
            }], click: [{
                type: Output
            }], hover: [{
                type: Output
            }], unHover: [{
                type: Output
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoia2V5LXZhbHVlLWNhcmQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvaW90LXVpLWxpYi9zcmMvbGliL2NhcmRzL2tleS12YWx1ZS1jYXJkLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQVUsTUFBTSxFQUFDLE1BQU0sZUFBZSxDQUFDOzs7Ozs7O0FBRTdFLE1BQU0sTUFBTSxHQUFHLEVBQUUsR0FBRyxPQUFPLENBQUM7QUFDNUIsTUFBTSxRQUFRLEdBQUcsRUFBRSxHQUFDLEVBQUUsR0FBRyxPQUFPLENBQUM7QUErRGpDLE1BQU0sT0FBTyxxQkFBcUI7SUFZaEM7UUFOVSxVQUFLLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUMzQixVQUFLLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUMzQixZQUFPLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUV2QyxxQkFBZ0IsR0FBYSxDQUFDLE9BQU8sRUFBRSxPQUFPLENBQUMsQ0FBQztJQUloRCxDQUFDO0lBR0QsUUFBUTtJQUlSLENBQUM7SUFFRCxnQkFBZ0IsQ0FBQyxPQUFZO1FBQzNCLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDWixPQUFPLEtBQUssQ0FBQztTQUNkO1FBQ0QsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUU7WUFDbEIsT0FBTyxLQUFLLENBQUM7U0FDZDtRQUNELElBQUksT0FBTyxDQUFDLEtBQUssS0FBSyxXQUFXLEVBQUU7WUFDakMsT0FBTyxLQUFLLENBQUM7U0FDZDtRQUNELElBQUksT0FBTyxDQUFDLEtBQUssR0FBRyxDQUFDLElBQUksSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLEdBQUcsTUFBTSxDQUFDLEVBQUU7WUFDbkQsT0FBTyxLQUFLLENBQUE7U0FDYjtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVELGVBQWUsQ0FBQyxPQUFZO1FBQzFCLElBQUksQ0FBQyxPQUFPO1lBQUUsT0FBTyxLQUFLLENBQUM7UUFDM0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLO1lBQUUsT0FBTyxLQUFLLENBQUM7UUFDakMsSUFBSSxPQUFPLENBQUMsS0FBSyxLQUFLLFdBQVc7WUFBRSxPQUFPLEtBQUssQ0FBQztRQUNoRCxJQUFJLE9BQU8sQ0FBQyxLQUFLLEdBQUcsQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDLE9BQU8sRUFBRSxHQUFHLFFBQVEsQ0FBQyxFQUFFO1lBQ3JELE9BQU8sS0FBSyxDQUFBO1NBQ2I7UUFDRCxJQUFJLE9BQU8sQ0FBQyxLQUFLLEdBQUcsQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDLE9BQU8sRUFBRSxHQUFHLE1BQU0sQ0FBQztZQUFFLE9BQU8sS0FBSyxDQUFDO1FBQ2xFLE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUdELG1CQUFtQixDQUFDLE9BQVk7UUFDOUIsSUFBSSxDQUFDLE9BQU87WUFBRSxPQUFPLEtBQUssQ0FBQztRQUMzQixJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUs7WUFBRSxPQUFPLEtBQUssQ0FBQztRQUNqQyxJQUFJLE9BQU8sQ0FBQyxLQUFLLEtBQUssV0FBVztZQUFFLE9BQU8sS0FBSyxDQUFDO1FBQ2hELElBQUksT0FBTyxDQUFDLEtBQUssR0FBRyxDQUFDLElBQUksSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLEdBQUcsUUFBUSxDQUFDO1lBQUUsT0FBTyxLQUFLLENBQUM7UUFDcEUsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRUQsU0FBUyxDQUFDLE9BQVk7UUFDcEIsSUFBSSxDQUFDLE9BQU87WUFBRSxPQUFPLElBQUksQ0FBQztRQUMxQixJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUs7WUFBRSxPQUFPLElBQUksQ0FBQztRQUNoQyxJQUFJLE9BQU8sQ0FBQyxLQUFLLElBQUksQ0FBQztZQUFFLE9BQU8sSUFBSSxDQUFDO1FBQ3BDLE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQzs7a0hBaEVVLHFCQUFxQjtzR0FBckIscUJBQXFCLCtNQWpEdEI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0dBK0NUOzJGQUVVLHFCQUFxQjtrQkFuRGpDLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtvQkFDOUIsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQStDVDtpQkFDRjswRUFHVSxVQUFVO3NCQUFsQixLQUFLO2dCQUNHLEtBQUs7c0JBQWIsS0FBSztnQkFDRyxZQUFZO3NCQUFwQixLQUFLO2dCQUVJLEtBQUs7c0JBQWQsTUFBTTtnQkFDRyxLQUFLO3NCQUFkLE1BQU07Z0JBQ0csT0FBTztzQkFBaEIsTUFBTSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIElucHV0LCBPbkluaXQsIE91dHB1dH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcblxuY29uc3Qgb25lZGF5ID0gMjQgKiAzNjAwMDAwO1xuY29uc3Qgb25lTW9udGggPSAzMSoyNCAqIDM2MDAwMDA7XG5cbmV4cG9ydCBpbnRlcmZhY2UgS2V5VmFsdWVDYXJkRGF0YSB7XG4gbGFiZWw6IHN0cmluZztcbiB2YWx1ZTogc3RyaW5nIHwgbnVtYmVyO1xuIGVycm9yPzpib29sZWFuO1xuIHR5cGU6ICdzdHJpbmcnIHwgJ251bWJlcicgfCAnZGF0ZSd8ICdib29sZWFuSWNvbic7XG4gdW5pdD86IHN0cmluZztcbiAgdHJ1ZVZhbHVlPzogc3RyaW5nO1xuICBmYWxzZVZhbHVlPzogc3RyaW5nO1xufVxuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdpb3Qta2V5LXZhbHVlLWNhcmQnLFxuICB0ZW1wbGF0ZTogYFxuICAgIDxpb3QtZGFzaGJvYXJkLWNhcmRcbiAgICAgIFtjb2xzcGFuXT1cIjFcIlxuICAgICAgW291dGVyVGl0bGVdPVwib3V0ZXJUaXRsZVwiPlxuICAgICAgPGRpdiAqbmdJZj1cInRpdGxlXCIgaW90LWNhcmQtaGVhZGVyIHBhZGRpbmc9XCJ0cnVlXCI+e3t0aXRsZSB8dHJhbnNsYXRlfX08L2Rpdj5cbiAgICAgIDxkaXYgIGlvdC1jYXJkLWNvbnRlbnQgPlxuICAgICAgICAgIDx0YWJsZSBtYXQtdGFibGUgW2RhdGFTb3VyY2VdPVwia2V5VmFsdWVEYXRhXCIgc3R5bGU9XCJ3aWR0aDoxMDAlXCI+XG5cbiAgICAgICAgICAgIDxuZy1jb250YWluZXIgbWF0Q29sdW1uRGVmPVwibGFiZWxcIj5cbiAgICAgICAgICAgICAgPHRoIG1hdC1oZWFkZXItY2VsbCAqbWF0SGVhZGVyQ2VsbERlZj4gSXRlbTwvdGg+XG4gICAgICAgICAgICAgIDx0ZFxuICAgICAgICAgICAgICAgIChjbGljayk9XCJjbGljay5lbWl0KGVudHJpZXMpXCJcbiAgICAgICAgICAgICAgICAobW91c2VlbnRlcik9XCJob3Zlci5lbWl0KGVudHJpZXMpXCJcbiAgICAgICAgICAgICAgICAobW91c2VsZWF2ZSk9XCJ1bkhvdmVyLmVtaXQoKVwiXG4gICAgICAgICAgICAgICAgY2xhc3M9XCJ0ZXh0IFwiIFtuZ0NsYXNzXT1cInt3YXJucm93OiBlbnRyaWVzLmVycm9yfVwiXG4gICAgICAgICAgICAgICAgICBtYXQtY2VsbCAqbWF0Q2VsbERlZj1cImxldCBlbnRyaWVzXCI+IHt7ZW50cmllcy5sYWJlbHx0cmFuc2xhdGV9fTwvdGQ+XG4gICAgICAgICAgICA8L25nLWNvbnRhaW5lcj5cblxuXG4gICAgICAgICAgICA8bmctY29udGFpbmVyIG1hdENvbHVtbkRlZj1cInZhbHVlXCI+XG4gICAgICAgICAgICAgIDx0aCBtYXQtaGVhZGVyLWNlbGwgKm1hdEhlYWRlckNlbGxEZWY+IENvc3Q8L3RoPlxuICAgICAgICAgICAgICA8dGQgY2xhc3M9XCJ2YWx1ZSB0ZXh0IHdhcm5yb3dcIiBbbmdDbGFzc109XCJ7d2FybnJvdzogZW50cmllcy5lcnJvcn1cIiAgbWF0LWNlbGwgKm1hdENlbGxEZWY9XCJsZXQgZW50cmllc1wiPlxuXG4gICAgICAgICAgICAgICAgPGRpdiAqbmdJZj1cIiEhZW50cmllcy52YWx1ZSAmJiBlbnRyaWVzLnZhbHVlID4gMCAmJiBlbnRyaWVzLnR5cGUgPT0nZGF0ZScgJiYgZ2V0U2hvd1Nob3J0VGltZShlbnRyaWVzKVwiPnt7ZW50cmllcy52YWx1ZSB8ZGF0ZTonSDptbSd9fTwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgKm5nSWY9XCIhIWVudHJpZXMudmFsdWUgJiYgZW50cmllcy52YWx1ZSA+IDAgJiYgZW50cmllcy50eXBlID09J2RhdGUnICYmIGdldFNob3dMb25nVGltZShlbnRyaWVzKVwiPnt7ZW50cmllcy52YWx1ZSB8ZGF0ZTonTU1NIGQsIEg6bW0nIH19PC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiAqbmdJZj1cIiEhZW50cmllcy52YWx1ZSAmJiBlbnRyaWVzLnZhbHVlID4gMCAmJiBlbnRyaWVzLnR5cGUgPT0nZGF0ZScgJiYgZ2V0U2hvd1ZlcnlMb25nVGltZShlbnRyaWVzKVwiPnt7ZW50cmllcy52YWx1ZSB8ZGF0ZTonc2hvcnREYXRlJ319PC9kaXY+XG5cbiAgICAgICAgICAgICAgICA8ZGl2ICpuZ0lmPVwiZW50cmllcy50eXBlID09J2RhdGUnICYmIGdldE5vVGltZShlbnRyaWVzKVwiPiAtLTwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgKm5nSWY9XCJlbnRyaWVzLnR5cGUgPT0nc3RyaW5nJ1wiPnt7ZW50cmllcy52YWx1ZSB9fTwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgKm5nSWY9XCJlbnRyaWVzLnR5cGUgPT0nbGluaydcIj48YSBbaHJlZl09XCJlbnRyaWVzLmxpbmtcIj4ge3tlbnRyaWVzLnZhbHVlIH19PC9hPjwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgKm5nSWY9XCJlbnRyaWVzLnR5cGUgPT0nYm9vbGVhbicgJiYgZW50cmllcy52YWx1ZVwiPnt7ZW50cmllcy50cnVlVmFsdWUgfCB0cmFuc2xhdGV9fTwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgKm5nSWY9XCJlbnRyaWVzLnR5cGUgPT0nYm9vbGVhbicgJiYgIWVudHJpZXMudmFsdWVcIj57e2VudHJpZXMuZmFsc2VWYWx1ZSB8IHRyYW5zbGF0ZX19PC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiAqbmdJZj1cImVudHJpZXMudHlwZSA9PSdib29sZWFuSWNvbicgJiYgZW50cmllcy52YWx1ZVwiPlxuICAgICAgICAgICAgICAgICAgPG1hdC1pY29uPnt7ZW50cmllcy50cnVlVmFsdWUgfX08L21hdC1pY29uPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgKm5nSWY9XCJlbnRyaWVzLnR5cGUgPT0nYm9vbGVhbkljb24nICYmICFlbnRyaWVzLnZhbHVlXCI+XG4gICAgICAgICAgICAgICAgICA8bWF0LWljb24+e3tlbnRyaWVzLmZhbHNlVmFsdWUgfX08L21hdC1pY29uPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgKm5nSWY9XCJlbnRyaWVzLnR5cGUgPT0nYXJyYXknXCI+e3tlbnRyaWVzLnZhbHVlc1tlbnRyaWVzLnZhbHVlXSB9fTwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgKm5nSWY9XCJlbnRyaWVzLnR5cGUgPT0nbnVtYmVyJ1wiPnt7ZW50cmllcy52YWx1ZSB8IG51bWJlcjogZW50cmllcy5kaWdpdHNJbmZvIHx8ICcxLjAtMCd9fXt7ZW50cmllcy51bml0IH19PC9kaXY+XG4gICAgICAgICAgICAgIDwvdGQ+XG5cbiAgICAgICAgICAgIDwvbmctY29udGFpbmVyPlxuICAgICAgICAgICAgPHRyIG1hdC1yb3cgKm1hdFJvd0RlZj1cImxldCByb3c7IGNvbHVtbnM6IGRpc3BsYXllZENvbHVtbnM7XCI+PC90cj5cbiAgICAgICAgICA8L3RhYmxlPlxuICAgICAgPC9kaXY+XG4gICAgPC9pb3QtZGFzaGJvYXJkLWNhcmQ+XG4gIGBcbn0pXG5leHBvcnQgY2xhc3MgS2V5VmFsdWVDYXJkQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBASW5wdXQoKSBvdXRlclRpdGxlOiBzdHJpbmc7XG4gIEBJbnB1dCgpIHRpdGxlOiBzdHJpbmc7XG4gIEBJbnB1dCgpIGtleVZhbHVlRGF0YTogS2V5VmFsdWVDYXJkRGF0YVtdO1xuXG4gIEBPdXRwdXQoKSBjbGljayA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgQE91dHB1dCgpIGhvdmVyID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAT3V0cHV0KCkgdW5Ib3ZlciA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuICBkaXNwbGF5ZWRDb2x1bW5zOiBzdHJpbmdbXSA9IFsnbGFiZWwnLCAndmFsdWUnXTtcblxuICBjb25zdHJ1Y3RvcigpIHtcblxuICB9XG5cblxuICBuZ09uSW5pdCgpIHtcblxuXG5cbiAgfVxuXG4gIGdldFNob3dTaG9ydFRpbWUoZW50cmllczogYW55KSB7XG4gICAgaWYgKCFlbnRyaWVzKSB7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIGlmICghZW50cmllcy52YWx1ZSkge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgICBpZiAoZW50cmllcy52YWx1ZSA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG4gICAgaWYgKGVudHJpZXMudmFsdWUgPCAobmV3IERhdGUoKS5nZXRUaW1lKCkgLSBvbmVkYXkpKSB7XG4gICAgICByZXR1cm4gZmFsc2VcbiAgICB9XG4gICAgcmV0dXJuIHRydWU7XG4gIH1cblxuICBnZXRTaG93TG9uZ1RpbWUoZW50cmllczogYW55KSB7XG4gICAgaWYgKCFlbnRyaWVzKSByZXR1cm4gZmFsc2U7XG4gICAgaWYgKCFlbnRyaWVzLnZhbHVlKSByZXR1cm4gZmFsc2U7XG4gICAgaWYgKGVudHJpZXMudmFsdWUgPT09ICd1bmRlZmluZWQnKSByZXR1cm4gZmFsc2U7XG4gICAgaWYgKGVudHJpZXMudmFsdWUgPCAobmV3IERhdGUoKS5nZXRUaW1lKCkgLSBvbmVNb250aCkpIHtcbiAgICAgIHJldHVybiBmYWxzZVxuICAgIH1cbiAgICBpZiAoZW50cmllcy52YWx1ZSA+IChuZXcgRGF0ZSgpLmdldFRpbWUoKSAtIG9uZWRheSkpIHJldHVybiBmYWxzZTtcbiAgICByZXR1cm4gdHJ1ZTtcbiAgfVxuXG5cbiAgZ2V0U2hvd1ZlcnlMb25nVGltZShlbnRyaWVzOiBhbnkpIHtcbiAgICBpZiAoIWVudHJpZXMpIHJldHVybiBmYWxzZTtcbiAgICBpZiAoIWVudHJpZXMudmFsdWUpIHJldHVybiBmYWxzZTtcbiAgICBpZiAoZW50cmllcy52YWx1ZSA9PT0gJ3VuZGVmaW5lZCcpIHJldHVybiBmYWxzZTtcbiAgICBpZiAoZW50cmllcy52YWx1ZSA+IChuZXcgRGF0ZSgpLmdldFRpbWUoKSAtIG9uZU1vbnRoKSkgcmV0dXJuIGZhbHNlO1xuICAgIHJldHVybiB0cnVlO1xuICB9XG5cbiAgZ2V0Tm9UaW1lKGVudHJpZXM6IGFueSkge1xuICAgIGlmICghZW50cmllcykgcmV0dXJuIHRydWU7XG4gICAgaWYgKCFlbnRyaWVzLnZhbHVlKSByZXR1cm4gdHJ1ZTtcbiAgICBpZiAoZW50cmllcy52YWx1ZSA9PSAwKSByZXR1cm4gdHJ1ZTtcbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cblxufVxuIl19