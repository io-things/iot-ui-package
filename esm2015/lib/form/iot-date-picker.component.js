import { Component, EventEmitter, forwardRef, Input, Output, TemplateRef, ViewChild } from '@angular/core';
import { DynamicFormWidth } from "./dynamic-form-width";
import { FormControl, FormGroup } from "@angular/forms";
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/form-field";
import * as i2 from "@angular/material/datepicker";
import * as i3 from "@angular/common";
import * as i4 from "@angular/forms";
import * as i5 from "@angular/material/input";
import * as i6 from "@ngx-translate/core";
export class IotDatePickerComponent extends DynamicFormWidth {
    constructor() {
        super(...arguments);
        this.timestampInSec = false;
        this.form = new FormGroup({
            dateId: new FormControl(''),
        });
        this.attribute = 'todo';
        this.description = 'explain this field';
        this.type = 'text';
        this.disabled = false;
        this.onChange = new EventEmitter();
    }
    get template() {
        return this._template;
    }
    get pristine() {
        return this.form.value.dateId === this.value;
    }
    get key() {
        return this.attribute;
    }
    get newValue() {
        if (this.timestampInSec) {
            if (this.form.value.dateId.unix) {
                return {
                    seconds: this.form.value.dateId.unix()
                };
            }
            else {
                return {
                    seconds: this.form.value.dateId.getTime() / 1000
                };
            }
        }
        return this.form.value.dateId;
    }
    ngOnInit() {
        super.ngOnInit();
        if (this.value && this.value.seconds) {
            this.value = new Date(this.value.seconds * 1000);
            this.timestampInSec = true;
        }
        this.form.setValue({ dateId: this.value });
        if (this.disabled) {
            this.form = new FormGroup({
                dateId: new FormControl({ value: '', disabled: this.disabled }),
            });
        }
    }
}
IotDatePickerComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotDatePickerComponent, deps: null, target: i0.ɵɵFactoryTarget.Component });
IotDatePickerComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotDatePickerComponent, selector: "iot-date-picker", inputs: { label: "label", attribute: "attribute", description: "description", value: "value", type: "type", disabled: "disabled" }, outputs: { onChange: "onChange" }, providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotDatePickerComponent) }], viewQueries: [{ propertyName: "_template", first: true, predicate: TemplateRef, descendants: true }], usesInheritance: true, ngImport: i0, template: `
    <ng-template>
      <div class="outer-margin">
        <div
          class="outeroneline"
          [ngClass]="{'outersmall':!isBig}">
          <div
            class="label-outer"
            [ngClass]="{'label-outer-thin':!isBig}"
          >
            <div class="label">{{label | translate}} </div>
          </div>
          <form [formGroup]="form" class="content">

            <mat-form-field appearance="outline" class="w-100">
              <mat-label>{{description | translate}}</mat-label>
              <input
                formControlName="dateId"
                (ngModelChange)="onChange.emit($event)"
                matInput [matDatepicker]="picker">
              <mat-datepicker-toggle matSuffix [for]="picker"></mat-datepicker-toggle>
              <mat-datepicker #picker></mat-datepicker>
            </mat-form-field>
            <span class="border-class"></span>
          </form>
        </div>
      </div>
    </ng-template>

  `, isInline: true, styles: ["\n    .outer-margin {\n      /*margin-left: -5px;*/\n\n      padding-right: 50px;\n    }\n\n    .label-outer.label-outer-thin {\n      min-height: 40px;\n    }\n\n    .label-outer {\n      flex-basis: 33%;\n      min-height: 64px;\n      min-width: 220px;\n      display: block;\n      padding-top: 13px;\n      color: #3c4043;\n    }\n\n    .label {\n\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 14px;\n      font-weight: 400;\n      height: 20px;\n      color: rgb(60, 64, 67);\n    }\n\n    .outeroneline {\n      display: flex;\n      max-width: 900px;\n    }\n\n\n    .outersmall {\n      flex-direction: column;\n      padding-bottom: 8px;\n    }\n\n\n    .content {\n      flex-direction: column;\n      flex-basis: 67%;\n      min-width: 280px;\n    }\n\n    .form-content {\n\n      width: 100%;\n      height: 46px;\n      padding: 0 16px;\n      border: 1px solid #dadce0;\n      border-radius: 4px;\n      border-top-left-radius: 4px;\n      border-top-right-radius: 4px;\n      border-top-right-radius: 4px;\n      border-bottom-right-radius: 4px;\n      border-bottom-right-radius: 4px;\n      border-bottom-left-radius: 4px;\n      pointer-events: none;\n    }\n\n\n    .form-content:hover {\n      border: 2px solid black;\n    }\n\n    .input-class {\n      height: 100%;\n      width: 100%;\n      display: flex;\n      border: none !important;\n      background-color: transparent;\n      pointer-events: auto;\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 1rem;\n      font-weight: 400;\n      letter-spacing: .00625em;\n    }\n\n    .input-class:focus {\n      outline: none;\n    }\n  "], components: [{ type: i1.MatFormField, selector: "mat-form-field", inputs: ["color", "floatLabel", "appearance", "hideRequiredMarker", "hintLabel"], exportAs: ["matFormField"] }, { type: i2.MatDatepickerToggle, selector: "mat-datepicker-toggle", inputs: ["tabIndex", "disabled", "for", "aria-label", "disableRipple"], exportAs: ["matDatepickerToggle"] }, { type: i2.MatDatepicker, selector: "mat-datepicker", exportAs: ["matDatepicker"] }], directives: [{ type: i3.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i4.ɵNgNoValidate, selector: "form:not([ngNoForm]):not([ngNativeValidate])" }, { type: i4.NgControlStatusGroup, selector: "[formGroupName],[formArrayName],[ngModelGroup],[formGroup],form:not([ngNoForm]),[ngForm]" }, { type: i4.FormGroupDirective, selector: "[formGroup]", inputs: ["formGroup"], outputs: ["ngSubmit"], exportAs: ["ngForm"] }, { type: i1.MatLabel, selector: "mat-label" }, { type: i4.DefaultValueAccessor, selector: "input:not([type=checkbox])[formControlName],textarea[formControlName],input:not([type=checkbox])[formControl],textarea[formControl],input:not([type=checkbox])[ngModel],textarea[ngModel],[ngDefaultControl]" }, { type: i5.MatInput, selector: "input[matInput], textarea[matInput], select[matNativeControl],      input[matNativeControl], textarea[matNativeControl]", inputs: ["id", "disabled", "required", "type", "value", "readonly", "placeholder", "errorStateMatcher", "aria-describedby"], exportAs: ["matInput"] }, { type: i2.MatDatepickerInput, selector: "input[matDatepicker]", inputs: ["matDatepicker", "min", "max", "matDatepickerFilter"], exportAs: ["matDatepickerInput"] }, { type: i4.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { type: i4.FormControlName, selector: "[formControlName]", inputs: ["disabled", "formControlName", "ngModel"], outputs: ["ngModelChange"] }, { type: i1.MatSuffix, selector: "[matSuffix]" }], pipes: { "translate": i6.TranslatePipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotDatePickerComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-date-picker',
                    template: `
    <ng-template>
      <div class="outer-margin">
        <div
          class="outeroneline"
          [ngClass]="{'outersmall':!isBig}">
          <div
            class="label-outer"
            [ngClass]="{'label-outer-thin':!isBig}"
          >
            <div class="label">{{label | translate}} </div>
          </div>
          <form [formGroup]="form" class="content">

            <mat-form-field appearance="outline" class="w-100">
              <mat-label>{{description | translate}}</mat-label>
              <input
                formControlName="dateId"
                (ngModelChange)="onChange.emit($event)"
                matInput [matDatepicker]="picker">
              <mat-datepicker-toggle matSuffix [for]="picker"></mat-datepicker-toggle>
              <mat-datepicker #picker></mat-datepicker>
            </mat-form-field>
            <span class="border-class"></span>
          </form>
        </div>
      </div>
    </ng-template>

  `,
                    styles: [`
    .outer-margin {
      /*margin-left: -5px;*/

      padding-right: 50px;
    }

    .label-outer.label-outer-thin {
      min-height: 40px;
    }

    .label-outer {
      flex-basis: 33%;
      min-height: 64px;
      min-width: 220px;
      display: block;
      padding-top: 13px;
      color: #3c4043;
    }

    .label {

      font-family: Roboto, Arial, sans-serif;
      font-size: 14px;
      font-weight: 400;
      height: 20px;
      color: rgb(60, 64, 67);
    }

    .outeroneline {
      display: flex;
      max-width: 900px;
    }


    .outersmall {
      flex-direction: column;
      padding-bottom: 8px;
    }


    .content {
      flex-direction: column;
      flex-basis: 67%;
      min-width: 280px;
    }

    .form-content {

      width: 100%;
      height: 46px;
      padding: 0 16px;
      border: 1px solid #dadce0;
      border-radius: 4px;
      border-top-left-radius: 4px;
      border-top-right-radius: 4px;
      border-top-right-radius: 4px;
      border-bottom-right-radius: 4px;
      border-bottom-right-radius: 4px;
      border-bottom-left-radius: 4px;
      pointer-events: none;
    }


    .form-content:hover {
      border: 2px solid black;
    }

    .input-class {
      height: 100%;
      width: 100%;
      display: flex;
      border: none !important;
      background-color: transparent;
      pointer-events: auto;
      font-family: Roboto, Arial, sans-serif;
      font-size: 1rem;
      font-weight: 400;
      letter-spacing: .00625em;
    }

    .input-class:focus {
      outline: none;
    }
  `],
                    providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotDatePickerComponent) }]
                }]
        }], propDecorators: { _template: [{
                type: ViewChild,
                args: [TemplateRef]
            }], label: [{
                type: Input
            }], attribute: [{
                type: Input
            }], description: [{
                type: Input
            }], value: [{
                type: Input
            }], type: [{
                type: Input
            }], disabled: [{
                type: Input
            }], onChange: [{
                type: Output
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW90LWRhdGUtcGlja2VyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2lvdC11aS1saWIvc3JjL2xpYi9mb3JtL2lvdC1kYXRlLXBpY2tlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFDLFNBQVMsRUFBRSxZQUFZLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBVSxNQUFNLEVBQUUsV0FBVyxFQUFFLFNBQVMsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUNqSCxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQztBQUN0RCxPQUFPLEVBQUMsV0FBVyxFQUFFLFNBQVMsRUFBQyxNQUFNLGdCQUFnQixDQUFDOzs7Ozs7OztBQXlIdEQsTUFBTSxPQUFPLHNCQUF1QixTQUFRLGdCQUFnQjtJQXZINUQ7O1FBMkhFLG1CQUFjLEdBQUcsS0FBSyxDQUFDO1FBK0J2QixTQUFJLEdBQUcsSUFBSSxTQUFTLENBQUM7WUFDbkIsTUFBTSxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsQ0FBQztTQUM1QixDQUFDLENBQUM7UUFHTSxjQUFTLEdBQUcsTUFBTSxDQUFDO1FBQ25CLGdCQUFXLEdBQUcsb0JBQW9CLENBQUM7UUFFbkMsU0FBSSxHQUFHLE1BQU0sQ0FBQztRQUNkLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFDaEIsYUFBUSxHQUFHLElBQUksWUFBWSxFQUFVLENBQUM7S0FlakQ7SUF2REMsSUFBVyxRQUFRO1FBRWpCLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUN4QixDQUFDO0lBRUQsSUFBVyxRQUFRO1FBRWpCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDL0MsQ0FBQztJQUVELElBQVcsR0FBRztRQUNaLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUN4QixDQUFDO0lBRUQsSUFBVyxRQUFRO1FBQ2pCLElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUN2QixJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUU7Z0JBQy9CLE9BQU87b0JBQ0wsT0FBTyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUU7aUJBQ3ZDLENBQUE7YUFDRjtpQkFBTTtnQkFDTCxPQUFPO29CQUNMLE9BQU8sRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLEdBQUUsSUFBSTtpQkFDaEQsQ0FBQTthQUNGO1NBRUY7UUFDRCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztJQUNoQyxDQUFDO0lBY0QsUUFBUTtRQUNOLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNqQixJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUU7WUFDcEMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsQ0FBQztZQUNqRCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztTQUM1QjtRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUMsQ0FBQyxDQUFDO1FBQ3pDLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNqQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksU0FBUyxDQUFDO2dCQUN4QixNQUFNLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBQyxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFDLENBQUM7YUFDOUQsQ0FBQyxDQUFDO1NBQ0o7SUFDSCxDQUFDOzttSEEzRFUsc0JBQXNCO3VHQUF0QixzQkFBc0IsaU5BRnRCLENBQUMsRUFBQyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsV0FBVyxFQUFFLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxzQkFBc0IsQ0FBQyxFQUFDLENBQUMscUVBS3BGLFdBQVcsdUVBeEhaOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQTZCVDsyRkF3RlUsc0JBQXNCO2tCQXZIbEMsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsaUJBQWlCO29CQUMzQixRQUFRLEVBQUU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0dBNkJUO29CQUNELE1BQU0sRUFBRSxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7R0FvRlIsQ0FBQztvQkFDRixTQUFTLEVBQUUsQ0FBQyxFQUFDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxXQUFXLEVBQUUsVUFBVSxDQUFDLEdBQUcsRUFBRSx1QkFBdUIsQ0FBQyxFQUFDLENBQUM7aUJBQ2hHOzhCQUl5QixTQUFTO3NCQUFoQyxTQUFTO3VCQUFDLFdBQVc7Z0JBb0NiLEtBQUs7c0JBQWIsS0FBSztnQkFDRyxTQUFTO3NCQUFqQixLQUFLO2dCQUNHLFdBQVc7c0JBQW5CLEtBQUs7Z0JBQ0csS0FBSztzQkFBYixLQUFLO2dCQUNHLElBQUk7c0JBQVosS0FBSztnQkFDRyxRQUFRO3NCQUFoQixLQUFLO2dCQUNJLFFBQVE7c0JBQWpCLE1BQU0iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBmb3J3YXJkUmVmLCBJbnB1dCwgT25Jbml0LCBPdXRwdXQsIFRlbXBsYXRlUmVmLCBWaWV3Q2hpbGR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtEeW5hbWljRm9ybVdpZHRofSBmcm9tIFwiLi9keW5hbWljLWZvcm0td2lkdGhcIjtcbmltcG9ydCB7Rm9ybUNvbnRyb2wsIEZvcm1Hcm91cH0gZnJvbSBcIkBhbmd1bGFyL2Zvcm1zXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2lvdC1kYXRlLXBpY2tlcicsXG4gIHRlbXBsYXRlOiBgXG4gICAgPG5nLXRlbXBsYXRlPlxuICAgICAgPGRpdiBjbGFzcz1cIm91dGVyLW1hcmdpblwiPlxuICAgICAgICA8ZGl2XG4gICAgICAgICAgY2xhc3M9XCJvdXRlcm9uZWxpbmVcIlxuICAgICAgICAgIFtuZ0NsYXNzXT1cInsnb3V0ZXJzbWFsbCc6IWlzQmlnfVwiPlxuICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgIGNsYXNzPVwibGFiZWwtb3V0ZXJcIlxuICAgICAgICAgICAgW25nQ2xhc3NdPVwieydsYWJlbC1vdXRlci10aGluJzohaXNCaWd9XCJcbiAgICAgICAgICA+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwibGFiZWxcIj57e2xhYmVsIHwgdHJhbnNsYXRlfX0gPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGZvcm0gW2Zvcm1Hcm91cF09XCJmb3JtXCIgY2xhc3M9XCJjb250ZW50XCI+XG5cbiAgICAgICAgICAgIDxtYXQtZm9ybS1maWVsZCBhcHBlYXJhbmNlPVwib3V0bGluZVwiIGNsYXNzPVwidy0xMDBcIj5cbiAgICAgICAgICAgICAgPG1hdC1sYWJlbD57e2Rlc2NyaXB0aW9uIHwgdHJhbnNsYXRlfX08L21hdC1sYWJlbD5cbiAgICAgICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICAgICAgZm9ybUNvbnRyb2xOYW1lPVwiZGF0ZUlkXCJcbiAgICAgICAgICAgICAgICAobmdNb2RlbENoYW5nZSk9XCJvbkNoYW5nZS5lbWl0KCRldmVudClcIlxuICAgICAgICAgICAgICAgIG1hdElucHV0IFttYXREYXRlcGlja2VyXT1cInBpY2tlclwiPlxuICAgICAgICAgICAgICA8bWF0LWRhdGVwaWNrZXItdG9nZ2xlIG1hdFN1ZmZpeCBbZm9yXT1cInBpY2tlclwiPjwvbWF0LWRhdGVwaWNrZXItdG9nZ2xlPlxuICAgICAgICAgICAgICA8bWF0LWRhdGVwaWNrZXIgI3BpY2tlcj48L21hdC1kYXRlcGlja2VyPlxuICAgICAgICAgICAgPC9tYXQtZm9ybS1maWVsZD5cbiAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiYm9yZGVyLWNsYXNzXCI+PC9zcGFuPlxuICAgICAgICAgIDwvZm9ybT5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICA8L25nLXRlbXBsYXRlPlxuXG4gIGAsXG4gIHN0eWxlczogW2BcbiAgICAub3V0ZXItbWFyZ2luIHtcbiAgICAgIC8qbWFyZ2luLWxlZnQ6IC01cHg7Ki9cblxuICAgICAgcGFkZGluZy1yaWdodDogNTBweDtcbiAgICB9XG5cbiAgICAubGFiZWwtb3V0ZXIubGFiZWwtb3V0ZXItdGhpbiB7XG4gICAgICBtaW4taGVpZ2h0OiA0MHB4O1xuICAgIH1cblxuICAgIC5sYWJlbC1vdXRlciB7XG4gICAgICBmbGV4LWJhc2lzOiAzMyU7XG4gICAgICBtaW4taGVpZ2h0OiA2NHB4O1xuICAgICAgbWluLXdpZHRoOiAyMjBweDtcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgcGFkZGluZy10b3A6IDEzcHg7XG4gICAgICBjb2xvcjogIzNjNDA0MztcbiAgICB9XG5cbiAgICAubGFiZWwge1xuXG4gICAgICBmb250LWZhbWlseTogUm9ib3RvLCBBcmlhbCwgc2Fucy1zZXJpZjtcbiAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICBoZWlnaHQ6IDIwcHg7XG4gICAgICBjb2xvcjogcmdiKDYwLCA2NCwgNjcpO1xuICAgIH1cblxuICAgIC5vdXRlcm9uZWxpbmUge1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIG1heC13aWR0aDogOTAwcHg7XG4gICAgfVxuXG5cbiAgICAub3V0ZXJzbWFsbCB7XG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgcGFkZGluZy1ib3R0b206IDhweDtcbiAgICB9XG5cblxuICAgIC5jb250ZW50IHtcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICBmbGV4LWJhc2lzOiA2NyU7XG4gICAgICBtaW4td2lkdGg6IDI4MHB4O1xuICAgIH1cblxuICAgIC5mb3JtLWNvbnRlbnQge1xuXG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIGhlaWdodDogNDZweDtcbiAgICAgIHBhZGRpbmc6IDAgMTZweDtcbiAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNkYWRjZTA7XG4gICAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiA0cHg7XG4gICAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogNHB4O1xuICAgICAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDRweDtcbiAgICAgIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiA0cHg7XG4gICAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogNHB4O1xuICAgICAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogNHB4O1xuICAgICAgcG9pbnRlci1ldmVudHM6IG5vbmU7XG4gICAgfVxuXG5cbiAgICAuZm9ybS1jb250ZW50OmhvdmVyIHtcbiAgICAgIGJvcmRlcjogMnB4IHNvbGlkIGJsYWNrO1xuICAgIH1cblxuICAgIC5pbnB1dC1jbGFzcyB7XG4gICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICBib3JkZXI6IG5vbmUgIWltcG9ydGFudDtcbiAgICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICAgICAgcG9pbnRlci1ldmVudHM6IGF1dG87XG4gICAgICBmb250LWZhbWlseTogUm9ib3RvLCBBcmlhbCwgc2Fucy1zZXJpZjtcbiAgICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICBsZXR0ZXItc3BhY2luZzogLjAwNjI1ZW07XG4gICAgfVxuXG4gICAgLmlucHV0LWNsYXNzOmZvY3VzIHtcbiAgICAgIG91dGxpbmU6IG5vbmU7XG4gICAgfVxuICBgXSxcbiAgcHJvdmlkZXJzOiBbe3Byb3ZpZGU6IER5bmFtaWNGb3JtV2lkdGgsIHVzZUV4aXN0aW5nOiBmb3J3YXJkUmVmKCgpID0+IElvdERhdGVQaWNrZXJDb21wb25lbnQpfV1cbn0pXG5leHBvcnQgY2xhc3MgSW90RGF0ZVBpY2tlckNvbXBvbmVudCBleHRlbmRzIER5bmFtaWNGb3JtV2lkdGggaW1wbGVtZW50cyBPbkluaXQge1xuICAvLyBvZmZzZXQ6IE9ic2VydmFibGU8bnVtYmVyPiA9IHRoaXMuc3RvcmUuc2VsZWN0KG5hdk9mZnNldCk7XG5cbiAgQFZpZXdDaGlsZChUZW1wbGF0ZVJlZikgX3RlbXBsYXRlOiBUZW1wbGF0ZVJlZjxhbnk+O1xuICB0aW1lc3RhbXBJblNlYyA9IGZhbHNlO1xuICBwdWJsaWMgZ2V0IHRlbXBsYXRlKCkge1xuXG4gICAgcmV0dXJuIHRoaXMuX3RlbXBsYXRlO1xuICB9XG5cbiAgcHVibGljIGdldCBwcmlzdGluZSgpOiBhbnkge1xuXG4gICAgcmV0dXJuIHRoaXMuZm9ybS52YWx1ZS5kYXRlSWQgPT09IHRoaXMudmFsdWU7XG4gIH1cblxuICBwdWJsaWMgZ2V0IGtleSgpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLmF0dHJpYnV0ZTtcbiAgfVxuXG4gIHB1YmxpYyBnZXQgbmV3VmFsdWUoKTogYW55IHtcbiAgICBpZiAodGhpcy50aW1lc3RhbXBJblNlYykge1xuICAgICAgaWYgKHRoaXMuZm9ybS52YWx1ZS5kYXRlSWQudW5peCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgIHNlY29uZHM6IHRoaXMuZm9ybS52YWx1ZS5kYXRlSWQudW5peCgpXG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgc2Vjb25kczogdGhpcy5mb3JtLnZhbHVlLmRhdGVJZC5nZXRUaW1lKCkgLzEwMDBcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgfVxuICAgIHJldHVybiB0aGlzLmZvcm0udmFsdWUuZGF0ZUlkO1xuICB9XG5cbiAgZm9ybSA9IG5ldyBGb3JtR3JvdXAoe1xuICAgIGRhdGVJZDogbmV3IEZvcm1Db250cm9sKCcnKSxcbiAgfSk7XG5cbiAgQElucHV0KCkgbGFiZWw7XG4gIEBJbnB1dCgpIGF0dHJpYnV0ZSA9ICd0b2RvJztcbiAgQElucHV0KCkgZGVzY3JpcHRpb24gPSAnZXhwbGFpbiB0aGlzIGZpZWxkJztcbiAgQElucHV0KCkgdmFsdWU7XG4gIEBJbnB1dCgpIHR5cGUgPSAndGV4dCc7XG4gIEBJbnB1dCgpIGRpc2FibGVkID0gZmFsc2U7XG4gIEBPdXRwdXQoKSBvbkNoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXI8c3RyaW5nPigpO1xuXG4gIG5nT25Jbml0KCkge1xuICAgIHN1cGVyLm5nT25Jbml0KCk7XG4gICAgaWYgKHRoaXMudmFsdWUgJiYgdGhpcy52YWx1ZS5zZWNvbmRzKSB7XG4gICAgICB0aGlzLnZhbHVlID0gbmV3IERhdGUodGhpcy52YWx1ZS5zZWNvbmRzICogMTAwMCk7XG4gICAgICB0aGlzLnRpbWVzdGFtcEluU2VjID0gdHJ1ZTtcbiAgICB9XG4gICAgdGhpcy5mb3JtLnNldFZhbHVlKHtkYXRlSWQ6IHRoaXMudmFsdWV9KTtcbiAgICBpZiAodGhpcy5kaXNhYmxlZCkge1xuICAgICAgdGhpcy5mb3JtID0gbmV3IEZvcm1Hcm91cCh7XG4gICAgICAgIGRhdGVJZDogbmV3IEZvcm1Db250cm9sKHt2YWx1ZTogJycsIGRpc2FibGVkOiB0aGlzLmRpc2FibGVkfSksXG4gICAgICB9KTtcbiAgICB9XG4gIH1cbn1cbiJdfQ==