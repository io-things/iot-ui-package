import { Component, EventEmitter, forwardRef, Input, Output, TemplateRef, ViewChild } from '@angular/core';
import { DynamicFormWidth } from "./dynamic-form-width";
import { FormControl, FormGroup } from "@angular/forms";
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
import * as i2 from "@angular/forms";
import * as i3 from "@angular/material/input";
import * as i4 from "@ngx-translate/core";
export class IotInputTextComponent extends DynamicFormWidth {
    constructor() {
        super(...arguments);
        this.form = new FormGroup({
            inputId: new FormControl(''),
        });
        this.attribute = 'todo';
        this.value = '';
        this.type = 'text';
        this.disabled = false;
        this.onChange = new EventEmitter();
    }
    get template() {
        return this._template;
    }
    get pristine() {
        return this.form.value.inputId === this.value;
    }
    get key() {
        return this.attribute;
    }
    get newValue() {
        return this.form.value.inputId;
    }
    ngOnInit() {
        super.ngOnInit();
        this.form = new FormGroup({
            inputId: new FormControl({ value: this.value, disabled: this.disabled }),
        });
        // if (this.disabled) {
        //
        // }
    }
    ngOnChanges(changes) {
        if (changes.value && !!this.value) {
            this.form.setValue({ inputId: this.value });
        }
    }
}
IotInputTextComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputTextComponent, deps: null, target: i0.ɵɵFactoryTarget.Component });
IotInputTextComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotInputTextComponent, selector: "iot-input-text", inputs: { label: "label", attribute: "attribute", description: "description", value: "value", type: "type", disabled: "disabled" }, outputs: { onChange: "onChange" }, providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputTextComponent) }], viewQueries: [{ propertyName: "_template", first: true, predicate: TemplateRef, descendants: true }], usesInheritance: true, usesOnChanges: true, ngImport: i0, template: `
    <ng-template>
        <div class="outer-margin">
            <div class="outeroneline" [ngClass]="{'outersmall':!isBig}">
                <div class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
                >
                    <div class="label">{{label | translate}} </div>
                </div>

                <form [formGroup]="form" class="content">
                  <div
                    *ngIf="!!description"
                    class="checkbox-content-description">{{description | translate}}</div>
                    <div class="form-content">

                        <input class="input-class" matInput [type]="type"
                               formControlName="inputId"
                               (ngModelChange)="onChange.emit($event)"

                               >
                        <span class="border-class"></span>
                    </div>
                </form>


            </div>

        </div>
    </ng-template>

    `, isInline: true, styles: ["\n        .outer-margin {\n            /*margin-left: -5px;*/\n\n            padding-right: 50px;\n        }\n\n        .label-outer.label-outer-thin {\n            min-height: 40px;\n        }\n\n        .checkbox-content-description {\n          font-family: Roboto, Arial, sans-serif;\n          font-size: 12px;\n          font-weight: 400;\n          letter-spacing: .025em;\n          line-height: 16px;\n          color: #5f6368;\n          margin-bottom: 13px;\n          margin-top: 2px;\n        }\n\n        .label-outer {\n            flex-basis: 33%;\n            min-height: 64px;\n            min-width: 220px;\n            display: block;\n            padding-top: 13px;\n            color: #3c4043;\n        }\n\n        .label {\n\n            font-family: Roboto, Arial, sans-serif;\n            font-size: 14px;\n            font-weight: 400;\n            height: 20px;\n            color: rgb(60, 64, 67);\n        }\n\n        .outeroneline {\n            display: flex;\n            max-width: 900px;\n        }\n\n\n        .outersmall {\n            flex-direction: column;\n            padding-bottom: 8px;\n        }\n\n\n        .content {\n            flex-direction: column;\n            flex-basis: 67%;\n            min-width: 280px;\n        }\n\n        .form-content {\n\n            width: 100%;\n            height: 46px;\n            padding: 0 16px;\n            border: 1px solid #dadce0;\n            border-radius: 4px;\n            border-top-left-radius: 4px;\n            border-top-right-radius: 4px;\n            border-top-right-radius: 4px;\n            border-bottom-right-radius: 4px;\n            border-bottom-right-radius: 4px;\n            border-bottom-left-radius: 4px;\n            pointer-events: none;\n        }\n\n\n        .form-content:hover {\n            border: 2px solid black;\n        }\n\n        .input-class {\n            height: 100%;\n            width: 100%;\n            display: flex;\n            border: none !important;\n            background-color: transparent;\n            pointer-events: auto;\n            font-family: Roboto, Arial, sans-serif;\n            font-size: 1rem;\n            font-weight: 400;\n            letter-spacing: .00625em;\n        }\n\n        .input-class:focus {\n            outline: none;\n        }\n    "], directives: [{ type: i1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i2.ɵNgNoValidate, selector: "form:not([ngNoForm]):not([ngNativeValidate])" }, { type: i2.NgControlStatusGroup, selector: "[formGroupName],[formArrayName],[ngModelGroup],[formGroup],form:not([ngNoForm]),[ngForm]" }, { type: i2.FormGroupDirective, selector: "[formGroup]", inputs: ["formGroup"], outputs: ["ngSubmit"], exportAs: ["ngForm"] }, { type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i3.MatInput, selector: "input[matInput], textarea[matInput], select[matNativeControl],      input[matNativeControl], textarea[matNativeControl]", inputs: ["id", "disabled", "required", "type", "value", "readonly", "placeholder", "errorStateMatcher", "aria-describedby"], exportAs: ["matInput"] }, { type: i2.DefaultValueAccessor, selector: "input:not([type=checkbox])[formControlName],textarea[formControlName],input:not([type=checkbox])[formControl],textarea[formControl],input:not([type=checkbox])[ngModel],textarea[ngModel],[ngDefaultControl]" }, { type: i2.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { type: i2.FormControlName, selector: "[formControlName]", inputs: ["disabled", "formControlName", "ngModel"], outputs: ["ngModelChange"] }], pipes: { "translate": i4.TranslatePipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputTextComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-input-text',
                    template: `
    <ng-template>
        <div class="outer-margin">
            <div class="outeroneline" [ngClass]="{'outersmall':!isBig}">
                <div class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
                >
                    <div class="label">{{label | translate}} </div>
                </div>

                <form [formGroup]="form" class="content">
                  <div
                    *ngIf="!!description"
                    class="checkbox-content-description">{{description | translate}}</div>
                    <div class="form-content">

                        <input class="input-class" matInput [type]="type"
                               formControlName="inputId"
                               (ngModelChange)="onChange.emit($event)"

                               >
                        <span class="border-class"></span>
                    </div>
                </form>


            </div>

        </div>
    </ng-template>

    `,
                    styles: [`
        .outer-margin {
            /*margin-left: -5px;*/

            padding-right: 50px;
        }

        .label-outer.label-outer-thin {
            min-height: 40px;
        }

        .checkbox-content-description {
          font-family: Roboto, Arial, sans-serif;
          font-size: 12px;
          font-weight: 400;
          letter-spacing: .025em;
          line-height: 16px;
          color: #5f6368;
          margin-bottom: 13px;
          margin-top: 2px;
        }

        .label-outer {
            flex-basis: 33%;
            min-height: 64px;
            min-width: 220px;
            display: block;
            padding-top: 13px;
            color: #3c4043;
        }

        .label {

            font-family: Roboto, Arial, sans-serif;
            font-size: 14px;
            font-weight: 400;
            height: 20px;
            color: rgb(60, 64, 67);
        }

        .outeroneline {
            display: flex;
            max-width: 900px;
        }


        .outersmall {
            flex-direction: column;
            padding-bottom: 8px;
        }


        .content {
            flex-direction: column;
            flex-basis: 67%;
            min-width: 280px;
        }

        .form-content {

            width: 100%;
            height: 46px;
            padding: 0 16px;
            border: 1px solid #dadce0;
            border-radius: 4px;
            border-top-left-radius: 4px;
            border-top-right-radius: 4px;
            border-top-right-radius: 4px;
            border-bottom-right-radius: 4px;
            border-bottom-right-radius: 4px;
            border-bottom-left-radius: 4px;
            pointer-events: none;
        }


        .form-content:hover {
            border: 2px solid black;
        }

        .input-class {
            height: 100%;
            width: 100%;
            display: flex;
            border: none !important;
            background-color: transparent;
            pointer-events: auto;
            font-family: Roboto, Arial, sans-serif;
            font-size: 1rem;
            font-weight: 400;
            letter-spacing: .00625em;
        }

        .input-class:focus {
            outline: none;
        }
    `],
                    providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputTextComponent) }]
                }]
        }], propDecorators: { _template: [{
                type: ViewChild,
                args: [TemplateRef]
            }], label: [{
                type: Input
            }], attribute: [{
                type: Input
            }], description: [{
                type: Input
            }], value: [{
                type: Input
            }], type: [{
                type: Input
            }], disabled: [{
                type: Input
            }], onChange: [{
                type: Output
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW90LWlucHV0LXRleHQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvaW90LXVpLWxpYi9zcmMvbGliL2Zvcm0vaW90LWlucHV0LXRleHQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFDTCxTQUFTLEVBQ1QsWUFBWSxFQUNaLFVBQVUsRUFDVixLQUFLLEVBR0wsTUFBTSxFQUNOLFdBQVcsRUFDWCxTQUFTLEVBQ1YsTUFBTSxlQUFlLENBQUM7QUFDdkIsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0sc0JBQXNCLENBQUM7QUFDdEQsT0FBTyxFQUFDLFdBQVcsRUFBRSxTQUFTLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQzs7Ozs7O0FBcUl0RCxNQUFNLE9BQU8scUJBQXNCLFNBQVEsZ0JBQWdCO0lBbkkzRDs7UUF5SkUsU0FBSSxHQUFHLElBQUksU0FBUyxDQUFDO1lBQ25CLE9BQU8sRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLENBQUM7U0FDN0IsQ0FBQyxDQUFDO1FBR00sY0FBUyxHQUFHLE1BQU0sQ0FBQztRQUVuQixVQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ1gsU0FBSSxHQUFHLE1BQU0sQ0FBQztRQUNkLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFDaEIsYUFBUSxHQUFHLElBQUksWUFBWSxFQUFVLENBQUM7S0FpQmpEO0lBN0NDLElBQVcsUUFBUTtRQUVqQixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7SUFDeEIsQ0FBQztJQUVELElBQVcsUUFBUTtRQUVqQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQ2hELENBQUM7SUFFRCxJQUFXLEdBQUc7UUFDWixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7SUFDeEIsQ0FBQztJQUVELElBQVcsUUFBUTtRQUNqQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQztJQUNqQyxDQUFDO0lBY0QsUUFBUTtRQUNOLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNqQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksU0FBUyxDQUFDO1lBQ3hCLE9BQU8sRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFDLEtBQUssRUFBQyxJQUFJLENBQUMsS0FBSyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFDLENBQUM7U0FDdEUsQ0FBQyxDQUFDO1FBQ0gsdUJBQXVCO1FBQ3ZCLEVBQUU7UUFDRixJQUFJO0lBQ04sQ0FBQztJQUVELFdBQVcsQ0FBQyxPQUFzQjtRQUNoQyxJQUFJLE9BQU8sQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDakMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBQyxDQUFDLENBQUM7U0FDM0M7SUFDSCxDQUFDOztrSEFoRFUscUJBQXFCO3NHQUFyQixxQkFBcUIsZ05BRnJCLENBQUMsRUFBQyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsV0FBVyxFQUFFLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFDLENBQUMscUVBSW5GLFdBQVcsNEZBbklaOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7S0E4QlA7MkZBbUdRLHFCQUFxQjtrQkFuSWpDLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLGdCQUFnQjtvQkFDMUIsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7S0E4QlA7b0JBQ0gsTUFBTSxFQUFFLENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0tBK0ZOLENBQUM7b0JBQ0osU0FBUyxFQUFFLENBQUMsRUFBQyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsV0FBVyxFQUFFLFVBQVUsQ0FBQyxHQUFHLEVBQUUsc0JBQXNCLENBQUMsRUFBQyxDQUFDO2lCQUMvRjs4QkFHeUIsU0FBUztzQkFBaEMsU0FBUzt1QkFBQyxXQUFXO2dCQXdCYixLQUFLO3NCQUFiLEtBQUs7Z0JBQ0csU0FBUztzQkFBakIsS0FBSztnQkFDRyxXQUFXO3NCQUFuQixLQUFLO2dCQUNHLEtBQUs7c0JBQWIsS0FBSztnQkFDRyxJQUFJO3NCQUFaLEtBQUs7Z0JBQ0csUUFBUTtzQkFBaEIsS0FBSztnQkFDSSxRQUFRO3NCQUFqQixNQUFNIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcbiAgQ29tcG9uZW50LFxuICBFdmVudEVtaXR0ZXIsXG4gIGZvcndhcmRSZWYsXG4gIElucHV0LFxuICBPbkNoYW5nZXMsXG4gIE9uSW5pdCxcbiAgT3V0cHV0LCBTaW1wbGVDaGFuZ2VzLFxuICBUZW1wbGF0ZVJlZixcbiAgVmlld0NoaWxkXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtEeW5hbWljRm9ybVdpZHRofSBmcm9tIFwiLi9keW5hbWljLWZvcm0td2lkdGhcIjtcbmltcG9ydCB7Rm9ybUNvbnRyb2wsIEZvcm1Hcm91cH0gZnJvbSBcIkBhbmd1bGFyL2Zvcm1zXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2lvdC1pbnB1dC10ZXh0JyxcbiAgdGVtcGxhdGU6IGBcbiAgICA8bmctdGVtcGxhdGU+XG4gICAgICAgIDxkaXYgY2xhc3M9XCJvdXRlci1tYXJnaW5cIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJvdXRlcm9uZWxpbmVcIiBbbmdDbGFzc109XCJ7J291dGVyc21hbGwnOiFpc0JpZ31cIj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwibGFiZWwtb3V0ZXJcIiBbbmdDbGFzc109XCJ7J2xhYmVsLW91dGVyLXRoaW4nOiFpc0JpZ31cIlxuICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImxhYmVsXCI+e3tsYWJlbCB8IHRyYW5zbGF0ZX19IDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuXG4gICAgICAgICAgICAgICAgPGZvcm0gW2Zvcm1Hcm91cF09XCJmb3JtXCIgY2xhc3M9XCJjb250ZW50XCI+XG4gICAgICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgICAgICpuZ0lmPVwiISFkZXNjcmlwdGlvblwiXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzPVwiY2hlY2tib3gtY29udGVudC1kZXNjcmlwdGlvblwiPnt7ZGVzY3JpcHRpb24gfCB0cmFuc2xhdGV9fTwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1jb250ZW50XCI+XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCBjbGFzcz1cImlucHV0LWNsYXNzXCIgbWF0SW5wdXQgW3R5cGVdPVwidHlwZVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9ybUNvbnRyb2xOYW1lPVwiaW5wdXRJZFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKG5nTW9kZWxDaGFuZ2UpPVwib25DaGFuZ2UuZW1pdCgkZXZlbnQpXCJcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiYm9yZGVyLWNsYXNzXCI+PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Zvcm0+XG5cblxuICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgPC9kaXY+XG4gICAgPC9uZy10ZW1wbGF0ZT5cblxuICAgIGAsXG4gIHN0eWxlczogW2BcbiAgICAgICAgLm91dGVyLW1hcmdpbiB7XG4gICAgICAgICAgICAvKm1hcmdpbi1sZWZ0OiAtNXB4OyovXG5cbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDUwcHg7XG4gICAgICAgIH1cblxuICAgICAgICAubGFiZWwtb3V0ZXIubGFiZWwtb3V0ZXItdGhpbiB7XG4gICAgICAgICAgICBtaW4taGVpZ2h0OiA0MHB4O1xuICAgICAgICB9XG5cbiAgICAgICAgLmNoZWNrYm94LWNvbnRlbnQtZGVzY3JpcHRpb24ge1xuICAgICAgICAgIGZvbnQtZmFtaWx5OiBSb2JvdG8sIEFyaWFsLCBzYW5zLXNlcmlmO1xuICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgICAgICBmb250LXdlaWdodDogNDAwO1xuICAgICAgICAgIGxldHRlci1zcGFjaW5nOiAuMDI1ZW07XG4gICAgICAgICAgbGluZS1oZWlnaHQ6IDE2cHg7XG4gICAgICAgICAgY29sb3I6ICM1ZjYzNjg7XG4gICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTNweDtcbiAgICAgICAgICBtYXJnaW4tdG9wOiAycHg7XG4gICAgICAgIH1cblxuICAgICAgICAubGFiZWwtb3V0ZXIge1xuICAgICAgICAgICAgZmxleC1iYXNpczogMzMlO1xuICAgICAgICAgICAgbWluLWhlaWdodDogNjRweDtcbiAgICAgICAgICAgIG1pbi13aWR0aDogMjIwcHg7XG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAxM3B4O1xuICAgICAgICAgICAgY29sb3I6ICMzYzQwNDM7XG4gICAgICAgIH1cblxuICAgICAgICAubGFiZWwge1xuXG4gICAgICAgICAgICBmb250LWZhbWlseTogUm9ib3RvLCBBcmlhbCwgc2Fucy1zZXJpZjtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICAgICAgICBoZWlnaHQ6IDIwcHg7XG4gICAgICAgICAgICBjb2xvcjogcmdiKDYwLCA2NCwgNjcpO1xuICAgICAgICB9XG5cbiAgICAgICAgLm91dGVyb25lbGluZSB7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgbWF4LXdpZHRoOiA5MDBweDtcbiAgICAgICAgfVxuXG5cbiAgICAgICAgLm91dGVyc21hbGwge1xuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiA4cHg7XG4gICAgICAgIH1cblxuXG4gICAgICAgIC5jb250ZW50IHtcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgICAgICBmbGV4LWJhc2lzOiA2NyU7XG4gICAgICAgICAgICBtaW4td2lkdGg6IDI4MHB4O1xuICAgICAgICB9XG5cbiAgICAgICAgLmZvcm0tY29udGVudCB7XG5cbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgaGVpZ2h0OiA0NnB4O1xuICAgICAgICAgICAgcGFkZGluZzogMCAxNnB4O1xuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2RhZGNlMDtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICAgICAgICAgIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDRweDtcbiAgICAgICAgICAgIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiA0cHg7XG4gICAgICAgICAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogNHB4O1xuICAgICAgICAgICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDRweDtcbiAgICAgICAgICAgIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiA0cHg7XG4gICAgICAgICAgICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiA0cHg7XG4gICAgICAgICAgICBwb2ludGVyLWV2ZW50czogbm9uZTtcbiAgICAgICAgfVxuXG5cbiAgICAgICAgLmZvcm0tY29udGVudDpob3ZlciB7XG4gICAgICAgICAgICBib3JkZXI6IDJweCBzb2xpZCBibGFjaztcbiAgICAgICAgfVxuXG4gICAgICAgIC5pbnB1dC1jbGFzcyB7XG4gICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBib3JkZXI6IG5vbmUgIWltcG9ydGFudDtcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICAgICAgICAgICAgcG9pbnRlci1ldmVudHM6IGF1dG87XG4gICAgICAgICAgICBmb250LWZhbWlseTogUm9ib3RvLCBBcmlhbCwgc2Fucy1zZXJpZjtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICAgICAgICBsZXR0ZXItc3BhY2luZzogLjAwNjI1ZW07XG4gICAgICAgIH1cblxuICAgICAgICAuaW5wdXQtY2xhc3M6Zm9jdXMge1xuICAgICAgICAgICAgb3V0bGluZTogbm9uZTtcbiAgICAgICAgfVxuICAgIGBdLFxuICBwcm92aWRlcnM6IFt7cHJvdmlkZTogRHluYW1pY0Zvcm1XaWR0aCwgdXNlRXhpc3Rpbmc6IGZvcndhcmRSZWYoKCkgPT4gSW90SW5wdXRUZXh0Q29tcG9uZW50KX1dXG59KVxuZXhwb3J0IGNsYXNzIElvdElucHV0VGV4dENvbXBvbmVudCBleHRlbmRzIER5bmFtaWNGb3JtV2lkdGggaW1wbGVtZW50cyBPbkluaXQsIE9uQ2hhbmdlcyB7XG4gIC8vIG9mZnNldDogT2JzZXJ2YWJsZTxudW1iZXI+ID0gdGhpcy5zdG9yZS5zZWxlY3QobmF2T2Zmc2V0KTtcbiAgQFZpZXdDaGlsZChUZW1wbGF0ZVJlZikgX3RlbXBsYXRlOiBUZW1wbGF0ZVJlZjxhbnk+O1xuXG4gIHB1YmxpYyBnZXQgdGVtcGxhdGUoKSB7XG5cbiAgICByZXR1cm4gdGhpcy5fdGVtcGxhdGU7XG4gIH1cblxuICBwdWJsaWMgZ2V0IHByaXN0aW5lKCk6IGFueSB7XG5cbiAgICByZXR1cm4gdGhpcy5mb3JtLnZhbHVlLmlucHV0SWQgPT09IHRoaXMudmFsdWU7XG4gIH1cblxuICBwdWJsaWMgZ2V0IGtleSgpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLmF0dHJpYnV0ZTtcbiAgfVxuXG4gIHB1YmxpYyBnZXQgbmV3VmFsdWUoKTogc3RyaW5nIHtcbiAgICByZXR1cm4gdGhpcy5mb3JtLnZhbHVlLmlucHV0SWQ7XG4gIH1cblxuICBmb3JtID0gbmV3IEZvcm1Hcm91cCh7XG4gICAgaW5wdXRJZDogbmV3IEZvcm1Db250cm9sKCcnKSxcbiAgfSk7XG5cbiAgQElucHV0KCkgbGFiZWw7XG4gIEBJbnB1dCgpIGF0dHJpYnV0ZSA9ICd0b2RvJztcbiAgQElucHV0KCkgZGVzY3JpcHRpb24gO1xuICBASW5wdXQoKSB2YWx1ZSA9ICcnO1xuICBASW5wdXQoKSB0eXBlID0gJ3RleHQnO1xuICBASW5wdXQoKSBkaXNhYmxlZCA9IGZhbHNlO1xuICBAT3V0cHV0KCkgb25DaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPHN0cmluZz4oKTtcblxuICBuZ09uSW5pdCgpIHtcbiAgICBzdXBlci5uZ09uSW5pdCgpO1xuICAgIHRoaXMuZm9ybSA9IG5ldyBGb3JtR3JvdXAoe1xuICAgICAgaW5wdXRJZDogbmV3IEZvcm1Db250cm9sKHt2YWx1ZTp0aGlzLnZhbHVlLCBkaXNhYmxlZDogdGhpcy5kaXNhYmxlZH0pLFxuICAgIH0pO1xuICAgIC8vIGlmICh0aGlzLmRpc2FibGVkKSB7XG4gICAgLy9cbiAgICAvLyB9XG4gIH1cblxuICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKTogdm9pZCB7XG4gICAgaWYgKGNoYW5nZXMudmFsdWUgJiYgISF0aGlzLnZhbHVlKSB7XG4gICAgICB0aGlzLmZvcm0uc2V0VmFsdWUoe2lucHV0SWQ6IHRoaXMudmFsdWV9KTtcbiAgICB9XG4gIH1cbn1cbiJdfQ==