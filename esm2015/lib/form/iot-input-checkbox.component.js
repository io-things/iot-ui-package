import { Component, EventEmitter, forwardRef, Input, Output, TemplateRef, ViewChild } from '@angular/core';
import { DynamicFormWidth } from './dynamic-form-width';
import { FormControl, FormGroup } from "@angular/forms";
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/checkbox";
import * as i2 from "@angular/common";
import * as i3 from "@angular/forms";
import * as i4 from "@ngx-translate/core";
export class IotInputCheckboxComponent extends DynamicFormWidth {
    constructor() {
        super(...arguments);
        this.form = new FormGroup({
            checkboxControl: new FormControl(false),
        });
        this.leftLabel = '';
        this.attribute = 'todo';
        this.description = 'explain this field';
        this.disabled = false;
        this.inverse = false;
        this.onChange = new EventEmitter();
    }
    get template() {
        return this._template;
    }
    get pristine() {
        if (this.inverse) {
            return (!!this.form.value.checkboxControl) === !this.value;
        }
        return (!!this.form.value.checkboxControl) === this.value;
    }
    get key() {
        return this.attribute;
    }
    get newValue() {
        console.log('checkbox value is ', this.attribute, this.form.value.checkboxControl);
        if (this.inverse) {
            return !this.form.value.checkboxControl;
        }
        return !!this.form.value.checkboxControl;
    }
    change(event) {
        this.onChange.emit(event);
    }
    ngOnInit() {
        super.ngOnInit();
        if (this.disabled) {
            this.form = new FormGroup({
                checkboxControl: new FormControl({ value: '', disabled: this.disabled }),
            });
        }
        if (this.inverse) {
            this.form.setValue({ checkboxControl: !this.value });
        }
        else {
            this.form.setValue({ checkboxControl: this.value });
        }
    }
    ngOnChanges(changes) {
        if (changes.value) {
            if (this.inverse) {
                this.form.setValue({ checkboxControl: !this.value });
            }
            else {
                this.form.setValue({ checkboxControl: !!this.value });
            }
        }
    }
}
IotInputCheckboxComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputCheckboxComponent, deps: null, target: i0.ɵɵFactoryTarget.Component });
IotInputCheckboxComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotInputCheckboxComponent, selector: "iot-input-checkbox", inputs: { leftLabel: "leftLabel", label: "label", attribute: "attribute", description: "description", value: "value", separator: "separator", disabled: "disabled", inverse: "inverse" }, outputs: { onChange: "onChange" }, providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputCheckboxComponent) }], viewQueries: [{ propertyName: "_template", first: true, predicate: TemplateRef, descendants: true }], usesInheritance: true, usesOnChanges: true, ngImport: i0, template: `
    <ng-template>
      <div
        [ngClass]="{'outer-margin':true, 'nopaddingtop' : leftLabel === ''}"
        >
        <div

          class="outer" [ngClass]="{'outersmall':!isBig}">
          <div
            class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
          >
            <div class="label">{{leftLabel | translate}}</div>
          </div>

          <form [formGroup]="form" class="content">
            <div class="checkbox-content">
              <div
                *ngIf="description !== ''"
                class="checkbox-content-description">{{description | translate}}</div>

              <mat-checkbox
                formControlName="checkboxControl"
                            (ngModelChange)="change($event)">
                {{label | translate}}
              </mat-checkbox>
            </div>
            <div *ngIf="separator" class="separator"></div>
          </form>


        </div>

      </div>
    </ng-template>
  `, isInline: true, styles: ["\n      .separator {\n        border-bottom: 1px solid #dadce0;\n        padding-top: 16px;\n      }\n\n      .checkbox-content-description {\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 12px;\n        font-weight: 400;\n        letter-spacing: .025em;\n        line-height: 16px;\n        color: #5f6368;\n        margin-bottom: 13px;\n        margin-top: 2px;\n      }\n\n      .outer-margin {\n        margin-left: 0px;\n        padding-right: 50px;\n      }\n\n      .label-outer.label-outer-thin {\n        min-height: 40px;\n      }\n\n      .label-outer {\n        flex-basis: 33%;\n        min-height: 64px;\n        min-width: 220px;\n        display: block;\n        padding-top: 13px;\n        color: #3c4043;\n      }\n\n      .label {\n\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 14px;\n        font-weight: 400;\n        height: 20px;\n        color: rgb(60, 64, 67);\n      }\n\n      .outer {\n        display: flex;\n        max-width: 900px;\n        margin-bottom: 16px;\n      }\n\n      .outersmall {\n        flex-direction: column;\n        padding-bottom: 8px;\n      }\n\n      .content {\n        flex-direction: column;\n        flex-basis: 67%;\n        min-width: 280px;\n      }\n\n      .checkbox-content {\n        display: flex;\n        flex-direction: column;\n        color: #3c4043;\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 14px;\n        font-weight: 400;\n        letter-spacing: 0.2px;\n        line-height: 20px;\n      }\n    "], components: [{ type: i1.MatCheckbox, selector: "mat-checkbox", inputs: ["disableRipple", "color", "tabIndex", "aria-label", "aria-labelledby", "id", "labelPosition", "name", "required", "checked", "disabled", "indeterminate", "aria-describedby", "value"], outputs: ["change", "indeterminateChange"], exportAs: ["matCheckbox"] }], directives: [{ type: i2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i3.ɵNgNoValidate, selector: "form:not([ngNoForm]):not([ngNativeValidate])" }, { type: i3.NgControlStatusGroup, selector: "[formGroupName],[formArrayName],[ngModelGroup],[formGroup],form:not([ngNoForm]),[ngForm]" }, { type: i3.FormGroupDirective, selector: "[formGroup]", inputs: ["formGroup"], outputs: ["ngSubmit"], exportAs: ["ngForm"] }, { type: i2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i3.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { type: i3.FormControlName, selector: "[formControlName]", inputs: ["disabled", "formControlName", "ngModel"], outputs: ["ngModelChange"] }], pipes: { "translate": i4.TranslatePipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputCheckboxComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-input-checkbox',
                    template: `
    <ng-template>
      <div
        [ngClass]="{'outer-margin':true, 'nopaddingtop' : leftLabel === ''}"
        >
        <div

          class="outer" [ngClass]="{'outersmall':!isBig}">
          <div
            class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
          >
            <div class="label">{{leftLabel | translate}}</div>
          </div>

          <form [formGroup]="form" class="content">
            <div class="checkbox-content">
              <div
                *ngIf="description !== ''"
                class="checkbox-content-description">{{description | translate}}</div>

              <mat-checkbox
                formControlName="checkboxControl"
                            (ngModelChange)="change($event)">
                {{label | translate}}
              </mat-checkbox>
            </div>
            <div *ngIf="separator" class="separator"></div>
          </form>


        </div>

      </div>
    </ng-template>
  `,
                    styles: [
                        `
      .separator {
        border-bottom: 1px solid #dadce0;
        padding-top: 16px;
      }

      .checkbox-content-description {
        font-family: Roboto, Arial, sans-serif;
        font-size: 12px;
        font-weight: 400;
        letter-spacing: .025em;
        line-height: 16px;
        color: #5f6368;
        margin-bottom: 13px;
        margin-top: 2px;
      }

      .outer-margin {
        margin-left: 0px;
        padding-right: 50px;
      }

      .label-outer.label-outer-thin {
        min-height: 40px;
      }

      .label-outer {
        flex-basis: 33%;
        min-height: 64px;
        min-width: 220px;
        display: block;
        padding-top: 13px;
        color: #3c4043;
      }

      .label {

        font-family: Roboto, Arial, sans-serif;
        font-size: 14px;
        font-weight: 400;
        height: 20px;
        color: rgb(60, 64, 67);
      }

      .outer {
        display: flex;
        max-width: 900px;
        margin-bottom: 16px;
      }

      .outersmall {
        flex-direction: column;
        padding-bottom: 8px;
      }

      .content {
        flex-direction: column;
        flex-basis: 67%;
        min-width: 280px;
      }

      .checkbox-content {
        display: flex;
        flex-direction: column;
        color: #3c4043;
        font-family: Roboto, Arial, sans-serif;
        font-size: 14px;
        font-weight: 400;
        letter-spacing: 0.2px;
        line-height: 20px;
      }
    `
                    ],
                    providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputCheckboxComponent) }]
                }]
        }], propDecorators: { _template: [{
                type: ViewChild,
                args: [TemplateRef]
            }], leftLabel: [{
                type: Input
            }], label: [{
                type: Input
            }], attribute: [{
                type: Input
            }], description: [{
                type: Input
            }], value: [{
                type: Input
            }], separator: [{
                type: Input
            }], disabled: [{
                type: Input
            }], inverse: [{
                type: Input
            }], onChange: [{
                type: Output
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW90LWlucHV0LWNoZWNrYm94LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2lvdC11aS1saWIvc3JjL2xpYi9mb3JtL2lvdC1pbnB1dC1jaGVja2JveC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUNMLFNBQVMsRUFDVCxZQUFZLEVBQ1osVUFBVSxFQUNWLEtBQUssRUFHTCxNQUFNLEVBQ04sV0FBVyxFQUNYLFNBQVMsRUFDVixNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQztBQUN0RCxPQUFPLEVBQUMsV0FBVyxFQUFFLFNBQVMsRUFBQyxNQUFNLGdCQUFnQixDQUFDOzs7Ozs7QUFvSHRELE1BQU0sT0FBTyx5QkFBMEIsU0FBUSxnQkFBZ0I7SUFsSC9EOztRQStJRSxTQUFJLEdBQUcsSUFBSSxTQUFTLENBQUM7WUFDbkIsZUFBZSxFQUFFLElBQUksV0FBVyxDQUFDLEtBQUssQ0FBQztTQUN4QyxDQUFDLENBQUM7UUFHTSxjQUFTLEdBQVcsRUFBRSxDQUFDO1FBRXZCLGNBQVMsR0FBRyxNQUFNLENBQUM7UUFDbkIsZ0JBQVcsR0FBRyxvQkFBb0IsQ0FBQztRQUduQyxhQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ2pCLFlBQU8sR0FBRyxLQUFLLENBQUM7UUFHZixhQUFRLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztLQWdDOUM7SUF4RUMsSUFBVyxRQUFRO1FBRWpCLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUN4QixDQUFDO0lBRUQsSUFBVyxRQUFRO1FBQ2pCLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNoQixPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztTQUM1RDtRQUVELE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQztJQUM1RCxDQUFDO0lBRUQsSUFBVyxHQUFHO1FBQ1osT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQ3hCLENBQUM7SUFFRCxJQUFXLFFBQVE7UUFDakIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxDQUFBO1FBQ2xGLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNoQixPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDO1NBQ3pDO1FBQ0QsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDO0lBQzNDLENBQUM7SUFtQkQsTUFBTSxDQUFDLEtBQVU7UUFDZixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUU1QixDQUFDO0lBRUQsUUFBUTtRQUNOLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNqQixJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDakIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLFNBQVMsQ0FBQztnQkFDeEIsZUFBZSxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUMsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBQyxDQUFDO2FBQ3ZFLENBQUMsQ0FBQztTQUNKO1FBQ0QsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ2hCLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUMsZUFBZSxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBQyxDQUFDLENBQUM7U0FDcEQ7YUFBTTtZQUNMLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUMsQ0FBQyxDQUFDO1NBQ25EO0lBRUgsQ0FBQztJQUVELFdBQVcsQ0FBQyxPQUFzQjtRQUNoQyxJQUFJLE9BQU8sQ0FBQyxLQUFLLEVBQUU7WUFDakIsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO2dCQUNoQixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFDLGVBQWUsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUMsQ0FBQyxDQUFDO2FBQ3BEO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUMsZUFBZSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFDLENBQUMsQ0FBQzthQUNyRDtTQUVGO0lBQ0gsQ0FBQzs7c0hBM0VVLHlCQUF5QjswR0FBekIseUJBQXlCLDBRQUZ6QixDQUFDLEVBQUMsT0FBTyxFQUFFLGdCQUFnQixFQUFFLFdBQVcsRUFBRSxVQUFVLENBQUMsR0FBRyxFQUFFLENBQUMseUJBQXlCLENBQUMsRUFBQyxDQUFDLHFFQUl2RixXQUFXLDRGQWxIWjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQWtDVDsyRkE4RVUseUJBQXlCO2tCQWxIckMsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsb0JBQW9CO29CQUM5QixRQUFRLEVBQUU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7R0FrQ1Q7b0JBQ0QsTUFBTSxFQUFFO3dCQUNOOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztLQXVFQztxQkFDRjtvQkFFRCxTQUFTLEVBQUUsQ0FBQyxFQUFDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxXQUFXLEVBQUUsVUFBVSxDQUFDLEdBQUcsRUFBRSwwQkFBMEIsQ0FBQyxFQUFDLENBQUM7aUJBQ25HOzhCQUd5QixTQUFTO3NCQUFoQyxTQUFTO3VCQUFDLFdBQVc7Z0JBZ0NiLFNBQVM7c0JBQWpCLEtBQUs7Z0JBQ0csS0FBSztzQkFBYixLQUFLO2dCQUNHLFNBQVM7c0JBQWpCLEtBQUs7Z0JBQ0csV0FBVztzQkFBbkIsS0FBSztnQkFDRyxLQUFLO3NCQUFiLEtBQUs7Z0JBQ0csU0FBUztzQkFBakIsS0FBSztnQkFDRyxRQUFRO3NCQUFoQixLQUFLO2dCQUNHLE9BQU87c0JBQWYsS0FBSztnQkFHSSxRQUFRO3NCQUFqQixNQUFNIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcbiAgQ29tcG9uZW50LFxuICBFdmVudEVtaXR0ZXIsXG4gIGZvcndhcmRSZWYsXG4gIElucHV0LFxuICBPbkNoYW5nZXMsXG4gIE9uSW5pdCxcbiAgT3V0cHV0LCBTaW1wbGVDaGFuZ2VzLFxuICBUZW1wbGF0ZVJlZixcbiAgVmlld0NoaWxkXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtEeW5hbWljRm9ybVdpZHRofSBmcm9tICcuL2R5bmFtaWMtZm9ybS13aWR0aCc7XG5pbXBvcnQge0Zvcm1Db250cm9sLCBGb3JtR3JvdXB9IGZyb20gXCJAYW5ndWxhci9mb3Jtc1wiO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdpb3QtaW5wdXQtY2hlY2tib3gnLFxuICB0ZW1wbGF0ZTogYFxuICAgIDxuZy10ZW1wbGF0ZT5cbiAgICAgIDxkaXZcbiAgICAgICAgW25nQ2xhc3NdPVwieydvdXRlci1tYXJnaW4nOnRydWUsICdub3BhZGRpbmd0b3AnIDogbGVmdExhYmVsID09PSAnJ31cIlxuICAgICAgICA+XG4gICAgICAgIDxkaXZcblxuICAgICAgICAgIGNsYXNzPVwib3V0ZXJcIiBbbmdDbGFzc109XCJ7J291dGVyc21hbGwnOiFpc0JpZ31cIj5cbiAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICBjbGFzcz1cImxhYmVsLW91dGVyXCIgW25nQ2xhc3NdPVwieydsYWJlbC1vdXRlci10aGluJzohaXNCaWd9XCJcbiAgICAgICAgICA+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwibGFiZWxcIj57e2xlZnRMYWJlbCB8IHRyYW5zbGF0ZX19PC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICA8Zm9ybSBbZm9ybUdyb3VwXT1cImZvcm1cIiBjbGFzcz1cImNvbnRlbnRcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjaGVja2JveC1jb250ZW50XCI+XG4gICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAqbmdJZj1cImRlc2NyaXB0aW9uICE9PSAnJ1wiXG4gICAgICAgICAgICAgICAgY2xhc3M9XCJjaGVja2JveC1jb250ZW50LWRlc2NyaXB0aW9uXCI+e3tkZXNjcmlwdGlvbiB8IHRyYW5zbGF0ZX19PC9kaXY+XG5cbiAgICAgICAgICAgICAgPG1hdC1jaGVja2JveFxuICAgICAgICAgICAgICAgIGZvcm1Db250cm9sTmFtZT1cImNoZWNrYm94Q29udHJvbFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKG5nTW9kZWxDaGFuZ2UpPVwiY2hhbmdlKCRldmVudClcIj5cbiAgICAgICAgICAgICAgICB7e2xhYmVsIHwgdHJhbnNsYXRlfX1cbiAgICAgICAgICAgICAgPC9tYXQtY2hlY2tib3g+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgKm5nSWY9XCJzZXBhcmF0b3JcIiBjbGFzcz1cInNlcGFyYXRvclwiPjwvZGl2PlxuICAgICAgICAgIDwvZm9ybT5cblxuXG4gICAgICAgIDwvZGl2PlxuXG4gICAgICA8L2Rpdj5cbiAgICA8L25nLXRlbXBsYXRlPlxuICBgLFxuICBzdHlsZXM6IFtcbiAgICBgXG4gICAgICAuc2VwYXJhdG9yIHtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkYWRjZTA7XG4gICAgICAgIHBhZGRpbmctdG9wOiAxNnB4O1xuICAgICAgfVxuXG4gICAgICAuY2hlY2tib3gtY29udGVudC1kZXNjcmlwdGlvbiB7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBSb2JvdG8sIEFyaWFsLCBzYW5zLXNlcmlmO1xuICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICAgIGxldHRlci1zcGFjaW5nOiAuMDI1ZW07XG4gICAgICAgIGxpbmUtaGVpZ2h0OiAxNnB4O1xuICAgICAgICBjb2xvcjogIzVmNjM2ODtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTNweDtcbiAgICAgICAgbWFyZ2luLXRvcDogMnB4O1xuICAgICAgfVxuXG4gICAgICAub3V0ZXItbWFyZ2luIHtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDBweDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogNTBweDtcbiAgICAgIH1cblxuICAgICAgLmxhYmVsLW91dGVyLmxhYmVsLW91dGVyLXRoaW4ge1xuICAgICAgICBtaW4taGVpZ2h0OiA0MHB4O1xuICAgICAgfVxuXG4gICAgICAubGFiZWwtb3V0ZXIge1xuICAgICAgICBmbGV4LWJhc2lzOiAzMyU7XG4gICAgICAgIG1pbi1oZWlnaHQ6IDY0cHg7XG4gICAgICAgIG1pbi13aWR0aDogMjIwcHg7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICBwYWRkaW5nLXRvcDogMTNweDtcbiAgICAgICAgY29sb3I6ICMzYzQwNDM7XG4gICAgICB9XG5cbiAgICAgIC5sYWJlbCB7XG5cbiAgICAgICAgZm9udC1mYW1pbHk6IFJvYm90bywgQXJpYWwsIHNhbnMtc2VyaWY7XG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICAgICAgaGVpZ2h0OiAyMHB4O1xuICAgICAgICBjb2xvcjogcmdiKDYwLCA2NCwgNjcpO1xuICAgICAgfVxuXG4gICAgICAub3V0ZXIge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBtYXgtd2lkdGg6IDkwMHB4O1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAxNnB4O1xuICAgICAgfVxuXG4gICAgICAub3V0ZXJzbWFsbCB7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgIHBhZGRpbmctYm90dG9tOiA4cHg7XG4gICAgICB9XG5cbiAgICAgIC5jb250ZW50IHtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAgZmxleC1iYXNpczogNjclO1xuICAgICAgICBtaW4td2lkdGg6IDI4MHB4O1xuICAgICAgfVxuXG4gICAgICAuY2hlY2tib3gtY29udGVudCB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgIGNvbG9yOiAjM2M0MDQzO1xuICAgICAgICBmb250LWZhbWlseTogUm9ib3RvLCBBcmlhbCwgc2Fucy1zZXJpZjtcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICBmb250LXdlaWdodDogNDAwO1xuICAgICAgICBsZXR0ZXItc3BhY2luZzogMC4ycHg7XG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICAgICAgfVxuICAgIGBcbiAgXVxuICAsXG4gIHByb3ZpZGVyczogW3twcm92aWRlOiBEeW5hbWljRm9ybVdpZHRoLCB1c2VFeGlzdGluZzogZm9yd2FyZFJlZigoKSA9PiBJb3RJbnB1dENoZWNrYm94Q29tcG9uZW50KX1dXG59KVxuZXhwb3J0IGNsYXNzIElvdElucHV0Q2hlY2tib3hDb21wb25lbnQgZXh0ZW5kcyBEeW5hbWljRm9ybVdpZHRoIGltcGxlbWVudHMgT25Jbml0LCBPbkNoYW5nZXMge1xuXG4gIEBWaWV3Q2hpbGQoVGVtcGxhdGVSZWYpIF90ZW1wbGF0ZTogVGVtcGxhdGVSZWY8YW55PjtcblxuICBwdWJsaWMgZ2V0IHRlbXBsYXRlKCkge1xuXG4gICAgcmV0dXJuIHRoaXMuX3RlbXBsYXRlO1xuICB9XG5cbiAgcHVibGljIGdldCBwcmlzdGluZSgpOiBhbnkge1xuICAgIGlmICh0aGlzLmludmVyc2UpIHtcbiAgICAgIHJldHVybiAoISF0aGlzLmZvcm0udmFsdWUuY2hlY2tib3hDb250cm9sKSA9PT0gIXRoaXMudmFsdWU7XG4gICAgfVxuXG4gICAgcmV0dXJuICghIXRoaXMuZm9ybS52YWx1ZS5jaGVja2JveENvbnRyb2wpID09PSB0aGlzLnZhbHVlO1xuICB9XG5cbiAgcHVibGljIGdldCBrZXkoKTogc3RyaW5nIHtcbiAgICByZXR1cm4gdGhpcy5hdHRyaWJ1dGU7XG4gIH1cblxuICBwdWJsaWMgZ2V0IG5ld1ZhbHVlKCk6IGFueSB7XG4gICAgY29uc29sZS5sb2coJ2NoZWNrYm94IHZhbHVlIGlzICcsIHRoaXMuYXR0cmlidXRlLCB0aGlzLmZvcm0udmFsdWUuY2hlY2tib3hDb250cm9sKVxuICAgIGlmICh0aGlzLmludmVyc2UpIHtcbiAgICAgIHJldHVybiAhdGhpcy5mb3JtLnZhbHVlLmNoZWNrYm94Q29udHJvbDtcbiAgICB9XG4gICAgcmV0dXJuICEhdGhpcy5mb3JtLnZhbHVlLmNoZWNrYm94Q29udHJvbDtcbiAgfVxuXG4gIGZvcm0gPSBuZXcgRm9ybUdyb3VwKHtcbiAgICBjaGVja2JveENvbnRyb2w6IG5ldyBGb3JtQ29udHJvbChmYWxzZSksXG4gIH0pO1xuXG5cbiAgQElucHV0KCkgbGVmdExhYmVsOiBzdHJpbmcgPSAnJztcbiAgQElucHV0KCkgbGFiZWw6IHN0cmluZztcbiAgQElucHV0KCkgYXR0cmlidXRlID0gJ3RvZG8nO1xuICBASW5wdXQoKSBkZXNjcmlwdGlvbiA9ICdleHBsYWluIHRoaXMgZmllbGQnO1xuICBASW5wdXQoKSB2YWx1ZTogYm9vbGVhbjtcbiAgQElucHV0KCkgc2VwYXJhdG9yOiBib29sZWFuO1xuICBASW5wdXQoKSBkaXNhYmxlZCA9IGZhbHNlO1xuICBASW5wdXQoKSBpbnZlcnNlID0gZmFsc2U7XG5cblxuICBAT3V0cHV0KCkgb25DaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcblxuICBjaGFuZ2UoZXZlbnQ6IGFueSkge1xuICAgIHRoaXMub25DaGFuZ2UuZW1pdChldmVudCk7XG5cbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIHN1cGVyLm5nT25Jbml0KCk7XG4gICAgaWYgKHRoaXMuZGlzYWJsZWQpIHtcbiAgICAgIHRoaXMuZm9ybSA9IG5ldyBGb3JtR3JvdXAoe1xuICAgICAgICBjaGVja2JveENvbnRyb2w6IG5ldyBGb3JtQ29udHJvbCh7dmFsdWU6ICcnLCBkaXNhYmxlZDogdGhpcy5kaXNhYmxlZH0pLFxuICAgICAgfSk7XG4gICAgfVxuICAgIGlmICh0aGlzLmludmVyc2UpIHtcbiAgICAgIHRoaXMuZm9ybS5zZXRWYWx1ZSh7Y2hlY2tib3hDb250cm9sOiAhdGhpcy52YWx1ZX0pO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmZvcm0uc2V0VmFsdWUoe2NoZWNrYm94Q29udHJvbDogdGhpcy52YWx1ZX0pO1xuICAgIH1cblxuICB9XG5cbiAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcyk6IHZvaWQge1xuICAgIGlmIChjaGFuZ2VzLnZhbHVlKSB7XG4gICAgICBpZiAodGhpcy5pbnZlcnNlKSB7XG4gICAgICAgIHRoaXMuZm9ybS5zZXRWYWx1ZSh7Y2hlY2tib3hDb250cm9sOiAhdGhpcy52YWx1ZX0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5mb3JtLnNldFZhbHVlKHtjaGVja2JveENvbnRyb2w6ICEhdGhpcy52YWx1ZX0pO1xuICAgICAgfVxuXG4gICAgfVxuICB9XG59XG4iXX0=