import { Component, EventEmitter, forwardRef, Input, Output, TemplateRef, ViewChild } from '@angular/core';
import { DynamicFormWidth } from "./dynamic-form-width";
import { FormControl, FormGroup } from "@angular/forms";
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
import * as i2 from "@angular/forms";
import * as i3 from "@angular/material/input";
import * as i4 from "@ngx-translate/core";
export class IotInputTextareaComponent extends DynamicFormWidth {
    constructor() {
        super(...arguments);
        this.form = new FormGroup({
            text: new FormControl(''),
        });
        this.attribute = 'todo';
        this.rows = 20;
        this.disabled = false;
        this.onChange = new EventEmitter();
    }
    get template() {
        return this._template;
    }
    get pristine() {
        return this.form.value.text === this.value;
    }
    get key() {
        return this.attribute;
    }
    get newValue() {
        return this.form.value.text;
    }
    ngOnInit() {
        super.ngOnInit();
        if (this.disabled) {
            this.form = new FormGroup({
                text: new FormControl({ value: '', disabled: this.disabled }),
            });
        }
    }
}
IotInputTextareaComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputTextareaComponent, deps: null, target: i0.ɵɵFactoryTarget.Component });
IotInputTextareaComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotInputTextareaComponent, selector: "iot-input-textarea", inputs: { label: "label", attribute: "attribute", value: "value", placeholder: "placeholder", rows: "rows", disabled: "disabled" }, outputs: { onChange: "onChange" }, providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputTextareaComponent) }], viewQueries: [{ propertyName: "_template", first: true, predicate: TemplateRef, descendants: true }], usesInheritance: true, ngImport: i0, template: `
    <ng-template>
      <div class="outer-margin">
        <div class="outeroneline" [ngClass]="{'outersmall':!isBig}">
          <div class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
          >
            <div class="label">{{label | translate}} </div>
          </div>

          <form [formGroup]="form" class="content">

            <textarea class="input-class" matInput type="text"
                      formControlName="text"
                      (ngModelChange)="onChange.emit($event)"
                      [placeholder]="placeholder"
                      [disabled]="disabled"
                      [rows]="rows"
                      [ngModel]="value"></textarea>


          </form>


        </div>

      </div>
    </ng-template>
  `, isInline: true, styles: ["\n    .outer-margin {\n      padding-right: 50px;\n    }\n\n    .label-outer.label-outer-thin {\n      min-height: 40px;\n    }\n\n    .label-outer {\n      flex-basis: 33%;\n      min-height: 64px;\n      min-width: 220px;\n      display: block;\n      padding-top: 13px;\n      color: #3c4043;\n    }\n\n    .label {\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 14px;\n      font-weight: 400;\n      height: 20px;\n      color: rgb(60, 64, 67);\n    }\n\n    .outeroneline {\n      display: flex;\n      max-width: 900px;\n    }\n\n    .outersmall {\n      flex-direction: column;\n      padding-bottom: 8px;\n    }\n\n    .content {\n      flex-direction: column;\n      flex-basis: 67%;\n      min-width: 280px;\n      padding-bottom: 20px;\n    }\n\n    .input-class {\n      height: 100%;\n      width: 100%;\n      display: flex;\n      border: none !important;\n      background-color: transparent;\n      pointer-events: auto;\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 1rem;\n      font-weight: 400;\n      letter-spacing: .00625em;\n    }\n\n    .input-class:focus {\n      outline: none;\n    }\n  "], directives: [{ type: i1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i2.ɵNgNoValidate, selector: "form:not([ngNoForm]):not([ngNativeValidate])" }, { type: i2.NgControlStatusGroup, selector: "[formGroupName],[formArrayName],[ngModelGroup],[formGroup],form:not([ngNoForm]),[ngForm]" }, { type: i2.FormGroupDirective, selector: "[formGroup]", inputs: ["formGroup"], outputs: ["ngSubmit"], exportAs: ["ngForm"] }, { type: i3.MatInput, selector: "input[matInput], textarea[matInput], select[matNativeControl],      input[matNativeControl], textarea[matNativeControl]", inputs: ["id", "disabled", "required", "type", "value", "readonly", "placeholder", "errorStateMatcher", "aria-describedby"], exportAs: ["matInput"] }, { type: i2.DefaultValueAccessor, selector: "input:not([type=checkbox])[formControlName],textarea[formControlName],input:not([type=checkbox])[formControl],textarea[formControl],input:not([type=checkbox])[ngModel],textarea[ngModel],[ngDefaultControl]" }, { type: i2.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { type: i2.FormControlName, selector: "[formControlName]", inputs: ["disabled", "formControlName", "ngModel"], outputs: ["ngModelChange"] }], pipes: { "translate": i4.TranslatePipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputTextareaComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-input-textarea',
                    template: `
    <ng-template>
      <div class="outer-margin">
        <div class="outeroneline" [ngClass]="{'outersmall':!isBig}">
          <div class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
          >
            <div class="label">{{label | translate}} </div>
          </div>

          <form [formGroup]="form" class="content">

            <textarea class="input-class" matInput type="text"
                      formControlName="text"
                      (ngModelChange)="onChange.emit($event)"
                      [placeholder]="placeholder"
                      [disabled]="disabled"
                      [rows]="rows"
                      [ngModel]="value"></textarea>


          </form>


        </div>

      </div>
    </ng-template>
  `,
                    styles: [`
    .outer-margin {
      padding-right: 50px;
    }

    .label-outer.label-outer-thin {
      min-height: 40px;
    }

    .label-outer {
      flex-basis: 33%;
      min-height: 64px;
      min-width: 220px;
      display: block;
      padding-top: 13px;
      color: #3c4043;
    }

    .label {
      font-family: Roboto, Arial, sans-serif;
      font-size: 14px;
      font-weight: 400;
      height: 20px;
      color: rgb(60, 64, 67);
    }

    .outeroneline {
      display: flex;
      max-width: 900px;
    }

    .outersmall {
      flex-direction: column;
      padding-bottom: 8px;
    }

    .content {
      flex-direction: column;
      flex-basis: 67%;
      min-width: 280px;
      padding-bottom: 20px;
    }

    .input-class {
      height: 100%;
      width: 100%;
      display: flex;
      border: none !important;
      background-color: transparent;
      pointer-events: auto;
      font-family: Roboto, Arial, sans-serif;
      font-size: 1rem;
      font-weight: 400;
      letter-spacing: .00625em;
    }

    .input-class:focus {
      outline: none;
    }
  `],
                    providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputTextareaComponent) }]
                }]
        }], propDecorators: { _template: [{
                type: ViewChild,
                args: [TemplateRef]
            }], label: [{
                type: Input
            }], attribute: [{
                type: Input
            }], value: [{
                type: Input
            }], placeholder: [{
                type: Input
            }], rows: [{
                type: Input
            }], disabled: [{
                type: Input
            }], onChange: [{
                type: Output
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW90LWlucHV0LXRleHRhcmVhLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2lvdC11aS1saWIvc3JjL2xpYi9mb3JtL2lvdC1pbnB1dC10ZXh0YXJlYS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFDLFNBQVMsRUFBRSxZQUFZLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBVSxNQUFNLEVBQUUsV0FBVyxFQUFFLFNBQVMsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUNqSCxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQztBQUN0RCxPQUFPLEVBQUMsV0FBVyxFQUFFLFNBQVMsRUFBQyxNQUFNLGdCQUFnQixDQUFDOzs7Ozs7QUE4RnRELE1BQU0sT0FBTyx5QkFBMEIsU0FBUSxnQkFBZ0I7SUE1Ri9EOztRQWtIRSxTQUFJLEdBQUcsSUFBSSxTQUFTLENBQUM7WUFDbkIsSUFBSSxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsQ0FBQztTQUMxQixDQUFDLENBQUM7UUFHTSxjQUFTLEdBQUcsTUFBTSxDQUFDO1FBR25CLFNBQUksR0FBRSxFQUFFLENBQUM7UUFDVCxhQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ2hCLGFBQVEsR0FBRyxJQUFJLFlBQVksRUFBVSxDQUFDO0tBVWpEO0lBdENDLElBQVcsUUFBUTtRQUVqQixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7SUFDeEIsQ0FBQztJQUVELElBQVcsUUFBUTtRQUVqQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQzdDLENBQUM7SUFFRCxJQUFXLEdBQUc7UUFDWixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7SUFDeEIsQ0FBQztJQUVELElBQVcsUUFBUTtRQUNqQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztJQUM5QixDQUFDO0lBY0QsUUFBUTtRQUNOLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNqQixJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDakIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLFNBQVMsQ0FBQztnQkFDeEIsSUFBSSxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUMsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBQyxDQUFDO2FBQzVELENBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQzs7c0hBekNVLHlCQUF5QjswR0FBekIseUJBQXlCLG9OQUZ6QixDQUFDLEVBQUMsT0FBTyxFQUFFLGdCQUFnQixFQUFFLFdBQVcsRUFBRSxVQUFVLENBQUMsR0FBRyxFQUFFLENBQUMseUJBQXlCLENBQUMsRUFBQyxDQUFDLHFFQUl2RixXQUFXLHVFQTVGWjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0dBMkJUOzJGQStEVSx5QkFBeUI7a0JBNUZyQyxTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSxvQkFBb0I7b0JBQzlCLFFBQVEsRUFBRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0dBMkJUO29CQUNELE1BQU0sRUFBRSxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQTJEUixDQUFDO29CQUNGLFNBQVMsRUFBRSxDQUFDLEVBQUMsT0FBTyxFQUFFLGdCQUFnQixFQUFFLFdBQVcsRUFBRSxVQUFVLENBQUMsR0FBRyxFQUFFLDBCQUEwQixDQUFDLEVBQUMsQ0FBQztpQkFDbkc7OEJBR3lCLFNBQVM7c0JBQWhDLFNBQVM7dUJBQUMsV0FBVztnQkF3QmIsS0FBSztzQkFBYixLQUFLO2dCQUNHLFNBQVM7c0JBQWpCLEtBQUs7Z0JBQ0csS0FBSztzQkFBYixLQUFLO2dCQUNHLFdBQVc7c0JBQW5CLEtBQUs7Z0JBQ0csSUFBSTtzQkFBWixLQUFLO2dCQUNHLFFBQVE7c0JBQWhCLEtBQUs7Z0JBQ0ksUUFBUTtzQkFBakIsTUFBTSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIGZvcndhcmRSZWYsIElucHV0LCBPbkluaXQsIE91dHB1dCwgVGVtcGxhdGVSZWYsIFZpZXdDaGlsZH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0R5bmFtaWNGb3JtV2lkdGh9IGZyb20gXCIuL2R5bmFtaWMtZm9ybS13aWR0aFwiO1xuaW1wb3J0IHtGb3JtQ29udHJvbCwgRm9ybUdyb3VwfSBmcm9tIFwiQGFuZ3VsYXIvZm9ybXNcIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnaW90LWlucHV0LXRleHRhcmVhJyxcbiAgdGVtcGxhdGU6IGBcbiAgICA8bmctdGVtcGxhdGU+XG4gICAgICA8ZGl2IGNsYXNzPVwib3V0ZXItbWFyZ2luXCI+XG4gICAgICAgIDxkaXYgY2xhc3M9XCJvdXRlcm9uZWxpbmVcIiBbbmdDbGFzc109XCJ7J291dGVyc21hbGwnOiFpc0JpZ31cIj5cbiAgICAgICAgICA8ZGl2IGNsYXNzPVwibGFiZWwtb3V0ZXJcIiBbbmdDbGFzc109XCJ7J2xhYmVsLW91dGVyLXRoaW4nOiFpc0JpZ31cIlxuICAgICAgICAgID5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJsYWJlbFwiPnt7bGFiZWwgfCB0cmFuc2xhdGV9fSA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgIDxmb3JtIFtmb3JtR3JvdXBdPVwiZm9ybVwiIGNsYXNzPVwiY29udGVudFwiPlxuXG4gICAgICAgICAgICA8dGV4dGFyZWEgY2xhc3M9XCJpbnB1dC1jbGFzc1wiIG1hdElucHV0IHR5cGU9XCJ0ZXh0XCJcbiAgICAgICAgICAgICAgICAgICAgICBmb3JtQ29udHJvbE5hbWU9XCJ0ZXh0XCJcbiAgICAgICAgICAgICAgICAgICAgICAobmdNb2RlbENoYW5nZSk9XCJvbkNoYW5nZS5lbWl0KCRldmVudClcIlxuICAgICAgICAgICAgICAgICAgICAgIFtwbGFjZWhvbGRlcl09XCJwbGFjZWhvbGRlclwiXG4gICAgICAgICAgICAgICAgICAgICAgW2Rpc2FibGVkXT1cImRpc2FibGVkXCJcbiAgICAgICAgICAgICAgICAgICAgICBbcm93c109XCJyb3dzXCJcbiAgICAgICAgICAgICAgICAgICAgICBbbmdNb2RlbF09XCJ2YWx1ZVwiPjwvdGV4dGFyZWE+XG5cblxuICAgICAgICAgIDwvZm9ybT5cblxuXG4gICAgICAgIDwvZGl2PlxuXG4gICAgICA8L2Rpdj5cbiAgICA8L25nLXRlbXBsYXRlPlxuICBgLFxuICBzdHlsZXM6IFtgXG4gICAgLm91dGVyLW1hcmdpbiB7XG4gICAgICBwYWRkaW5nLXJpZ2h0OiA1MHB4O1xuICAgIH1cblxuICAgIC5sYWJlbC1vdXRlci5sYWJlbC1vdXRlci10aGluIHtcbiAgICAgIG1pbi1oZWlnaHQ6IDQwcHg7XG4gICAgfVxuXG4gICAgLmxhYmVsLW91dGVyIHtcbiAgICAgIGZsZXgtYmFzaXM6IDMzJTtcbiAgICAgIG1pbi1oZWlnaHQ6IDY0cHg7XG4gICAgICBtaW4td2lkdGg6IDIyMHB4O1xuICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICBwYWRkaW5nLXRvcDogMTNweDtcbiAgICAgIGNvbG9yOiAjM2M0MDQzO1xuICAgIH1cblxuICAgIC5sYWJlbCB7XG4gICAgICBmb250LWZhbWlseTogUm9ib3RvLCBBcmlhbCwgc2Fucy1zZXJpZjtcbiAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICBoZWlnaHQ6IDIwcHg7XG4gICAgICBjb2xvcjogcmdiKDYwLCA2NCwgNjcpO1xuICAgIH1cblxuICAgIC5vdXRlcm9uZWxpbmUge1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIG1heC13aWR0aDogOTAwcHg7XG4gICAgfVxuXG4gICAgLm91dGVyc21hbGwge1xuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgIHBhZGRpbmctYm90dG9tOiA4cHg7XG4gICAgfVxuXG4gICAgLmNvbnRlbnQge1xuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgIGZsZXgtYmFzaXM6IDY3JTtcbiAgICAgIG1pbi13aWR0aDogMjgwcHg7XG4gICAgICBwYWRkaW5nLWJvdHRvbTogMjBweDtcbiAgICB9XG5cbiAgICAuaW5wdXQtY2xhc3Mge1xuICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgYm9yZGVyOiBub25lICFpbXBvcnRhbnQ7XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgICAgIHBvaW50ZXItZXZlbnRzOiBhdXRvO1xuICAgICAgZm9udC1mYW1pbHk6IFJvYm90bywgQXJpYWwsIHNhbnMtc2VyaWY7XG4gICAgICBmb250LXNpemU6IDFyZW07XG4gICAgICBmb250LXdlaWdodDogNDAwO1xuICAgICAgbGV0dGVyLXNwYWNpbmc6IC4wMDYyNWVtO1xuICAgIH1cblxuICAgIC5pbnB1dC1jbGFzczpmb2N1cyB7XG4gICAgICBvdXRsaW5lOiBub25lO1xuICAgIH1cbiAgYF0sXG4gIHByb3ZpZGVyczogW3twcm92aWRlOiBEeW5hbWljRm9ybVdpZHRoLCB1c2VFeGlzdGluZzogZm9yd2FyZFJlZigoKSA9PiBJb3RJbnB1dFRleHRhcmVhQ29tcG9uZW50KX1dXG59KVxuZXhwb3J0IGNsYXNzIElvdElucHV0VGV4dGFyZWFDb21wb25lbnQgZXh0ZW5kcyBEeW5hbWljRm9ybVdpZHRoIGltcGxlbWVudHMgT25Jbml0IHtcblxuICBAVmlld0NoaWxkKFRlbXBsYXRlUmVmKSBfdGVtcGxhdGU6IFRlbXBsYXRlUmVmPGFueT47XG5cbiAgcHVibGljIGdldCB0ZW1wbGF0ZSgpIHtcblxuICAgIHJldHVybiB0aGlzLl90ZW1wbGF0ZTtcbiAgfVxuXG4gIHB1YmxpYyBnZXQgcHJpc3RpbmUoKTogYW55IHtcblxuICAgIHJldHVybiB0aGlzLmZvcm0udmFsdWUudGV4dCA9PT0gdGhpcy52YWx1ZTtcbiAgfVxuXG4gIHB1YmxpYyBnZXQga2V5KCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIHRoaXMuYXR0cmlidXRlO1xuICB9XG5cbiAgcHVibGljIGdldCBuZXdWYWx1ZSgpOiBhbnkge1xuICAgIHJldHVybiB0aGlzLmZvcm0udmFsdWUudGV4dDtcbiAgfVxuXG4gIGZvcm0gPSBuZXcgRm9ybUdyb3VwKHtcbiAgICB0ZXh0OiBuZXcgRm9ybUNvbnRyb2woJycpLFxuICB9KTtcblxuICBASW5wdXQoKSBsYWJlbDtcbiAgQElucHV0KCkgYXR0cmlidXRlID0gJ3RvZG8nO1xuICBASW5wdXQoKSB2YWx1ZTtcbiAgQElucHV0KCkgcGxhY2Vob2xkZXI7XG4gIEBJbnB1dCgpIHJvd3MgPTIwO1xuICBASW5wdXQoKSBkaXNhYmxlZCA9IGZhbHNlO1xuICBAT3V0cHV0KCkgb25DaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPHN0cmluZz4oKTtcblxuICBuZ09uSW5pdCgpIHtcbiAgICBzdXBlci5uZ09uSW5pdCgpO1xuICAgIGlmICh0aGlzLmRpc2FibGVkKSB7XG4gICAgICB0aGlzLmZvcm0gPSBuZXcgRm9ybUdyb3VwKHtcbiAgICAgICAgdGV4dDogbmV3IEZvcm1Db250cm9sKHt2YWx1ZTogJycsIGRpc2FibGVkOiB0aGlzLmRpc2FibGVkfSksXG4gICAgICB9KTtcbiAgICB9XG4gIH1cbn1cbiJdfQ==