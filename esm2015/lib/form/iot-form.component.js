import { Component, ContentChildren, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { DynamicFormWidth } from "./dynamic-form-width";
import { Subscription } from "rxjs";
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/button";
import * as i2 from "@angular/material/progress-spinner";
import * as i3 from "@angular/material/icon";
import * as i4 from "@angular/common";
import * as i5 from "@ngx-translate/core";
export class IotFormComponent {
    constructor() {
        this.readonly = false;
        this.emitAll = false;
        this.alwaysShowDiscard = false;
        this.saveButtonText = "CONFIG.SAVE";
        this.statesDescription = {
            1: { text: '', loading: false, check: false },
            2: { text: 'CONFIG.SUBMITTING', loading: true, check: false },
            3: { text: 'CONFIG.CHANGES_SAVED', loading: false, check: true },
        };
        this.save = new EventEmitter();
        this.discard = new EventEmitter();
        this.active = false;
        this.tabs = [];
        this.formSubscription = new Subscription();
    }
    ngAfterContentInit() {
        setTimeout(() => {
            this.inputTabs.forEach((tab) => {
                if (tab.form) {
                    this.formSubscription.add(tab.form.valueChanges.subscribe((formChange) => {
                        this.pristineCheck();
                    }));
                }
            });
            this.tabs = this.inputTabs.toArray();
        }, 0);
    }
    pristineCheck() {
        let pristine = true;
        this.inputTabs.forEach((tab) => {
            pristine = pristine && tab.pristine;
        });
        this.active = !pristine;
    }
    _save() {
        const toReturn = {};
        this.inputTabs.forEach((tab) => {
            const attr = tab.key;
            if (attr != '') {
                if ((!tab.pristine) || this.emitAll) {
                    toReturn[attr] = tab.newValue;
                }
            }
        });
        this.save.emit(toReturn);
    }
}
IotFormComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotFormComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
IotFormComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotFormComponent, selector: "iot-form", inputs: { readonly: "readonly", state: "state", emitAll: "emitAll", alwaysShowDiscard: "alwaysShowDiscard", saveButtonText: "saveButtonText", statesDescription: "statesDescription", active: "active" }, outputs: { save: "save", discard: "discard" }, queries: [{ propertyName: "inputTabs", predicate: DynamicFormWidth, descendants: true }], ngImport: i0, template: `
    <div
      class="iot-drawer-content">
      <ng-content select="[page-header]"></ng-content>
      <ng-content select="[below-header]"></ng-content>
      <div *ngFor="let tab of tabs; let i = index" class="fields-container"
           role="tabpanel"
           attr.aria-labelledby="{{i}}-tab">
        <ng-container *ngTemplateOutlet="tab.template"></ng-container>
      </div>
    </div>

    <div class="outer-save-bar" *ngIf="!readonly">
      <div class="button-row">
        <button *ngIf="alwaysShowDiscard || active"
                (click)="discard.emit()"
                mat-button>{{'CONFIG.CANCEL'|translate}}
        </button>
        <button [disabled]="!active"
                (click)="_save()"
                mat-raised-button color="primary">{{saveButtonText |translate}}
        </button>

      </div>

      <div  *ngIf="saveButtonText != ''" class="bottom-font">
        <div class="message-outer" *ngIf="statesDescription && statesDescription[state]">
          <mat-spinner
            *ngIf="statesDescription[state].loading"
            class="icon"
            [diameter]="20"
          ></mat-spinner>
          <mat-icon
            *ngIf="statesDescription[state].check"
            class="icon">check_circle_outline
          </mat-icon>
          <div class="message">{{statesDescription[state].text |translate}} </div>
        </div>
      </div>
    </div>
  `, isInline: true, styles: ["\n      .fields-container .outer-margin{\n        padding-right: 24px;\n        padding-top: 16px;\n      }\n\n      .fields-container .outer-margin.nopaddingtop {\n        padding-top: 0px;\n      }\n\n      .outer-save-bar {\n        z-index: 3;\n        background: #fff;\n        bottom: 0;\n        box-shadow: 0 -5px 5px -5px #999;\n\n        left: 0;\n        position: fixed;\n        right: 0;\n        /*height: 36px;*/\n        display: flex;\n        flex-direction: row-reverse;\n        justify-content: space-between;\n\n        padding: 16px;\n      }\n\n      .button-row {\n\n      }\n\n      .icon {\n        margin-right: 8px;\n      }\n\n      .message-outer {\n        display: flex;\n        align-items: center;\n      }\n\n      .bottom-font {\n        color: #3c4043;\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 14px;\n        font-weight: 400;\n        letter-spacing: .0142857143em;\n        line-height: 1.25rem;\n      }\n\n      .message {\n        line-height: 20px;\n        height: 20px;\n      }\n    "], components: [{ type: i1.MatButton, selector: "button[mat-button], button[mat-raised-button], button[mat-icon-button],             button[mat-fab], button[mat-mini-fab], button[mat-stroked-button],             button[mat-flat-button]", inputs: ["disabled", "disableRipple", "color"], exportAs: ["matButton"] }, { type: i2.MatSpinner, selector: "mat-spinner", inputs: ["color"] }, { type: i3.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }], directives: [{ type: i4.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { type: i4.NgTemplateOutlet, selector: "[ngTemplateOutlet]", inputs: ["ngTemplateOutletContext", "ngTemplateOutlet"] }, { type: i4.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }], pipes: { "translate": i5.TranslatePipe }, encapsulation: i0.ViewEncapsulation.None });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotFormComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-form',
                    template: `
    <div
      class="iot-drawer-content">
      <ng-content select="[page-header]"></ng-content>
      <ng-content select="[below-header]"></ng-content>
      <div *ngFor="let tab of tabs; let i = index" class="fields-container"
           role="tabpanel"
           attr.aria-labelledby="{{i}}-tab">
        <ng-container *ngTemplateOutlet="tab.template"></ng-container>
      </div>
    </div>

    <div class="outer-save-bar" *ngIf="!readonly">
      <div class="button-row">
        <button *ngIf="alwaysShowDiscard || active"
                (click)="discard.emit()"
                mat-button>{{'CONFIG.CANCEL'|translate}}
        </button>
        <button [disabled]="!active"
                (click)="_save()"
                mat-raised-button color="primary">{{saveButtonText |translate}}
        </button>

      </div>

      <div  *ngIf="saveButtonText != ''" class="bottom-font">
        <div class="message-outer" *ngIf="statesDescription && statesDescription[state]">
          <mat-spinner
            *ngIf="statesDescription[state].loading"
            class="icon"
            [diameter]="20"
          ></mat-spinner>
          <mat-icon
            *ngIf="statesDescription[state].check"
            class="icon">check_circle_outline
          </mat-icon>
          <div class="message">{{statesDescription[state].text |translate}} </div>
        </div>
      </div>
    </div>
  `,
                    styles: [
                        `
      .fields-container .outer-margin{
        padding-right: 24px;
        padding-top: 16px;
      }

      .fields-container .outer-margin.nopaddingtop {
        padding-top: 0px;
      }

      .outer-save-bar {
        z-index: 3;
        background: #fff;
        bottom: 0;
        box-shadow: 0 -5px 5px -5px #999;

        left: 0;
        position: fixed;
        right: 0;
        /*height: 36px;*/
        display: flex;
        flex-direction: row-reverse;
        justify-content: space-between;

        padding: 16px;
      }

      .button-row {

      }

      .icon {
        margin-right: 8px;
      }

      .message-outer {
        display: flex;
        align-items: center;
      }

      .bottom-font {
        color: #3c4043;
        font-family: Roboto, Arial, sans-serif;
        font-size: 14px;
        font-weight: 400;
        letter-spacing: .0142857143em;
        line-height: 1.25rem;
      }

      .message {
        line-height: 20px;
        height: 20px;
      }
    `
                    ],
                    encapsulation: ViewEncapsulation.None
                }]
        }], ctorParameters: function () { return []; }, propDecorators: { readonly: [{
                type: Input
            }], state: [{
                type: Input
            }], emitAll: [{
                type: Input
            }], alwaysShowDiscard: [{
                type: Input
            }], saveButtonText: [{
                type: Input
            }], statesDescription: [{
                type: Input
            }], save: [{
                type: Output
            }], discard: [{
                type: Output
            }], inputTabs: [{
                type: ContentChildren,
                args: [DynamicFormWidth, { descendants: true }]
            }], active: [{
                type: Input
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW90LWZvcm0uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvaW90LXVpLWxpYi9zcmMvbGliL2Zvcm0vaW90LWZvcm0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUUsZUFBZSxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFhLGlCQUFpQixFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3BILE9BQU8sRUFBQyxnQkFBZ0IsRUFBQyxNQUFNLHNCQUFzQixDQUFDO0FBQ3RELE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxNQUFNLENBQUM7Ozs7Ozs7QUF5R2xDLE1BQU0sT0FBTyxnQkFBZ0I7SUF5RDNCO1FBeERTLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFFakIsWUFBTyxHQUFHLEtBQUssQ0FBQztRQUNoQixzQkFBaUIsR0FBRyxLQUFLLENBQUM7UUFDMUIsbUJBQWMsR0FBVyxhQUFhLENBQUM7UUFDdkMsc0JBQWlCLEdBQXNCO1lBQzlDLENBQUMsRUFBRSxFQUFDLElBQUksRUFBRSxFQUFFLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFDO1lBQzNDLENBQUMsRUFBRSxFQUFDLElBQUksRUFBRSxtQkFBbUIsRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUM7WUFDM0QsQ0FBQyxFQUFFLEVBQUMsSUFBSSxFQUFFLHNCQUFzQixFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBQztTQUMvRCxDQUFDO1FBRVEsU0FBSSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDL0IsWUFBTyxHQUFHLElBQUksWUFBWSxFQUFXLENBQUM7UUFHdkMsV0FBTSxHQUFHLEtBQUssQ0FBQztRQUNqQixTQUFJLEdBQXVCLEVBQUUsQ0FBQztRQUM5QixxQkFBZ0IsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO0lBd0M3QyxDQUFDO0lBdENNLGtCQUFrQjtRQUN2QixVQUFVLENBQUMsR0FBRSxFQUFFO1lBQ2IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFRLEVBQUUsRUFBRTtnQkFDbEMsSUFBSSxHQUFHLENBQUMsSUFBSSxFQUFFO29CQUNaLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUMsVUFBVSxFQUFDLEVBQUU7d0JBQ3RFLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztvQkFDdkIsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDTDtZQUNILENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ3ZDLENBQUMsRUFBQyxDQUFDLENBQUMsQ0FBQTtJQUVOLENBQUM7SUFFTyxhQUFhO1FBQ25CLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQztRQUNwQixJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEdBQUcsRUFBQyxFQUFFO1lBQzVCLFFBQVEsR0FBRyxRQUFRLElBQUksR0FBRyxDQUFDLFFBQVEsQ0FBQztRQUN0QyxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxRQUFRLENBQUM7SUFDMUIsQ0FBQztJQUVNLEtBQUs7UUFDVixNQUFNLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDcEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFxQixFQUFFLEVBQUU7WUFDL0MsTUFBTSxJQUFJLEdBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQztZQUNuQixJQUFJLElBQUksSUFBSSxFQUFFLEVBQUU7Z0JBQ2QsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7b0JBQ25DLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDO2lCQUMvQjthQUNGO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUMzQixDQUFDOzs2R0FyRFUsZ0JBQWdCO2lHQUFoQixnQkFBZ0IsbVVBZVYsZ0JBQWdCLGdEQXBIdkI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7R0F3Q1Q7MkZBNkRVLGdCQUFnQjtrQkF2RzVCLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLFVBQVU7b0JBQ3BCLFFBQVEsRUFBRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQXdDVDtvQkFFRCxNQUFNLEVBQUU7d0JBQ047Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0tBcURDO3FCQUNGO29CQUNELGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJO2lCQUV0QzswRUFFVSxRQUFRO3NCQUFoQixLQUFLO2dCQUNHLEtBQUs7c0JBQWIsS0FBSztnQkFDRyxPQUFPO3NCQUFmLEtBQUs7Z0JBQ0csaUJBQWlCO3NCQUF6QixLQUFLO2dCQUNHLGNBQWM7c0JBQXRCLEtBQUs7Z0JBQ0csaUJBQWlCO3NCQUF6QixLQUFLO2dCQU1JLElBQUk7c0JBQWIsTUFBTTtnQkFDRyxPQUFPO3NCQUFoQixNQUFNO2dCQUVpRCxTQUFTO3NCQUFoRSxlQUFlO3VCQUFDLGdCQUFnQixFQUFFLEVBQUMsV0FBVyxFQUFFLElBQUksRUFBQztnQkFDN0MsTUFBTTtzQkFBZCxLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIENvbnRlbnRDaGlsZHJlbiwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT3V0cHV0LCBRdWVyeUxpc3QsIFZpZXdFbmNhcHN1bGF0aW9ufSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7RHluYW1pY0Zvcm1XaWR0aH0gZnJvbSBcIi4vZHluYW1pYy1mb3JtLXdpZHRoXCI7XG5pbXBvcnQge1N1YnNjcmlwdGlvbn0gZnJvbSBcInJ4anNcIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnaW90LWZvcm0nLFxuICB0ZW1wbGF0ZTogYFxuICAgIDxkaXZcbiAgICAgIGNsYXNzPVwiaW90LWRyYXdlci1jb250ZW50XCI+XG4gICAgICA8bmctY29udGVudCBzZWxlY3Q9XCJbcGFnZS1oZWFkZXJdXCI+PC9uZy1jb250ZW50PlxuICAgICAgPG5nLWNvbnRlbnQgc2VsZWN0PVwiW2JlbG93LWhlYWRlcl1cIj48L25nLWNvbnRlbnQ+XG4gICAgICA8ZGl2ICpuZ0Zvcj1cImxldCB0YWIgb2YgdGFiczsgbGV0IGkgPSBpbmRleFwiIGNsYXNzPVwiZmllbGRzLWNvbnRhaW5lclwiXG4gICAgICAgICAgIHJvbGU9XCJ0YWJwYW5lbFwiXG4gICAgICAgICAgIGF0dHIuYXJpYS1sYWJlbGxlZGJ5PVwie3tpfX0tdGFiXCI+XG4gICAgICAgIDxuZy1jb250YWluZXIgKm5nVGVtcGxhdGVPdXRsZXQ9XCJ0YWIudGVtcGxhdGVcIj48L25nLWNvbnRhaW5lcj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuXG4gICAgPGRpdiBjbGFzcz1cIm91dGVyLXNhdmUtYmFyXCIgKm5nSWY9XCIhcmVhZG9ubHlcIj5cbiAgICAgIDxkaXYgY2xhc3M9XCJidXR0b24tcm93XCI+XG4gICAgICAgIDxidXR0b24gKm5nSWY9XCJhbHdheXNTaG93RGlzY2FyZCB8fCBhY3RpdmVcIlxuICAgICAgICAgICAgICAgIChjbGljayk9XCJkaXNjYXJkLmVtaXQoKVwiXG4gICAgICAgICAgICAgICAgbWF0LWJ1dHRvbj57eydDT05GSUcuQ0FOQ0VMJ3x0cmFuc2xhdGV9fVxuICAgICAgICA8L2J1dHRvbj5cbiAgICAgICAgPGJ1dHRvbiBbZGlzYWJsZWRdPVwiIWFjdGl2ZVwiXG4gICAgICAgICAgICAgICAgKGNsaWNrKT1cIl9zYXZlKClcIlxuICAgICAgICAgICAgICAgIG1hdC1yYWlzZWQtYnV0dG9uIGNvbG9yPVwicHJpbWFyeVwiPnt7c2F2ZUJ1dHRvblRleHQgfHRyYW5zbGF0ZX19XG4gICAgICAgIDwvYnV0dG9uPlxuXG4gICAgICA8L2Rpdj5cblxuICAgICAgPGRpdiAgKm5nSWY9XCJzYXZlQnV0dG9uVGV4dCAhPSAnJ1wiIGNsYXNzPVwiYm90dG9tLWZvbnRcIj5cbiAgICAgICAgPGRpdiBjbGFzcz1cIm1lc3NhZ2Utb3V0ZXJcIiAqbmdJZj1cInN0YXRlc0Rlc2NyaXB0aW9uICYmIHN0YXRlc0Rlc2NyaXB0aW9uW3N0YXRlXVwiPlxuICAgICAgICAgIDxtYXQtc3Bpbm5lclxuICAgICAgICAgICAgKm5nSWY9XCJzdGF0ZXNEZXNjcmlwdGlvbltzdGF0ZV0ubG9hZGluZ1wiXG4gICAgICAgICAgICBjbGFzcz1cImljb25cIlxuICAgICAgICAgICAgW2RpYW1ldGVyXT1cIjIwXCJcbiAgICAgICAgICA+PC9tYXQtc3Bpbm5lcj5cbiAgICAgICAgICA8bWF0LWljb25cbiAgICAgICAgICAgICpuZ0lmPVwic3RhdGVzRGVzY3JpcHRpb25bc3RhdGVdLmNoZWNrXCJcbiAgICAgICAgICAgIGNsYXNzPVwiaWNvblwiPmNoZWNrX2NpcmNsZV9vdXRsaW5lXG4gICAgICAgICAgPC9tYXQtaWNvbj5cbiAgICAgICAgICA8ZGl2IGNsYXNzPVwibWVzc2FnZVwiPnt7c3RhdGVzRGVzY3JpcHRpb25bc3RhdGVdLnRleHQgfHRyYW5zbGF0ZX19IDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICBgLFxuXG4gIHN0eWxlczogW1xuICAgIGBcbiAgICAgIC5maWVsZHMtY29udGFpbmVyIC5vdXRlci1tYXJnaW57XG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDI0cHg7XG4gICAgICAgIHBhZGRpbmctdG9wOiAxNnB4O1xuICAgICAgfVxuXG4gICAgICAuZmllbGRzLWNvbnRhaW5lciAub3V0ZXItbWFyZ2luLm5vcGFkZGluZ3RvcCB7XG4gICAgICAgIHBhZGRpbmctdG9wOiAwcHg7XG4gICAgICB9XG5cbiAgICAgIC5vdXRlci1zYXZlLWJhciB7XG4gICAgICAgIHotaW5kZXg6IDM7XG4gICAgICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gICAgICAgIGJvdHRvbTogMDtcbiAgICAgICAgYm94LXNoYWRvdzogMCAtNXB4IDVweCAtNXB4ICM5OTk7XG5cbiAgICAgICAgbGVmdDogMDtcbiAgICAgICAgcG9zaXRpb246IGZpeGVkO1xuICAgICAgICByaWdodDogMDtcbiAgICAgICAgLypoZWlnaHQ6IDM2cHg7Ki9cbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG5cbiAgICAgICAgcGFkZGluZzogMTZweDtcbiAgICAgIH1cblxuICAgICAgLmJ1dHRvbi1yb3cge1xuXG4gICAgICB9XG5cbiAgICAgIC5pY29uIHtcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiA4cHg7XG4gICAgICB9XG5cbiAgICAgIC5tZXNzYWdlLW91dGVyIHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgIH1cblxuICAgICAgLmJvdHRvbS1mb250IHtcbiAgICAgICAgY29sb3I6ICMzYzQwNDM7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBSb2JvdG8sIEFyaWFsLCBzYW5zLXNlcmlmO1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICAgIGxldHRlci1zcGFjaW5nOiAuMDE0Mjg1NzE0M2VtO1xuICAgICAgICBsaW5lLWhlaWdodDogMS4yNXJlbTtcbiAgICAgIH1cblxuICAgICAgLm1lc3NhZ2Uge1xuICAgICAgICBsaW5lLWhlaWdodDogMjBweDtcbiAgICAgICAgaGVpZ2h0OiAyMHB4O1xuICAgICAgfVxuICAgIGBcbiAgXSxcbiAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZVxuXG59KVxuZXhwb3J0IGNsYXNzIElvdEZvcm1Db21wb25lbnQge1xuICBASW5wdXQoKSByZWFkb25seSA9IGZhbHNlO1xuICBASW5wdXQoKSBzdGF0ZTogbnVtYmVyO1xuICBASW5wdXQoKSBlbWl0QWxsID0gZmFsc2U7XG4gIEBJbnB1dCgpIGFsd2F5c1Nob3dEaXNjYXJkID0gZmFsc2U7XG4gIEBJbnB1dCgpIHNhdmVCdXR0b25UZXh0OiBzdHJpbmcgPSBcIkNPTkZJRy5TQVZFXCI7XG4gIEBJbnB1dCgpIHN0YXRlc0Rlc2NyaXB0aW9uOiBTdGF0ZXNEZXNjcmlwdGlvbiA9IHtcbiAgICAxOiB7dGV4dDogJycsIGxvYWRpbmc6IGZhbHNlLCBjaGVjazogZmFsc2V9LFxuICAgIDI6IHt0ZXh0OiAnQ09ORklHLlNVQk1JVFRJTkcnLCBsb2FkaW5nOiB0cnVlLCBjaGVjazogZmFsc2V9LFxuICAgIDM6IHt0ZXh0OiAnQ09ORklHLkNIQU5HRVNfU0FWRUQnLCBsb2FkaW5nOiBmYWxzZSwgY2hlY2s6IHRydWV9LFxuICB9O1xuXG4gIEBPdXRwdXQoKSBzYXZlID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XG4gIEBPdXRwdXQoKSBkaXNjYXJkID0gbmV3IEV2ZW50RW1pdHRlcjxib29sZWFuPigpO1xuXG4gIEBDb250ZW50Q2hpbGRyZW4oRHluYW1pY0Zvcm1XaWR0aCwge2Rlc2NlbmRhbnRzOiB0cnVlfSkgaW5wdXRUYWJzOiBRdWVyeUxpc3Q8RHluYW1pY0Zvcm1XaWR0aD47XG4gIEBJbnB1dCgpIGFjdGl2ZSA9IGZhbHNlO1xuICBwdWJsaWMgdGFiczogRHluYW1pY0Zvcm1XaWR0aFtdID0gW107XG4gIHB1YmxpYyBmb3JtU3Vic2NyaXB0aW9uID0gbmV3IFN1YnNjcmlwdGlvbigpO1xuXG4gIHB1YmxpYyBuZ0FmdGVyQ29udGVudEluaXQoKTogdm9pZCB7XG4gICAgc2V0VGltZW91dCgoKT0+e1xuICAgICAgdGhpcy5pbnB1dFRhYnMuZm9yRWFjaCgodGFiOiBhbnkpID0+IHtcbiAgICAgICAgaWYgKHRhYi5mb3JtKSB7XG4gICAgICAgICAgdGhpcy5mb3JtU3Vic2NyaXB0aW9uLmFkZCh0YWIuZm9ybS52YWx1ZUNoYW5nZXMuc3Vic2NyaWJlKChmb3JtQ2hhbmdlKT0+e1xuICAgICAgICAgICAgdGhpcy5wcmlzdGluZUNoZWNrKCk7XG4gICAgICAgICAgfSkpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICAgIHRoaXMudGFicyA9IHRoaXMuaW5wdXRUYWJzLnRvQXJyYXkoKTtcbiAgICB9LDApXG5cbiAgfVxuXG4gIHByaXZhdGUgcHJpc3RpbmVDaGVjaygpOiB2b2lkIHtcbiAgICBsZXQgcHJpc3RpbmUgPSB0cnVlO1xuICAgIHRoaXMuaW5wdXRUYWJzLmZvckVhY2goKHRhYik9PntcbiAgICAgIHByaXN0aW5lID0gcHJpc3RpbmUgJiYgdGFiLnByaXN0aW5lO1xuICAgIH0pO1xuICAgIHRoaXMuYWN0aXZlID0gIXByaXN0aW5lO1xuICB9XG5cbiAgcHVibGljIF9zYXZlKCkge1xuICAgIGNvbnN0IHRvUmV0dXJuID0ge307XG4gICAgdGhpcy5pbnB1dFRhYnMuZm9yRWFjaCgodGFiOiBEeW5hbWljRm9ybVdpZHRoKSA9PiB7XG4gICAgICBjb25zdCBhdHRyPXRhYi5rZXk7XG4gICAgICBpZiAoYXR0ciAhPSAnJykge1xuICAgICAgICBpZiAoKCF0YWIucHJpc3RpbmUpIHx8IHRoaXMuZW1pdEFsbCkge1xuICAgICAgICAgIHRvUmV0dXJuW2F0dHJdID0gdGFiLm5ld1ZhbHVlO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfSk7XG4gICAgdGhpcy5zYXZlLmVtaXQodG9SZXR1cm4pO1xuICB9XG5cblxuXG4gIGNvbnN0cnVjdG9yKCkge1xuICB9XG5cblxufVxuXG5leHBvcnQgaW50ZXJmYWNlIFN0YXRlc0Rlc2NyaXB0aW9uIHtcbiAgW2lkOiBudW1iZXJdOiB7ICd0ZXh0Jzogc3RyaW5nLCAnbG9hZGluZyc6IGJvb2xlYW4sICdjaGVjayc6IGJvb2xlYW4gfTtcbn1cbiJdfQ==