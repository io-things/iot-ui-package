import { Component, EventEmitter, forwardRef, Input, Output, TemplateRef, ViewChild } from '@angular/core';
import { DynamicFormWidth } from "./dynamic-form-width";
import { FormControl, FormGroup } from "@angular/forms";
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/chips";
import * as i2 from "@angular/material/icon";
import * as i3 from "@angular/common";
import * as i4 from "@angular/forms";
import * as i5 from "@ngx-translate/core";
export class IotInputLabelsComponent extends DynamicFormWidth {
    constructor() {
        super(...arguments);
        this.separatorKeysCodes = [ENTER, COMMA];
        this.form = new FormGroup({
            inputId: new FormControl(''),
        });
        this.attribute = 'todo';
        this.value = [];
        this.copyArray = [];
        this.type = 'text';
        this.disabled = false;
        this.onChange = new EventEmitter();
    }
    get template() {
        return this._template;
    }
    get pristine() {
        return JSON.stringify(this.value) == JSON.stringify(this.copyArray);
        // return this.value.toString() === this.copyArray.toString();
    }
    get key() {
        return this.attribute;
    }
    get newValue() {
        return this.copyArray;
    }
    remove(fruit) {
        const index = this.copyArray.indexOf(fruit);
        if (index >= 0) {
            this.copyArray.splice(index, 1);
        }
        this.onChange.emit(this.copyArray);
    }
    add(event) {
        const value = (event.value || '').trim();
        if (value) {
            this.copyArray.push(value);
        }
        this.form.setValue({ inputId: null });
        this.onChange.emit(this.copyArray);
    }
    ngOnInit() {
        super.ngOnInit();
        this.form = new FormGroup({
            inputId: new FormControl({ value: null, disabled: this.disabled }),
        });
    }
    ngOnChanges(changes) {
        if (changes.value && !!this.value) {
            this.copyArray = [...this.value];
        }
    }
}
IotInputLabelsComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputLabelsComponent, deps: null, target: i0.ɵɵFactoryTarget.Component });
IotInputLabelsComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotInputLabelsComponent, selector: "iot-input-labels", inputs: { label: "label", attribute: "attribute", description: "description", value: "value", type: "type", disabled: "disabled" }, outputs: { onChange: "onChange" }, providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputLabelsComponent) }], viewQueries: [{ propertyName: "_template", first: true, predicate: TemplateRef, descendants: true }, { propertyName: "fruitInput", first: true, predicate: ["labelInput"], descendants: true }], usesInheritance: true, usesOnChanges: true, ngImport: i0, template: `
    <ng-template>
      <div class="outer-margin">
        <div class="outeroneline" [ngClass]="{'outersmall':!isBig}">
          <div class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
          >
            <div class="label">{{label | translate}} </div>
          </div>

          <form [formGroup]="form" class="content">
            <div
              *ngIf="!!description"
              class="checkbox-content-description">{{description | translate}}</div>

            <div class="form-label">
              <mat-chip-list #chipList aria-label="Fruit selection">
                <mat-chip
                  *ngFor="let label of copyArray"
                  [selectable]="true"
                  [removable]="true"
                  (removed)="remove(label)">
                  {{label}}
                  <mat-icon matChipRemove>cancel</mat-icon>
                </mat-chip>
                <input
                  class="label-input"
                  [placeholder]="'DEVICE.NEW_LABEL' | translate"
                  #labelInput
                  formControlName="inputId"
                  [matChipInputFor]="chipList"
                  [matChipInputSeparatorKeyCodes]="separatorKeysCodes"
                  (matChipInputTokenEnd)="add($event)">
              </mat-chip-list>
            </div>
            <!--                    <div class="form-content">-->

            <!--                        <input class="input-class" matInput [type]="type"-->
            <!--                               formControlName="inputId"-->
            <!--                               (ngModelChange)="onChange.emit($event)"-->

            <!--                               >-->
            <!--                        <span class="border-class"></span>-->
            <!--                    </div>-->
          </form>


        </div>

      </div>
    </ng-template>

  `, isInline: true, styles: ["\n    input.label-input {\n      background: transparent;\n      border-width: 0;\n    }\n\n    .outer-margin {\n      /*margin-left: -5px;*/\n\n      padding-right: 50px;\n    }\n\n    .label-outer.label-outer-thin {\n      min-height: 40px;\n    }\n\n    .checkbox-content-description {\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 12px;\n      font-weight: 400;\n      letter-spacing: .025em;\n      line-height: 16px;\n      color: #5f6368;\n      margin-bottom: 13px;\n      margin-top: 2px;\n    }\n\n    .label-outer {\n      flex-basis: 33%;\n      min-height: 64px;\n      min-width: 220px;\n      display: block;\n      padding-top: 13px;\n      color: #3c4043;\n    }\n\n    .label {\n\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 14px;\n      font-weight: 400;\n      height: 20px;\n      color: rgb(60, 64, 67);\n    }\n\n    .outeroneline {\n      display: flex;\n      max-width: 900px;\n    }\n\n\n    .outersmall {\n      flex-direction: column;\n      padding-bottom: 8px;\n    }\n\n\n    .content {\n      flex-direction: column;\n      flex-basis: 67%;\n      min-width: 280px;\n    }\n\n    .form-content {\n\n      width: 100%;\n      height: 46px;\n      padding: 0 16px;\n      border: 1px solid #dadce0;\n      border-radius: 4px;\n      border-top-left-radius: 4px;\n      border-top-right-radius: 4px;\n      border-top-right-radius: 4px;\n      border-bottom-right-radius: 4px;\n      border-bottom-right-radius: 4px;\n      border-bottom-left-radius: 4px;\n      pointer-events: none;\n    }\n\n\n    .form-content:hover {\n      border: 2px solid black;\n    }\n\n    .input-class {\n      height: 100%;\n      width: 100%;\n      display: flex;\n      border: none !important;\n      background-color: transparent;\n      pointer-events: auto;\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 1rem;\n      font-weight: 400;\n      letter-spacing: .00625em;\n    }\n\n    .input-class:focus {\n      outline: none;\n    }\n  "], components: [{ type: i1.MatChipList, selector: "mat-chip-list", inputs: ["aria-orientation", "multiple", "compareWith", "value", "required", "placeholder", "disabled", "selectable", "tabIndex", "errorStateMatcher"], outputs: ["change", "valueChange"], exportAs: ["matChipList"] }, { type: i2.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }], directives: [{ type: i3.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i4.ɵNgNoValidate, selector: "form:not([ngNoForm]):not([ngNativeValidate])" }, { type: i4.NgControlStatusGroup, selector: "[formGroupName],[formArrayName],[ngModelGroup],[formGroup],form:not([ngNoForm]),[ngForm]" }, { type: i4.FormGroupDirective, selector: "[formGroup]", inputs: ["formGroup"], outputs: ["ngSubmit"], exportAs: ["ngForm"] }, { type: i3.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i3.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { type: i1.MatChip, selector: "mat-basic-chip, [mat-basic-chip], mat-chip, [mat-chip]", inputs: ["color", "disableRipple", "tabIndex", "selected", "value", "selectable", "disabled", "removable"], outputs: ["selectionChange", "destroyed", "removed"], exportAs: ["matChip"] }, { type: i1.MatChipRemove, selector: "[matChipRemove]" }, { type: i4.DefaultValueAccessor, selector: "input:not([type=checkbox])[formControlName],textarea[formControlName],input:not([type=checkbox])[formControl],textarea[formControl],input:not([type=checkbox])[ngModel],textarea[ngModel],[ngDefaultControl]" }, { type: i1.MatChipInput, selector: "input[matChipInputFor]", inputs: ["matChipInputSeparatorKeyCodes", "placeholder", "id", "matChipInputFor", "matChipInputAddOnBlur", "disabled"], outputs: ["matChipInputTokenEnd"], exportAs: ["matChipInput", "matChipInputFor"] }, { type: i4.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { type: i4.FormControlName, selector: "[formControlName]", inputs: ["disabled", "formControlName", "ngModel"], outputs: ["ngModelChange"] }], pipes: { "translate": i5.TranslatePipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputLabelsComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-input-labels',
                    template: `
    <ng-template>
      <div class="outer-margin">
        <div class="outeroneline" [ngClass]="{'outersmall':!isBig}">
          <div class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
          >
            <div class="label">{{label | translate}} </div>
          </div>

          <form [formGroup]="form" class="content">
            <div
              *ngIf="!!description"
              class="checkbox-content-description">{{description | translate}}</div>

            <div class="form-label">
              <mat-chip-list #chipList aria-label="Fruit selection">
                <mat-chip
                  *ngFor="let label of copyArray"
                  [selectable]="true"
                  [removable]="true"
                  (removed)="remove(label)">
                  {{label}}
                  <mat-icon matChipRemove>cancel</mat-icon>
                </mat-chip>
                <input
                  class="label-input"
                  [placeholder]="'DEVICE.NEW_LABEL' | translate"
                  #labelInput
                  formControlName="inputId"
                  [matChipInputFor]="chipList"
                  [matChipInputSeparatorKeyCodes]="separatorKeysCodes"
                  (matChipInputTokenEnd)="add($event)">
              </mat-chip-list>
            </div>
            <!--                    <div class="form-content">-->

            <!--                        <input class="input-class" matInput [type]="type"-->
            <!--                               formControlName="inputId"-->
            <!--                               (ngModelChange)="onChange.emit($event)"-->

            <!--                               >-->
            <!--                        <span class="border-class"></span>-->
            <!--                    </div>-->
          </form>


        </div>

      </div>
    </ng-template>

  `,
                    styles: [`
    input.label-input {
      background: transparent;
      border-width: 0;
    }

    .outer-margin {
      /*margin-left: -5px;*/

      padding-right: 50px;
    }

    .label-outer.label-outer-thin {
      min-height: 40px;
    }

    .checkbox-content-description {
      font-family: Roboto, Arial, sans-serif;
      font-size: 12px;
      font-weight: 400;
      letter-spacing: .025em;
      line-height: 16px;
      color: #5f6368;
      margin-bottom: 13px;
      margin-top: 2px;
    }

    .label-outer {
      flex-basis: 33%;
      min-height: 64px;
      min-width: 220px;
      display: block;
      padding-top: 13px;
      color: #3c4043;
    }

    .label {

      font-family: Roboto, Arial, sans-serif;
      font-size: 14px;
      font-weight: 400;
      height: 20px;
      color: rgb(60, 64, 67);
    }

    .outeroneline {
      display: flex;
      max-width: 900px;
    }


    .outersmall {
      flex-direction: column;
      padding-bottom: 8px;
    }


    .content {
      flex-direction: column;
      flex-basis: 67%;
      min-width: 280px;
    }

    .form-content {

      width: 100%;
      height: 46px;
      padding: 0 16px;
      border: 1px solid #dadce0;
      border-radius: 4px;
      border-top-left-radius: 4px;
      border-top-right-radius: 4px;
      border-top-right-radius: 4px;
      border-bottom-right-radius: 4px;
      border-bottom-right-radius: 4px;
      border-bottom-left-radius: 4px;
      pointer-events: none;
    }


    .form-content:hover {
      border: 2px solid black;
    }

    .input-class {
      height: 100%;
      width: 100%;
      display: flex;
      border: none !important;
      background-color: transparent;
      pointer-events: auto;
      font-family: Roboto, Arial, sans-serif;
      font-size: 1rem;
      font-weight: 400;
      letter-spacing: .00625em;
    }

    .input-class:focus {
      outline: none;
    }
  `],
                    providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputLabelsComponent) }]
                }]
        }], propDecorators: { _template: [{
                type: ViewChild,
                args: [TemplateRef]
            }], fruitInput: [{
                type: ViewChild,
                args: ['labelInput']
            }], label: [{
                type: Input
            }], attribute: [{
                type: Input
            }], description: [{
                type: Input
            }], value: [{
                type: Input
            }], type: [{
                type: Input
            }], disabled: [{
                type: Input
            }], onChange: [{
                type: Output
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW90LWlucHV0LWxhYmVscy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9pb3QtdWktbGliL3NyYy9saWIvZm9ybS9pb3QtaW5wdXQtbGFiZWxzLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQ0wsU0FBUyxFQUNULFlBQVksRUFDWixVQUFVLEVBQ1YsS0FBSyxFQUdMLE1BQU0sRUFDTixXQUFXLEVBQ1gsU0FBUyxFQUNWLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBQyxnQkFBZ0IsRUFBQyxNQUFNLHNCQUFzQixDQUFDO0FBQ3RELE9BQU8sRUFBQyxXQUFXLEVBQUUsU0FBUyxFQUFDLE1BQU0sZ0JBQWdCLENBQUM7QUFDdEQsT0FBTyxFQUFDLEtBQUssRUFBRSxLQUFLLEVBQUMsTUFBTSx1QkFBdUIsQ0FBQzs7Ozs7OztBQWdLbkQsTUFBTSxPQUFPLHVCQUF3QixTQUFRLGdCQUFnQjtJQTdKN0Q7O1FBaUtFLHVCQUFrQixHQUFhLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBb0I5QyxTQUFJLEdBQUcsSUFBSSxTQUFTLENBQUM7WUFDbkIsT0FBTyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsQ0FBQztTQUM3QixDQUFDLENBQUM7UUFHTSxjQUFTLEdBQUcsTUFBTSxDQUFDO1FBRW5CLFVBQUssR0FBYSxFQUFFLENBQUM7UUFDOUIsY0FBUyxHQUFhLEVBQUUsQ0FBQztRQUNoQixTQUFJLEdBQUcsTUFBTSxDQUFDO1FBQ2QsYUFBUSxHQUFHLEtBQUssQ0FBQztRQUNoQixhQUFRLEdBQUcsSUFBSSxZQUFZLEVBQVksQ0FBQztLQW1DbkQ7SUFoRUMsSUFBVyxRQUFRO1FBRWpCLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUN4QixDQUFDO0lBRUQsSUFBVyxRQUFRO1FBQ3JCLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDOUQsOERBQThEO0lBQ2hFLENBQUM7SUFFRCxJQUFXLEdBQUc7UUFDWixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7SUFDeEIsQ0FBQztJQUVELElBQVcsUUFBUTtRQUNqQixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7SUFDeEIsQ0FBQztJQWdCRCxNQUFNLENBQUMsS0FBYTtRQUNsQixNQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUU1QyxJQUFJLEtBQUssSUFBSSxDQUFDLEVBQUU7WUFDZCxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDakM7UUFDRCxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDckMsQ0FBQztJQUVELEdBQUcsQ0FBQyxLQUF3QjtRQUMxQixNQUFNLEtBQUssR0FBRyxDQUFDLEtBQUssQ0FBQyxLQUFLLElBQUksRUFBRSxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7UUFFekMsSUFBSSxLQUFLLEVBQUU7WUFDVCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUM1QjtRQUVELElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUMsT0FBTyxFQUFFLElBQUksRUFBQyxDQUFDLENBQUM7UUFDcEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ3JDLENBQUM7SUFFRCxRQUFRO1FBQ04sS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ2pCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxTQUFTLENBQUM7WUFDeEIsT0FBTyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUMsS0FBSyxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBQyxDQUFDO1NBQ2pFLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxXQUFXLENBQUMsT0FBc0I7UUFDaEMsSUFBSSxPQUFPLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ2pDLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNsQztJQUNILENBQUM7O29IQXJFVSx1QkFBdUI7d0dBQXZCLHVCQUF1QixrTkFGdkIsQ0FBQyxFQUFDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxXQUFXLEVBQUUsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDLHVCQUF1QixDQUFDLEVBQUMsQ0FBQyxxRUFJckYsV0FBVyx1TEE3Slo7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQW1EVDsyRkF3R1UsdUJBQXVCO2tCQTdKbkMsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsa0JBQWtCO29CQUM1QixRQUFRLEVBQUU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQW1EVDtvQkFDRCxNQUFNLEVBQUUsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQW9HUixDQUFDO29CQUNGLFNBQVMsRUFBRSxDQUFDLEVBQUMsT0FBTyxFQUFFLGdCQUFnQixFQUFFLFdBQVcsRUFBRSxVQUFVLENBQUMsR0FBRyxFQUFFLHdCQUF3QixDQUFDLEVBQUMsQ0FBQztpQkFDakc7OEJBR3lCLFNBQVM7c0JBQWhDLFNBQVM7dUJBQUMsV0FBVztnQkFDRyxVQUFVO3NCQUFsQyxTQUFTO3VCQUFDLFlBQVk7Z0JBeUJkLEtBQUs7c0JBQWIsS0FBSztnQkFDRyxTQUFTO3NCQUFqQixLQUFLO2dCQUNHLFdBQVc7c0JBQW5CLEtBQUs7Z0JBQ0csS0FBSztzQkFBYixLQUFLO2dCQUVHLElBQUk7c0JBQVosS0FBSztnQkFDRyxRQUFRO3NCQUFoQixLQUFLO2dCQUNJLFFBQVE7c0JBQWpCLE1BQU0iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuICBDb21wb25lbnQsIEVsZW1lbnRSZWYsXG4gIEV2ZW50RW1pdHRlcixcbiAgZm9yd2FyZFJlZixcbiAgSW5wdXQsXG4gIE9uQ2hhbmdlcyxcbiAgT25Jbml0LFxuICBPdXRwdXQsIFNpbXBsZUNoYW5nZXMsXG4gIFRlbXBsYXRlUmVmLFxuICBWaWV3Q2hpbGRcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0R5bmFtaWNGb3JtV2lkdGh9IGZyb20gXCIuL2R5bmFtaWMtZm9ybS13aWR0aFwiO1xuaW1wb3J0IHtGb3JtQ29udHJvbCwgRm9ybUdyb3VwfSBmcm9tIFwiQGFuZ3VsYXIvZm9ybXNcIjtcbmltcG9ydCB7Q09NTUEsIEVOVEVSfSBmcm9tICdAYW5ndWxhci9jZGsva2V5Y29kZXMnO1xuaW1wb3J0IHtNYXRDaGlwSW5wdXRFdmVudH0gZnJvbSBcIkBhbmd1bGFyL21hdGVyaWFsL2NoaXBzXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2lvdC1pbnB1dC1sYWJlbHMnLFxuICB0ZW1wbGF0ZTogYFxuICAgIDxuZy10ZW1wbGF0ZT5cbiAgICAgIDxkaXYgY2xhc3M9XCJvdXRlci1tYXJnaW5cIj5cbiAgICAgICAgPGRpdiBjbGFzcz1cIm91dGVyb25lbGluZVwiIFtuZ0NsYXNzXT1cInsnb3V0ZXJzbWFsbCc6IWlzQmlnfVwiPlxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJsYWJlbC1vdXRlclwiIFtuZ0NsYXNzXT1cInsnbGFiZWwtb3V0ZXItdGhpbic6IWlzQmlnfVwiXG4gICAgICAgICAgPlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImxhYmVsXCI+e3tsYWJlbCB8IHRyYW5zbGF0ZX19IDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuXG4gICAgICAgICAgPGZvcm0gW2Zvcm1Hcm91cF09XCJmb3JtXCIgY2xhc3M9XCJjb250ZW50XCI+XG4gICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICpuZ0lmPVwiISFkZXNjcmlwdGlvblwiXG4gICAgICAgICAgICAgIGNsYXNzPVwiY2hlY2tib3gtY29udGVudC1kZXNjcmlwdGlvblwiPnt7ZGVzY3JpcHRpb24gfCB0cmFuc2xhdGV9fTwvZGl2PlxuXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1sYWJlbFwiPlxuICAgICAgICAgICAgICA8bWF0LWNoaXAtbGlzdCAjY2hpcExpc3QgYXJpYS1sYWJlbD1cIkZydWl0IHNlbGVjdGlvblwiPlxuICAgICAgICAgICAgICAgIDxtYXQtY2hpcFxuICAgICAgICAgICAgICAgICAgKm5nRm9yPVwibGV0IGxhYmVsIG9mIGNvcHlBcnJheVwiXG4gICAgICAgICAgICAgICAgICBbc2VsZWN0YWJsZV09XCJ0cnVlXCJcbiAgICAgICAgICAgICAgICAgIFtyZW1vdmFibGVdPVwidHJ1ZVwiXG4gICAgICAgICAgICAgICAgICAocmVtb3ZlZCk9XCJyZW1vdmUobGFiZWwpXCI+XG4gICAgICAgICAgICAgICAgICB7e2xhYmVsfX1cbiAgICAgICAgICAgICAgICAgIDxtYXQtaWNvbiBtYXRDaGlwUmVtb3ZlPmNhbmNlbDwvbWF0LWljb24+XG4gICAgICAgICAgICAgICAgPC9tYXQtY2hpcD5cbiAgICAgICAgICAgICAgICA8aW5wdXRcbiAgICAgICAgICAgICAgICAgIGNsYXNzPVwibGFiZWwtaW5wdXRcIlxuICAgICAgICAgICAgICAgICAgW3BsYWNlaG9sZGVyXT1cIidERVZJQ0UuTkVXX0xBQkVMJyB8IHRyYW5zbGF0ZVwiXG4gICAgICAgICAgICAgICAgICAjbGFiZWxJbnB1dFxuICAgICAgICAgICAgICAgICAgZm9ybUNvbnRyb2xOYW1lPVwiaW5wdXRJZFwiXG4gICAgICAgICAgICAgICAgICBbbWF0Q2hpcElucHV0Rm9yXT1cImNoaXBMaXN0XCJcbiAgICAgICAgICAgICAgICAgIFttYXRDaGlwSW5wdXRTZXBhcmF0b3JLZXlDb2Rlc109XCJzZXBhcmF0b3JLZXlzQ29kZXNcIlxuICAgICAgICAgICAgICAgICAgKG1hdENoaXBJbnB1dFRva2VuRW5kKT1cImFkZCgkZXZlbnQpXCI+XG4gICAgICAgICAgICAgIDwvbWF0LWNoaXAtbGlzdD5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPCEtLSAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tY29udGVudFwiPi0tPlxuXG4gICAgICAgICAgICA8IS0tICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IGNsYXNzPVwiaW5wdXQtY2xhc3NcIiBtYXRJbnB1dCBbdHlwZV09XCJ0eXBlXCItLT5cbiAgICAgICAgICAgIDwhLS0gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9ybUNvbnRyb2xOYW1lPVwiaW5wdXRJZFwiLS0+XG4gICAgICAgICAgICA8IS0tICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChuZ01vZGVsQ2hhbmdlKT1cIm9uQ2hhbmdlLmVtaXQoJGV2ZW50KVwiLS0+XG5cbiAgICAgICAgICAgIDwhLS0gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPi0tPlxuICAgICAgICAgICAgPCEtLSAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiYm9yZGVyLWNsYXNzXCI+PC9zcGFuPi0tPlxuICAgICAgICAgICAgPCEtLSAgICAgICAgICAgICAgICAgICAgPC9kaXY+LS0+XG4gICAgICAgICAgPC9mb3JtPlxuXG5cbiAgICAgICAgPC9kaXY+XG5cbiAgICAgIDwvZGl2PlxuICAgIDwvbmctdGVtcGxhdGU+XG5cbiAgYCxcbiAgc3R5bGVzOiBbYFxuICAgIGlucHV0LmxhYmVsLWlucHV0IHtcbiAgICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgICAgYm9yZGVyLXdpZHRoOiAwO1xuICAgIH1cblxuICAgIC5vdXRlci1tYXJnaW4ge1xuICAgICAgLyptYXJnaW4tbGVmdDogLTVweDsqL1xuXG4gICAgICBwYWRkaW5nLXJpZ2h0OiA1MHB4O1xuICAgIH1cblxuICAgIC5sYWJlbC1vdXRlci5sYWJlbC1vdXRlci10aGluIHtcbiAgICAgIG1pbi1oZWlnaHQ6IDQwcHg7XG4gICAgfVxuXG4gICAgLmNoZWNrYm94LWNvbnRlbnQtZGVzY3JpcHRpb24ge1xuICAgICAgZm9udC1mYW1pbHk6IFJvYm90bywgQXJpYWwsIHNhbnMtc2VyaWY7XG4gICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICBmb250LXdlaWdodDogNDAwO1xuICAgICAgbGV0dGVyLXNwYWNpbmc6IC4wMjVlbTtcbiAgICAgIGxpbmUtaGVpZ2h0OiAxNnB4O1xuICAgICAgY29sb3I6ICM1ZjYzNjg7XG4gICAgICBtYXJnaW4tYm90dG9tOiAxM3B4O1xuICAgICAgbWFyZ2luLXRvcDogMnB4O1xuICAgIH1cblxuICAgIC5sYWJlbC1vdXRlciB7XG4gICAgICBmbGV4LWJhc2lzOiAzMyU7XG4gICAgICBtaW4taGVpZ2h0OiA2NHB4O1xuICAgICAgbWluLXdpZHRoOiAyMjBweDtcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgcGFkZGluZy10b3A6IDEzcHg7XG4gICAgICBjb2xvcjogIzNjNDA0MztcbiAgICB9XG5cbiAgICAubGFiZWwge1xuXG4gICAgICBmb250LWZhbWlseTogUm9ib3RvLCBBcmlhbCwgc2Fucy1zZXJpZjtcbiAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICBoZWlnaHQ6IDIwcHg7XG4gICAgICBjb2xvcjogcmdiKDYwLCA2NCwgNjcpO1xuICAgIH1cblxuICAgIC5vdXRlcm9uZWxpbmUge1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIG1heC13aWR0aDogOTAwcHg7XG4gICAgfVxuXG5cbiAgICAub3V0ZXJzbWFsbCB7XG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgcGFkZGluZy1ib3R0b206IDhweDtcbiAgICB9XG5cblxuICAgIC5jb250ZW50IHtcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICBmbGV4LWJhc2lzOiA2NyU7XG4gICAgICBtaW4td2lkdGg6IDI4MHB4O1xuICAgIH1cblxuICAgIC5mb3JtLWNvbnRlbnQge1xuXG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIGhlaWdodDogNDZweDtcbiAgICAgIHBhZGRpbmc6IDAgMTZweDtcbiAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNkYWRjZTA7XG4gICAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiA0cHg7XG4gICAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogNHB4O1xuICAgICAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDRweDtcbiAgICAgIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiA0cHg7XG4gICAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogNHB4O1xuICAgICAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogNHB4O1xuICAgICAgcG9pbnRlci1ldmVudHM6IG5vbmU7XG4gICAgfVxuXG5cbiAgICAuZm9ybS1jb250ZW50OmhvdmVyIHtcbiAgICAgIGJvcmRlcjogMnB4IHNvbGlkIGJsYWNrO1xuICAgIH1cblxuICAgIC5pbnB1dC1jbGFzcyB7XG4gICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICBib3JkZXI6IG5vbmUgIWltcG9ydGFudDtcbiAgICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICAgICAgcG9pbnRlci1ldmVudHM6IGF1dG87XG4gICAgICBmb250LWZhbWlseTogUm9ib3RvLCBBcmlhbCwgc2Fucy1zZXJpZjtcbiAgICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICBsZXR0ZXItc3BhY2luZzogLjAwNjI1ZW07XG4gICAgfVxuXG4gICAgLmlucHV0LWNsYXNzOmZvY3VzIHtcbiAgICAgIG91dGxpbmU6IG5vbmU7XG4gICAgfVxuICBgXSxcbiAgcHJvdmlkZXJzOiBbe3Byb3ZpZGU6IER5bmFtaWNGb3JtV2lkdGgsIHVzZUV4aXN0aW5nOiBmb3J3YXJkUmVmKCgpID0+IElvdElucHV0TGFiZWxzQ29tcG9uZW50KX1dXG59KVxuZXhwb3J0IGNsYXNzIElvdElucHV0TGFiZWxzQ29tcG9uZW50IGV4dGVuZHMgRHluYW1pY0Zvcm1XaWR0aCBpbXBsZW1lbnRzIE9uSW5pdCwgT25DaGFuZ2VzIHtcbiAgLy8gb2Zmc2V0OiBPYnNlcnZhYmxlPG51bWJlcj4gPSB0aGlzLnN0b3JlLnNlbGVjdChuYXZPZmZzZXQpO1xuICBAVmlld0NoaWxkKFRlbXBsYXRlUmVmKSBfdGVtcGxhdGU6IFRlbXBsYXRlUmVmPGFueT47XG4gIEBWaWV3Q2hpbGQoJ2xhYmVsSW5wdXQnKSBmcnVpdElucHV0OiBFbGVtZW50UmVmPEhUTUxJbnB1dEVsZW1lbnQ+O1xuICBzZXBhcmF0b3JLZXlzQ29kZXM6IG51bWJlcltdID0gW0VOVEVSLCBDT01NQV07XG5cbiAgcHVibGljIGdldCB0ZW1wbGF0ZSgpIHtcblxuICAgIHJldHVybiB0aGlzLl90ZW1wbGF0ZTtcbiAgfVxuXG4gIHB1YmxpYyBnZXQgcHJpc3RpbmUoKTogYW55IHtcbnJldHVybiBKU09OLnN0cmluZ2lmeSh0aGlzLnZhbHVlKT09SlNPTi5zdHJpbmdpZnkodGhpcy5jb3B5QXJyYXkpO1xuICAgIC8vIHJldHVybiB0aGlzLnZhbHVlLnRvU3RyaW5nKCkgPT09IHRoaXMuY29weUFycmF5LnRvU3RyaW5nKCk7XG4gIH1cblxuICBwdWJsaWMgZ2V0IGtleSgpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLmF0dHJpYnV0ZTtcbiAgfVxuXG4gIHB1YmxpYyBnZXQgbmV3VmFsdWUoKTogc3RyaW5nW10ge1xuICAgIHJldHVybiB0aGlzLmNvcHlBcnJheTtcbiAgfVxuXG4gIGZvcm0gPSBuZXcgRm9ybUdyb3VwKHtcbiAgICBpbnB1dElkOiBuZXcgRm9ybUNvbnRyb2woJycpLFxuICB9KTtcblxuICBASW5wdXQoKSBsYWJlbDtcbiAgQElucHV0KCkgYXR0cmlidXRlID0gJ3RvZG8nO1xuICBASW5wdXQoKSBkZXNjcmlwdGlvbjtcbiAgQElucHV0KCkgdmFsdWU6IHN0cmluZ1tdID0gW107XG4gIGNvcHlBcnJheTogc3RyaW5nW10gPSBbXTtcbiAgQElucHV0KCkgdHlwZSA9ICd0ZXh0JztcbiAgQElucHV0KCkgZGlzYWJsZWQgPSBmYWxzZTtcbiAgQE91dHB1dCgpIG9uQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxzdHJpbmdbXT4oKTtcblxuXG4gIHJlbW92ZShmcnVpdDogc3RyaW5nKTogdm9pZCB7XG4gICAgY29uc3QgaW5kZXggPSB0aGlzLmNvcHlBcnJheS5pbmRleE9mKGZydWl0KTtcblxuICAgIGlmIChpbmRleCA+PSAwKSB7XG4gICAgICB0aGlzLmNvcHlBcnJheS5zcGxpY2UoaW5kZXgsIDEpO1xuICAgIH1cbiAgICB0aGlzLm9uQ2hhbmdlLmVtaXQodGhpcy5jb3B5QXJyYXkpO1xuICB9XG5cbiAgYWRkKGV2ZW50OiBNYXRDaGlwSW5wdXRFdmVudCk6IHZvaWQge1xuICAgIGNvbnN0IHZhbHVlID0gKGV2ZW50LnZhbHVlIHx8ICcnKS50cmltKCk7XG5cbiAgICBpZiAodmFsdWUpIHtcbiAgICAgIHRoaXMuY29weUFycmF5LnB1c2godmFsdWUpO1xuICAgIH1cblxuICAgIHRoaXMuZm9ybS5zZXRWYWx1ZSh7aW5wdXRJZDogbnVsbH0pO1xuICAgIHRoaXMub25DaGFuZ2UuZW1pdCh0aGlzLmNvcHlBcnJheSk7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICBzdXBlci5uZ09uSW5pdCgpO1xuICAgIHRoaXMuZm9ybSA9IG5ldyBGb3JtR3JvdXAoe1xuICAgICAgaW5wdXRJZDogbmV3IEZvcm1Db250cm9sKHt2YWx1ZTogbnVsbCwgZGlzYWJsZWQ6IHRoaXMuZGlzYWJsZWR9KSxcbiAgICB9KTtcbiAgfVxuXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpOiB2b2lkIHtcbiAgICBpZiAoY2hhbmdlcy52YWx1ZSAmJiAhIXRoaXMudmFsdWUpIHtcbiAgICAgIHRoaXMuY29weUFycmF5ID0gWy4uLnRoaXMudmFsdWVdO1xuICAgIH1cbiAgfVxufVxuIl19