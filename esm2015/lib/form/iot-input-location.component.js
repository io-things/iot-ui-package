import { Component, EventEmitter, forwardRef, Input, Output, TemplateRef, ViewChild } from '@angular/core';
import { DynamicFormWidth } from "./dynamic-form-width";
import { FormControl, FormGroup } from "@angular/forms";
import * as i0 from "@angular/core";
import * as i1 from "@agm/core";
import * as i2 from "@angular/common";
import * as i3 from "@ngx-translate/core";
export class IotInputLocationComponent extends DynamicFormWidth {
    constructor() {
        super(...arguments);
        this.zoom = 15;
        this.form = new FormGroup({
            inputId: new FormControl(''),
        });
        this.attribute = 'todo';
        this.type = 'text';
        this.disabled = false;
        this.onChange = new EventEmitter();
    }
    get template() {
        return this._template;
    }
    get pristine() {
        return this.lat == this.latCopy && this.lng == this.lngCopy && this.zoom == this.zoomCopy;
    }
    get key() {
        return this.attribute;
    }
    get newValue() {
        return {
            latitude: this.latCopy,
            longitude: this.lngCopy,
            zoom: this.zoomCopy
        };
    }
    markerChange(event) {
        this.latCopy = event.lat || event.coords.lat;
        this.lngCopy = event.lng || event.coords.lng;
        this.form.setValue({ inputId: '' + this.lat });
        this.onChange.emit(event.coords || event);
    }
    zoomChange(event) {
        this.zoomCopy = event;
        this.form.setValue({ inputId: '' + event });
    }
    ngOnInit() {
        super.ngOnInit();
        this.latCopy = this.lat;
        this.lngCopy = this.lng;
        this.zoomCopy = this.zoom;
        if (this.disabled) {
            this.form = new FormGroup({
                inputId: new FormControl({ value: '' + this.lat, disabled: this.disabled }),
            });
        }
    }
}
IotInputLocationComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputLocationComponent, deps: null, target: i0.ɵɵFactoryTarget.Component });
IotInputLocationComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotInputLocationComponent, selector: "iot-input-location", inputs: { zoom: "zoom", lat: "lat", lng: "lng", label: "label", attribute: "attribute", value: "value", type: "type", disabled: "disabled" }, outputs: { onChange: "onChange" }, providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputLocationComponent) }], viewQueries: [{ propertyName: "_template", first: true, predicate: TemplateRef, descendants: true }], usesInheritance: true, ngImport: i0, template: `
    <ng-template>
        <div class="outer-margin">
            <div class="outeroneline" [ngClass]="{'outersmall':!isBig}">
                <div class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
                >
                    <div class="label">{{label | translate}} </div>
                </div>

              <div class="content ">
                <agm-map
                  class="input-map"
                  [zoom]="zoom"
                  [mapTypeId]="'roadmap'"
                  (zoomChange)="zoomChange($event)"
                  (centerChange)="markerChange($event)"
                  [latitude]="lat" [longitude]="lng">

                  <agm-marker
                              [markerDraggable]="true"
                              (dragEnd)="markerChange($event)"
                              [latitude]="latCopy"
                              [longitude]="lngCopy">
                  </agm-marker>
                </agm-map>

              </div>



            </div>

        </div>
    </ng-template>

    `, isInline: true, styles: ["\n    agm-map {\n      height: 200px;\n    }\n        .outer-margin {\n            /*margin-left: -5px;*/\n\n            padding-right: 50px;\n        }\n\n        .label-outer.label-outer-thin {\n            min-height: 40px;\n        }\n\n        .label-outer {\n            flex-basis: 33%;\n            min-height: 64px;\n            min-width: 220px;\n            display: block;\n            padding-top: 13px;\n            color: #3c4043;\n        }\n\n        .label {\n\n            font-family: Roboto, Arial, sans-serif;\n            font-size: 14px;\n            font-weight: 400;\n            height: 20px;\n            color: rgb(60, 64, 67);\n        }\n\n        .outeroneline {\n            display: flex;\n            max-width: 900px;\n        }\n\n\n        .outersmall {\n            flex-direction: column;\n            padding-bottom: 8px;\n        }\n\n\n        .content {\n            flex-direction: column;\n            flex-basis: 67%;\n            min-width: 280px;\n          border: 1px solid #dadce0;\n          border-radius: 4px;\n        }\n\n        .form-content > div {\n\n\n          border: 2px solid red;\n          border-radius: 25px;\n\n        }\n\n\n\n    .input-map {\n\n      height: 300px;\n      width: 100%;\n      display: flex;\n      border: none !important;\n      background-color: transparent;\n      pointer-events: auto;\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 1rem;\n      font-weight: 400;\n      letter-spacing: .00625em;\n    }\n    "], components: [{ type: i1.AgmMap, selector: "agm-map", inputs: ["longitude", "latitude", "zoom", "mapDraggable", "disableDoubleClickZoom", "disableDefaultUI", "scrollwheel", "keyboardShortcuts", "styles", "usePanning", "fitBounds", "scaleControl", "mapTypeControl", "panControl", "rotateControl", "fullscreenControl", "mapTypeId", "clickableIcons", "showDefaultInfoWindow", "gestureHandling", "tilt", "minZoom", "maxZoom", "controlSize", "backgroundColor", "draggableCursor", "draggingCursor", "zoomControl", "zoomControlOptions", "streetViewControl", "streetViewControlOptions", "fitBoundsPadding", "scaleControlOptions", "mapTypeControlOptions", "panControlOptions", "rotateControlOptions", "fullscreenControlOptions", "restriction"], outputs: ["mapClick", "mapRightClick", "mapDblClick", "centerChange", "boundsChange", "mapTypeIdChange", "idle", "zoomChange", "mapReady", "tilesLoaded"] }], directives: [{ type: i2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i1.AgmMarker, selector: "agm-marker", inputs: ["latitude", "longitude", "title", "label", "markerDraggable", "iconUrl", "openInfoWindow", "opacity", "visible", "zIndex", "animation", "markerClickable"], outputs: ["markerClick", "dragStart", "drag", "dragEnd", "mouseOver", "mouseOut", "animationChange", "markerDblClick", "markerRightClick"] }], pipes: { "translate": i3.TranslatePipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputLocationComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-input-location',
                    template: `
    <ng-template>
        <div class="outer-margin">
            <div class="outeroneline" [ngClass]="{'outersmall':!isBig}">
                <div class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
                >
                    <div class="label">{{label | translate}} </div>
                </div>

              <div class="content ">
                <agm-map
                  class="input-map"
                  [zoom]="zoom"
                  [mapTypeId]="'roadmap'"
                  (zoomChange)="zoomChange($event)"
                  (centerChange)="markerChange($event)"
                  [latitude]="lat" [longitude]="lng">

                  <agm-marker
                              [markerDraggable]="true"
                              (dragEnd)="markerChange($event)"
                              [latitude]="latCopy"
                              [longitude]="lngCopy">
                  </agm-marker>
                </agm-map>

              </div>



            </div>

        </div>
    </ng-template>

    `,
                    styles: [`
    agm-map {
      height: 200px;
    }
        .outer-margin {
            /*margin-left: -5px;*/

            padding-right: 50px;
        }

        .label-outer.label-outer-thin {
            min-height: 40px;
        }

        .label-outer {
            flex-basis: 33%;
            min-height: 64px;
            min-width: 220px;
            display: block;
            padding-top: 13px;
            color: #3c4043;
        }

        .label {

            font-family: Roboto, Arial, sans-serif;
            font-size: 14px;
            font-weight: 400;
            height: 20px;
            color: rgb(60, 64, 67);
        }

        .outeroneline {
            display: flex;
            max-width: 900px;
        }


        .outersmall {
            flex-direction: column;
            padding-bottom: 8px;
        }


        .content {
            flex-direction: column;
            flex-basis: 67%;
            min-width: 280px;
          border: 1px solid #dadce0;
          border-radius: 4px;
        }

        .form-content > div {


          border: 2px solid red;
          border-radius: 25px;

        }



    .input-map {

      height: 300px;
      width: 100%;
      display: flex;
      border: none !important;
      background-color: transparent;
      pointer-events: auto;
      font-family: Roboto, Arial, sans-serif;
      font-size: 1rem;
      font-weight: 400;
      letter-spacing: .00625em;
    }
    `],
                    providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputLocationComponent) }]
                }]
        }], propDecorators: { _template: [{
                type: ViewChild,
                args: [TemplateRef]
            }], zoom: [{
                type: Input
            }], lat: [{
                type: Input
            }], lng: [{
                type: Input
            }], label: [{
                type: Input
            }], attribute: [{
                type: Input
            }], value: [{
                type: Input
            }], type: [{
                type: Input
            }], disabled: [{
                type: Input
            }], onChange: [{
                type: Output
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW90LWlucHV0LWxvY2F0aW9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2lvdC11aS1saWIvc3JjL2xpYi9mb3JtL2lvdC1pbnB1dC1sb2NhdGlvbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFDLFNBQVMsRUFBRSxZQUFZLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBVSxNQUFNLEVBQUUsV0FBVyxFQUFFLFNBQVMsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUNqSCxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQztBQUN0RCxPQUFPLEVBQUMsV0FBVyxFQUFFLFNBQVMsRUFBQyxNQUFNLGdCQUFnQixDQUFDOzs7OztBQXVIdEQsTUFBTSxPQUFPLHlCQUEwQixTQUFRLGdCQUFnQjtJQXBIL0Q7O1FBdUhXLFNBQUksR0FBVyxFQUFFLENBQUM7UUE0QjNCLFNBQUksR0FBRyxJQUFJLFNBQVMsQ0FBQztZQUNuQixPQUFPLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxDQUFDO1NBQzdCLENBQUMsQ0FBQztRQWtCTSxjQUFTLEdBQUcsTUFBTSxDQUFDO1FBRW5CLFNBQUksR0FBRyxNQUFNLENBQUM7UUFDZCxhQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ2hCLGFBQVEsR0FBRyxJQUFJLFlBQVksRUFBVSxDQUFDO0tBYWpEO0lBMURDLElBQVcsUUFBUTtRQUVqQixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7SUFDeEIsQ0FBQztJQUVELElBQVcsUUFBUTtRQUNqQixPQUFPLElBQUksQ0FBQyxHQUFHLElBQUksSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsR0FBRyxJQUFJLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQzVGLENBQUM7SUFFRCxJQUFXLEdBQUc7UUFDWixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7SUFDeEIsQ0FBQztJQUVELElBQVcsUUFBUTtRQUNqQixPQUFPO1lBQ0wsUUFBUSxFQUFFLElBQUksQ0FBQyxPQUFPO1lBQ3RCLFNBQVMsRUFBRSxJQUFJLENBQUMsT0FBTztZQUN2QixJQUFJLEVBQUUsSUFBSSxDQUFDLFFBQVE7U0FDcEIsQ0FBQztJQUNKLENBQUM7SUFNRCxZQUFZLENBQUMsS0FBVTtRQUNyQixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQyxHQUFHLElBQUksS0FBSyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUM7UUFDN0MsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUMsR0FBRyxJQUFJLEtBQUssQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDO1FBQzdDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUMsT0FBTyxFQUFFLEVBQUUsR0FBQyxJQUFJLENBQUMsR0FBRyxFQUFDLENBQUMsQ0FBQztRQUMzQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxJQUFJLEtBQUssQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFJRCxVQUFVLENBQUMsS0FBVTtRQUNuQixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFDLE9BQU8sRUFBRSxFQUFFLEdBQUMsS0FBSyxFQUFDLENBQUMsQ0FBQztJQUMxQyxDQUFDO0lBVUQsUUFBUTtRQUNOLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNqQixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUM7UUFDeEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztRQUMxQixJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDakIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLFNBQVMsQ0FBQztnQkFDeEIsT0FBTyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUMsS0FBSyxFQUFDLEVBQUUsR0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFDLENBQUM7YUFDdkUsQ0FBQyxDQUFDO1NBQ0o7SUFDSCxDQUFDOztzSEFuRVUseUJBQXlCOzBHQUF6Qix5QkFBeUIsOE5BRnpCLENBQUMsRUFBQyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsV0FBVyxFQUFFLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyx5QkFBeUIsQ0FBQyxFQUFDLENBQUMscUVBSXZGLFdBQVcsdUVBcEhaOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztLQW1DUDsyRkErRVEseUJBQXlCO2tCQXBIckMsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUsb0JBQW9CO29CQUM5QixRQUFRLEVBQUU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0tBbUNQO29CQUNILE1BQU0sRUFBRSxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7S0EyRU4sQ0FBQztvQkFDSixTQUFTLEVBQUUsQ0FBQyxFQUFDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxXQUFXLEVBQUUsVUFBVSxDQUFDLEdBQUcsRUFBRSwwQkFBMEIsQ0FBQyxFQUFDLENBQUM7aUJBQ25HOzhCQUd5QixTQUFTO3NCQUFoQyxTQUFTO3VCQUFDLFdBQVc7Z0JBQ2IsSUFBSTtzQkFBWixLQUFLO2dCQUNHLEdBQUc7c0JBQVgsS0FBSztnQkFDRyxHQUFHO3NCQUFYLEtBQUs7Z0JBNkNHLEtBQUs7c0JBQWIsS0FBSztnQkFDRyxTQUFTO3NCQUFqQixLQUFLO2dCQUNHLEtBQUs7c0JBQWIsS0FBSztnQkFDRyxJQUFJO3NCQUFaLEtBQUs7Z0JBQ0csUUFBUTtzQkFBaEIsS0FBSztnQkFDSSxRQUFRO3NCQUFqQixNQUFNIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgZm9yd2FyZFJlZiwgSW5wdXQsIE9uSW5pdCwgT3V0cHV0LCBUZW1wbGF0ZVJlZiwgVmlld0NoaWxkfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7RHluYW1pY0Zvcm1XaWR0aH0gZnJvbSBcIi4vZHluYW1pYy1mb3JtLXdpZHRoXCI7XG5pbXBvcnQge0Zvcm1Db250cm9sLCBGb3JtR3JvdXB9IGZyb20gXCJAYW5ndWxhci9mb3Jtc1wiO1xuaW1wb3J0IHtnZXRMb2NhbGVGaXJzdERheU9mV2Vla30gZnJvbSBcIkBhbmd1bGFyL2NvbW1vblwiO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdpb3QtaW5wdXQtbG9jYXRpb24nLFxuICB0ZW1wbGF0ZTogYFxuICAgIDxuZy10ZW1wbGF0ZT5cbiAgICAgICAgPGRpdiBjbGFzcz1cIm91dGVyLW1hcmdpblwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm91dGVyb25lbGluZVwiIFtuZ0NsYXNzXT1cInsnb3V0ZXJzbWFsbCc6IWlzQmlnfVwiPlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJsYWJlbC1vdXRlclwiIFtuZ0NsYXNzXT1cInsnbGFiZWwtb3V0ZXItdGhpbic6IWlzQmlnfVwiXG4gICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwibGFiZWxcIj57e2xhYmVsIHwgdHJhbnNsYXRlfX0gPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbnRlbnQgXCI+XG4gICAgICAgICAgICAgICAgPGFnbS1tYXBcbiAgICAgICAgICAgICAgICAgIGNsYXNzPVwiaW5wdXQtbWFwXCJcbiAgICAgICAgICAgICAgICAgIFt6b29tXT1cInpvb21cIlxuICAgICAgICAgICAgICAgICAgW21hcFR5cGVJZF09XCIncm9hZG1hcCdcIlxuICAgICAgICAgICAgICAgICAgKHpvb21DaGFuZ2UpPVwiem9vbUNoYW5nZSgkZXZlbnQpXCJcbiAgICAgICAgICAgICAgICAgIChjZW50ZXJDaGFuZ2UpPVwibWFya2VyQ2hhbmdlKCRldmVudClcIlxuICAgICAgICAgICAgICAgICAgW2xhdGl0dWRlXT1cImxhdFwiIFtsb25naXR1ZGVdPVwibG5nXCI+XG5cbiAgICAgICAgICAgICAgICAgIDxhZ20tbWFya2VyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbbWFya2VyRHJhZ2dhYmxlXT1cInRydWVcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGRyYWdFbmQpPVwibWFya2VyQ2hhbmdlKCRldmVudClcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW2xhdGl0dWRlXT1cImxhdENvcHlcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW2xvbmdpdHVkZV09XCJsbmdDb3B5XCI+XG4gICAgICAgICAgICAgICAgICA8L2FnbS1tYXJrZXI+XG4gICAgICAgICAgICAgICAgPC9hZ20tbWFwPlxuXG4gICAgICAgICAgICAgIDwvZGl2PlxuXG5cblxuICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgPC9kaXY+XG4gICAgPC9uZy10ZW1wbGF0ZT5cblxuICAgIGAsXG4gIHN0eWxlczogW2BcbiAgICBhZ20tbWFwIHtcbiAgICAgIGhlaWdodDogMjAwcHg7XG4gICAgfVxuICAgICAgICAub3V0ZXItbWFyZ2luIHtcbiAgICAgICAgICAgIC8qbWFyZ2luLWxlZnQ6IC01cHg7Ki9cblxuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogNTBweDtcbiAgICAgICAgfVxuXG4gICAgICAgIC5sYWJlbC1vdXRlci5sYWJlbC1vdXRlci10aGluIHtcbiAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDQwcHg7XG4gICAgICAgIH1cblxuICAgICAgICAubGFiZWwtb3V0ZXIge1xuICAgICAgICAgICAgZmxleC1iYXNpczogMzMlO1xuICAgICAgICAgICAgbWluLWhlaWdodDogNjRweDtcbiAgICAgICAgICAgIG1pbi13aWR0aDogMjIwcHg7XG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAxM3B4O1xuICAgICAgICAgICAgY29sb3I6ICMzYzQwNDM7XG4gICAgICAgIH1cblxuICAgICAgICAubGFiZWwge1xuXG4gICAgICAgICAgICBmb250LWZhbWlseTogUm9ib3RvLCBBcmlhbCwgc2Fucy1zZXJpZjtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICAgICAgICBoZWlnaHQ6IDIwcHg7XG4gICAgICAgICAgICBjb2xvcjogcmdiKDYwLCA2NCwgNjcpO1xuICAgICAgICB9XG5cbiAgICAgICAgLm91dGVyb25lbGluZSB7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgbWF4LXdpZHRoOiA5MDBweDtcbiAgICAgICAgfVxuXG5cbiAgICAgICAgLm91dGVyc21hbGwge1xuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiA4cHg7XG4gICAgICAgIH1cblxuXG4gICAgICAgIC5jb250ZW50IHtcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgICAgICBmbGV4LWJhc2lzOiA2NyU7XG4gICAgICAgICAgICBtaW4td2lkdGg6IDI4MHB4O1xuICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNkYWRjZTA7XG4gICAgICAgICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgICAgICB9XG5cbiAgICAgICAgLmZvcm0tY29udGVudCA+IGRpdiB7XG5cblxuICAgICAgICAgIGJvcmRlcjogMnB4IHNvbGlkIHJlZDtcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xuXG4gICAgICAgIH1cblxuXG5cbiAgICAuaW5wdXQtbWFwIHtcblxuICAgICAgaGVpZ2h0OiAzMDBweDtcbiAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGJvcmRlcjogbm9uZSAhaW1wb3J0YW50O1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgICBwb2ludGVyLWV2ZW50czogYXV0bztcbiAgICAgIGZvbnQtZmFtaWx5OiBSb2JvdG8sIEFyaWFsLCBzYW5zLXNlcmlmO1xuICAgICAgZm9udC1zaXplOiAxcmVtO1xuICAgICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICAgIGxldHRlci1zcGFjaW5nOiAuMDA2MjVlbTtcbiAgICB9XG4gICAgYF0sXG4gIHByb3ZpZGVyczogW3twcm92aWRlOiBEeW5hbWljRm9ybVdpZHRoLCB1c2VFeGlzdGluZzogZm9yd2FyZFJlZigoKSA9PiBJb3RJbnB1dExvY2F0aW9uQ29tcG9uZW50KX1dXG59KVxuZXhwb3J0IGNsYXNzIElvdElucHV0TG9jYXRpb25Db21wb25lbnQgZXh0ZW5kcyBEeW5hbWljRm9ybVdpZHRoIGltcGxlbWVudHMgT25Jbml0IHtcbiAgLy8gb2Zmc2V0OiBPYnNlcnZhYmxlPG51bWJlcj4gPSB0aGlzLnN0b3JlLnNlbGVjdChuYXZPZmZzZXQpO1xuICBAVmlld0NoaWxkKFRlbXBsYXRlUmVmKSBfdGVtcGxhdGU6IFRlbXBsYXRlUmVmPGFueT47XG4gIEBJbnB1dCgpIHpvb206IG51bWJlciA9IDE1O1xuICBASW5wdXQoKSBsYXQ6IG51bWJlcjtcbiAgQElucHV0KCkgbG5nOiBudW1iZXI7XG4gIGxhdENvcHk6IG51bWJlcjtcbiAgbG5nQ29weTogbnVtYmVyO1xuICB6b29tQ29weTogbnVtYmVyO1xuXG4gIHB1YmxpYyBnZXQgdGVtcGxhdGUoKSB7XG5cbiAgICByZXR1cm4gdGhpcy5fdGVtcGxhdGU7XG4gIH1cblxuICBwdWJsaWMgZ2V0IHByaXN0aW5lKCk6IGFueSB7XG4gICAgcmV0dXJuIHRoaXMubGF0ID09IHRoaXMubGF0Q29weSAmJiB0aGlzLmxuZyA9PSB0aGlzLmxuZ0NvcHkgJiYgdGhpcy56b29tID09IHRoaXMuem9vbUNvcHk7XG4gIH1cblxuICBwdWJsaWMgZ2V0IGtleSgpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLmF0dHJpYnV0ZTtcbiAgfVxuXG4gIHB1YmxpYyBnZXQgbmV3VmFsdWUoKTogYW55IHtcbiAgICByZXR1cm4ge1xuICAgICAgbGF0aXR1ZGU6IHRoaXMubGF0Q29weSxcbiAgICAgIGxvbmdpdHVkZTogdGhpcy5sbmdDb3B5LFxuICAgICAgem9vbTogdGhpcy56b29tQ29weVxuICAgIH07XG4gIH1cblxuICBmb3JtID0gbmV3IEZvcm1Hcm91cCh7XG4gICAgaW5wdXRJZDogbmV3IEZvcm1Db250cm9sKCcnKSxcbiAgfSk7XG5cbiAgbWFya2VyQ2hhbmdlKGV2ZW50OiBhbnkpIHtcbiAgICB0aGlzLmxhdENvcHkgPSBldmVudC5sYXQgfHwgZXZlbnQuY29vcmRzLmxhdDtcbiAgICB0aGlzLmxuZ0NvcHkgPSBldmVudC5sbmcgfHwgZXZlbnQuY29vcmRzLmxuZztcbiAgICB0aGlzLmZvcm0uc2V0VmFsdWUoe2lucHV0SWQ6ICcnK3RoaXMubGF0fSk7XG4gICAgdGhpcy5vbkNoYW5nZS5lbWl0KGV2ZW50LmNvb3JkcyB8fCBldmVudCk7XG4gIH1cblxuXG5cbiAgem9vbUNoYW5nZShldmVudDogYW55KSB7XG4gICAgdGhpcy56b29tQ29weSA9IGV2ZW50O1xuICAgIHRoaXMuZm9ybS5zZXRWYWx1ZSh7aW5wdXRJZDogJycrZXZlbnR9KTtcbiAgfVxuXG5cbiAgQElucHV0KCkgbGFiZWw7XG4gIEBJbnB1dCgpIGF0dHJpYnV0ZSA9ICd0b2RvJztcbiAgQElucHV0KCkgdmFsdWU7XG4gIEBJbnB1dCgpIHR5cGUgPSAndGV4dCc7XG4gIEBJbnB1dCgpIGRpc2FibGVkID0gZmFsc2U7XG4gIEBPdXRwdXQoKSBvbkNoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXI8c3RyaW5nPigpO1xuXG4gIG5nT25Jbml0KCkge1xuICAgIHN1cGVyLm5nT25Jbml0KCk7XG4gICAgdGhpcy5sYXRDb3B5ID0gdGhpcy5sYXQ7XG4gICAgdGhpcy5sbmdDb3B5ID0gdGhpcy5sbmc7XG4gICAgdGhpcy56b29tQ29weSA9IHRoaXMuem9vbTtcbiAgICBpZiAodGhpcy5kaXNhYmxlZCkge1xuICAgICAgdGhpcy5mb3JtID0gbmV3IEZvcm1Hcm91cCh7XG4gICAgICAgIGlucHV0SWQ6IG5ldyBGb3JtQ29udHJvbCh7dmFsdWU6JycrdGhpcy5sYXQsIGRpc2FibGVkOiB0aGlzLmRpc2FibGVkfSksXG4gICAgICB9KTtcbiAgICB9XG4gIH1cbn1cbiJdfQ==