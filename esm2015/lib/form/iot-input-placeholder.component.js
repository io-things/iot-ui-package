import { Component, EventEmitter, forwardRef, Input, Output, TemplateRef, ViewChild } from '@angular/core';
import { DynamicFormWidth } from "./dynamic-form-width";
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
import * as i2 from "@ngx-translate/core";
export class IotInputPlaceholderComponent extends DynamicFormWidth {
    constructor() {
        super(...arguments);
        this.fullWidth = false;
        this.attribute = 'todo';
        this.type = 'text';
        this.disabled = false;
        this.onChange = new EventEmitter();
    }
    get template() {
        return this._template;
    }
    get pristine() {
        // return this.form.value.inputId === this.value;
        return true;
    }
    get key() {
        return '';
    }
    get newValue() {
        return '';
    }
    ngOnInit() {
        super.ngOnInit();
        if (this.disabled) {
            // this.form = new FormGroup({
            //   inputId: new FormControl({value:'', disabled: this.disabled}),
            // });
        }
    }
}
IotInputPlaceholderComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputPlaceholderComponent, deps: null, target: i0.ɵɵFactoryTarget.Component });
IotInputPlaceholderComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotInputPlaceholderComponent, selector: "iot-input-placeholder", inputs: { label: "label", fullWidth: "fullWidth", attribute: "attribute", value: "value", type: "type", disabled: "disabled" }, outputs: { onChange: "onChange" }, providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputPlaceholderComponent) }], viewQueries: [{ propertyName: "_template", first: true, predicate: TemplateRef, descendants: true }], usesInheritance: true, ngImport: i0, template: `
    <ng-template>
      <div *ngIf="!disabled && fullWidth">
        <ng-content select="[full-width]"></ng-content>
      </div>
      <div *ngIf="!disabled && !fullWidth" class="outer-margin">
            <div class="outeroneline" [ngClass]="{'outersmall':!isBig}">
                <div class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
                >
                    <div class="label">{{label | translate}} </div>
                </div>

                <div class="content">
                    <ng-content select="[field-positioned]"></ng-content>

                </div>


            </div>

        </div>
    </ng-template>

    `, isInline: true, styles: ["\n        .outer-margin {\n            padding-right: 50px;\n        }\n\n        .label-outer.label-outer-thin {\n            min-height: 40px;\n        }\n\n        .label-outer {\n            flex-basis: 33%;\n            min-height: 64px;\n            min-width: 220px;\n            display: block;\n            padding-top: 13px;\n            color: #3c4043;\n        }\n\n        .label {\n\n            font-family: Roboto, Arial, sans-serif;\n            font-size: 14px;\n            font-weight: 400;\n            height: 20px;\n            color: rgb(60, 64, 67);\n        }\n\n        .outeroneline {\n            display: flex;\n            max-width: 900px;\n        }\n\n\n        .outersmall {\n            flex-direction: column;\n            padding-bottom: 8px;\n        }\n\n\n        .content {\n            flex-direction: column;\n            flex-basis: 67%;\n            min-width: 280px;\n        }\n\n\n\n\n    "], directives: [{ type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }], pipes: { "translate": i2.TranslatePipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputPlaceholderComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-input-placeholder',
                    template: `
    <ng-template>
      <div *ngIf="!disabled && fullWidth">
        <ng-content select="[full-width]"></ng-content>
      </div>
      <div *ngIf="!disabled && !fullWidth" class="outer-margin">
            <div class="outeroneline" [ngClass]="{'outersmall':!isBig}">
                <div class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
                >
                    <div class="label">{{label | translate}} </div>
                </div>

                <div class="content">
                    <ng-content select="[field-positioned]"></ng-content>

                </div>


            </div>

        </div>
    </ng-template>

    `,
                    styles: [`
        .outer-margin {
            padding-right: 50px;
        }

        .label-outer.label-outer-thin {
            min-height: 40px;
        }

        .label-outer {
            flex-basis: 33%;
            min-height: 64px;
            min-width: 220px;
            display: block;
            padding-top: 13px;
            color: #3c4043;
        }

        .label {

            font-family: Roboto, Arial, sans-serif;
            font-size: 14px;
            font-weight: 400;
            height: 20px;
            color: rgb(60, 64, 67);
        }

        .outeroneline {
            display: flex;
            max-width: 900px;
        }


        .outersmall {
            flex-direction: column;
            padding-bottom: 8px;
        }


        .content {
            flex-direction: column;
            flex-basis: 67%;
            min-width: 280px;
        }




    `],
                    providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputPlaceholderComponent) }]
                }]
        }], propDecorators: { _template: [{
                type: ViewChild,
                args: [TemplateRef]
            }], label: [{
                type: Input
            }], fullWidth: [{
                type: Input
            }], attribute: [{
                type: Input
            }], value: [{
                type: Input
            }], type: [{
                type: Input
            }], disabled: [{
                type: Input
            }], onChange: [{
                type: Output
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW90LWlucHV0LXBsYWNlaG9sZGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2lvdC11aS1saWIvc3JjL2xpYi9mb3JtL2lvdC1pbnB1dC1wbGFjZWhvbGRlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFDLFNBQVMsRUFBRSxZQUFZLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBVSxNQUFNLEVBQUUsV0FBVyxFQUFFLFNBQVMsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUNqSCxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQzs7OztBQWdGdEQsTUFBTSxPQUFPLDRCQUE2QixTQUFRLGdCQUFnQjtJQTdFbEU7O1FBeUdXLGNBQVMsR0FBRyxLQUFLLENBQUM7UUFDbEIsY0FBUyxHQUFHLE1BQU0sQ0FBQztRQUVuQixTQUFJLEdBQUcsTUFBTSxDQUFDO1FBQ2QsYUFBUSxHQUFHLEtBQUssQ0FBQztRQUNoQixhQUFRLEdBQUcsSUFBSSxZQUFZLEVBQVUsQ0FBQztLQVVqRDtJQXZDQyxJQUFXLFFBQVE7UUFFakIsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQ3hCLENBQUM7SUFFRCxJQUFXLFFBQVE7UUFFakIsaURBQWlEO1FBQ2pELE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVELElBQVcsR0FBRztRQUNaLE9BQU8sRUFBRSxDQUFDO0lBQ1osQ0FBQztJQUVELElBQVcsUUFBUTtRQUNqQixPQUFPLEVBQUUsQ0FBQztJQUNaLENBQUM7SUFjRCxRQUFRO1FBQ04sS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ2pCLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNqQiw4QkFBOEI7WUFDOUIsbUVBQW1FO1lBQ25FLE1BQU07U0FDUDtJQUNILENBQUM7O3lIQTFDVSw0QkFBNEI7NkdBQTVCLDRCQUE0QixtTkFGNUIsQ0FBQyxFQUFDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxXQUFXLEVBQUUsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDLDRCQUE0QixDQUFDLEVBQUMsQ0FBQyxxRUFHMUYsV0FBVyx1RUE1RVo7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0tBdUJQOzJGQW9EUSw0QkFBNEI7a0JBN0V4QyxTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSx1QkFBdUI7b0JBQ2pDLFFBQVEsRUFBRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7S0F1QlA7b0JBQ0gsTUFBTSxFQUFFLENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztLQWdETixDQUFDO29CQUNKLFNBQVMsRUFBRSxDQUFDLEVBQUMsT0FBTyxFQUFFLGdCQUFnQixFQUFFLFdBQVcsRUFBRSxVQUFVLENBQUMsR0FBRyxFQUFFLDZCQUE2QixDQUFDLEVBQUMsQ0FBQztpQkFDdEc7OEJBRXlCLFNBQVM7c0JBQWhDLFNBQVM7dUJBQUMsV0FBVztnQkEwQmIsS0FBSztzQkFBYixLQUFLO2dCQUNHLFNBQVM7c0JBQWpCLEtBQUs7Z0JBQ0csU0FBUztzQkFBakIsS0FBSztnQkFDRyxLQUFLO3NCQUFiLEtBQUs7Z0JBQ0csSUFBSTtzQkFBWixLQUFLO2dCQUNHLFFBQVE7c0JBQWhCLEtBQUs7Z0JBQ0ksUUFBUTtzQkFBakIsTUFBTSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIGZvcndhcmRSZWYsIElucHV0LCBPbkluaXQsIE91dHB1dCwgVGVtcGxhdGVSZWYsIFZpZXdDaGlsZH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0R5bmFtaWNGb3JtV2lkdGh9IGZyb20gXCIuL2R5bmFtaWMtZm9ybS13aWR0aFwiO1xuaW1wb3J0IHtGb3JtQ29udHJvbCwgRm9ybUdyb3VwfSBmcm9tIFwiQGFuZ3VsYXIvZm9ybXNcIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnaW90LWlucHV0LXBsYWNlaG9sZGVyJyxcbiAgdGVtcGxhdGU6IGBcbiAgICA8bmctdGVtcGxhdGU+XG4gICAgICA8ZGl2ICpuZ0lmPVwiIWRpc2FibGVkICYmIGZ1bGxXaWR0aFwiPlxuICAgICAgICA8bmctY29udGVudCBzZWxlY3Q9XCJbZnVsbC13aWR0aF1cIj48L25nLWNvbnRlbnQ+XG4gICAgICA8L2Rpdj5cbiAgICAgIDxkaXYgKm5nSWY9XCIhZGlzYWJsZWQgJiYgIWZ1bGxXaWR0aFwiIGNsYXNzPVwib3V0ZXItbWFyZ2luXCI+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwib3V0ZXJvbmVsaW5lXCIgW25nQ2xhc3NdPVwieydvdXRlcnNtYWxsJzohaXNCaWd9XCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImxhYmVsLW91dGVyXCIgW25nQ2xhc3NdPVwieydsYWJlbC1vdXRlci10aGluJzohaXNCaWd9XCJcbiAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJsYWJlbFwiPnt7bGFiZWwgfCB0cmFuc2xhdGV9fSA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb250ZW50XCI+XG4gICAgICAgICAgICAgICAgICAgIDxuZy1jb250ZW50IHNlbGVjdD1cIltmaWVsZC1wb3NpdGlvbmVkXVwiPjwvbmctY29udGVudD5cblxuICAgICAgICAgICAgICAgIDwvZGl2PlxuXG5cbiAgICAgICAgICAgIDwvZGl2PlxuXG4gICAgICAgIDwvZGl2PlxuICAgIDwvbmctdGVtcGxhdGU+XG5cbiAgICBgLFxuICBzdHlsZXM6IFtgXG4gICAgICAgIC5vdXRlci1tYXJnaW4ge1xuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogNTBweDtcbiAgICAgICAgfVxuXG4gICAgICAgIC5sYWJlbC1vdXRlci5sYWJlbC1vdXRlci10aGluIHtcbiAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDQwcHg7XG4gICAgICAgIH1cblxuICAgICAgICAubGFiZWwtb3V0ZXIge1xuICAgICAgICAgICAgZmxleC1iYXNpczogMzMlO1xuICAgICAgICAgICAgbWluLWhlaWdodDogNjRweDtcbiAgICAgICAgICAgIG1pbi13aWR0aDogMjIwcHg7XG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiAxM3B4O1xuICAgICAgICAgICAgY29sb3I6ICMzYzQwNDM7XG4gICAgICAgIH1cblxuICAgICAgICAubGFiZWwge1xuXG4gICAgICAgICAgICBmb250LWZhbWlseTogUm9ib3RvLCBBcmlhbCwgc2Fucy1zZXJpZjtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICAgICAgICBoZWlnaHQ6IDIwcHg7XG4gICAgICAgICAgICBjb2xvcjogcmdiKDYwLCA2NCwgNjcpO1xuICAgICAgICB9XG5cbiAgICAgICAgLm91dGVyb25lbGluZSB7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgbWF4LXdpZHRoOiA5MDBweDtcbiAgICAgICAgfVxuXG5cbiAgICAgICAgLm91dGVyc21hbGwge1xuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiA4cHg7XG4gICAgICAgIH1cblxuXG4gICAgICAgIC5jb250ZW50IHtcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgICAgICBmbGV4LWJhc2lzOiA2NyU7XG4gICAgICAgICAgICBtaW4td2lkdGg6IDI4MHB4O1xuICAgICAgICB9XG5cblxuXG5cbiAgICBgXSxcbiAgcHJvdmlkZXJzOiBbe3Byb3ZpZGU6IER5bmFtaWNGb3JtV2lkdGgsIHVzZUV4aXN0aW5nOiBmb3J3YXJkUmVmKCgpID0+IElvdElucHV0UGxhY2Vob2xkZXJDb21wb25lbnQpfV1cbn0pXG5leHBvcnQgY2xhc3MgSW90SW5wdXRQbGFjZWhvbGRlckNvbXBvbmVudCBleHRlbmRzIER5bmFtaWNGb3JtV2lkdGggaW1wbGVtZW50cyBPbkluaXQge1xuICBAVmlld0NoaWxkKFRlbXBsYXRlUmVmKSBfdGVtcGxhdGU6IFRlbXBsYXRlUmVmPGFueT47XG5cblxuICBwdWJsaWMgZ2V0IHRlbXBsYXRlKCkge1xuXG4gICAgcmV0dXJuIHRoaXMuX3RlbXBsYXRlO1xuICB9XG5cbiAgcHVibGljIGdldCBwcmlzdGluZSgpOiBhbnkge1xuXG4gICAgLy8gcmV0dXJuIHRoaXMuZm9ybS52YWx1ZS5pbnB1dElkID09PSB0aGlzLnZhbHVlO1xuICAgIHJldHVybiB0cnVlO1xuICB9XG5cbiAgcHVibGljIGdldCBrZXkoKTogc3RyaW5nIHtcbiAgICByZXR1cm4gJyc7XG4gIH1cblxuICBwdWJsaWMgZ2V0IG5ld1ZhbHVlKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuICcnO1xuICB9XG5cbiAgLy8gZm9ybSA9IG5ldyBGb3JtR3JvdXAoe1xuICAvLyAgIGlucHV0SWQ6IG5ldyBGb3JtQ29udHJvbCgnJyksXG4gIC8vIH0pO1xuXG4gIEBJbnB1dCgpIGxhYmVsO1xuICBASW5wdXQoKSBmdWxsV2lkdGggPSBmYWxzZTtcbiAgQElucHV0KCkgYXR0cmlidXRlID0gJ3RvZG8nO1xuICBASW5wdXQoKSB2YWx1ZTtcbiAgQElucHV0KCkgdHlwZSA9ICd0ZXh0JztcbiAgQElucHV0KCkgZGlzYWJsZWQgPSBmYWxzZTtcbiAgQE91dHB1dCgpIG9uQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxzdHJpbmc+KCk7XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgc3VwZXIubmdPbkluaXQoKTtcbiAgICBpZiAodGhpcy5kaXNhYmxlZCkge1xuICAgICAgLy8gdGhpcy5mb3JtID0gbmV3IEZvcm1Hcm91cCh7XG4gICAgICAvLyAgIGlucHV0SWQ6IG5ldyBGb3JtQ29udHJvbCh7dmFsdWU6JycsIGRpc2FibGVkOiB0aGlzLmRpc2FibGVkfSksXG4gICAgICAvLyB9KTtcbiAgICB9XG4gIH1cbn1cbiJdfQ==