import { Component, EventEmitter, Input, Output, TemplateRef, ViewChild, forwardRef } from '@angular/core';
import { DynamicFormWidth } from "./dynamic-form-width";
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
export class IotInputHorizontalLineComponent extends DynamicFormWidth {
    constructor() {
        super(...arguments);
        this.value = "none";
        this.label = "none";
        this.disabled = false;
        this.onChange = new EventEmitter();
    }
    get template() {
        return this._template;
    }
    ngOnInit() {
        // this.template = this.t;
        super.ngOnInit();
    }
}
IotInputHorizontalLineComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputHorizontalLineComponent, deps: null, target: i0.ɵɵFactoryTarget.Component });
IotInputHorizontalLineComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotInputHorizontalLineComponent, selector: "iot-horizontal-line", inputs: { value: "value", label: "label", disabled: "disabled" }, outputs: { onChange: "onChange" }, providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputHorizontalLineComponent) }], viewQueries: [{ propertyName: "_template", first: true, predicate: TemplateRef, descendants: true }], usesInheritance: true, ngImport: i0, template: `

    <ng-template>
      <div *ngIf="!isBig" class="content horizontal-line">
      </div>

      <div *ngIf="isBig" class="horizontal-line big-line">
      </div>
    </ng-template>

  `, isInline: true, styles: ["\n    .horizontal-line {\n      border-bottom: 1px solid #dadce0;\n      margin-right: 17px;\n      margin-bottom: 10px;\n      min-width: 395px;\n    }\n\n    .big-line {\n      max-width: 928px;\n    }\n\n  "], directives: [{ type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputHorizontalLineComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-horizontal-line',
                    template: `

    <ng-template>
      <div *ngIf="!isBig" class="content horizontal-line">
      </div>

      <div *ngIf="isBig" class="horizontal-line big-line">
      </div>
    </ng-template>

  `,
                    styles: [`
    .horizontal-line {
      border-bottom: 1px solid #dadce0;
      margin-right: 17px;
      margin-bottom: 10px;
      min-width: 395px;
    }

    .big-line {
      max-width: 928px;
    }

  `],
                    providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputHorizontalLineComponent) }]
                }]
        }], propDecorators: { _template: [{
                type: ViewChild,
                args: [TemplateRef]
            }], value: [{
                type: Input
            }], label: [{
                type: Input
            }], disabled: [{
                type: Input
            }], onChange: [{
                type: Output
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW90LWlucHV0LWhvcml6b250YWwtbGluZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9pb3QtdWktbGliL3NyYy9saWIvZm9ybS9pb3QtaW5wdXQtaG9yaXpvbnRhbC1saW5lLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQVUsTUFBTSxFQUFFLFdBQVcsRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ2pILE9BQU8sRUFBQyxnQkFBZ0IsRUFBQyxNQUFNLHNCQUFzQixDQUFDOzs7QUFnQ3RELE1BQU0sT0FBTywrQkFBZ0MsU0FBUSxnQkFBZ0I7SUE3QnJFOztRQW1DVyxVQUFLLEdBQUcsTUFBTSxDQUFDO1FBQ2YsVUFBSyxHQUFHLE1BQU0sQ0FBQztRQUNmLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFDaEIsYUFBUSxHQUFHLElBQUksWUFBWSxFQUFVLENBQUM7S0FNakQ7SUFaQyxJQUFXLFFBQVE7UUFDakIsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQ3hCLENBQUM7SUFNRCxRQUFRO1FBQ04sMEJBQTBCO1FBQzFCLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNuQixDQUFDOzs0SEFkVSwrQkFBK0I7Z0hBQS9CLCtCQUErQixtSkFGL0IsQ0FBQyxFQUFDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxXQUFXLEVBQUUsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDLCtCQUErQixDQUFDLEVBQUMsQ0FBQyxxRUFHN0YsV0FBVyx1RUE1Qlo7Ozs7Ozs7Ozs7R0FVVDsyRkFpQlUsK0JBQStCO2tCQTdCM0MsU0FBUzttQkFBQztvQkFDVCxRQUFRLEVBQUUscUJBQXFCO29CQUMvQixRQUFRLEVBQUU7Ozs7Ozs7Ozs7R0FVVDtvQkFDRCxNQUFNLEVBQUUsQ0FBQzs7Ozs7Ozs7Ozs7O0dBWVIsQ0FBQztvQkFFRixTQUFTLEVBQUUsQ0FBQyxFQUFDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxXQUFXLEVBQUUsVUFBVSxDQUFDLEdBQUcsRUFBRSxnQ0FBZ0MsQ0FBQyxFQUFDLENBQUM7aUJBQ3pHOzhCQUV5QixTQUFTO3NCQUFoQyxTQUFTO3VCQUFDLFdBQVc7Z0JBS2IsS0FBSztzQkFBYixLQUFLO2dCQUNHLEtBQUs7c0JBQWIsS0FBSztnQkFDRyxRQUFRO3NCQUFoQixLQUFLO2dCQUNJLFFBQVE7c0JBQWpCLE1BQU0iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25Jbml0LCBPdXRwdXQsIFRlbXBsYXRlUmVmLCBWaWV3Q2hpbGQsIGZvcndhcmRSZWZ9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtEeW5hbWljRm9ybVdpZHRofSBmcm9tIFwiLi9keW5hbWljLWZvcm0td2lkdGhcIjtcblxuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdpb3QtaG9yaXpvbnRhbC1saW5lJyxcbiAgdGVtcGxhdGU6IGBcblxuICAgIDxuZy10ZW1wbGF0ZT5cbiAgICAgIDxkaXYgKm5nSWY9XCIhaXNCaWdcIiBjbGFzcz1cImNvbnRlbnQgaG9yaXpvbnRhbC1saW5lXCI+XG4gICAgICA8L2Rpdj5cblxuICAgICAgPGRpdiAqbmdJZj1cImlzQmlnXCIgY2xhc3M9XCJob3Jpem9udGFsLWxpbmUgYmlnLWxpbmVcIj5cbiAgICAgIDwvZGl2PlxuICAgIDwvbmctdGVtcGxhdGU+XG5cbiAgYCxcbiAgc3R5bGVzOiBbYFxuICAgIC5ob3Jpem9udGFsLWxpbmUge1xuICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkYWRjZTA7XG4gICAgICBtYXJnaW4tcmlnaHQ6IDE3cHg7XG4gICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgICAgbWluLXdpZHRoOiAzOTVweDtcbiAgICB9XG5cbiAgICAuYmlnLWxpbmUge1xuICAgICAgbWF4LXdpZHRoOiA5MjhweDtcbiAgICB9XG5cbiAgYF0sXG5cbiAgcHJvdmlkZXJzOiBbe3Byb3ZpZGU6IER5bmFtaWNGb3JtV2lkdGgsIHVzZUV4aXN0aW5nOiBmb3J3YXJkUmVmKCgpID0+IElvdElucHV0SG9yaXpvbnRhbExpbmVDb21wb25lbnQpfV1cbn0pXG5leHBvcnQgY2xhc3MgSW90SW5wdXRIb3Jpem9udGFsTGluZUNvbXBvbmVudCBleHRlbmRzIER5bmFtaWNGb3JtV2lkdGggaW1wbGVtZW50cyBPbkluaXQge1xuICBAVmlld0NoaWxkKFRlbXBsYXRlUmVmKSBfdGVtcGxhdGU6IFRlbXBsYXRlUmVmPGFueT47XG5cbiAgcHVibGljIGdldCB0ZW1wbGF0ZSgpIHtcbiAgICByZXR1cm4gdGhpcy5fdGVtcGxhdGU7XG4gIH1cbiAgQElucHV0KCkgdmFsdWUgPSBcIm5vbmVcIjtcbiAgQElucHV0KCkgbGFiZWwgPSBcIm5vbmVcIjtcbiAgQElucHV0KCkgZGlzYWJsZWQgPSBmYWxzZTtcbiAgQE91dHB1dCgpIG9uQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxzdHJpbmc+KCk7XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgLy8gdGhpcy50ZW1wbGF0ZSA9IHRoaXMudDtcbiAgICBzdXBlci5uZ09uSW5pdCgpO1xuICB9XG59XG4iXX0=