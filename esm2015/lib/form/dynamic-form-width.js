import { Directive, Input } from "@angular/core";
import * as i0 from "@angular/core";
import * as i1 from "@angular/cdk/layout";
export class DynamicFormWidth {
    constructor(mediaMatcher) {
        this.mediaMatcher = mediaMatcher;
        this.label = "test";
        this.offset = 0;
        this.isBig = true;
        const mediaQueryList = mediaMatcher.matchMedia('(min-width: 1px)');
    }
    get template() {
        return null;
    }
    get pristine() {
        return true;
    }
    get key() {
        return '';
    }
    get newValue() {
        return '';
    }
    ngOnInit() {
        // this.offset.subscribe(o => {
        if (this.mediaQueryList) {
            if (this.mediaQueryList.removeEventListener) {
                this.mediaQueryList.removeEventListener("change", this.eventListener);
            }
            else {
                this.mediaQueryList.removeListener(this.eventListener);
            }
        }
        this.mediaQueryList = this.mediaMatcher.matchMedia('(min-width: ' + (700 + this.offset) + 'px)');
        this.eventListener = (e) => {
            this.isBig = e.matches;
        };
        this.isBig = this.mediaQueryList.matches;
        if (this.mediaQueryList.addEventListener) {
            this.mediaQueryList.addEventListener("change", this.eventListener);
        }
        else {
            this.mediaQueryList.addListener(this.eventListener);
        }
        // });
    }
}
DynamicFormWidth.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: DynamicFormWidth, deps: [{ token: i1.MediaMatcher }], target: i0.ɵɵFactoryTarget.Directive });
DynamicFormWidth.ɵdir = i0.ɵɵngDeclareDirective({ minVersion: "12.0.0", version: "12.2.6", type: DynamicFormWidth, selector: "[test]", inputs: { label: "label" }, ngImport: i0 });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: DynamicFormWidth, decorators: [{
            type: Directive,
            args: [{ selector: '[test]' }]
        }], ctorParameters: function () { return [{ type: i1.MediaMatcher }]; }, propDecorators: { label: [{
                type: Input
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHluYW1pYy1mb3JtLXdpZHRoLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvaW90LXVpLWxpYi9zcmMvbGliL2Zvcm0vZHluYW1pYy1mb3JtLXdpZHRoLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQVNBLE9BQU8sRUFBQyxTQUFTLEVBQUUsS0FBSyxFQUEyQyxNQUFNLGVBQWUsQ0FBQzs7O0FBSXpGLE1BQU0sT0FBTyxnQkFBZ0I7SUFTM0IsWUFDUyxZQUEwQjtRQUExQixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQVIxQixVQUFLLEdBQUcsTUFBTSxDQUFDO1FBRXhCLFdBQU0sR0FBRyxDQUFDLENBQUM7UUFDWCxVQUFLLEdBQUcsSUFBSSxDQUFDO1FBT1gsTUFBTSxjQUFjLEdBQUcsWUFBWSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO0lBRXJFLENBQUM7SUFFRCxJQUFXLFFBQVE7UUFDakIsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRUQsSUFBVyxRQUFRO1FBQ2pCLE9BQU8sSUFBSSxDQUFBO0lBQ2IsQ0FBQztJQUVELElBQVcsR0FBRztRQUNaLE9BQU8sRUFBRSxDQUFDO0lBQ1osQ0FBQztJQUVELElBQVcsUUFBUTtRQUNqQixPQUFPLEVBQUUsQ0FBQztJQUNaLENBQUM7SUFFRCxRQUFRO1FBQ04sK0JBQStCO1FBQzdCLElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUN2QixJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsbUJBQW1CLEVBQUU7Z0JBQzNDLElBQUksQ0FBQyxjQUFjLENBQUMsbUJBQW1CLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQTthQUN0RTtpQkFBTTtnQkFDTCxJQUFJLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7YUFDeEQ7U0FFRjtRQUNELElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsY0FBYyxHQUFHLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxLQUFLLENBQUMsQ0FBQztRQUNqRyxJQUFJLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQyxFQUFFLEVBQUU7WUFDekIsSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDO1FBQ3pCLENBQUMsQ0FBQTtRQUNELElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUM7UUFFekMsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixFQUFFO1lBQ3hDLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztTQUNwRTthQUFNO1lBQ0wsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1NBQ3JEO1FBQ0gsTUFBTTtJQUNSLENBQUM7OzZHQXREVSxnQkFBZ0I7aUdBQWhCLGdCQUFnQjsyRkFBaEIsZ0JBQWdCO2tCQUQ1QixTQUFTO21CQUFDLEVBQUMsUUFBUSxFQUFFLFFBQVEsRUFBQzttR0FHcEIsS0FBSztzQkFBYixLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiLypcbiAqIENvcHlyaWdodCAoQykgSW8tVGhpbmdzLCBCViAtIEFsbCBSaWdodHMgUmVzZXJ2ZWRcbiAqIFVuYXV0aG9yaXplZCBjb3B5aW5nIG9mIHRoaXMgZmlsZSwgdmlhIGFueSBtZWRpdW0gaXMgc3RyaWN0bHkgcHJvaGliaXRlZFxuICogUHJvcHJpZXRhcnkgYW5kIGNvbmZpZGVudGlhbFxuICogV3JpdHRlbiBieSBTdGVmYWFuIFRlcm5pZXIgPHN0ZWZhYW5AaW8tdGhpbmdzLmV1PiwgMjgvOC8yMDIwXG4gKi9cblxuXG5pbXBvcnQge01lZGlhTWF0Y2hlcn0gZnJvbSAnQGFuZ3VsYXIvY2RrL2xheW91dCc7XG5pbXBvcnQge0RpcmVjdGl2ZSwgSW5wdXQsIFRlbXBsYXRlUmVmLCBWaWV3Q2hpbGQsIFZpZXdDb250YWluZXJSZWZ9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5cblxuQERpcmVjdGl2ZSh7c2VsZWN0b3I6ICdbdGVzdF0nfSlcbmV4cG9ydCBjbGFzcyBEeW5hbWljRm9ybVdpZHRoIHtcblxuICBASW5wdXQoKSBsYWJlbCA9IFwidGVzdFwiO1xuXG4gIG9mZnNldCA9IDA7XG4gIGlzQmlnID0gdHJ1ZTtcbiAgbWVkaWFRdWVyeUxpc3Q6IE1lZGlhUXVlcnlMaXN0Oy8vID0gbWVkaWFNYXRjaGVyLm1hdGNoTWVkaWEoJyhtaW4td2lkdGg6IDFweCknKTtcbiAgcHJpdmF0ZSBldmVudExpc3RlbmVyO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHB1YmxpYyBtZWRpYU1hdGNoZXI6IE1lZGlhTWF0Y2hlcixcbiAgKSB7XG4gICAgY29uc3QgbWVkaWFRdWVyeUxpc3QgPSBtZWRpYU1hdGNoZXIubWF0Y2hNZWRpYSgnKG1pbi13aWR0aDogMXB4KScpO1xuXG4gIH1cblxuICBwdWJsaWMgZ2V0IHRlbXBsYXRlKCkge1xuICAgIHJldHVybiBudWxsO1xuICB9XG5cbiAgcHVibGljIGdldCBwcmlzdGluZSgpOiBhbnkge1xuICAgIHJldHVybiB0cnVlXG4gIH1cblxuICBwdWJsaWMgZ2V0IGtleSgpOiBzdHJpbmcge1xuICAgIHJldHVybiAnJztcbiAgfVxuXG4gIHB1YmxpYyBnZXQgbmV3VmFsdWUoKTogYW55IHtcbiAgICByZXR1cm4gJyc7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICAvLyB0aGlzLm9mZnNldC5zdWJzY3JpYmUobyA9PiB7XG4gICAgICBpZiAodGhpcy5tZWRpYVF1ZXJ5TGlzdCkge1xuICAgICAgICBpZiAodGhpcy5tZWRpYVF1ZXJ5TGlzdC5yZW1vdmVFdmVudExpc3RlbmVyKSB7XG4gICAgICAgICAgdGhpcy5tZWRpYVF1ZXJ5TGlzdC5yZW1vdmVFdmVudExpc3RlbmVyKFwiY2hhbmdlXCIsIHRoaXMuZXZlbnRMaXN0ZW5lcilcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB0aGlzLm1lZGlhUXVlcnlMaXN0LnJlbW92ZUxpc3RlbmVyKHRoaXMuZXZlbnRMaXN0ZW5lcik7XG4gICAgICAgIH1cblxuICAgICAgfVxuICAgICAgdGhpcy5tZWRpYVF1ZXJ5TGlzdCA9IHRoaXMubWVkaWFNYXRjaGVyLm1hdGNoTWVkaWEoJyhtaW4td2lkdGg6ICcgKyAoNzAwICsgdGhpcy5vZmZzZXQpICsgJ3B4KScpO1xuICAgICAgdGhpcy5ldmVudExpc3RlbmVyID0gKGUpID0+IHtcbiAgICAgICAgdGhpcy5pc0JpZyA9IGUubWF0Y2hlcztcbiAgICAgIH1cbiAgICAgIHRoaXMuaXNCaWcgPSB0aGlzLm1lZGlhUXVlcnlMaXN0Lm1hdGNoZXM7XG5cbiAgICAgIGlmICh0aGlzLm1lZGlhUXVlcnlMaXN0LmFkZEV2ZW50TGlzdGVuZXIpIHtcbiAgICAgICAgdGhpcy5tZWRpYVF1ZXJ5TGlzdC5hZGRFdmVudExpc3RlbmVyKFwiY2hhbmdlXCIsIHRoaXMuZXZlbnRMaXN0ZW5lcik7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLm1lZGlhUXVlcnlMaXN0LmFkZExpc3RlbmVyKHRoaXMuZXZlbnRMaXN0ZW5lcik7XG4gICAgICB9XG4gICAgLy8gfSk7XG4gIH1cbn1cbiJdfQ==