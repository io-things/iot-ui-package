import { Component, EventEmitter, forwardRef, Input, Output, TemplateRef, ViewChild } from '@angular/core';
import { DynamicFormWidth } from './dynamic-form-width';
import { FormControl, FormGroup } from "@angular/forms";
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/select";
import * as i2 from "@angular/material/core";
import * as i3 from "@angular/common";
import * as i4 from "@angular/forms";
import * as i5 from "@ngx-translate/core";
export class IotInputSelectValueComponent extends DynamicFormWidth {
    constructor() {
        super(...arguments);
        this.form = new FormGroup({
            selectId: new FormControl(''),
        });
        this.attribute = 'todo';
        this.description = 'explain this field';
        this.disabled = false;
        this.onChange = new EventEmitter();
    }
    get template() {
        return this._template;
    }
    get pristine() {
        return this.form.value.selectId === this.value;
    }
    get key() {
        return this.attribute;
    }
    get newValue() {
        return this.form.value.selectId;
    }
    change(event) {
        this.onChange.emit(event);
    }
    ngOnInit() {
        super.ngOnInit();
        if (this.disabled) {
            this.form = new FormGroup({
                selectId: new FormControl({ value: '', disabled: this.disabled }),
            });
        }
        if (this.value) {
            this.form.setValue({ selectId: this.value });
        }
    }
}
IotInputSelectValueComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputSelectValueComponent, deps: null, target: i0.ɵɵFactoryTarget.Component });
IotInputSelectValueComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotInputSelectValueComponent, selector: "iot-input-select-values", inputs: { label: "label", attribute: "attribute", description: "description", value: "value", separator: "separator", values: "values", disabled: "disabled" }, outputs: { onChange: "onChange" }, providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputSelectValueComponent) }], viewQueries: [{ propertyName: "_template", first: true, predicate: TemplateRef, descendants: true }], usesInheritance: true, ngImport: i0, template: `
    <ng-template>
      <div class="outer-margin">
        <div
          *ngIf="label && label !== ''"
          class="outer" [ngClass]="{'outersmall':!isBig}">
          <div class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
          >
            <div class="label">{{label | translate}}</div>

          </div>

          <form [formGroup]="form" class="content">
            <div class="checkbox-content">
              <div class="checkbox-content-description">{{description | translate}}</div>
              <mat-select
                formControlName="selectId"
                (ngModelChange)="change($event)">

                <mat-option *ngFor="let v of values" [value]="v.id">{{v.name}} </mat-option>
              </mat-select>
            </div>
            <div *ngIf="separator" class="separator"></div>
          </form>


        </div>

      </div>
    </ng-template>
  `, isInline: true, styles: ["\n      .separator {\n        border-bottom: 1px solid #dadce0;\n        padding-top: 16px;\n      }\n\n      .checkbox-content-description {\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 12px;\n        font-weight: 400;\n        letter-spacing: .025em;\n        line-height: 16px;\n        color: #5f6368;\n        margin-bottom: 13px;\n        margin-top: 2px;\n      }\n\n      .outer-margin {\n        margin-left: 0px;\n        padding-right: 50px;\n      }\n\n      .label-outer.label-outer-thin {\n        min-height: 40px;\n      }\n\n      .label-outer {\n        flex-basis: 33%;\n        min-height: 64px;\n        min-width: 220px;\n        display: block;\n        padding-top: 13px;\n        color: #3c4043;\n      }\n\n      .label {\n\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 14px;\n        font-weight: 400;\n        height: 20px;\n        color: rgb(60, 64, 67);\n      }\n\n      .outer {\n        display: flex;\n        max-width: 900px;\n        margin-bottom: 16px;\n      }\n\n      .outersmall {\n        flex-direction: column;\n        padding-bottom: 8px;\n      }\n\n      .content {\n        flex-direction: column;\n        flex-basis: 67%;\n        min-width: 280px;\n      }\n\n      .checkbox-content {\n        display: flex;\n        flex-direction: column;\n        color: #3c4043;\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 14px;\n        font-weight: 400;\n        letter-spacing: 0.2px;\n        line-height: 20px;\n      }\n    "], components: [{ type: i1.MatSelect, selector: "mat-select", inputs: ["disabled", "disableRipple", "tabIndex"], exportAs: ["matSelect"] }, { type: i2.MatOption, selector: "mat-option", exportAs: ["matOption"] }], directives: [{ type: i3.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i3.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i4.ɵNgNoValidate, selector: "form:not([ngNoForm]):not([ngNativeValidate])" }, { type: i4.NgControlStatusGroup, selector: "[formGroupName],[formArrayName],[ngModelGroup],[formGroup],form:not([ngNoForm]),[ngForm]" }, { type: i4.FormGroupDirective, selector: "[formGroup]", inputs: ["formGroup"], outputs: ["ngSubmit"], exportAs: ["ngForm"] }, { type: i4.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { type: i4.FormControlName, selector: "[formControlName]", inputs: ["disabled", "formControlName", "ngModel"], outputs: ["ngModelChange"] }, { type: i3.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }], pipes: { "translate": i5.TranslatePipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputSelectValueComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-input-select-values',
                    template: `
    <ng-template>
      <div class="outer-margin">
        <div
          *ngIf="label && label !== ''"
          class="outer" [ngClass]="{'outersmall':!isBig}">
          <div class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
          >
            <div class="label">{{label | translate}}</div>

          </div>

          <form [formGroup]="form" class="content">
            <div class="checkbox-content">
              <div class="checkbox-content-description">{{description | translate}}</div>
              <mat-select
                formControlName="selectId"
                (ngModelChange)="change($event)">

                <mat-option *ngFor="let v of values" [value]="v.id">{{v.name}} </mat-option>
              </mat-select>
            </div>
            <div *ngIf="separator" class="separator"></div>
          </form>


        </div>

      </div>
    </ng-template>
  `,
                    styles: [
                        `
      .separator {
        border-bottom: 1px solid #dadce0;
        padding-top: 16px;
      }

      .checkbox-content-description {
        font-family: Roboto, Arial, sans-serif;
        font-size: 12px;
        font-weight: 400;
        letter-spacing: .025em;
        line-height: 16px;
        color: #5f6368;
        margin-bottom: 13px;
        margin-top: 2px;
      }

      .outer-margin {
        margin-left: 0px;
        padding-right: 50px;
      }

      .label-outer.label-outer-thin {
        min-height: 40px;
      }

      .label-outer {
        flex-basis: 33%;
        min-height: 64px;
        min-width: 220px;
        display: block;
        padding-top: 13px;
        color: #3c4043;
      }

      .label {

        font-family: Roboto, Arial, sans-serif;
        font-size: 14px;
        font-weight: 400;
        height: 20px;
        color: rgb(60, 64, 67);
      }

      .outer {
        display: flex;
        max-width: 900px;
        margin-bottom: 16px;
      }

      .outersmall {
        flex-direction: column;
        padding-bottom: 8px;
      }

      .content {
        flex-direction: column;
        flex-basis: 67%;
        min-width: 280px;
      }

      .checkbox-content {
        display: flex;
        flex-direction: column;
        color: #3c4043;
        font-family: Roboto, Arial, sans-serif;
        font-size: 14px;
        font-weight: 400;
        letter-spacing: 0.2px;
        line-height: 20px;
      }
    `
                    ],
                    providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputSelectValueComponent) }]
                }]
        }], propDecorators: { _template: [{
                type: ViewChild,
                args: [TemplateRef]
            }], label: [{
                type: Input
            }], attribute: [{
                type: Input
            }], description: [{
                type: Input
            }], value: [{
                type: Input
            }], separator: [{
                type: Input
            }], values: [{
                type: Input
            }], disabled: [{
                type: Input
            }], onChange: [{
                type: Output
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW90LWlucHV0LXNlbGVjdC12YWx1ZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9pb3QtdWktbGliL3NyYy9saWIvZm9ybS9pb3QtaW5wdXQtc2VsZWN0LXZhbHVlLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLFlBQVksRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFVLE1BQU0sRUFBRSxXQUFXLEVBQUUsU0FBUyxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ2pILE9BQU8sRUFBQyxnQkFBZ0IsRUFBQyxNQUFNLHNCQUFzQixDQUFDO0FBQ3RELE9BQU8sRUFBQyxXQUFXLEVBQUUsU0FBUyxFQUFDLE1BQU0sZ0JBQWdCLENBQUM7Ozs7Ozs7QUFnSHRELE1BQU0sT0FBTyw0QkFBNkIsU0FBUSxnQkFBZ0I7SUE5R2xFOztRQW9JRSxTQUFJLEdBQUcsSUFBSSxTQUFTLENBQUM7WUFDbkIsUUFBUSxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsQ0FBQztTQUM5QixDQUFDLENBQUM7UUFJTSxjQUFTLEdBQUcsTUFBTSxDQUFDO1FBQ25CLGdCQUFXLEdBQUcsb0JBQW9CLENBQUM7UUFJbkMsYUFBUSxHQUFHLEtBQUssQ0FBQztRQUdoQixhQUFRLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztLQWtCOUM7SUFsREMsSUFBVyxRQUFRO1FBRWpCLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUN4QixDQUFDO0lBRUQsSUFBVyxRQUFRO1FBRWpCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDakQsQ0FBQztJQUVELElBQVcsR0FBRztRQUNaLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUN4QixDQUFDO0lBRUQsSUFBVyxRQUFRO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO0lBQ2xDLENBQUM7SUFrQkQsTUFBTSxDQUFDLEtBQVU7UUFDZixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUU1QixDQUFDO0lBRUQsUUFBUTtRQUNOLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNqQixJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDakIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLFNBQVMsQ0FBQztnQkFDeEIsUUFBUSxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUMsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBQyxDQUFDO2FBQ2hFLENBQUMsQ0FBQztTQUNKO1FBQ0QsSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ2QsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBQyxDQUFDLENBQUM7U0FDNUM7SUFDSCxDQUFDOzt5SEFyRFUsNEJBQTRCOzZHQUE1Qiw0QkFBNEIscVBBRjVCLENBQUMsRUFBQyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsV0FBVyxFQUFFLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyw0QkFBNEIsQ0FBQyxFQUFDLENBQUMscUVBSTFGLFdBQVcsdUVBOUdaOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7R0E4QlQ7MkZBOEVVLDRCQUE0QjtrQkE5R3hDLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLHlCQUF5QjtvQkFDbkMsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7R0E4QlQ7b0JBQ0QsTUFBTSxFQUFFO3dCQUNOOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztLQXVFQztxQkFDRjtvQkFFRCxTQUFTLEVBQUUsQ0FBQyxFQUFDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxXQUFXLEVBQUUsVUFBVSxDQUFDLEdBQUcsRUFBRSw2QkFBNkIsQ0FBQyxFQUFDLENBQUM7aUJBQ3RHOzhCQUd5QixTQUFTO3NCQUFoQyxTQUFTO3VCQUFDLFdBQVc7Z0JBeUJiLEtBQUs7c0JBQWIsS0FBSztnQkFDRyxTQUFTO3NCQUFqQixLQUFLO2dCQUNHLFdBQVc7c0JBQW5CLEtBQUs7Z0JBQ0csS0FBSztzQkFBYixLQUFLO2dCQUNHLFNBQVM7c0JBQWpCLEtBQUs7Z0JBQ0csTUFBTTtzQkFBZCxLQUFLO2dCQUNHLFFBQVE7c0JBQWhCLEtBQUs7Z0JBR0ksUUFBUTtzQkFBakIsTUFBTSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIGZvcndhcmRSZWYsIElucHV0LCBPbkluaXQsIE91dHB1dCwgVGVtcGxhdGVSZWYsIFZpZXdDaGlsZH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0R5bmFtaWNGb3JtV2lkdGh9IGZyb20gJy4vZHluYW1pYy1mb3JtLXdpZHRoJztcbmltcG9ydCB7Rm9ybUNvbnRyb2wsIEZvcm1Hcm91cH0gZnJvbSBcIkBhbmd1bGFyL2Zvcm1zXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2lvdC1pbnB1dC1zZWxlY3QtdmFsdWVzJyxcbiAgdGVtcGxhdGU6IGBcbiAgICA8bmctdGVtcGxhdGU+XG4gICAgICA8ZGl2IGNsYXNzPVwib3V0ZXItbWFyZ2luXCI+XG4gICAgICAgIDxkaXZcbiAgICAgICAgICAqbmdJZj1cImxhYmVsICYmIGxhYmVsICE9PSAnJ1wiXG4gICAgICAgICAgY2xhc3M9XCJvdXRlclwiIFtuZ0NsYXNzXT1cInsnb3V0ZXJzbWFsbCc6IWlzQmlnfVwiPlxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJsYWJlbC1vdXRlclwiIFtuZ0NsYXNzXT1cInsnbGFiZWwtb3V0ZXItdGhpbic6IWlzQmlnfVwiXG4gICAgICAgICAgPlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImxhYmVsXCI+e3tsYWJlbCB8IHRyYW5zbGF0ZX19PC9kaXY+XG5cbiAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgIDxmb3JtIFtmb3JtR3JvdXBdPVwiZm9ybVwiIGNsYXNzPVwiY29udGVudFwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNoZWNrYm94LWNvbnRlbnRcIj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNoZWNrYm94LWNvbnRlbnQtZGVzY3JpcHRpb25cIj57e2Rlc2NyaXB0aW9uIHwgdHJhbnNsYXRlfX08L2Rpdj5cbiAgICAgICAgICAgICAgPG1hdC1zZWxlY3RcbiAgICAgICAgICAgICAgICBmb3JtQ29udHJvbE5hbWU9XCJzZWxlY3RJZFwiXG4gICAgICAgICAgICAgICAgKG5nTW9kZWxDaGFuZ2UpPVwiY2hhbmdlKCRldmVudClcIj5cblxuICAgICAgICAgICAgICAgIDxtYXQtb3B0aW9uICpuZ0Zvcj1cImxldCB2IG9mIHZhbHVlc1wiIFt2YWx1ZV09XCJ2LmlkXCI+e3t2Lm5hbWV9fSA8L21hdC1vcHRpb24+XG4gICAgICAgICAgICAgIDwvbWF0LXNlbGVjdD5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdiAqbmdJZj1cInNlcGFyYXRvclwiIGNsYXNzPVwic2VwYXJhdG9yXCI+PC9kaXY+XG4gICAgICAgICAgPC9mb3JtPlxuXG5cbiAgICAgICAgPC9kaXY+XG5cbiAgICAgIDwvZGl2PlxuICAgIDwvbmctdGVtcGxhdGU+XG4gIGAsXG4gIHN0eWxlczogW1xuICAgIGBcbiAgICAgIC5zZXBhcmF0b3Ige1xuICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2RhZGNlMDtcbiAgICAgICAgcGFkZGluZy10b3A6IDE2cHg7XG4gICAgICB9XG5cbiAgICAgIC5jaGVja2JveC1jb250ZW50LWRlc2NyaXB0aW9uIHtcbiAgICAgICAgZm9udC1mYW1pbHk6IFJvYm90bywgQXJpYWwsIHNhbnMtc2VyaWY7XG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICAgICAgbGV0dGVyLXNwYWNpbmc6IC4wMjVlbTtcbiAgICAgICAgbGluZS1oZWlnaHQ6IDE2cHg7XG4gICAgICAgIGNvbG9yOiAjNWY2MzY4O1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAxM3B4O1xuICAgICAgICBtYXJnaW4tdG9wOiAycHg7XG4gICAgICB9XG5cbiAgICAgIC5vdXRlci1tYXJnaW4ge1xuICAgICAgICBtYXJnaW4tbGVmdDogMHB4O1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiA1MHB4O1xuICAgICAgfVxuXG4gICAgICAubGFiZWwtb3V0ZXIubGFiZWwtb3V0ZXItdGhpbiB7XG4gICAgICAgIG1pbi1oZWlnaHQ6IDQwcHg7XG4gICAgICB9XG5cbiAgICAgIC5sYWJlbC1vdXRlciB7XG4gICAgICAgIGZsZXgtYmFzaXM6IDMzJTtcbiAgICAgICAgbWluLWhlaWdodDogNjRweDtcbiAgICAgICAgbWluLXdpZHRoOiAyMjBweDtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIHBhZGRpbmctdG9wOiAxM3B4O1xuICAgICAgICBjb2xvcjogIzNjNDA0MztcbiAgICAgIH1cblxuICAgICAgLmxhYmVsIHtcblxuICAgICAgICBmb250LWZhbWlseTogUm9ib3RvLCBBcmlhbCwgc2Fucy1zZXJpZjtcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICBmb250LXdlaWdodDogNDAwO1xuICAgICAgICBoZWlnaHQ6IDIwcHg7XG4gICAgICAgIGNvbG9yOiByZ2IoNjAsIDY0LCA2Nyk7XG4gICAgICB9XG5cbiAgICAgIC5vdXRlciB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIG1heC13aWR0aDogOTAwcHg7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDE2cHg7XG4gICAgICB9XG5cbiAgICAgIC5vdXRlcnNtYWxsIHtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAgcGFkZGluZy1ib3R0b206IDhweDtcbiAgICAgIH1cblxuICAgICAgLmNvbnRlbnQge1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICBmbGV4LWJhc2lzOiA2NyU7XG4gICAgICAgIG1pbi13aWR0aDogMjgwcHg7XG4gICAgICB9XG5cbiAgICAgIC5jaGVja2JveC1jb250ZW50IHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAgY29sb3I6ICMzYzQwNDM7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBSb2JvdG8sIEFyaWFsLCBzYW5zLXNlcmlmO1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICAgIGxldHRlci1zcGFjaW5nOiAwLjJweDtcbiAgICAgICAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gICAgICB9XG4gICAgYFxuICBdXG4gICxcbiAgcHJvdmlkZXJzOiBbe3Byb3ZpZGU6IER5bmFtaWNGb3JtV2lkdGgsIHVzZUV4aXN0aW5nOiBmb3J3YXJkUmVmKCgpID0+IElvdElucHV0U2VsZWN0VmFsdWVDb21wb25lbnQpfV1cbn0pXG5leHBvcnQgY2xhc3MgSW90SW5wdXRTZWxlY3RWYWx1ZUNvbXBvbmVudCBleHRlbmRzIER5bmFtaWNGb3JtV2lkdGggaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIEBWaWV3Q2hpbGQoVGVtcGxhdGVSZWYpIF90ZW1wbGF0ZTogVGVtcGxhdGVSZWY8YW55PjtcblxuICBwdWJsaWMgZ2V0IHRlbXBsYXRlKCkge1xuXG4gICAgcmV0dXJuIHRoaXMuX3RlbXBsYXRlO1xuICB9XG5cbiAgcHVibGljIGdldCBwcmlzdGluZSgpOiBhbnkge1xuXG4gICAgcmV0dXJuIHRoaXMuZm9ybS52YWx1ZS5zZWxlY3RJZCA9PT0gdGhpcy52YWx1ZTtcbiAgfVxuXG4gIHB1YmxpYyBnZXQga2V5KCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIHRoaXMuYXR0cmlidXRlO1xuICB9XG5cbiAgcHVibGljIGdldCBuZXdWYWx1ZSgpOiBhbnkge1xuICAgIHJldHVybiB0aGlzLmZvcm0udmFsdWUuc2VsZWN0SWQ7XG4gIH1cblxuICBmb3JtID0gbmV3IEZvcm1Hcm91cCh7XG4gICAgc2VsZWN0SWQ6IG5ldyBGb3JtQ29udHJvbCgnJyksXG4gIH0pO1xuXG5cbiAgQElucHV0KCkgbGFiZWw6IHN0cmluZztcbiAgQElucHV0KCkgYXR0cmlidXRlID0gJ3RvZG8nO1xuICBASW5wdXQoKSBkZXNjcmlwdGlvbiA9ICdleHBsYWluIHRoaXMgZmllbGQnO1xuICBASW5wdXQoKSB2YWx1ZTogYm9vbGVhbjtcbiAgQElucHV0KCkgc2VwYXJhdG9yOiBib29sZWFuO1xuICBASW5wdXQoKSB2YWx1ZXM6IHsgaWQ6IHN0cmluZzsgbmFtZTogc3RyaW5nIH1bXTtcbiAgQElucHV0KCkgZGlzYWJsZWQgPSBmYWxzZTtcblxuXG4gIEBPdXRwdXQoKSBvbkNoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xuXG4gIGNoYW5nZShldmVudDogYW55KSB7XG4gICAgdGhpcy5vbkNoYW5nZS5lbWl0KGV2ZW50KTtcblxuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgc3VwZXIubmdPbkluaXQoKTtcbiAgICBpZiAodGhpcy5kaXNhYmxlZCkge1xuICAgICAgdGhpcy5mb3JtID0gbmV3IEZvcm1Hcm91cCh7XG4gICAgICAgIHNlbGVjdElkOiBuZXcgRm9ybUNvbnRyb2woe3ZhbHVlOiAnJywgZGlzYWJsZWQ6IHRoaXMuZGlzYWJsZWR9KSxcbiAgICAgIH0pO1xuICAgIH1cbiAgICBpZiAodGhpcy52YWx1ZSkge1xuICAgICAgdGhpcy5mb3JtLnNldFZhbHVlKHtzZWxlY3RJZDogdGhpcy52YWx1ZX0pO1xuICAgIH1cbiAgfVxufVxuIl19