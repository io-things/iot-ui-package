import { Component, forwardRef, Input, TemplateRef, ViewChild } from '@angular/core';
import { DynamicFormWidth } from './dynamic-form-width';
import { FormControl, FormGroup } from "@angular/forms";
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/form-field";
import * as i2 from "@angular/material/button";
import * as i3 from "@angular/material/icon";
import * as i4 from "@angular/material/select";
import * as i5 from "@angular/material/core";
import * as i6 from "@angular/common";
import * as i7 from "@angular/material/input";
import * as i8 from "@angular/forms";
import * as i9 from "@ngx-translate/core";
export class IotDefineIdentifierComponent extends DynamicFormWidth {
    constructor() {
        super(...arguments);
        this.prefix = [
            {
                'nid': 'smaxx',
                'name': 'S)maxx'
            },
            {
                'nid': 'iot',
                'name': 'Io-Things'
            },
        ];
        this.form = new FormGroup({
            value: new FormControl(''),
        });
        this.attribute = 'todo';
        this.description = 'explain this field';
        this.disabled = false;
        this.new = '';
        this.thirpartySelection = 'smaxx';
    }
    get template() {
        return this._template;
    }
    get pristine() {
        return JSON.stringify(this.value) === JSON.stringify(this._value);
    }
    get key() {
        return this.attribute;
    }
    get newValue() {
        return this._value;
    }
    change(event) {
    }
    ngOnInit() {
        super.ngOnInit();
        this._value = this.value;
        if (this.disabled) {
            // this.form = new FormGroup({
            //   selectId: new FormControl({value: '', disabled: this.disabled}),
            // });
        }
        this.form.setValue({ value: this.value });
    }
    addIdentifier() {
        this._value = [...this._value, `urn:${this.thirpartySelection}:${this.new}`];
        this.form.setValue({ value: [...this.value, `urn:${this.thirpartySelection}:${this.new}`] });
    }
    remove(id) {
        this._value = this._value.filter(urn => urn !== id);
        this.form.setValue({ value: this._value });
    }
}
IotDefineIdentifierComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotDefineIdentifierComponent, deps: null, target: i0.ɵɵFactoryTarget.Component });
IotDefineIdentifierComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotDefineIdentifierComponent, selector: "iot-define-identifier", inputs: { prefix: "prefix", label: "label", attribute: "attribute", description: "description", value: "value", separator: "separator", disabled: "disabled" }, providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotDefineIdentifierComponent) }], viewQueries: [{ propertyName: "_template", first: true, predicate: TemplateRef, descendants: true }], usesInheritance: true, ngImport: i0, template: `
    <ng-template>
      <div class="outer-margin">
        <div class="outer" [ngClass]="{'outersmall':!isBig}">
          <div class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
          >
            <div class="label">{{label | translate}}</div>
          </div>

          <div class="content">
            <div class="checkbox-content">
              <div class="checkbox-content-description">{{description | translate}}</div>

              <div *ngFor="let id of _value">
                <div>
                  <mat-form-field appearance="outline" class="w-100">
                    <input class="input-class" matInput type="text"
                           [disabled]="true"
                           [ngModel]="id">
                    <span class="border-class"></span>
                    <button
                      (click)="remove(id)"
                      matSuffix mat-icon-button
                      color="primary" aria-label="Example icon button with a menu icon">
                      <mat-icon>delete</mat-icon>
                    </button>

                  </mat-form-field>
                </div>
              </div>

              <div class="d-flex flex-row w-100">

                <div class="select-button mr-2">
                  <mat-form-field appearance="outline">
                    <mat-label>Third party</mat-label>

                    <mat-select
                      [(ngModel)]="thirpartySelection"

                    >
                      <mat-option

                        *ngFor="let p of prefix" [value]="p.nid">{{p.name}} </mat-option>
                    </mat-select>
                  </mat-form-field>
                </div>
                <div class="flex-grow-1">
                  <mat-form-field appearance="outline" class="w-100">
                    <input class="input-class" matInput type="text"
                           (keyup.enter)="addIdentifier()"
                           [disabled]="false"
                           [(ngModel)]="new">
                    <button
                      (click)="addIdentifier()"
                      matSuffix mat-icon-button
                      color="primary" aria-label="Example icon button with a menu icon">
                      <mat-icon>addv</mat-icon>
                    </button>
                  </mat-form-field>
                </div>
              </div>

            </div>
            <div *ngIf="separator" class="separator"></div>
          </div>


        </div>
      </div>
    </ng-template>
  `, isInline: true, styles: ["\n      .separator {\n        border-bottom: 1px solid #dadce0;\n        padding-top: 16px;\n      }\n\n      .checkbox-content-description {\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 12px;\n        font-weight: 400;\n        letter-spacing: .025em;\n        line-height: 16px;\n        color: #5f6368;\n        margin-bottom: 13px;\n        margin-top: 2px;\n      }\n\n      .outer-margin {\n        margin-left: 0px;\n        padding-right: 50px;\n      }\n\n      .label-outer.label-outer-thin {\n        min-height: 40px;\n      }\n\n      .label-outer {\n        flex-basis: 33%;\n        min-height: 64px;\n        min-width: 220px;\n        display: block;\n        padding-top: 13px;\n        color: #3c4043;\n      }\n\n      .label {\n\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 14px;\n        font-weight: 400;\n        height: 20px;\n        color: rgb(60, 64, 67);\n      }\n\n      .outer {\n        display: flex;\n        max-width: 900px;\n        margin-bottom: 16px;\n      }\n\n      .outersmall {\n        flex-direction: column;\n        padding-bottom: 8px;\n      }\n\n      .content {\n        flex-direction: column;\n        flex-basis: 67%;\n        min-width: 280px;\n      }\n\n      .checkbox-content {\n        display: flex;\n        flex-direction: column;\n        color: #3c4043;\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 14px;\n        font-weight: 400;\n        letter-spacing: 0.2px;\n        line-height: 20px;\n      }\n    "], components: [{ type: i1.MatFormField, selector: "mat-form-field", inputs: ["color", "floatLabel", "appearance", "hideRequiredMarker", "hintLabel"], exportAs: ["matFormField"] }, { type: i2.MatButton, selector: "button[mat-button], button[mat-raised-button], button[mat-icon-button],             button[mat-fab], button[mat-mini-fab], button[mat-stroked-button],             button[mat-flat-button]", inputs: ["disabled", "disableRipple", "color"], exportAs: ["matButton"] }, { type: i3.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }, { type: i4.MatSelect, selector: "mat-select", inputs: ["disabled", "disableRipple", "tabIndex"], exportAs: ["matSelect"] }, { type: i5.MatOption, selector: "mat-option", exportAs: ["matOption"] }], directives: [{ type: i6.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i6.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { type: i7.MatInput, selector: "input[matInput], textarea[matInput], select[matNativeControl],      input[matNativeControl], textarea[matNativeControl]", inputs: ["id", "disabled", "required", "type", "value", "readonly", "placeholder", "errorStateMatcher", "aria-describedby"], exportAs: ["matInput"] }, { type: i8.DefaultValueAccessor, selector: "input:not([type=checkbox])[formControlName],textarea[formControlName],input:not([type=checkbox])[formControl],textarea[formControl],input:not([type=checkbox])[ngModel],textarea[ngModel],[ngDefaultControl]" }, { type: i8.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { type: i8.NgModel, selector: "[ngModel]:not([formControlName]):not([formControl])", inputs: ["name", "disabled", "ngModel", "ngModelOptions"], outputs: ["ngModelChange"], exportAs: ["ngModel"] }, { type: i1.MatSuffix, selector: "[matSuffix]" }, { type: i1.MatLabel, selector: "mat-label" }, { type: i6.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }], pipes: { "translate": i9.TranslatePipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotDefineIdentifierComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-define-identifier',
                    template: `
    <ng-template>
      <div class="outer-margin">
        <div class="outer" [ngClass]="{'outersmall':!isBig}">
          <div class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
          >
            <div class="label">{{label | translate}}</div>
          </div>

          <div class="content">
            <div class="checkbox-content">
              <div class="checkbox-content-description">{{description | translate}}</div>

              <div *ngFor="let id of _value">
                <div>
                  <mat-form-field appearance="outline" class="w-100">
                    <input class="input-class" matInput type="text"
                           [disabled]="true"
                           [ngModel]="id">
                    <span class="border-class"></span>
                    <button
                      (click)="remove(id)"
                      matSuffix mat-icon-button
                      color="primary" aria-label="Example icon button with a menu icon">
                      <mat-icon>delete</mat-icon>
                    </button>

                  </mat-form-field>
                </div>
              </div>

              <div class="d-flex flex-row w-100">

                <div class="select-button mr-2">
                  <mat-form-field appearance="outline">
                    <mat-label>Third party</mat-label>

                    <mat-select
                      [(ngModel)]="thirpartySelection"

                    >
                      <mat-option

                        *ngFor="let p of prefix" [value]="p.nid">{{p.name}} </mat-option>
                    </mat-select>
                  </mat-form-field>
                </div>
                <div class="flex-grow-1">
                  <mat-form-field appearance="outline" class="w-100">
                    <input class="input-class" matInput type="text"
                           (keyup.enter)="addIdentifier()"
                           [disabled]="false"
                           [(ngModel)]="new">
                    <button
                      (click)="addIdentifier()"
                      matSuffix mat-icon-button
                      color="primary" aria-label="Example icon button with a menu icon">
                      <mat-icon>addv</mat-icon>
                    </button>
                  </mat-form-field>
                </div>
              </div>

            </div>
            <div *ngIf="separator" class="separator"></div>
          </div>


        </div>
      </div>
    </ng-template>
  `,
                    styles: [
                        `
      .separator {
        border-bottom: 1px solid #dadce0;
        padding-top: 16px;
      }

      .checkbox-content-description {
        font-family: Roboto, Arial, sans-serif;
        font-size: 12px;
        font-weight: 400;
        letter-spacing: .025em;
        line-height: 16px;
        color: #5f6368;
        margin-bottom: 13px;
        margin-top: 2px;
      }

      .outer-margin {
        margin-left: 0px;
        padding-right: 50px;
      }

      .label-outer.label-outer-thin {
        min-height: 40px;
      }

      .label-outer {
        flex-basis: 33%;
        min-height: 64px;
        min-width: 220px;
        display: block;
        padding-top: 13px;
        color: #3c4043;
      }

      .label {

        font-family: Roboto, Arial, sans-serif;
        font-size: 14px;
        font-weight: 400;
        height: 20px;
        color: rgb(60, 64, 67);
      }

      .outer {
        display: flex;
        max-width: 900px;
        margin-bottom: 16px;
      }

      .outersmall {
        flex-direction: column;
        padding-bottom: 8px;
      }

      .content {
        flex-direction: column;
        flex-basis: 67%;
        min-width: 280px;
      }

      .checkbox-content {
        display: flex;
        flex-direction: column;
        color: #3c4043;
        font-family: Roboto, Arial, sans-serif;
        font-size: 14px;
        font-weight: 400;
        letter-spacing: 0.2px;
        line-height: 20px;
      }
    `
                    ],
                    providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotDefineIdentifierComponent) }]
                }]
        }], propDecorators: { _template: [{
                type: ViewChild,
                args: [TemplateRef]
            }], prefix: [{
                type: Input
            }], label: [{
                type: Input
            }], attribute: [{
                type: Input
            }], description: [{
                type: Input
            }], value: [{
                type: Input
            }], separator: [{
                type: Input
            }], disabled: [{
                type: Input
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW90LWRlZmluZS1pZGVudGlmaWVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2lvdC11aS1saWIvc3JjL2xpYi9mb3JtL2lvdC1kZWZpbmUtaWRlbnRpZmllci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFDLFNBQVMsRUFBZ0IsVUFBVSxFQUFFLEtBQUssRUFBa0IsV0FBVyxFQUFFLFNBQVMsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUNqSCxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQztBQUN0RCxPQUFPLEVBQUMsV0FBVyxFQUFFLFNBQVMsRUFBQyxNQUFNLGdCQUFnQixDQUFDOzs7Ozs7Ozs7OztBQTBKdEQsTUFBTSxPQUFPLDRCQUE2QixTQUFRLGdCQUFnQjtJQXZKbEU7O1FBMEpZLFdBQU0sR0FBRztZQUNqQjtnQkFDRSxLQUFLLEVBQUUsT0FBTztnQkFDZCxNQUFNLEVBQUUsUUFBUTthQUNqQjtZQUNEO2dCQUNFLEtBQUssRUFBRSxLQUFLO2dCQUNaLE1BQU0sRUFBRSxXQUFXO2FBQ3BCO1NBQ0YsQ0FBQztRQWtCRixTQUFJLEdBQUcsSUFBSSxTQUFTLENBQUM7WUFDbkIsS0FBSyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsQ0FBQztTQUMzQixDQUFDLENBQUM7UUFJTSxjQUFTLEdBQUcsTUFBTSxDQUFDO1FBQ25CLGdCQUFXLEdBQUcsb0JBQW9CLENBQUM7UUFJbkMsYUFBUSxHQUFHLEtBQUssQ0FBQztRQUkxQixRQUFHLEdBQVcsRUFBRSxDQUFDO1FBQ2pCLHVCQUFrQixHQUFHLE9BQU8sQ0FBQztLQTRCOUI7SUE3REMsSUFBVyxRQUFRO1FBRWpCLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUN4QixDQUFDO0lBRUQsSUFBVyxRQUFRO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDbEUsQ0FBQztJQUVELElBQVcsR0FBRztRQUNaLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUN4QixDQUFDO0lBRUQsSUFBVyxRQUFRO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUNyQixDQUFDO0lBb0JELE1BQU0sQ0FBQyxLQUFVO0lBQ2pCLENBQUM7SUFFRCxRQUFRO1FBQ04sS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ2pCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUN6QixJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDakIsOEJBQThCO1lBQzlCLHFFQUFxRTtZQUNyRSxNQUFNO1NBQ1A7UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFDLENBQUMsQ0FBQztJQUMxQyxDQUFDO0lBRUQsYUFBYTtRQUNYLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsT0FBTyxJQUFJLENBQUMsa0JBQWtCLElBQUksSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUM7UUFFN0UsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBQyxLQUFLLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLEVBQUUsT0FBTyxJQUFJLENBQUMsa0JBQWtCLElBQUksSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUMsQ0FBQyxDQUFDO0lBQzdGLENBQUM7SUFHRCxNQUFNLENBQUMsRUFBVTtRQUNmLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssRUFBRSxDQUFDLENBQUM7UUFDcEQsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBQyxDQUFDLENBQUM7SUFFM0MsQ0FBQzs7eUhBekVVLDRCQUE0Qjs2R0FBNUIsNEJBQTRCLGdOQUY1QixDQUFDLEVBQUMsT0FBTyxFQUFFLGdCQUFnQixFQUFFLFdBQVcsRUFBRSxVQUFVLENBQUMsR0FBRyxFQUFFLENBQUMsNEJBQTRCLENBQUMsRUFBQyxDQUFDLHFFQUkxRixXQUFXLHVFQXZKWjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7R0F1RVQ7MkZBOEVVLDRCQUE0QjtrQkF2SnhDLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLHVCQUF1QjtvQkFDakMsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQXVFVDtvQkFDRCxNQUFNLEVBQUU7d0JBQ047Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0tBdUVDO3FCQUNGO29CQUVELFNBQVMsRUFBRSxDQUFDLEVBQUMsT0FBTyxFQUFFLGdCQUFnQixFQUFFLFdBQVcsRUFBRSxVQUFVLENBQUMsR0FBRyxFQUFFLDZCQUE2QixDQUFDLEVBQUMsQ0FBQztpQkFDdEc7OEJBR3lCLFNBQVM7c0JBQWhDLFNBQVM7dUJBQUMsV0FBVztnQkFDWixNQUFNO3NCQUFmLEtBQUs7Z0JBZ0NHLEtBQUs7c0JBQWIsS0FBSztnQkFDRyxTQUFTO3NCQUFqQixLQUFLO2dCQUNHLFdBQVc7c0JBQW5CLEtBQUs7Z0JBQ0csS0FBSztzQkFBYixLQUFLO2dCQUVHLFNBQVM7c0JBQWpCLEtBQUs7Z0JBQ0csUUFBUTtzQkFBaEIsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIGZvcndhcmRSZWYsIElucHV0LCBPbkluaXQsIE91dHB1dCwgVGVtcGxhdGVSZWYsIFZpZXdDaGlsZH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0R5bmFtaWNGb3JtV2lkdGh9IGZyb20gJy4vZHluYW1pYy1mb3JtLXdpZHRoJztcbmltcG9ydCB7Rm9ybUNvbnRyb2wsIEZvcm1Hcm91cH0gZnJvbSBcIkBhbmd1bGFyL2Zvcm1zXCI7XG5pbXBvcnQge2FuYWx5c2VTb3VyY2VzVHJhbnNmb3JtfSBmcm9tIFwibmctcGFja2Fnci9saWIvbmctcGFja2FnZS9lbnRyeS1wb2ludC9hbmFseXNlLXNvdXJjZXMudHJhbnNmb3JtXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2lvdC1kZWZpbmUtaWRlbnRpZmllcicsXG4gIHRlbXBsYXRlOiBgXG4gICAgPG5nLXRlbXBsYXRlPlxuICAgICAgPGRpdiBjbGFzcz1cIm91dGVyLW1hcmdpblwiPlxuICAgICAgICA8ZGl2IGNsYXNzPVwib3V0ZXJcIiBbbmdDbGFzc109XCJ7J291dGVyc21hbGwnOiFpc0JpZ31cIj5cbiAgICAgICAgICA8ZGl2IGNsYXNzPVwibGFiZWwtb3V0ZXJcIiBbbmdDbGFzc109XCJ7J2xhYmVsLW91dGVyLXRoaW4nOiFpc0JpZ31cIlxuICAgICAgICAgID5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJsYWJlbFwiPnt7bGFiZWwgfCB0cmFuc2xhdGV9fTwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuXG4gICAgICAgICAgPGRpdiBjbGFzcz1cImNvbnRlbnRcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjaGVja2JveC1jb250ZW50XCI+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjaGVja2JveC1jb250ZW50LWRlc2NyaXB0aW9uXCI+e3tkZXNjcmlwdGlvbiB8IHRyYW5zbGF0ZX19PC9kaXY+XG5cbiAgICAgICAgICAgICAgPGRpdiAqbmdGb3I9XCJsZXQgaWQgb2YgX3ZhbHVlXCI+XG4gICAgICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgICAgIDxtYXQtZm9ybS1maWVsZCBhcHBlYXJhbmNlPVwib3V0bGluZVwiIGNsYXNzPVwidy0xMDBcIj5cbiAgICAgICAgICAgICAgICAgICAgPGlucHV0IGNsYXNzPVwiaW5wdXQtY2xhc3NcIiBtYXRJbnB1dCB0eXBlPVwidGV4dFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICBbZGlzYWJsZWRdPVwidHJ1ZVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICBbbmdNb2RlbF09XCJpZFwiPlxuICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImJvcmRlci1jbGFzc1wiPjwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgPGJ1dHRvblxuICAgICAgICAgICAgICAgICAgICAgIChjbGljayk9XCJyZW1vdmUoaWQpXCJcbiAgICAgICAgICAgICAgICAgICAgICBtYXRTdWZmaXggbWF0LWljb24tYnV0dG9uXG4gICAgICAgICAgICAgICAgICAgICAgY29sb3I9XCJwcmltYXJ5XCIgYXJpYS1sYWJlbD1cIkV4YW1wbGUgaWNvbiBidXR0b24gd2l0aCBhIG1lbnUgaWNvblwiPlxuICAgICAgICAgICAgICAgICAgICAgIDxtYXQtaWNvbj5kZWxldGU8L21hdC1pY29uPlxuICAgICAgICAgICAgICAgICAgICA8L2J1dHRvbj5cblxuICAgICAgICAgICAgICAgICAgPC9tYXQtZm9ybS1maWVsZD5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImQtZmxleCBmbGV4LXJvdyB3LTEwMFwiPlxuXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInNlbGVjdC1idXR0b24gbXItMlwiPlxuICAgICAgICAgICAgICAgICAgPG1hdC1mb3JtLWZpZWxkIGFwcGVhcmFuY2U9XCJvdXRsaW5lXCI+XG4gICAgICAgICAgICAgICAgICAgIDxtYXQtbGFiZWw+VGhpcmQgcGFydHk8L21hdC1sYWJlbD5cblxuICAgICAgICAgICAgICAgICAgICA8bWF0LXNlbGVjdFxuICAgICAgICAgICAgICAgICAgICAgIFsobmdNb2RlbCldPVwidGhpcnBhcnR5U2VsZWN0aW9uXCJcblxuICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgPG1hdC1vcHRpb25cblxuICAgICAgICAgICAgICAgICAgICAgICAgKm5nRm9yPVwibGV0IHAgb2YgcHJlZml4XCIgW3ZhbHVlXT1cInAubmlkXCI+e3twLm5hbWV9fSA8L21hdC1vcHRpb24+XG4gICAgICAgICAgICAgICAgICAgIDwvbWF0LXNlbGVjdD5cbiAgICAgICAgICAgICAgICAgIDwvbWF0LWZvcm0tZmllbGQ+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZsZXgtZ3Jvdy0xXCI+XG4gICAgICAgICAgICAgICAgICA8bWF0LWZvcm0tZmllbGQgYXBwZWFyYW5jZT1cIm91dGxpbmVcIiBjbGFzcz1cInctMTAwXCI+XG4gICAgICAgICAgICAgICAgICAgIDxpbnB1dCBjbGFzcz1cImlucHV0LWNsYXNzXCIgbWF0SW5wdXQgdHlwZT1cInRleHRcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgKGtleXVwLmVudGVyKT1cImFkZElkZW50aWZpZXIoKVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICBbZGlzYWJsZWRdPVwiZmFsc2VcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgWyhuZ01vZGVsKV09XCJuZXdcIj5cbiAgICAgICAgICAgICAgICAgICAgPGJ1dHRvblxuICAgICAgICAgICAgICAgICAgICAgIChjbGljayk9XCJhZGRJZGVudGlmaWVyKClcIlxuICAgICAgICAgICAgICAgICAgICAgIG1hdFN1ZmZpeCBtYXQtaWNvbi1idXR0b25cbiAgICAgICAgICAgICAgICAgICAgICBjb2xvcj1cInByaW1hcnlcIiBhcmlhLWxhYmVsPVwiRXhhbXBsZSBpY29uIGJ1dHRvbiB3aXRoIGEgbWVudSBpY29uXCI+XG4gICAgICAgICAgICAgICAgICAgICAgPG1hdC1pY29uPmFkZHY8L21hdC1pY29uPlxuICAgICAgICAgICAgICAgICAgICA8L2J1dHRvbj5cbiAgICAgICAgICAgICAgICAgIDwvbWF0LWZvcm0tZmllbGQ+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDwvZGl2PlxuXG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgKm5nSWY9XCJzZXBhcmF0b3JcIiBjbGFzcz1cInNlcGFyYXRvclwiPjwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuXG5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICA8L25nLXRlbXBsYXRlPlxuICBgLFxuICBzdHlsZXM6IFtcbiAgICBgXG4gICAgICAuc2VwYXJhdG9yIHtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkYWRjZTA7XG4gICAgICAgIHBhZGRpbmctdG9wOiAxNnB4O1xuICAgICAgfVxuXG4gICAgICAuY2hlY2tib3gtY29udGVudC1kZXNjcmlwdGlvbiB7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBSb2JvdG8sIEFyaWFsLCBzYW5zLXNlcmlmO1xuICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICAgIGxldHRlci1zcGFjaW5nOiAuMDI1ZW07XG4gICAgICAgIGxpbmUtaGVpZ2h0OiAxNnB4O1xuICAgICAgICBjb2xvcjogIzVmNjM2ODtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTNweDtcbiAgICAgICAgbWFyZ2luLXRvcDogMnB4O1xuICAgICAgfVxuXG4gICAgICAub3V0ZXItbWFyZ2luIHtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDBweDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogNTBweDtcbiAgICAgIH1cblxuICAgICAgLmxhYmVsLW91dGVyLmxhYmVsLW91dGVyLXRoaW4ge1xuICAgICAgICBtaW4taGVpZ2h0OiA0MHB4O1xuICAgICAgfVxuXG4gICAgICAubGFiZWwtb3V0ZXIge1xuICAgICAgICBmbGV4LWJhc2lzOiAzMyU7XG4gICAgICAgIG1pbi1oZWlnaHQ6IDY0cHg7XG4gICAgICAgIG1pbi13aWR0aDogMjIwcHg7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICBwYWRkaW5nLXRvcDogMTNweDtcbiAgICAgICAgY29sb3I6ICMzYzQwNDM7XG4gICAgICB9XG5cbiAgICAgIC5sYWJlbCB7XG5cbiAgICAgICAgZm9udC1mYW1pbHk6IFJvYm90bywgQXJpYWwsIHNhbnMtc2VyaWY7XG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICAgICAgaGVpZ2h0OiAyMHB4O1xuICAgICAgICBjb2xvcjogcmdiKDYwLCA2NCwgNjcpO1xuICAgICAgfVxuXG4gICAgICAub3V0ZXIge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBtYXgtd2lkdGg6IDkwMHB4O1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAxNnB4O1xuICAgICAgfVxuXG4gICAgICAub3V0ZXJzbWFsbCB7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgIHBhZGRpbmctYm90dG9tOiA4cHg7XG4gICAgICB9XG5cbiAgICAgIC5jb250ZW50IHtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAgZmxleC1iYXNpczogNjclO1xuICAgICAgICBtaW4td2lkdGg6IDI4MHB4O1xuICAgICAgfVxuXG4gICAgICAuY2hlY2tib3gtY29udGVudCB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgIGNvbG9yOiAjM2M0MDQzO1xuICAgICAgICBmb250LWZhbWlseTogUm9ib3RvLCBBcmlhbCwgc2Fucy1zZXJpZjtcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICBmb250LXdlaWdodDogNDAwO1xuICAgICAgICBsZXR0ZXItc3BhY2luZzogMC4ycHg7XG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICAgICAgfVxuICAgIGBcbiAgXVxuICAsXG4gIHByb3ZpZGVyczogW3twcm92aWRlOiBEeW5hbWljRm9ybVdpZHRoLCB1c2VFeGlzdGluZzogZm9yd2FyZFJlZigoKSA9PiBJb3REZWZpbmVJZGVudGlmaWVyQ29tcG9uZW50KX1dXG59KVxuZXhwb3J0IGNsYXNzIElvdERlZmluZUlkZW50aWZpZXJDb21wb25lbnQgZXh0ZW5kcyBEeW5hbWljRm9ybVdpZHRoIGltcGxlbWVudHMgT25Jbml0IHtcblxuICBAVmlld0NoaWxkKFRlbXBsYXRlUmVmKSBfdGVtcGxhdGU6IFRlbXBsYXRlUmVmPGFueT47XG4gIEBJbnB1dCgpICBwcmVmaXggPSBbXG4gICAge1xuICAgICAgJ25pZCc6ICdzbWF4eCcsXG4gICAgICAnbmFtZSc6ICdTKW1heHgnXG4gICAgfSxcbiAgICB7XG4gICAgICAnbmlkJzogJ2lvdCcsXG4gICAgICAnbmFtZSc6ICdJby1UaGluZ3MnXG4gICAgfSxcbiAgXTtcbiAgcHVibGljIGdldCB0ZW1wbGF0ZSgpIHtcblxuICAgIHJldHVybiB0aGlzLl90ZW1wbGF0ZTtcbiAgfVxuXG4gIHB1YmxpYyBnZXQgcHJpc3RpbmUoKTogYW55IHtcbiAgICByZXR1cm4gSlNPTi5zdHJpbmdpZnkodGhpcy52YWx1ZSk9PT1KU09OLnN0cmluZ2lmeSh0aGlzLl92YWx1ZSk7XG4gIH1cblxuICBwdWJsaWMgZ2V0IGtleSgpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLmF0dHJpYnV0ZTtcbiAgfVxuXG4gIHB1YmxpYyBnZXQgbmV3VmFsdWUoKTogYW55IHtcbiAgICByZXR1cm4gdGhpcy5fdmFsdWU7XG4gIH1cblxuICBmb3JtID0gbmV3IEZvcm1Hcm91cCh7XG4gICAgdmFsdWU6IG5ldyBGb3JtQ29udHJvbCgnJyksXG4gIH0pO1xuXG5cbiAgQElucHV0KCkgbGFiZWw6IHN0cmluZztcbiAgQElucHV0KCkgYXR0cmlidXRlID0gJ3RvZG8nO1xuICBASW5wdXQoKSBkZXNjcmlwdGlvbiA9ICdleHBsYWluIHRoaXMgZmllbGQnO1xuICBASW5wdXQoKSB2YWx1ZTogc3RyaW5nW107XG4gIF92YWx1ZTogc3RyaW5nW107XG4gIEBJbnB1dCgpIHNlcGFyYXRvcjogYm9vbGVhbjtcbiAgQElucHV0KCkgZGlzYWJsZWQgPSBmYWxzZTtcblxuXG5cbiAgbmV3OiBzdHJpbmcgPSAnJztcbiAgdGhpcnBhcnR5U2VsZWN0aW9uID0gJ3NtYXh4JztcblxuICBjaGFuZ2UoZXZlbnQ6IGFueSkge1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgc3VwZXIubmdPbkluaXQoKTtcbiAgICB0aGlzLl92YWx1ZSA9IHRoaXMudmFsdWU7XG4gICAgaWYgKHRoaXMuZGlzYWJsZWQpIHtcbiAgICAgIC8vIHRoaXMuZm9ybSA9IG5ldyBGb3JtR3JvdXAoe1xuICAgICAgLy8gICBzZWxlY3RJZDogbmV3IEZvcm1Db250cm9sKHt2YWx1ZTogJycsIGRpc2FibGVkOiB0aGlzLmRpc2FibGVkfSksXG4gICAgICAvLyB9KTtcbiAgICB9XG4gICAgdGhpcy5mb3JtLnNldFZhbHVlKHt2YWx1ZTogdGhpcy52YWx1ZX0pO1xuICB9XG5cbiAgYWRkSWRlbnRpZmllcigpIHtcbiAgICB0aGlzLl92YWx1ZSA9IFsuLi50aGlzLl92YWx1ZSwgYHVybjoke3RoaXMudGhpcnBhcnR5U2VsZWN0aW9ufToke3RoaXMubmV3fWBdO1xuXG4gICAgdGhpcy5mb3JtLnNldFZhbHVlKHt2YWx1ZTogWy4uLnRoaXMudmFsdWUsIGB1cm46JHt0aGlzLnRoaXJwYXJ0eVNlbGVjdGlvbn06JHt0aGlzLm5ld31gXX0pO1xuICB9XG5cblxuICByZW1vdmUoaWQ6IHN0cmluZykge1xuICAgIHRoaXMuX3ZhbHVlID0gdGhpcy5fdmFsdWUuZmlsdGVyKHVybiA9PiB1cm4gIT09IGlkKTtcbiAgICB0aGlzLmZvcm0uc2V0VmFsdWUoe3ZhbHVlOiB0aGlzLl92YWx1ZX0pO1xuXG4gIH1cbn1cbiJdfQ==