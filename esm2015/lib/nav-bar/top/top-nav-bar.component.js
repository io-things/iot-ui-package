import { Component, EventEmitter, Output } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/button";
import * as i2 from "@angular/material/icon";
export class TopNavBarComponent {
    constructor() {
        this.menuClick = new EventEmitter();
    }
    ngOnInit() {
    }
}
TopNavBarComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: TopNavBarComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
TopNavBarComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: TopNavBarComponent, selector: "iot-top-nav-bar", outputs: { menuClick: "menuClick" }, ngImport: i0, template: `
    <nav >
      <button
        mat-icon-button (click)="menuClick.emit($event)">
        <mat-icon>menu</mat-icon>
      </button>
      <ng-content select="[portaltitle]"></ng-content>
      <ng-content select="[customerselect]"></ng-content>
      <div class="flex-spacer"></div>

      <ng-content select="[icon1]"></ng-content>
      <ng-content select="[language]"></ng-content>
      <ng-content select="[account]"></ng-content>
    </nav>
  `, isInline: true, components: [{ type: i1.MatButton, selector: "button[mat-button], button[mat-raised-button], button[mat-icon-button],             button[mat-fab], button[mat-mini-fab], button[mat-stroked-button],             button[mat-flat-button]", inputs: ["disabled", "disableRipple", "color"], exportAs: ["matButton"] }, { type: i2.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: TopNavBarComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-top-nav-bar',
                    template: `
    <nav >
      <button
        mat-icon-button (click)="menuClick.emit($event)">
        <mat-icon>menu</mat-icon>
      </button>
      <ng-content select="[portaltitle]"></ng-content>
      <ng-content select="[customerselect]"></ng-content>
      <div class="flex-spacer"></div>

      <ng-content select="[icon1]"></ng-content>
      <ng-content select="[language]"></ng-content>
      <ng-content select="[account]"></ng-content>
    </nav>
  `,
                    styles: []
                }]
        }], ctorParameters: function () { return []; }, propDecorators: { menuClick: [{
                type: Output
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9wLW5hdi1iYXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvaW90LXVpLWxpYi9zcmMvbGliL25hdi1iYXIvdG9wL3RvcC1uYXYtYmFyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLFlBQVksRUFBVSxNQUFNLEVBQUMsTUFBTSxlQUFlLENBQUM7Ozs7QUFzQnRFLE1BQU0sT0FBTyxrQkFBa0I7SUFJN0I7UUFIVSxjQUFTLEdBQUcsSUFBSSxZQUFZLEVBQWMsQ0FBQztJQUdyQyxDQUFDO0lBRWpCLFFBQVE7SUFDUixDQUFDOzsrR0FQVSxrQkFBa0I7bUdBQWxCLGtCQUFrQiw0RkFsQm5COzs7Ozs7Ozs7Ozs7OztHQWNUOzJGQUlVLGtCQUFrQjtrQkFwQjlCLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLGlCQUFpQjtvQkFDM0IsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7OztHQWNUO29CQUNELE1BQU0sRUFBRSxFQUNQO2lCQUNGOzBFQUVXLFNBQVM7c0JBQWxCLE1BQU0iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBPbkluaXQsIE91dHB1dH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2lvdC10b3AtbmF2LWJhcicsXG4gIHRlbXBsYXRlOiBgXG4gICAgPG5hdiA+XG4gICAgICA8YnV0dG9uXG4gICAgICAgIG1hdC1pY29uLWJ1dHRvbiAoY2xpY2spPVwibWVudUNsaWNrLmVtaXQoJGV2ZW50KVwiPlxuICAgICAgICA8bWF0LWljb24+bWVudTwvbWF0LWljb24+XG4gICAgICA8L2J1dHRvbj5cbiAgICAgIDxuZy1jb250ZW50IHNlbGVjdD1cIltwb3J0YWx0aXRsZV1cIj48L25nLWNvbnRlbnQ+XG4gICAgICA8bmctY29udGVudCBzZWxlY3Q9XCJbY3VzdG9tZXJzZWxlY3RdXCI+PC9uZy1jb250ZW50PlxuICAgICAgPGRpdiBjbGFzcz1cImZsZXgtc3BhY2VyXCI+PC9kaXY+XG5cbiAgICAgIDxuZy1jb250ZW50IHNlbGVjdD1cIltpY29uMV1cIj48L25nLWNvbnRlbnQ+XG4gICAgICA8bmctY29udGVudCBzZWxlY3Q9XCJbbGFuZ3VhZ2VdXCI+PC9uZy1jb250ZW50PlxuICAgICAgPG5nLWNvbnRlbnQgc2VsZWN0PVwiW2FjY291bnRdXCI+PC9uZy1jb250ZW50PlxuICAgIDwvbmF2PlxuICBgLFxuICBzdHlsZXM6IFtcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBUb3BOYXZCYXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBAT3V0cHV0KCkgbWVudUNsaWNrID0gbmV3IEV2ZW50RW1pdHRlcjxNb3VzZUV2ZW50PigpO1xuXG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgfVxuXG59XG4iXX0=