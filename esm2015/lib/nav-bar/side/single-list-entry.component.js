import { Component, Input } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/list";
import * as i2 from "@angular/material/icon";
import * as i3 from "@angular/router";
import * as i4 from "@angular/material/core";
import * as i5 from "@ngx-translate/core";
export class SingleListEntryComponent {
    constructor() {
        this.routerLink = '';
    }
    ngOnChanges(changes) {
        if (this.params) {
            this.ngOnInit();
        }
    }
    ngOnInit() {
        if (this.entry.route && this.params) {
            this.routerLink = this.entry.route;
            this.params.keys.forEach((key) => {
                if (this.entry.route.indexOf(`:${key}`) !== -1) {
                    this.routerLink = this.routerLink.replace(`:${key}`, this.params.get(key));
                }
            });
            Object.keys(this.paramsExtra || {}).forEach((key) => {
                if (this.entry.route.indexOf(`:${key}`) !== -1) {
                    this.routerLink = this.routerLink.replace(`:${key}`, '' + this.paramsExtra[key]);
                }
            });
        }
    }
}
SingleListEntryComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: SingleListEntryComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
SingleListEntryComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: SingleListEntryComponent, selector: "iot-single-list-entry", inputs: { entry: "entry", params: "params", paramsExtra: "paramsExtra" }, usesOnChanges: true, ngImport: i0, template: `
    <mat-list-item
      [routerLink]="routerLink">
      <mat-icon mat-list-icon>{{entry.icon }}</mat-icon>
      <a matLine>{{entry.label | translate}} </a>
    </mat-list-item>
  `, isInline: true, components: [{ type: i1.MatListItem, selector: "mat-list-item, a[mat-list-item], button[mat-list-item]", inputs: ["disableRipple", "disabled"], exportAs: ["matListItem"] }, { type: i2.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }], directives: [{ type: i3.RouterLink, selector: ":not(a):not(area)[routerLink]", inputs: ["routerLink", "queryParams", "fragment", "queryParamsHandling", "preserveFragment", "skipLocationChange", "replaceUrl", "state", "relativeTo"] }, { type: i1.MatListIconCssMatStyler, selector: "[mat-list-icon], [matListIcon]" }, { type: i4.MatLine, selector: "[mat-line], [matLine]" }], pipes: { "translate": i5.TranslatePipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: SingleListEntryComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-single-list-entry',
                    template: `
    <mat-list-item
      [routerLink]="routerLink">
      <mat-icon mat-list-icon>{{entry.icon }}</mat-icon>
      <a matLine>{{entry.label | translate}} </a>
    </mat-list-item>
  `,
                    styles: []
                }]
        }], ctorParameters: function () { return []; }, propDecorators: { entry: [{
                type: Input
            }], params: [{
                type: Input
            }], paramsExtra: [{
                type: Input
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2luZ2xlLWxpc3QtZW50cnkuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvaW90LXVpLWxpYi9zcmMvbGliL25hdi1iYXIvc2lkZS9zaW5nbGUtbGlzdC1lbnRyeS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFDLFNBQVMsRUFBRSxLQUFLLEVBQThDLE1BQU0sZUFBZSxDQUFDOzs7Ozs7O0FBZTVGLE1BQU0sT0FBTyx3QkFBd0I7SUFRbkM7UUFIQSxlQUFVLEdBQUcsRUFBRSxDQUFDO0lBSWhCLENBQUM7SUFFRCxXQUFXLENBQUMsT0FBc0I7UUFDNUIsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2YsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1NBQ2pCO0lBQ1AsQ0FBQztJQUVELFFBQVE7UUFDTixJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDbkMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQztZQUNuQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFXLEVBQUUsRUFBRTtnQkFDdkMsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO29CQUM5QyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxFQUFFLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztpQkFDNUU7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUNILE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsSUFBSSxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFXLEVBQUUsRUFBRTtnQkFDMUQsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO29CQUM5QyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxFQUFFLEVBQUUsRUFBRSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztpQkFDbEY7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQzs7cUhBL0JVLHdCQUF3Qjt5R0FBeEIsd0JBQXdCLDRKQVR6Qjs7Ozs7O0dBTVQ7MkZBR1Usd0JBQXdCO2tCQVhwQyxTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSx1QkFBdUI7b0JBQ2pDLFFBQVEsRUFBRTs7Ozs7O0dBTVQ7b0JBQ0QsTUFBTSxFQUFFLEVBQUU7aUJBQ1g7MEVBRVUsS0FBSztzQkFBYixLQUFLO2dCQUNVLE1BQU07c0JBQXJCLEtBQUs7Z0JBQ0csV0FBVztzQkFBbkIsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25DaGFuZ2VzLCBPbkRlc3Ryb3ksIE9uSW5pdCwgU2ltcGxlQ2hhbmdlc30gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge09ic2VydmFibGUsIFN1YnNjcmlwdGlvbn0gZnJvbSBcInJ4anNcIjtcbmltcG9ydCB7UGFyYW1NYXB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnaW90LXNpbmdsZS1saXN0LWVudHJ5JyxcbiAgdGVtcGxhdGU6IGBcbiAgICA8bWF0LWxpc3QtaXRlbVxuICAgICAgW3JvdXRlckxpbmtdPVwicm91dGVyTGlua1wiPlxuICAgICAgPG1hdC1pY29uIG1hdC1saXN0LWljb24+e3tlbnRyeS5pY29uIH19PC9tYXQtaWNvbj5cbiAgICAgIDxhIG1hdExpbmU+e3tlbnRyeS5sYWJlbCB8IHRyYW5zbGF0ZX19IDwvYT5cbiAgICA8L21hdC1saXN0LWl0ZW0+XG4gIGAsXG4gIHN0eWxlczogW11cbn0pXG5leHBvcnQgY2xhc3MgU2luZ2xlTGlzdEVudHJ5Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkNoYW5nZXMge1xuICBASW5wdXQoKSBlbnRyeTtcbiAgQElucHV0KCkgcHVibGljIHBhcmFtczogUGFyYW1NYXA7XG4gIEBJbnB1dCgpIHBhcmFtc0V4dHJhOiB7IFtrZXk6IHN0cmluZ106IChzdHJpbmcgfCBudW1iZXIpIH07XG5cbiAgcm91dGVyTGluayA9ICcnO1xuXG5cbiAgY29uc3RydWN0b3IoKSB7XG4gIH1cblxuICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKTogdm9pZCB7XG4gICAgICAgIGlmICh0aGlzLnBhcmFtcykge1xuICAgICAgICAgIHRoaXMubmdPbkluaXQoKTtcbiAgICAgICAgfVxuICB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgaWYgKHRoaXMuZW50cnkucm91dGUgJiYgdGhpcy5wYXJhbXMpIHtcbiAgICAgIHRoaXMucm91dGVyTGluayA9IHRoaXMuZW50cnkucm91dGU7XG4gICAgICB0aGlzLnBhcmFtcy5rZXlzLmZvckVhY2goKGtleTogc3RyaW5nKSA9PiB7XG4gICAgICAgIGlmICh0aGlzLmVudHJ5LnJvdXRlLmluZGV4T2YoYDoke2tleX1gKSAhPT0gLTEpIHtcbiAgICAgICAgICB0aGlzLnJvdXRlckxpbmsgPSB0aGlzLnJvdXRlckxpbmsucmVwbGFjZShgOiR7a2V5fWAsIHRoaXMucGFyYW1zLmdldChrZXkpKTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgICBPYmplY3Qua2V5cyh0aGlzLnBhcmFtc0V4dHJhIHx8IHt9KS5mb3JFYWNoKChrZXk6IHN0cmluZykgPT4ge1xuICAgICAgICBpZiAodGhpcy5lbnRyeS5yb3V0ZS5pbmRleE9mKGA6JHtrZXl9YCkgIT09IC0xKSB7XG4gICAgICAgICAgdGhpcy5yb3V0ZXJMaW5rID0gdGhpcy5yb3V0ZXJMaW5rLnJlcGxhY2UoYDoke2tleX1gLCAnJyArIHRoaXMucGFyYW1zRXh0cmFba2V5XSk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH1cbiAgfVxuXG4gIC8vXG4gIC8vIG5nT25EZXN0cm95KCkge1xuICAvLyAgIHRoaXMuc3ViLnVuc3Vic2NyaWJlKCk7XG4gIC8vIH1cblxufVxuIl19