import { Component, Input } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/list";
import * as i2 from "@angular/material/icon";
import * as i3 from "./single-list-entry.component";
import * as i4 from "@angular/material/core";
import * as i5 from "@angular/common";
import * as i6 from "@ngx-translate/core";
export class CompositeListEntryComponent {
    constructor() {
        this.entry = {};
        this.showSettingsMenu = false;
    }
    ngOnInit() {
    }
}
CompositeListEntryComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: CompositeListEntryComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
CompositeListEntryComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: CompositeListEntryComponent, selector: "iot-composite-list-entry", inputs: { entry: "entry", params: "params", paramsExtra: "paramsExtra" }, ngImport: i0, template: `
    <mat-list-item
      (click)="showSettingsMenu = !showSettingsMenu"
    >
      <mat-icon mat-list-icon>{{entry.icon }}</mat-icon>
      <a matLine>{{entry.label | translate}}</a>
      <mat-icon class="menu-button" [ngClass]="{'rotated' : showSettingsMenu}">expand_more
      </mat-icon>
    </mat-list-item>
    <div class="submenu" [ngClass]="{'expanded' : showSettingsMenu}"
         *ngIf="showSettingsMenu">
      <iot-single-list-entry
        *ngFor="let item of entry.items"
        [entry]="item"
        [params]="params"
        [paramsExtra]="paramsExtra"
      ></iot-single-list-entry>
    </div>
  `, isInline: true, styles: ["\n    .menu-button {\n      transition: 300ms ease-in-out;\n      transform: rotate(0deg);\n    }\n\n    .menu-button.rotated {\n      transform: rotate(180deg);\n    }\n\n    .submenu {\n      overflow-y: hidden;\n      height: 0px;\n      transition: transform 300ms ease;\n      transform: scaleY(0);\n      transform-origin: top;\n      padding-left: 30px;\n    }\n\n    .submenu.expanded {\n      transform: scaleY(1);\n      height: unset;\n    }\n  "], components: [{ type: i1.MatListItem, selector: "mat-list-item, a[mat-list-item], button[mat-list-item]", inputs: ["disableRipple", "disabled"], exportAs: ["matListItem"] }, { type: i2.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }, { type: i3.SingleListEntryComponent, selector: "iot-single-list-entry", inputs: ["entry", "params", "paramsExtra"] }], directives: [{ type: i1.MatListIconCssMatStyler, selector: "[mat-list-icon], [matListIcon]" }, { type: i4.MatLine, selector: "[mat-line], [matLine]" }, { type: i5.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i5.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i5.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }], pipes: { "translate": i6.TranslatePipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: CompositeListEntryComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-composite-list-entry',
                    template: `
    <mat-list-item
      (click)="showSettingsMenu = !showSettingsMenu"
    >
      <mat-icon mat-list-icon>{{entry.icon }}</mat-icon>
      <a matLine>{{entry.label | translate}}</a>
      <mat-icon class="menu-button" [ngClass]="{'rotated' : showSettingsMenu}">expand_more
      </mat-icon>
    </mat-list-item>
    <div class="submenu" [ngClass]="{'expanded' : showSettingsMenu}"
         *ngIf="showSettingsMenu">
      <iot-single-list-entry
        *ngFor="let item of entry.items"
        [entry]="item"
        [params]="params"
        [paramsExtra]="paramsExtra"
      ></iot-single-list-entry>
    </div>
  `,
                    styles: [`
    .menu-button {
      transition: 300ms ease-in-out;
      transform: rotate(0deg);
    }

    .menu-button.rotated {
      transform: rotate(180deg);
    }

    .submenu {
      overflow-y: hidden;
      height: 0px;
      transition: transform 300ms ease;
      transform: scaleY(0);
      transform-origin: top;
      padding-left: 30px;
    }

    .submenu.expanded {
      transform: scaleY(1);
      height: unset;
    }
  `
                    ]
                }]
        }], ctorParameters: function () { return []; }, propDecorators: { entry: [{
                type: Input
            }], params: [{
                type: Input
            }], paramsExtra: [{
                type: Input
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9zaXRlLWxpc3QtZW50cnkuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvaW90LXVpLWxpYi9zcmMvbGliL25hdi1iYXIvc2lkZS9jb21wb3NpdGUtbGlzdC1lbnRyeS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFDLFNBQVMsRUFBRSxLQUFLLEVBQVMsTUFBTSxlQUFlLENBQUM7Ozs7Ozs7O0FBa0R2RCxNQUFNLE9BQU8sMkJBQTJCO0lBUXRDO1FBTlMsVUFBSyxHQUFRLEVBQUUsQ0FBQztRQUl6QixxQkFBZ0IsR0FBRyxLQUFLLENBQUM7SUFFVCxDQUFDO0lBRWpCLFFBQVE7SUFDUixDQUFDOzt3SEFYVSwyQkFBMkI7NEdBQTNCLDJCQUEyQiwwSUE3QzVCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7R0FrQlQ7MkZBMkJVLDJCQUEyQjtrQkEvQ3ZDLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLDBCQUEwQjtvQkFDcEMsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7R0FrQlQ7b0JBQ0QsTUFBTSxFQUFFLENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0dBdUJSO3FCQUNBO2lCQUNGOzBFQUdVLEtBQUs7c0JBQWIsS0FBSztnQkFDVSxNQUFNO3NCQUFyQixLQUFLO2dCQUNHLFdBQVc7c0JBQW5CLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge1BhcmFtTWFwfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2lvdC1jb21wb3NpdGUtbGlzdC1lbnRyeScsXG4gIHRlbXBsYXRlOiBgXG4gICAgPG1hdC1saXN0LWl0ZW1cbiAgICAgIChjbGljayk9XCJzaG93U2V0dGluZ3NNZW51ID0gIXNob3dTZXR0aW5nc01lbnVcIlxuICAgID5cbiAgICAgIDxtYXQtaWNvbiBtYXQtbGlzdC1pY29uPnt7ZW50cnkuaWNvbiB9fTwvbWF0LWljb24+XG4gICAgICA8YSBtYXRMaW5lPnt7ZW50cnkubGFiZWwgfCB0cmFuc2xhdGV9fTwvYT5cbiAgICAgIDxtYXQtaWNvbiBjbGFzcz1cIm1lbnUtYnV0dG9uXCIgW25nQ2xhc3NdPVwieydyb3RhdGVkJyA6IHNob3dTZXR0aW5nc01lbnV9XCI+ZXhwYW5kX21vcmVcbiAgICAgIDwvbWF0LWljb24+XG4gICAgPC9tYXQtbGlzdC1pdGVtPlxuICAgIDxkaXYgY2xhc3M9XCJzdWJtZW51XCIgW25nQ2xhc3NdPVwieydleHBhbmRlZCcgOiBzaG93U2V0dGluZ3NNZW51fVwiXG4gICAgICAgICAqbmdJZj1cInNob3dTZXR0aW5nc01lbnVcIj5cbiAgICAgIDxpb3Qtc2luZ2xlLWxpc3QtZW50cnlcbiAgICAgICAgKm5nRm9yPVwibGV0IGl0ZW0gb2YgZW50cnkuaXRlbXNcIlxuICAgICAgICBbZW50cnldPVwiaXRlbVwiXG4gICAgICAgIFtwYXJhbXNdPVwicGFyYW1zXCJcbiAgICAgICAgW3BhcmFtc0V4dHJhXT1cInBhcmFtc0V4dHJhXCJcbiAgICAgID48L2lvdC1zaW5nbGUtbGlzdC1lbnRyeT5cbiAgICA8L2Rpdj5cbiAgYCxcbiAgc3R5bGVzOiBbYFxuICAgIC5tZW51LWJ1dHRvbiB7XG4gICAgICB0cmFuc2l0aW9uOiAzMDBtcyBlYXNlLWluLW91dDtcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xuICAgIH1cblxuICAgIC5tZW51LWJ1dHRvbi5yb3RhdGVkIHtcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDE4MGRlZyk7XG4gICAgfVxuXG4gICAgLnN1Ym1lbnUge1xuICAgICAgb3ZlcmZsb3cteTogaGlkZGVuO1xuICAgICAgaGVpZ2h0OiAwcHg7XG4gICAgICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMzAwbXMgZWFzZTtcbiAgICAgIHRyYW5zZm9ybTogc2NhbGVZKDApO1xuICAgICAgdHJhbnNmb3JtLW9yaWdpbjogdG9wO1xuICAgICAgcGFkZGluZy1sZWZ0OiAzMHB4O1xuICAgIH1cblxuICAgIC5zdWJtZW51LmV4cGFuZGVkIHtcbiAgICAgIHRyYW5zZm9ybTogc2NhbGVZKDEpO1xuICAgICAgaGVpZ2h0OiB1bnNldDtcbiAgICB9XG4gIGBcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBDb21wb3NpdGVMaXN0RW50cnlDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIEBJbnB1dCgpIGVudHJ5OiBhbnkgPSB7fTtcbiAgQElucHV0KCkgcHVibGljIHBhcmFtczogUGFyYW1NYXAgO1xuICBASW5wdXQoKSBwYXJhbXNFeHRyYTogeyBba2V5OiBzdHJpbmddOiAgKHN0cmluZyB8IG51bWJlcikgfTtcblxuICBzaG93U2V0dGluZ3NNZW51ID0gZmFsc2U7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgfVxuXG59XG4iXX0=