import { Component, Input } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
import * as i2 from "@angular/material/sidenav";
import * as i3 from "@angular/material/list";
import * as i4 from "./single-list-entry.component";
import * as i5 from "./composite-list-entry.component";
import * as i6 from "@angular/common";
export class SideNavBarComponent {
    constructor(route) {
        this.route = route;
        this.belowTopNav = true;
        this.isMobile = true;
        this.isOpen = true;
        this.items = [];
    }
    ngOnInit() {
        if (this.data) {
            this.items = this.data.items;
        }
        this.sub = this.route
            .paramMap
            .subscribe((pMap) => {
            this.paramMap = pMap;
        });
    }
    ngOnDestroy() {
        if (this.sub) {
            this.sub.unsubscribe();
        }
    }
    ngOnChanges(changes) {
        if (changes.data && this.data) {
            this.items = this.data.items;
        }
        if (changes.params) {
            this.items = [].concat(this.items); //[...this.items];
        }
    }
}
SideNavBarComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: SideNavBarComponent, deps: [{ token: i1.ActivatedRoute }], target: i0.ɵɵFactoryTarget.Component });
SideNavBarComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: SideNavBarComponent, selector: "iot-side-nav-bar", inputs: { belowTopNav: "belowTopNav", isMobile: "isMobile", isOpen: "isOpen", data: "data", params: "params", width: "width" }, usesOnChanges: true, ngImport: i0, template: `
    <mat-drawer-container
      autosize [hasBackdrop]="false"
    >
      <mat-drawer
        [ngStyle]="{'width': width == undefined?'unset':width}"
        #drawer [mode]="(isMobile)?'over':'side'"
                  [opened]="isOpen">
        <ng-content select="[sideNavTop]"></ng-content>
        <mat-nav-list>
          <div *ngFor="let item of items">
            <iot-single-list-entry
              [entry]="item"
              [params]="paramMap"
              [paramsExtra]="params"
              *ngIf="!item.items"
            ></iot-single-list-entry>
            <iot-composite-list-entry
              [entry]="item"
              [params]="paramMap"
              [paramsExtra]="params"
              *ngIf="item.items"
            >
            </iot-composite-list-entry>
          </div>
        </mat-nav-list>
      </mat-drawer>
      <ng-content></ng-content>
      <router-outlet></router-outlet>
    </mat-drawer-container>

  `, isInline: true, styles: ["\n\n    "], components: [{ type: i2.MatDrawerContainer, selector: "mat-drawer-container", inputs: ["autosize", "hasBackdrop"], outputs: ["backdropClick"], exportAs: ["matDrawerContainer"] }, { type: i2.MatDrawer, selector: "mat-drawer", inputs: ["position", "mode", "disableClose", "autoFocus", "opened"], outputs: ["openedChange", "opened", "openedStart", "closed", "closedStart", "positionChanged"], exportAs: ["matDrawer"] }, { type: i3.MatNavList, selector: "mat-nav-list", inputs: ["disableRipple", "disabled"], exportAs: ["matNavList"] }, { type: i4.SingleListEntryComponent, selector: "iot-single-list-entry", inputs: ["entry", "params", "paramsExtra"] }, { type: i5.CompositeListEntryComponent, selector: "iot-composite-list-entry", inputs: ["entry", "params", "paramsExtra"] }], directives: [{ type: i6.NgStyle, selector: "[ngStyle]", inputs: ["ngStyle"] }, { type: i6.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { type: i6.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i1.RouterOutlet, selector: "router-outlet", outputs: ["activate", "deactivate"], exportAs: ["outlet"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: SideNavBarComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-side-nav-bar',
                    template: `
    <mat-drawer-container
      autosize [hasBackdrop]="false"
    >
      <mat-drawer
        [ngStyle]="{'width': width == undefined?'unset':width}"
        #drawer [mode]="(isMobile)?'over':'side'"
                  [opened]="isOpen">
        <ng-content select="[sideNavTop]"></ng-content>
        <mat-nav-list>
          <div *ngFor="let item of items">
            <iot-single-list-entry
              [entry]="item"
              [params]="paramMap"
              [paramsExtra]="params"
              *ngIf="!item.items"
            ></iot-single-list-entry>
            <iot-composite-list-entry
              [entry]="item"
              [params]="paramMap"
              [paramsExtra]="params"
              *ngIf="item.items"
            >
            </iot-composite-list-entry>
          </div>
        </mat-nav-list>
      </mat-drawer>
      <ng-content></ng-content>
      <router-outlet></router-outlet>
    </mat-drawer-container>

  `,
                    styles: [
                        `

    `
                    ]
                }]
        }], ctorParameters: function () { return [{ type: i1.ActivatedRoute }]; }, propDecorators: { belowTopNav: [{
                type: Input
            }], isMobile: [{
                type: Input
            }], isOpen: [{
                type: Input
            }], data: [{
                type: Input
            }], params: [{
                type: Input
            }], width: [{
                type: Input
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2lkZS1uYXYtYmFyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2lvdC11aS1saWIvc3JjL2xpYi9uYXYtYmFyL3NpZGUvc2lkZS1uYXYtYmFyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLEtBQUssRUFBOEMsTUFBTSxlQUFlLENBQUM7Ozs7Ozs7O0FBNkM1RixNQUFNLE9BQU8sbUJBQW1CO0lBYTlCLFlBQ1UsS0FBcUI7UUFBckIsVUFBSyxHQUFMLEtBQUssQ0FBZ0I7UUFidEIsZ0JBQVcsR0FBRyxJQUFJLENBQUM7UUFDbkIsYUFBUSxHQUFHLElBQUksQ0FBQztRQUNoQixXQUFNLEdBQUcsSUFBSSxDQUFDO1FBTWhCLFVBQUssR0FBUSxFQUFFLENBQUM7SUFRdkIsQ0FBQztJQUVELFFBQVE7UUFDTixJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFDYixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO1NBQzlCO1FBQ0QsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsS0FBSzthQUNsQixRQUFRO2FBQ1IsU0FBUyxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7WUFDbEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDdkIsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsV0FBVztRQUNULElBQUksSUFBSSxDQUFDLEdBQUcsRUFBRTtZQUNaLElBQUksQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDeEI7SUFFSCxDQUFDO0lBRUQsV0FBVyxDQUFDLE9BQXNCO1FBRWhDLElBQUcsT0FBTyxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQzVCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7U0FDOUI7UUFDRCxJQUFJLE9BQU8sQ0FBQyxNQUFNLEVBQUU7WUFDbEIsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLGtCQUFrQjtTQUN2RDtJQUNILENBQUM7O2dIQTdDVSxtQkFBbUI7b0dBQW5CLG1CQUFtQiw2TUF0Q3BCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0dBK0JUOzJGQU9VLG1CQUFtQjtrQkF4Qy9CLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLGtCQUFrQjtvQkFDNUIsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0dBK0JUO29CQUNELE1BQU0sRUFBRTt3QkFDTjs7S0FFQztxQkFDRjtpQkFDRjtxR0FFVSxXQUFXO3NCQUFuQixLQUFLO2dCQUNHLFFBQVE7c0JBQWhCLEtBQUs7Z0JBQ0csTUFBTTtzQkFBZCxLQUFLO2dCQUNHLElBQUk7c0JBQVosS0FBSztnQkFDRyxNQUFNO3NCQUFkLEtBQUs7Z0JBQ0csS0FBSztzQkFBYixLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIElucHV0LCBPbkNoYW5nZXMsIE9uRGVzdHJveSwgT25Jbml0LCBTaW1wbGVDaGFuZ2VzfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7QWN0aXZhdGVkUm91dGUsIFBhcmFtTWFwLCBSb3V0ZXJ9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcbmltcG9ydCB7T2JzZXJ2YWJsZSwgU3Vic2NyaXB0aW9ufSBmcm9tIFwicnhqc1wiO1xuaW1wb3J0IHthbmFseXNlU291cmNlc1RyYW5zZm9ybX0gZnJvbSBcIm5nLXBhY2thZ3IvbGliL25nLXBhY2thZ2UvZW50cnktcG9pbnQvYW5hbHlzZS1zb3VyY2VzLnRyYW5zZm9ybVwiO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdpb3Qtc2lkZS1uYXYtYmFyJyxcbiAgdGVtcGxhdGU6IGBcbiAgICA8bWF0LWRyYXdlci1jb250YWluZXJcbiAgICAgIGF1dG9zaXplIFtoYXNCYWNrZHJvcF09XCJmYWxzZVwiXG4gICAgPlxuICAgICAgPG1hdC1kcmF3ZXJcbiAgICAgICAgW25nU3R5bGVdPVwieyd3aWR0aCc6IHdpZHRoID09IHVuZGVmaW5lZD8ndW5zZXQnOndpZHRofVwiXG4gICAgICAgICNkcmF3ZXIgW21vZGVdPVwiKGlzTW9iaWxlKT8nb3Zlcic6J3NpZGUnXCJcbiAgICAgICAgICAgICAgICAgIFtvcGVuZWRdPVwiaXNPcGVuXCI+XG4gICAgICAgIDxuZy1jb250ZW50IHNlbGVjdD1cIltzaWRlTmF2VG9wXVwiPjwvbmctY29udGVudD5cbiAgICAgICAgPG1hdC1uYXYtbGlzdD5cbiAgICAgICAgICA8ZGl2ICpuZ0Zvcj1cImxldCBpdGVtIG9mIGl0ZW1zXCI+XG4gICAgICAgICAgICA8aW90LXNpbmdsZS1saXN0LWVudHJ5XG4gICAgICAgICAgICAgIFtlbnRyeV09XCJpdGVtXCJcbiAgICAgICAgICAgICAgW3BhcmFtc109XCJwYXJhbU1hcFwiXG4gICAgICAgICAgICAgIFtwYXJhbXNFeHRyYV09XCJwYXJhbXNcIlxuICAgICAgICAgICAgICAqbmdJZj1cIiFpdGVtLml0ZW1zXCJcbiAgICAgICAgICAgID48L2lvdC1zaW5nbGUtbGlzdC1lbnRyeT5cbiAgICAgICAgICAgIDxpb3QtY29tcG9zaXRlLWxpc3QtZW50cnlcbiAgICAgICAgICAgICAgW2VudHJ5XT1cIml0ZW1cIlxuICAgICAgICAgICAgICBbcGFyYW1zXT1cInBhcmFtTWFwXCJcbiAgICAgICAgICAgICAgW3BhcmFtc0V4dHJhXT1cInBhcmFtc1wiXG4gICAgICAgICAgICAgICpuZ0lmPVwiaXRlbS5pdGVtc1wiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICA8L2lvdC1jb21wb3NpdGUtbGlzdC1lbnRyeT5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9tYXQtbmF2LWxpc3Q+XG4gICAgICA8L21hdC1kcmF3ZXI+XG4gICAgICA8bmctY29udGVudD48L25nLWNvbnRlbnQ+XG4gICAgICA8cm91dGVyLW91dGxldD48L3JvdXRlci1vdXRsZXQ+XG4gICAgPC9tYXQtZHJhd2VyLWNvbnRhaW5lcj5cblxuICBgLFxuICBzdHlsZXM6IFtcbiAgICBgXG5cbiAgICBgXG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgU2lkZU5hdkJhckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95LCBPbkNoYW5nZXMge1xuICBASW5wdXQoKSBiZWxvd1RvcE5hdiA9IHRydWU7XG4gIEBJbnB1dCgpIGlzTW9iaWxlID0gdHJ1ZTtcbiAgQElucHV0KCkgaXNPcGVuID0gdHJ1ZTtcbiAgQElucHV0KCkgZGF0YTtcbiAgQElucHV0KCkgcGFyYW1zOiB7IFtrZXk6IHN0cmluZ106IChzdHJpbmcgfCBudW1iZXIpIH07XG4gIEBJbnB1dCgpIHdpZHRoO1xuXG4gIHB1YmxpYyBwYXJhbU1hcDogUGFyYW1NYXA7XG4gIHB1YmxpYyBpdGVtczogYW55ID0gW107XG5cbiAgcHJpdmF0ZSBzdWI6IFN1YnNjcmlwdGlvbjtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZVxuICApIHtcblxuICB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gICAgaWYgKHRoaXMuZGF0YSkge1xuICAgICAgdGhpcy5pdGVtcyA9IHRoaXMuZGF0YS5pdGVtcztcbiAgICB9XG4gICAgdGhpcy5zdWIgPSB0aGlzLnJvdXRlXG4gICAgICAucGFyYW1NYXBcbiAgICAgIC5zdWJzY3JpYmUoKHBNYXApID0+IHtcbiAgICAgICAgdGhpcy5wYXJhbU1hcCA9IHBNYXA7XG4gICAgICB9KTtcbiAgfVxuXG4gIG5nT25EZXN0cm95KCkge1xuICAgIGlmICh0aGlzLnN1Yikge1xuICAgICAgdGhpcy5zdWIudW5zdWJzY3JpYmUoKTtcbiAgICB9XG5cbiAgfVxuXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpOiB2b2lkIHtcblxuICAgIGlmKGNoYW5nZXMuZGF0YSAmJiB0aGlzLmRhdGEpIHtcbiAgICAgIHRoaXMuaXRlbXMgPSB0aGlzLmRhdGEuaXRlbXM7XG4gICAgfVxuICAgIGlmIChjaGFuZ2VzLnBhcmFtcykge1xuICAgICAgdGhpcy5pdGVtcyA9IFtdLmNvbmNhdCh0aGlzLml0ZW1zKTsgLy9bLi4udGhpcy5pdGVtc107XG4gICAgfVxuICB9XG5cbn1cbiJdfQ==