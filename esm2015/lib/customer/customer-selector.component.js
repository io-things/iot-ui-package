import { Component, Input, Output, EventEmitter } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/button";
import * as i2 from "@angular/material/icon";
import * as i3 from "@angular/common";
export class CustomerSelectorComponent {
    constructor() {
        this.mobile = false;
        this.label = '';
        this.click = new EventEmitter();
    }
    openDialog() {
        // const dialogRef = this.dialog.open(CustomerSelectComponent, {
        //   width: '760px',
        //   data: {mobile: this.mobile}
        // });
    }
    ngOnInit() {
    }
}
CustomerSelectorComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: CustomerSelectorComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
CustomerSelectorComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: CustomerSelectorComponent, selector: "iot-customer-selector", inputs: { mobile: "mobile", label: "label" }, outputs: { click: "click" }, ngImport: i0, template: `
    <div class="outer-switch-button" [ngClass]="{mobile: mobile}" (click)="openDialog()">
      <button
        (click)="click.emit();$event.stopPropagation();"
        mat-button >
        <div class="button-text">{{label}}</div>
      </button>
      <button
        mat-icon-button class="display-button">
        <mat-icon class="icon">unfold_more</mat-icon>
      </button>
    </div>
  `, isInline: true, styles: ["\n        .outer-switch-button {\n          box-sizing: border-box;\n            display: flex;\n            align-items: center;\n            height: 36px;\n            margin: 0 8px;\n            border-radius: 4px;\n            border: 1px solid rgba(255, 255, 255, .42);\n            overflow: hidden;\n            /*margin-bottom: 20px;*/\n        }\n\n\n        .mobile.outer-switch-button {\n            border: 1px solid #dadce0;;\n            margin-left: 20px;\n            width:244px;\n            border-radius: 8px;\n        }\n\n        .outer-switch-button .button-text {\n            font-weight: 300;\n            font-size: 14px;\n            font-family: \"roboto\";\n        }\n\n        .display-button {\n            height: 36px;\n            min-width: 36px;\n            padding: 0;\n            border-left: 1px solid rgba(255, 255, 255, .42);\n            border-radius: 0;\n            color: #fff;\n            margin-right: 0;\n        }\n\n        .icon {\n            position: absolute;\n            top: 6px;\n            left: 8px;\n            font-size: 20px;\n        }\n\n    "], components: [{ type: i1.MatButton, selector: "button[mat-button], button[mat-raised-button], button[mat-icon-button],             button[mat-fab], button[mat-mini-fab], button[mat-stroked-button],             button[mat-flat-button]", inputs: ["disabled", "disableRipple", "color"], exportAs: ["matButton"] }, { type: i2.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }], directives: [{ type: i3.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: CustomerSelectorComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-customer-selector',
                    template: `
    <div class="outer-switch-button" [ngClass]="{mobile: mobile}" (click)="openDialog()">
      <button
        (click)="click.emit();$event.stopPropagation();"
        mat-button >
        <div class="button-text">{{label}}</div>
      </button>
      <button
        mat-icon-button class="display-button">
        <mat-icon class="icon">unfold_more</mat-icon>
      </button>
    </div>
  `,
                    styles: [`
        .outer-switch-button {
          box-sizing: border-box;
            display: flex;
            align-items: center;
            height: 36px;
            margin: 0 8px;
            border-radius: 4px;
            border: 1px solid rgba(255, 255, 255, .42);
            overflow: hidden;
            /*margin-bottom: 20px;*/
        }


        .mobile.outer-switch-button {
            border: 1px solid #dadce0;;
            margin-left: 20px;
            width:244px;
            border-radius: 8px;
        }

        .outer-switch-button .button-text {
            font-weight: 300;
            font-size: 14px;
            font-family: "roboto";
        }

        .display-button {
            height: 36px;
            min-width: 36px;
            padding: 0;
            border-left: 1px solid rgba(255, 255, 255, .42);
            border-radius: 0;
            color: #fff;
            margin-right: 0;
        }

        .icon {
            position: absolute;
            top: 6px;
            left: 8px;
            font-size: 20px;
        }

    `]
                }]
        }], ctorParameters: function () { return []; }, propDecorators: { mobile: [{
                type: Input
            }], label: [{
                type: Input
            }], click: [{
                type: Output
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VzdG9tZXItc2VsZWN0b3IuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvaW90LXVpLWxpYi9zcmMvbGliL2N1c3RvbWVyL2N1c3RvbWVyLXNlbGVjdG9yLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLEtBQUssRUFBVSxNQUFNLEVBQUUsWUFBWSxFQUFDLE1BQU0sZUFBZSxDQUFDOzs7OztBQStEN0UsTUFBTSxPQUFPLHlCQUF5QjtJQUtwQztRQUpTLFdBQU0sR0FBRyxLQUFLLENBQUM7UUFDZixVQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ1YsVUFBSyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7SUFFckIsQ0FBQztJQUVqQixVQUFVO1FBQ1IsZ0VBQWdFO1FBQ2hFLG9CQUFvQjtRQUNwQixnQ0FBZ0M7UUFDaEMsTUFBTTtJQUVSLENBQUM7SUFFRCxRQUFRO0lBQ1IsQ0FBQzs7c0hBaEJVLHlCQUF5QjswR0FBekIseUJBQXlCLHdJQTNEMUI7Ozs7Ozs7Ozs7OztHQVlUOzJGQStDVSx5QkFBeUI7a0JBN0RyQyxTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSx1QkFBdUI7b0JBQ2pDLFFBQVEsRUFBRTs7Ozs7Ozs7Ozs7O0dBWVQ7b0JBQ0QsTUFBTSxFQUFFLENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0tBNENOLENBQUM7aUJBQ0w7MEVBRVUsTUFBTTtzQkFBZCxLQUFLO2dCQUNHLEtBQUs7c0JBQWIsS0FBSztnQkFDSSxLQUFLO3NCQUFkLE1BQU0iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXQsIE9uSW5pdCwgT3V0cHV0LCBFdmVudEVtaXR0ZXJ9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdpb3QtY3VzdG9tZXItc2VsZWN0b3InLFxuICB0ZW1wbGF0ZTogYFxuICAgIDxkaXYgY2xhc3M9XCJvdXRlci1zd2l0Y2gtYnV0dG9uXCIgW25nQ2xhc3NdPVwie21vYmlsZTogbW9iaWxlfVwiIChjbGljayk9XCJvcGVuRGlhbG9nKClcIj5cbiAgICAgIDxidXR0b25cbiAgICAgICAgKGNsaWNrKT1cImNsaWNrLmVtaXQoKTskZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XCJcbiAgICAgICAgbWF0LWJ1dHRvbiA+XG4gICAgICAgIDxkaXYgY2xhc3M9XCJidXR0b24tdGV4dFwiPnt7bGFiZWx9fTwvZGl2PlxuICAgICAgPC9idXR0b24+XG4gICAgICA8YnV0dG9uXG4gICAgICAgIG1hdC1pY29uLWJ1dHRvbiBjbGFzcz1cImRpc3BsYXktYnV0dG9uXCI+XG4gICAgICAgIDxtYXQtaWNvbiBjbGFzcz1cImljb25cIj51bmZvbGRfbW9yZTwvbWF0LWljb24+XG4gICAgICA8L2J1dHRvbj5cbiAgICA8L2Rpdj5cbiAgYCxcbiAgc3R5bGVzOiBbYFxuICAgICAgICAub3V0ZXItc3dpdGNoLWJ1dHRvbiB7XG4gICAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICAgICAgaGVpZ2h0OiAzNnB4O1xuICAgICAgICAgICAgbWFyZ2luOiAwIDhweDtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMjU1LCAyNTUsIDI1NSwgLjQyKTtcbiAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgICAgICAgICAvKm1hcmdpbi1ib3R0b206IDIwcHg7Ki9cbiAgICAgICAgfVxuXG5cbiAgICAgICAgLm1vYmlsZS5vdXRlci1zd2l0Y2gtYnV0dG9uIHtcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNkYWRjZTA7O1xuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDIwcHg7XG4gICAgICAgICAgICB3aWR0aDoyNDRweDtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgICAgICAgfVxuXG4gICAgICAgIC5vdXRlci1zd2l0Y2gtYnV0dG9uIC5idXR0b24tdGV4dCB7XG4gICAgICAgICAgICBmb250LXdlaWdodDogMzAwO1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgZm9udC1mYW1pbHk6IFwicm9ib3RvXCI7XG4gICAgICAgIH1cblxuICAgICAgICAuZGlzcGxheS1idXR0b24ge1xuICAgICAgICAgICAgaGVpZ2h0OiAzNnB4O1xuICAgICAgICAgICAgbWluLXdpZHRoOiAzNnB4O1xuICAgICAgICAgICAgcGFkZGluZzogMDtcbiAgICAgICAgICAgIGJvcmRlci1sZWZ0OiAxcHggc29saWQgcmdiYSgyNTUsIDI1NSwgMjU1LCAuNDIpO1xuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMDtcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiAwO1xuICAgICAgICB9XG5cbiAgICAgICAgLmljb24ge1xuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgICAgdG9wOiA2cHg7XG4gICAgICAgICAgICBsZWZ0OiA4cHg7XG4gICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgIH1cblxuICAgIGBdXG59KVxuZXhwb3J0IGNsYXNzIEN1c3RvbWVyU2VsZWN0b3JDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBASW5wdXQoKSBtb2JpbGUgPSBmYWxzZTtcbiAgQElucHV0KCkgbGFiZWwgPSAnJztcbiAgQE91dHB1dCgpIGNsaWNrID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgb3BlbkRpYWxvZygpOiB2b2lkIHtcbiAgICAvLyBjb25zdCBkaWFsb2dSZWYgPSB0aGlzLmRpYWxvZy5vcGVuKEN1c3RvbWVyU2VsZWN0Q29tcG9uZW50LCB7XG4gICAgLy8gICB3aWR0aDogJzc2MHB4JyxcbiAgICAvLyAgIGRhdGE6IHttb2JpbGU6IHRoaXMubW9iaWxlfVxuICAgIC8vIH0pO1xuXG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgfVxuXG59XG4iXX0=