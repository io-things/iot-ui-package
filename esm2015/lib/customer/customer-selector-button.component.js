import { Component, Input, Output, EventEmitter } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/button";
export class CustomerSelectorButtonComponent {
    constructor() {
        this.label = '';
        this.click = new EventEmitter();
    }
}
CustomerSelectorButtonComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: CustomerSelectorButtonComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
CustomerSelectorButtonComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: CustomerSelectorButtonComponent, selector: "iot-customer-selector-button", inputs: { label: "label" }, outputs: { click: "click" }, ngImport: i0, template: `

    <div class="outer-switch-button"
         (click)="click.emit();$event.stopPropagation();"
         >
      <button mat-button>
        <div class="button-text w-100">{{label}}</div>
      </button>
      <!--            (click)="toggleUnfold()"-->

    </div>
  `, isInline: true, styles: ["\n    .outer-switch-button {\n      box-sizing: border-box;\n      height: 36px;\n      margin: 8px;\n      border: 1px solid #dadce0;\n      margin-left: 20px;\n      width: 244px;\n      border-radius: 8px;\n      overflow: hidden;\n    }\n\n    .outer-switch-button button {\n      width: 100%;\n    }\n\n    .outer-switch-button button .button-text {\n      font-weight: 300;\n      font-size: 14px;\n      text-align: left;\n    }\n\n  "], components: [{ type: i1.MatButton, selector: "button[mat-button], button[mat-raised-button], button[mat-icon-button],             button[mat-fab], button[mat-mini-fab], button[mat-stroked-button],             button[mat-flat-button]", inputs: ["disabled", "disableRipple", "color"], exportAs: ["matButton"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: CustomerSelectorButtonComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-customer-selector-button',
                    template: `

    <div class="outer-switch-button"
         (click)="click.emit();$event.stopPropagation();"
         >
      <button mat-button>
        <div class="button-text w-100">{{label}}</div>
      </button>
      <!--            (click)="toggleUnfold()"-->

    </div>
  `,
                    styles: [`
    .outer-switch-button {
      box-sizing: border-box;
      height: 36px;
      margin: 8px;
      border: 1px solid #dadce0;
      margin-left: 20px;
      width: 244px;
      border-radius: 8px;
      overflow: hidden;
    }

    .outer-switch-button button {
      width: 100%;
    }

    .outer-switch-button button .button-text {
      font-weight: 300;
      font-size: 14px;
      text-align: left;
    }

  `]
                }]
        }], ctorParameters: function () { return []; }, propDecorators: { label: [{
                type: Input
            }], click: [{
                type: Output
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3VzdG9tZXItc2VsZWN0b3ItYnV0dG9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2lvdC11aS1saWIvc3JjL2xpYi9jdXN0b21lci9jdXN0b21lci1zZWxlY3Rvci1idXR0b24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUUsS0FBSyxFQUFVLE1BQU0sRUFBRSxZQUFZLEVBQUMsTUFBTSxlQUFlLENBQUM7OztBQXdDN0UsTUFBTSxPQUFPLCtCQUErQjtJQUkxQztRQUZTLFVBQUssR0FBRyxFQUFFLENBQUM7UUFDVixVQUFLLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztJQUVyQyxDQUFDOzs0SEFMVSwrQkFBK0I7Z0hBQS9CLCtCQUErQiw2SEFwQ2hDOzs7Ozs7Ozs7OztHQVdUOzJGQXlCVSwrQkFBK0I7a0JBdEMzQyxTQUFTO21CQUFDO29CQUNULFFBQVEsRUFBRSw4QkFBOEI7b0JBQ3hDLFFBQVEsRUFBRTs7Ozs7Ozs7Ozs7R0FXVDtvQkFDRCxNQUFNLEVBQUUsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQXNCUixDQUFDO2lCQUNIOzBFQUdVLEtBQUs7c0JBQWIsS0FBSztnQkFDSSxLQUFLO3NCQUFkLE1BQU0iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXQsIE9uSW5pdCwgT3V0cHV0LCBFdmVudEVtaXR0ZXJ9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdpb3QtY3VzdG9tZXItc2VsZWN0b3ItYnV0dG9uJyxcbiAgdGVtcGxhdGU6IGBcblxuICAgIDxkaXYgY2xhc3M9XCJvdXRlci1zd2l0Y2gtYnV0dG9uXCJcbiAgICAgICAgIChjbGljayk9XCJjbGljay5lbWl0KCk7JGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1wiXG4gICAgICAgICA+XG4gICAgICA8YnV0dG9uIG1hdC1idXR0b24+XG4gICAgICAgIDxkaXYgY2xhc3M9XCJidXR0b24tdGV4dCB3LTEwMFwiPnt7bGFiZWx9fTwvZGl2PlxuICAgICAgPC9idXR0b24+XG4gICAgICA8IS0tICAgICAgICAgICAgKGNsaWNrKT1cInRvZ2dsZVVuZm9sZCgpXCItLT5cblxuICAgIDwvZGl2PlxuICBgLFxuICBzdHlsZXM6IFtgXG4gICAgLm91dGVyLXN3aXRjaC1idXR0b24ge1xuICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICAgIGhlaWdodDogMzZweDtcbiAgICAgIG1hcmdpbjogOHB4O1xuICAgICAgYm9yZGVyOiAxcHggc29saWQgI2RhZGNlMDtcbiAgICAgIG1hcmdpbi1sZWZ0OiAyMHB4O1xuICAgICAgd2lkdGg6IDI0NHB4O1xuICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICB9XG5cbiAgICAub3V0ZXItc3dpdGNoLWJ1dHRvbiBidXR0b24ge1xuICAgICAgd2lkdGg6IDEwMCU7XG4gICAgfVxuXG4gICAgLm91dGVyLXN3aXRjaC1idXR0b24gYnV0dG9uIC5idXR0b24tdGV4dCB7XG4gICAgICBmb250LXdlaWdodDogMzAwO1xuICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICB9XG5cbiAgYF1cbn0pXG5leHBvcnQgY2xhc3MgQ3VzdG9tZXJTZWxlY3RvckJ1dHRvbkNvbXBvbmVudCB7XG5cbiAgQElucHV0KCkgbGFiZWwgPSAnJztcbiAgQE91dHB1dCgpIGNsaWNrID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgfVxuXG59XG4iXX0=