import { Component, EventEmitter, Input, Output } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/button";
import * as i2 from "@angular/material/icon";
import * as i3 from "@angular/common";
import * as i4 from "@angular/router";
import * as i5 from "@ngx-translate/core";
export class PageHeaderComponent {
    constructor() {
        this.back = new EventEmitter();
    }
    ngOnInit() {
    }
}
PageHeaderComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: PageHeaderComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
PageHeaderComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: PageHeaderComponent, selector: "iot-page-header", inputs: { title: "title", subTitle: "subTitle", routerLink: "routerLink", backLabel: "backLabel" }, outputs: { back: "back" }, ngImport: i0, template: `
    <div class="header d-flex flex-column align-items-start">
      <a
        *ngIf="routerLink && backLabel"
        mat-button [routerLink]="routerLink">
        <mat-icon>keyboard_backspace</mat-icon>
        {{backLabel | translate }} </a>
      <a
        *ngIf="!routerLink && backLabel"
        mat-button (click)="back.emit()" >
        <mat-icon>keyboard_backspace</mat-icon>
        {{backLabel | translate }} </a>
      <div class="d-flex flex-row w-100">
        <div class="header-text flex-grow-1">{{title | translate}} </div>

        <ng-content select="[button3]"></ng-content>
        <ng-content select="[button2]"></ng-content>
        <ng-content select="[button1]"></ng-content>
      </div>
      <div
        *ngIf="subTitle"
        class="header-sub-text">{{subTitle | translate}} </div>
    </div>
  `, isInline: true, components: [{ type: i1.MatAnchor, selector: "a[mat-button], a[mat-raised-button], a[mat-icon-button], a[mat-fab],             a[mat-mini-fab], a[mat-stroked-button], a[mat-flat-button]", inputs: ["disabled", "disableRipple", "color", "tabIndex"], exportAs: ["matButton", "matAnchor"] }, { type: i2.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }], directives: [{ type: i3.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i4.RouterLinkWithHref, selector: "a[routerLink],area[routerLink]", inputs: ["routerLink", "target", "queryParams", "fragment", "queryParamsHandling", "preserveFragment", "skipLocationChange", "replaceUrl", "state", "relativeTo"] }], pipes: { "translate": i5.TranslatePipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: PageHeaderComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-page-header',
                    template: `
    <div class="header d-flex flex-column align-items-start">
      <a
        *ngIf="routerLink && backLabel"
        mat-button [routerLink]="routerLink">
        <mat-icon>keyboard_backspace</mat-icon>
        {{backLabel | translate }} </a>
      <a
        *ngIf="!routerLink && backLabel"
        mat-button (click)="back.emit()" >
        <mat-icon>keyboard_backspace</mat-icon>
        {{backLabel | translate }} </a>
      <div class="d-flex flex-row w-100">
        <div class="header-text flex-grow-1">{{title | translate}} </div>

        <ng-content select="[button3]"></ng-content>
        <ng-content select="[button2]"></ng-content>
        <ng-content select="[button1]"></ng-content>
      </div>
      <div
        *ngIf="subTitle"
        class="header-sub-text">{{subTitle | translate}} </div>
    </div>
  `
                }]
        }], ctorParameters: function () { return []; }, propDecorators: { title: [{
                type: Input
            }], subTitle: [{
                type: Input
            }], routerLink: [{
                type: Input
            }], backLabel: [{
                type: Input
            }], back: [{
                type: Output
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnZS1oZWFkZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvaW90LXVpLWxpYi9zcmMvbGliL3BhZ2UvcGFnZS1oZWFkZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBVSxNQUFNLEVBQUMsTUFBTSxlQUFlLENBQUM7Ozs7Ozs7QUE2QjdFLE1BQU0sT0FBTyxtQkFBbUI7SUFTOUI7UUFIVSxTQUFJLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztJQUlwQyxDQUFDO0lBRUQsUUFBUTtJQUNSLENBQUM7O2dIQWJVLG1CQUFtQjtvR0FBbkIsbUJBQW1CLHNMQXpCcEI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0dBdUJUOzJGQUVVLG1CQUFtQjtrQkEzQi9CLFNBQVM7bUJBQUM7b0JBQ1QsUUFBUSxFQUFFLGlCQUFpQjtvQkFDM0IsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQXVCVDtpQkFDRjswRUFHVSxLQUFLO3NCQUFiLEtBQUs7Z0JBQ0csUUFBUTtzQkFBaEIsS0FBSztnQkFDRyxVQUFVO3NCQUFsQixLQUFLO2dCQUNHLFNBQVM7c0JBQWpCLEtBQUs7Z0JBQ0ksSUFBSTtzQkFBYixNQUFNIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE9uSW5pdCwgT3V0cHV0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnaW90LXBhZ2UtaGVhZGVyJyxcbiAgdGVtcGxhdGU6IGBcbiAgICA8ZGl2IGNsYXNzPVwiaGVhZGVyIGQtZmxleCBmbGV4LWNvbHVtbiBhbGlnbi1pdGVtcy1zdGFydFwiPlxuICAgICAgPGFcbiAgICAgICAgKm5nSWY9XCJyb3V0ZXJMaW5rICYmIGJhY2tMYWJlbFwiXG4gICAgICAgIG1hdC1idXR0b24gW3JvdXRlckxpbmtdPVwicm91dGVyTGlua1wiPlxuICAgICAgICA8bWF0LWljb24+a2V5Ym9hcmRfYmFja3NwYWNlPC9tYXQtaWNvbj5cbiAgICAgICAge3tiYWNrTGFiZWwgfCB0cmFuc2xhdGUgfX0gPC9hPlxuICAgICAgPGFcbiAgICAgICAgKm5nSWY9XCIhcm91dGVyTGluayAmJiBiYWNrTGFiZWxcIlxuICAgICAgICBtYXQtYnV0dG9uIChjbGljayk9XCJiYWNrLmVtaXQoKVwiID5cbiAgICAgICAgPG1hdC1pY29uPmtleWJvYXJkX2JhY2tzcGFjZTwvbWF0LWljb24+XG4gICAgICAgIHt7YmFja0xhYmVsIHwgdHJhbnNsYXRlIH19IDwvYT5cbiAgICAgIDxkaXYgY2xhc3M9XCJkLWZsZXggZmxleC1yb3cgdy0xMDBcIj5cbiAgICAgICAgPGRpdiBjbGFzcz1cImhlYWRlci10ZXh0IGZsZXgtZ3Jvdy0xXCI+e3t0aXRsZSB8IHRyYW5zbGF0ZX19IDwvZGl2PlxuXG4gICAgICAgIDxuZy1jb250ZW50IHNlbGVjdD1cIltidXR0b24zXVwiPjwvbmctY29udGVudD5cbiAgICAgICAgPG5nLWNvbnRlbnQgc2VsZWN0PVwiW2J1dHRvbjJdXCI+PC9uZy1jb250ZW50PlxuICAgICAgICA8bmctY29udGVudCBzZWxlY3Q9XCJbYnV0dG9uMV1cIj48L25nLWNvbnRlbnQ+XG4gICAgICA8L2Rpdj5cbiAgICAgIDxkaXZcbiAgICAgICAgKm5nSWY9XCJzdWJUaXRsZVwiXG4gICAgICAgIGNsYXNzPVwiaGVhZGVyLXN1Yi10ZXh0XCI+e3tzdWJUaXRsZSB8IHRyYW5zbGF0ZX19IDwvZGl2PlxuICAgIDwvZGl2PlxuICBgXG59KVxuZXhwb3J0IGNsYXNzIFBhZ2VIZWFkZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIEBJbnB1dCgpIHRpdGxlO1xuICBASW5wdXQoKSBzdWJUaXRsZTtcbiAgQElucHV0KCkgcm91dGVyTGluaztcbiAgQElucHV0KCkgYmFja0xhYmVsO1xuICBAT3V0cHV0KCkgYmFjayA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuXG4gIGNvbnN0cnVjdG9yKCkge1xuICB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gIH1cblxufVxuIl19