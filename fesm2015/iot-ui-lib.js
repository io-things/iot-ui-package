import * as i0 from '@angular/core';
import { Injectable, Component, Input, EventEmitter, Output, Directive, ContentChildren, TemplateRef, ViewEncapsulation, ContentChild, Inject, forwardRef, ViewChild, NgModule } from '@angular/core';
import * as i4 from '@angular/material/menu';
import { MatMenuModule } from '@angular/material/menu';
import * as i2 from '@angular/material/card';
import { MatCardModule } from '@angular/material/card';
import * as i3 from '@angular/material/tooltip';
import { MatTooltipModule } from '@angular/material/tooltip';
import * as i1 from '@angular/material/button';
import { MatButtonModule } from '@angular/material/button';
import * as i2$1 from '@angular/material/icon';
import { MatIconModule } from '@angular/material/icon';
import * as i1$2 from '@angular/router';
import { RouterModule } from '@angular/router';
import * as i2$2 from '@angular/material/sidenav';
import { MatSidenavModule } from '@angular/material/sidenav';
import * as i1$1 from '@angular/material/list';
import { MatListModule } from '@angular/material/list';
import * as i4$1 from '@angular/material/core';
import * as i5 from '@ngx-translate/core';
import { TranslateModule } from '@ngx-translate/core';
import * as i4$2 from '@angular/common';
import { CommonModule } from '@angular/common';
import * as i1$3 from '@angular/material/grid-list';
import { MatGridListModule } from '@angular/material/grid-list';
import * as moment from 'moment';
import * as i2$3 from '@angular/material/table';
import { MatTableModule } from '@angular/material/table';
import * as i1$4 from '@angular/material/dialog';
import { MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';
import * as i1$5 from '@angular/cdk/layout';
import { Subscription } from 'rxjs';
import * as i2$4 from '@angular/material/progress-spinner';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import * as i4$3 from '@angular/forms';
import { FormGroup, FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import * as i3$1 from '@angular/material/input';
import { MatInputModule } from '@angular/material/input';
import * as i1$6 from '@angular/material/select';
import { MatSelectModule } from '@angular/material/select';
import * as i1$7 from '@angular/material/form-field';
import * as i2$5 from '@angular/material/datepicker';
import { MatDatepickerModule } from '@angular/material/datepicker';
import * as i1$8 from '@agm/core';
import { AgmCoreModule } from '@agm/core';
import * as i1$9 from '@angular/material/checkbox';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import * as i1$a from '@angular/material/chips';
import { MatChipsModule } from '@angular/material/chips';

class IotUiLibService {
    constructor() { }
}
IotUiLibService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotUiLibService, deps: [], target: i0.ɵɵFactoryTarget.Injectable });
IotUiLibService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotUiLibService, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotUiLibService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return []; } });

class AccountMenuDisplayButtonComponent {
    constructor() {
        this.avatar = '--';
    }
    ngOnInit() {
    }
    ngOnChanges(changes) {
        if (changes.user) {
            this.avatar = '';
            (this.user.displayName || '-').split(' ')
                .filter(w => w != '').forEach(word => this.avatar += word[0]);
        }
    }
}
AccountMenuDisplayButtonComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: AccountMenuDisplayButtonComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
AccountMenuDisplayButtonComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: AccountMenuDisplayButtonComponent, selector: "iot-account-menu-display-button", inputs: { user: "user" }, usesOnChanges: true, ngImport: i0, template: `
<!--    <button mat-button [matMenuTriggerFor]="aboveMenu">Above</button>-->
<!--    <mat-menu #aboveMenu="matMenu" yPosition="above">-->
<!--      <button mat-menu-item>Item 1</button>-->
<!--      <button mat-menu-item>Item 2</button>-->
<!--    </mat-menu>-->
<!--    -->
    <a class="outer" [matMenuTriggerFor]="accountMenu" matTooltip="Account">

      <div class="avatar">
          {{avatar}}
      </div>
      <mat-menu #accountMenu="matMenu" yPosition="below" xPosition="before" >
        <mat-card  class="mat-elevation-z0">
          <mat-card-header>
            <div class="avatar" mat-card-avatar>
              {{avatar}}
            </div>

            <mat-card-title> {{user.displayName}}</mat-card-title>
            <mat-card-subtitle>{{user.email}}</mat-card-subtitle>
          </mat-card-header>

          <mat-card-actions>
            <ng-content select="[button1]"></ng-content>
            <ng-content select="[button2]"></ng-content>
<!--            <button mat-button >{{'HOME.SIGNOUT'}}</button>-->

          </mat-card-actions>
        </mat-card>
      </mat-menu>
    </a>

  `, isInline: true, styles: ["\n      .outer {\n        position: relative;\n      }\n\n      .avatar {\n        border-radius: 50%;\n        display: flex;\n        justify-content: center;\n        align-items: center;\n        color: rgba(0, 0, 0, 0.3);\n\n        right: 0px;\n        width: 40px;\n        height: 40px;\n        background: #E4E9EB 0% 0% no-repeat padding-box;\n        opacity: 1;\n      }\n    "], components: [{ type: i4.MatMenu, selector: "mat-menu", exportAs: ["matMenu"] }, { type: i2.MatCard, selector: "mat-card", exportAs: ["matCard"] }, { type: i2.MatCardHeader, selector: "mat-card-header" }], directives: [{ type: i3.MatTooltip, selector: "[matTooltip]", exportAs: ["matTooltip"] }, { type: i4.MatMenuTrigger, selector: "[mat-menu-trigger-for], [matMenuTriggerFor]", exportAs: ["matMenuTrigger"] }, { type: i2.MatCardAvatar, selector: "[mat-card-avatar], [matCardAvatar]" }, { type: i2.MatCardTitle, selector: "mat-card-title, [mat-card-title], [matCardTitle]" }, { type: i2.MatCardSubtitle, selector: "mat-card-subtitle, [mat-card-subtitle], [matCardSubtitle]" }, { type: i2.MatCardActions, selector: "mat-card-actions", inputs: ["align"], exportAs: ["matCardActions"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: AccountMenuDisplayButtonComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-account-menu-display-button',
                    template: `
<!--    <button mat-button [matMenuTriggerFor]="aboveMenu">Above</button>-->
<!--    <mat-menu #aboveMenu="matMenu" yPosition="above">-->
<!--      <button mat-menu-item>Item 1</button>-->
<!--      <button mat-menu-item>Item 2</button>-->
<!--    </mat-menu>-->
<!--    -->
    <a class="outer" [matMenuTriggerFor]="accountMenu" matTooltip="Account">

      <div class="avatar">
          {{avatar}}
      </div>
      <mat-menu #accountMenu="matMenu" yPosition="below" xPosition="before" >
        <mat-card  class="mat-elevation-z0">
          <mat-card-header>
            <div class="avatar" mat-card-avatar>
              {{avatar}}
            </div>

            <mat-card-title> {{user.displayName}}</mat-card-title>
            <mat-card-subtitle>{{user.email}}</mat-card-subtitle>
          </mat-card-header>

          <mat-card-actions>
            <ng-content select="[button1]"></ng-content>
            <ng-content select="[button2]"></ng-content>
<!--            <button mat-button >{{'HOME.SIGNOUT'}}</button>-->

          </mat-card-actions>
        </mat-card>
      </mat-menu>
    </a>

  `,
                    styles: [
                        `
      .outer {
        position: relative;
      }

      .avatar {
        border-radius: 50%;
        display: flex;
        justify-content: center;
        align-items: center;
        color: rgba(0, 0, 0, 0.3);

        right: 0px;
        width: 40px;
        height: 40px;
        background: #E4E9EB 0% 0% no-repeat padding-box;
        opacity: 1;
      }
    `
                    ]
                }]
        }], ctorParameters: function () { return []; }, propDecorators: { user: [{
                type: Input
            }] } });

class TopNavBarComponent {
    constructor() {
        this.menuClick = new EventEmitter();
    }
    ngOnInit() {
    }
}
TopNavBarComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: TopNavBarComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
TopNavBarComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: TopNavBarComponent, selector: "iot-top-nav-bar", outputs: { menuClick: "menuClick" }, ngImport: i0, template: `
    <nav >
      <button
        mat-icon-button (click)="menuClick.emit($event)">
        <mat-icon>menu</mat-icon>
      </button>
      <ng-content select="[portaltitle]"></ng-content>
      <ng-content select="[customerselect]"></ng-content>
      <div class="flex-spacer"></div>

      <ng-content select="[icon1]"></ng-content>
      <ng-content select="[language]"></ng-content>
      <ng-content select="[account]"></ng-content>
    </nav>
  `, isInline: true, components: [{ type: i1.MatButton, selector: "button[mat-button], button[mat-raised-button], button[mat-icon-button],             button[mat-fab], button[mat-mini-fab], button[mat-stroked-button],             button[mat-flat-button]", inputs: ["disabled", "disableRipple", "color"], exportAs: ["matButton"] }, { type: i2$1.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: TopNavBarComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-top-nav-bar',
                    template: `
    <nav >
      <button
        mat-icon-button (click)="menuClick.emit($event)">
        <mat-icon>menu</mat-icon>
      </button>
      <ng-content select="[portaltitle]"></ng-content>
      <ng-content select="[customerselect]"></ng-content>
      <div class="flex-spacer"></div>

      <ng-content select="[icon1]"></ng-content>
      <ng-content select="[language]"></ng-content>
      <ng-content select="[account]"></ng-content>
    </nav>
  `,
                    styles: []
                }]
        }], ctorParameters: function () { return []; }, propDecorators: { menuClick: [{
                type: Output
            }] } });

class SingleListEntryComponent {
    constructor() {
        this.routerLink = '';
    }
    ngOnChanges(changes) {
        if (this.params) {
            this.ngOnInit();
        }
    }
    ngOnInit() {
        if (this.entry.route && this.params) {
            this.routerLink = this.entry.route;
            this.params.keys.forEach((key) => {
                if (this.entry.route.indexOf(`:${key}`) !== -1) {
                    this.routerLink = this.routerLink.replace(`:${key}`, this.params.get(key));
                }
            });
            Object.keys(this.paramsExtra || {}).forEach((key) => {
                if (this.entry.route.indexOf(`:${key}`) !== -1) {
                    this.routerLink = this.routerLink.replace(`:${key}`, '' + this.paramsExtra[key]);
                }
            });
        }
    }
}
SingleListEntryComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: SingleListEntryComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
SingleListEntryComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: SingleListEntryComponent, selector: "iot-single-list-entry", inputs: { entry: "entry", params: "params", paramsExtra: "paramsExtra" }, usesOnChanges: true, ngImport: i0, template: `
    <mat-list-item
      [routerLink]="routerLink">
      <mat-icon mat-list-icon>{{entry.icon }}</mat-icon>
      <a matLine>{{entry.label | translate}} </a>
    </mat-list-item>
  `, isInline: true, components: [{ type: i1$1.MatListItem, selector: "mat-list-item, a[mat-list-item], button[mat-list-item]", inputs: ["disableRipple", "disabled"], exportAs: ["matListItem"] }, { type: i2$1.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }], directives: [{ type: i1$2.RouterLink, selector: ":not(a):not(area)[routerLink]", inputs: ["routerLink", "queryParams", "fragment", "queryParamsHandling", "preserveFragment", "skipLocationChange", "replaceUrl", "state", "relativeTo"] }, { type: i1$1.MatListIconCssMatStyler, selector: "[mat-list-icon], [matListIcon]" }, { type: i4$1.MatLine, selector: "[mat-line], [matLine]" }], pipes: { "translate": i5.TranslatePipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: SingleListEntryComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-single-list-entry',
                    template: `
    <mat-list-item
      [routerLink]="routerLink">
      <mat-icon mat-list-icon>{{entry.icon }}</mat-icon>
      <a matLine>{{entry.label | translate}} </a>
    </mat-list-item>
  `,
                    styles: []
                }]
        }], ctorParameters: function () { return []; }, propDecorators: { entry: [{
                type: Input
            }], params: [{
                type: Input
            }], paramsExtra: [{
                type: Input
            }] } });

class CompositeListEntryComponent {
    constructor() {
        this.entry = {};
        this.showSettingsMenu = false;
    }
    ngOnInit() {
    }
}
CompositeListEntryComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: CompositeListEntryComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
CompositeListEntryComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: CompositeListEntryComponent, selector: "iot-composite-list-entry", inputs: { entry: "entry", params: "params", paramsExtra: "paramsExtra" }, ngImport: i0, template: `
    <mat-list-item
      (click)="showSettingsMenu = !showSettingsMenu"
    >
      <mat-icon mat-list-icon>{{entry.icon }}</mat-icon>
      <a matLine>{{entry.label | translate}}</a>
      <mat-icon class="menu-button" [ngClass]="{'rotated' : showSettingsMenu}">expand_more
      </mat-icon>
    </mat-list-item>
    <div class="submenu" [ngClass]="{'expanded' : showSettingsMenu}"
         *ngIf="showSettingsMenu">
      <iot-single-list-entry
        *ngFor="let item of entry.items"
        [entry]="item"
        [params]="params"
        [paramsExtra]="paramsExtra"
      ></iot-single-list-entry>
    </div>
  `, isInline: true, styles: ["\n    .menu-button {\n      transition: 300ms ease-in-out;\n      transform: rotate(0deg);\n    }\n\n    .menu-button.rotated {\n      transform: rotate(180deg);\n    }\n\n    .submenu {\n      overflow-y: hidden;\n      height: 0px;\n      transition: transform 300ms ease;\n      transform: scaleY(0);\n      transform-origin: top;\n      padding-left: 30px;\n    }\n\n    .submenu.expanded {\n      transform: scaleY(1);\n      height: unset;\n    }\n  "], components: [{ type: i1$1.MatListItem, selector: "mat-list-item, a[mat-list-item], button[mat-list-item]", inputs: ["disableRipple", "disabled"], exportAs: ["matListItem"] }, { type: i2$1.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }, { type: SingleListEntryComponent, selector: "iot-single-list-entry", inputs: ["entry", "params", "paramsExtra"] }], directives: [{ type: i1$1.MatListIconCssMatStyler, selector: "[mat-list-icon], [matListIcon]" }, { type: i4$1.MatLine, selector: "[mat-line], [matLine]" }, { type: i4$2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i4$2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i4$2.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }], pipes: { "translate": i5.TranslatePipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: CompositeListEntryComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-composite-list-entry',
                    template: `
    <mat-list-item
      (click)="showSettingsMenu = !showSettingsMenu"
    >
      <mat-icon mat-list-icon>{{entry.icon }}</mat-icon>
      <a matLine>{{entry.label | translate}}</a>
      <mat-icon class="menu-button" [ngClass]="{'rotated' : showSettingsMenu}">expand_more
      </mat-icon>
    </mat-list-item>
    <div class="submenu" [ngClass]="{'expanded' : showSettingsMenu}"
         *ngIf="showSettingsMenu">
      <iot-single-list-entry
        *ngFor="let item of entry.items"
        [entry]="item"
        [params]="params"
        [paramsExtra]="paramsExtra"
      ></iot-single-list-entry>
    </div>
  `,
                    styles: [`
    .menu-button {
      transition: 300ms ease-in-out;
      transform: rotate(0deg);
    }

    .menu-button.rotated {
      transform: rotate(180deg);
    }

    .submenu {
      overflow-y: hidden;
      height: 0px;
      transition: transform 300ms ease;
      transform: scaleY(0);
      transform-origin: top;
      padding-left: 30px;
    }

    .submenu.expanded {
      transform: scaleY(1);
      height: unset;
    }
  `
                    ]
                }]
        }], ctorParameters: function () { return []; }, propDecorators: { entry: [{
                type: Input
            }], params: [{
                type: Input
            }], paramsExtra: [{
                type: Input
            }] } });

class SideNavBarComponent {
    constructor(route) {
        this.route = route;
        this.belowTopNav = true;
        this.isMobile = true;
        this.isOpen = true;
        this.items = [];
    }
    ngOnInit() {
        if (this.data) {
            this.items = this.data.items;
        }
        this.sub = this.route
            .paramMap
            .subscribe((pMap) => {
            this.paramMap = pMap;
        });
    }
    ngOnDestroy() {
        if (this.sub) {
            this.sub.unsubscribe();
        }
    }
    ngOnChanges(changes) {
        if (changes.data && this.data) {
            this.items = this.data.items;
        }
        if (changes.params) {
            this.items = [].concat(this.items); //[...this.items];
        }
    }
}
SideNavBarComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: SideNavBarComponent, deps: [{ token: i1$2.ActivatedRoute }], target: i0.ɵɵFactoryTarget.Component });
SideNavBarComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: SideNavBarComponent, selector: "iot-side-nav-bar", inputs: { belowTopNav: "belowTopNav", isMobile: "isMobile", isOpen: "isOpen", data: "data", params: "params", width: "width" }, usesOnChanges: true, ngImport: i0, template: `
    <mat-drawer-container
      autosize [hasBackdrop]="false"
    >
      <mat-drawer
        [ngStyle]="{'width': width == undefined?'unset':width}"
        #drawer [mode]="(isMobile)?'over':'side'"
                  [opened]="isOpen">
        <ng-content select="[sideNavTop]"></ng-content>
        <mat-nav-list>
          <div *ngFor="let item of items">
            <iot-single-list-entry
              [entry]="item"
              [params]="paramMap"
              [paramsExtra]="params"
              *ngIf="!item.items"
            ></iot-single-list-entry>
            <iot-composite-list-entry
              [entry]="item"
              [params]="paramMap"
              [paramsExtra]="params"
              *ngIf="item.items"
            >
            </iot-composite-list-entry>
          </div>
        </mat-nav-list>
      </mat-drawer>
      <ng-content></ng-content>
      <router-outlet></router-outlet>
    </mat-drawer-container>

  `, isInline: true, styles: ["\n\n    "], components: [{ type: i2$2.MatDrawerContainer, selector: "mat-drawer-container", inputs: ["autosize", "hasBackdrop"], outputs: ["backdropClick"], exportAs: ["matDrawerContainer"] }, { type: i2$2.MatDrawer, selector: "mat-drawer", inputs: ["position", "mode", "disableClose", "autoFocus", "opened"], outputs: ["openedChange", "opened", "openedStart", "closed", "closedStart", "positionChanged"], exportAs: ["matDrawer"] }, { type: i1$1.MatNavList, selector: "mat-nav-list", inputs: ["disableRipple", "disabled"], exportAs: ["matNavList"] }, { type: SingleListEntryComponent, selector: "iot-single-list-entry", inputs: ["entry", "params", "paramsExtra"] }, { type: CompositeListEntryComponent, selector: "iot-composite-list-entry", inputs: ["entry", "params", "paramsExtra"] }], directives: [{ type: i4$2.NgStyle, selector: "[ngStyle]", inputs: ["ngStyle"] }, { type: i4$2.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { type: i4$2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i1$2.RouterOutlet, selector: "router-outlet", outputs: ["activate", "deactivate"], exportAs: ["outlet"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: SideNavBarComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-side-nav-bar',
                    template: `
    <mat-drawer-container
      autosize [hasBackdrop]="false"
    >
      <mat-drawer
        [ngStyle]="{'width': width == undefined?'unset':width}"
        #drawer [mode]="(isMobile)?'over':'side'"
                  [opened]="isOpen">
        <ng-content select="[sideNavTop]"></ng-content>
        <mat-nav-list>
          <div *ngFor="let item of items">
            <iot-single-list-entry
              [entry]="item"
              [params]="paramMap"
              [paramsExtra]="params"
              *ngIf="!item.items"
            ></iot-single-list-entry>
            <iot-composite-list-entry
              [entry]="item"
              [params]="paramMap"
              [paramsExtra]="params"
              *ngIf="item.items"
            >
            </iot-composite-list-entry>
          </div>
        </mat-nav-list>
      </mat-drawer>
      <ng-content></ng-content>
      <router-outlet></router-outlet>
    </mat-drawer-container>

  `,
                    styles: [
                        `

    `
                    ]
                }]
        }], ctorParameters: function () { return [{ type: i1$2.ActivatedRoute }]; }, propDecorators: { belowTopNav: [{
                type: Input
            }], isMobile: [{
                type: Input
            }], isOpen: [{
                type: Input
            }], data: [{
                type: Input
            }], params: [{
                type: Input
            }], width: [{
                type: Input
            }] } });

class CustomerSelectorComponent {
    constructor() {
        this.mobile = false;
        this.label = '';
        this.click = new EventEmitter();
    }
    openDialog() {
        // const dialogRef = this.dialog.open(CustomerSelectComponent, {
        //   width: '760px',
        //   data: {mobile: this.mobile}
        // });
    }
    ngOnInit() {
    }
}
CustomerSelectorComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: CustomerSelectorComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
CustomerSelectorComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: CustomerSelectorComponent, selector: "iot-customer-selector", inputs: { mobile: "mobile", label: "label" }, outputs: { click: "click" }, ngImport: i0, template: `
    <div class="outer-switch-button" [ngClass]="{mobile: mobile}" (click)="openDialog()">
      <button
        (click)="click.emit();$event.stopPropagation();"
        mat-button >
        <div class="button-text">{{label}}</div>
      </button>
      <button
        mat-icon-button class="display-button">
        <mat-icon class="icon">unfold_more</mat-icon>
      </button>
    </div>
  `, isInline: true, styles: ["\n        .outer-switch-button {\n          box-sizing: border-box;\n            display: flex;\n            align-items: center;\n            height: 36px;\n            margin: 0 8px;\n            border-radius: 4px;\n            border: 1px solid rgba(255, 255, 255, .42);\n            overflow: hidden;\n            /*margin-bottom: 20px;*/\n        }\n\n\n        .mobile.outer-switch-button {\n            border: 1px solid #dadce0;;\n            margin-left: 20px;\n            width:244px;\n            border-radius: 8px;\n        }\n\n        .outer-switch-button .button-text {\n            font-weight: 300;\n            font-size: 14px;\n            font-family: \"roboto\";\n        }\n\n        .display-button {\n            height: 36px;\n            min-width: 36px;\n            padding: 0;\n            border-left: 1px solid rgba(255, 255, 255, .42);\n            border-radius: 0;\n            color: #fff;\n            margin-right: 0;\n        }\n\n        .icon {\n            position: absolute;\n            top: 6px;\n            left: 8px;\n            font-size: 20px;\n        }\n\n    "], components: [{ type: i1.MatButton, selector: "button[mat-button], button[mat-raised-button], button[mat-icon-button],             button[mat-fab], button[mat-mini-fab], button[mat-stroked-button],             button[mat-flat-button]", inputs: ["disabled", "disableRipple", "color"], exportAs: ["matButton"] }, { type: i2$1.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }], directives: [{ type: i4$2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: CustomerSelectorComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-customer-selector',
                    template: `
    <div class="outer-switch-button" [ngClass]="{mobile: mobile}" (click)="openDialog()">
      <button
        (click)="click.emit();$event.stopPropagation();"
        mat-button >
        <div class="button-text">{{label}}</div>
      </button>
      <button
        mat-icon-button class="display-button">
        <mat-icon class="icon">unfold_more</mat-icon>
      </button>
    </div>
  `,
                    styles: [`
        .outer-switch-button {
          box-sizing: border-box;
            display: flex;
            align-items: center;
            height: 36px;
            margin: 0 8px;
            border-radius: 4px;
            border: 1px solid rgba(255, 255, 255, .42);
            overflow: hidden;
            /*margin-bottom: 20px;*/
        }


        .mobile.outer-switch-button {
            border: 1px solid #dadce0;;
            margin-left: 20px;
            width:244px;
            border-radius: 8px;
        }

        .outer-switch-button .button-text {
            font-weight: 300;
            font-size: 14px;
            font-family: "roboto";
        }

        .display-button {
            height: 36px;
            min-width: 36px;
            padding: 0;
            border-left: 1px solid rgba(255, 255, 255, .42);
            border-radius: 0;
            color: #fff;
            margin-right: 0;
        }

        .icon {
            position: absolute;
            top: 6px;
            left: 8px;
            font-size: 20px;
        }

    `]
                }]
        }], ctorParameters: function () { return []; }, propDecorators: { mobile: [{
                type: Input
            }], label: [{
                type: Input
            }], click: [{
                type: Output
            }] } });

class CustomerSelectorButtonComponent {
    constructor() {
        this.label = '';
        this.click = new EventEmitter();
    }
}
CustomerSelectorButtonComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: CustomerSelectorButtonComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
CustomerSelectorButtonComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: CustomerSelectorButtonComponent, selector: "iot-customer-selector-button", inputs: { label: "label" }, outputs: { click: "click" }, ngImport: i0, template: `

    <div class="outer-switch-button"
         (click)="click.emit();$event.stopPropagation();"
         >
      <button mat-button>
        <div class="button-text w-100">{{label}}</div>
      </button>
      <!--            (click)="toggleUnfold()"-->

    </div>
  `, isInline: true, styles: ["\n    .outer-switch-button {\n      box-sizing: border-box;\n      height: 36px;\n      margin: 8px;\n      border: 1px solid #dadce0;\n      margin-left: 20px;\n      width: 244px;\n      border-radius: 8px;\n      overflow: hidden;\n    }\n\n    .outer-switch-button button {\n      width: 100%;\n    }\n\n    .outer-switch-button button .button-text {\n      font-weight: 300;\n      font-size: 14px;\n      text-align: left;\n    }\n\n  "], components: [{ type: i1.MatButton, selector: "button[mat-button], button[mat-raised-button], button[mat-icon-button],             button[mat-fab], button[mat-mini-fab], button[mat-stroked-button],             button[mat-flat-button]", inputs: ["disabled", "disableRipple", "color"], exportAs: ["matButton"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: CustomerSelectorButtonComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-customer-selector-button',
                    template: `

    <div class="outer-switch-button"
         (click)="click.emit();$event.stopPropagation();"
         >
      <button mat-button>
        <div class="button-text w-100">{{label}}</div>
      </button>
      <!--            (click)="toggleUnfold()"-->

    </div>
  `,
                    styles: [`
    .outer-switch-button {
      box-sizing: border-box;
      height: 36px;
      margin: 8px;
      border: 1px solid #dadce0;
      margin-left: 20px;
      width: 244px;
      border-radius: 8px;
      overflow: hidden;
    }

    .outer-switch-button button {
      width: 100%;
    }

    .outer-switch-button button .button-text {
      font-weight: 300;
      font-size: 14px;
      text-align: left;
    }

  `]
                }]
        }], ctorParameters: function () { return []; }, propDecorators: { label: [{
                type: Input
            }], click: [{
                type: Output
            }] } });

class LanguageSelectorComponent {
    constructor(translate) {
        this.translate = translate;
        this.lang = 'en';
        this.langList = [];
        this.setLang = new EventEmitter();
        translate.setDefaultLang(this.lang);
    }
    ngOnInit() {
    }
    selectLang(langKey) {
        this.translate.use(langKey);
        this.setLang.emit(langKey);
    }
    ngOnChanges(changes) {
        if (changes.lang) {
            this.translate.setDefaultLang(changes.lang.currentValue);
        }
    }
}
LanguageSelectorComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: LanguageSelectorComponent, deps: [{ token: i5.TranslateService }], target: i0.ɵɵFactoryTarget.Component });
LanguageSelectorComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: LanguageSelectorComponent, selector: "iot-language-selector", inputs: { lang: "lang", langList: "langList" }, outputs: { setLang: "setLang" }, usesOnChanges: true, ngImport: i0, template: `
    <button mat-icon-button
            [mat-menu-trigger-for]="themeMenu"
            matTooltip="Select a language!"
            tabindex="-1">
      <mat-icon>language</mat-icon>
    </button>


    <mat-menu #themeMenu="matMenu" x-position="before">
      <button
        *ngFor="let langEntry of langList"
        mat-menu-item
        (click)="selectLang(langEntry.key)">
        {{langEntry.name | translate}}
      </button>
      <!--      <button mat-menu-item-->
      <!--              (click)="selectLang('nl')">-->
      <!--        {{'CONFIG.LANG.NL'}}-->
      <!--      </button>-->
    </mat-menu>
  `, isInline: true, components: [{ type: i1.MatButton, selector: "button[mat-button], button[mat-raised-button], button[mat-icon-button],             button[mat-fab], button[mat-mini-fab], button[mat-stroked-button],             button[mat-flat-button]", inputs: ["disabled", "disableRipple", "color"], exportAs: ["matButton"] }, { type: i2$1.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }, { type: i4.MatMenu, selector: "mat-menu", exportAs: ["matMenu"] }, { type: i4.MatMenuItem, selector: "[mat-menu-item]", inputs: ["disabled", "disableRipple", "role"], exportAs: ["matMenuItem"] }], directives: [{ type: i3.MatTooltip, selector: "[matTooltip]", exportAs: ["matTooltip"] }, { type: i4.MatMenuTrigger, selector: "[mat-menu-trigger-for], [matMenuTriggerFor]", exportAs: ["matMenuTrigger"] }, { type: i4$2.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }], pipes: { "translate": i5.TranslatePipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: LanguageSelectorComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-language-selector',
                    template: `
    <button mat-icon-button
            [mat-menu-trigger-for]="themeMenu"
            matTooltip="Select a language!"
            tabindex="-1">
      <mat-icon>language</mat-icon>
    </button>


    <mat-menu #themeMenu="matMenu" x-position="before">
      <button
        *ngFor="let langEntry of langList"
        mat-menu-item
        (click)="selectLang(langEntry.key)">
        {{langEntry.name | translate}}
      </button>
      <!--      <button mat-menu-item-->
      <!--              (click)="selectLang('nl')">-->
      <!--        {{'CONFIG.LANG.NL'}}-->
      <!--      </button>-->
    </mat-menu>
  `,
                    styles: []
                }]
        }], ctorParameters: function () { return [{ type: i5.TranslateService }]; }, propDecorators: { lang: [{
                type: Input
            }], langList: [{
                type: Input
            }], setLang: [{
                type: Output
            }] } });

class DashboardMediaQueryComponent {
    constructor() {
        this.offsetSize = 192;
        this.small = matchMedia(`(max-width: 700px)`);
        this.medium = matchMedia(`(max-width: 1000px)`);
        this.large = matchMedia(`(min-width: 1700px)`);
        this.absoluteStyle = {
            'width': "400px"
        };
    }
    setMediaQueries() {
        if (!!this.smallEventListener) {
            if (this.small.removeEventListener) {
                this.small.removeEventListener("change", this.smallEventListener);
            }
            else {
                this.small.removeListener(this.smallEventListener);
            }
        }
        if (!!this.mediumEventListener) {
            if (this.medium.removeEventListener) {
                this.medium.removeEventListener("change", this.mediumEventListener);
            }
            else {
                this.medium.removeListener(this.mediumEventListener);
            }
        }
        if (!!this.bigEventListener) {
            if (this.large.removeEventListener) {
                this.large.removeEventListener("change", this.bigEventListener);
            }
            else {
                this.large.removeListener(this.bigEventListener);
            }
        }
        const smallOffset = this.offsetSize + 656 + 26;
        const mediumOffset = this.offsetSize + 656 + 26 + 16 + 320;
        const bigOffset = mediumOffset + 16 + 320;
        this.small = matchMedia(`(max-width: ${smallOffset}px)`);
        this.medium = matchMedia(`(max-width: ${mediumOffset}px)`);
        this.large = matchMedia(`(min-width: ${bigOffset}px)`);
        this.smallEventListener = (e) => {
            if (e.matches) {
                this.match1Columns();
            }
            else {
                if (this.medium.matches) {
                    this.match2Columns();
                }
                else if (this.large.matches) {
                    this.match4Columns();
                }
                else {
                    // console.log(`not small, not wide` + this.large.media)
                    this.match3Columns();
                }
                // console.log(`This is a not small screen — more than ${smallOffset} wide.`+e.media)
            }
        };
        if (!!this.smallEventListener) {
            // console.log("before adding ", this.smallEventListener);
            if (this.small.addEventListener) {
                this.small.addEventListener("change", this.smallEventListener);
            }
            else {
                this.small.addListener(this.smallEventListener);
            }
        }
        this.mediumEventListener = (e) => {
            // console.log("medium");
            if (e.matches) {
                if (this.small.matches) {
                    this.match1Columns();
                }
                else {
                    this.match2Columns();
                }
            }
            else {
                if (this.large.matches) {
                    this.match4Columns();
                }
                else {
                    // console.log(`not small, not wide` + this.large.media)
                    this.match3Columns();
                }
            }
        };
        if (!!this.mediumEventListener) {
            // console.log("before adding ", this.smallEventListener);
            if (this.medium.addEventListener) {
                this.medium.addEventListener("change", this.mediumEventListener);
            }
            else {
                this.medium.addListener(this.mediumEventListener);
            }
        }
        // console.log("---- smallE ", this.smallEventListener);
        this.bigEventListener = (e) => {
            // console.log("big");
            if (e.matches) {
                // console.log(`exact wide match`+ e.media)
                if (this.large.matches) {
                    this.match4Columns();
                }
                else {
                    this.match3Columns();
                }
            }
            else {
                if (this.small.matches) {
                    // console.log(`not wide, is small`+ this.small.media)
                    this.match1Columns();
                }
                else if (this.medium.matches) {
                    this.match2Columns();
                }
                else {
                    // console.log(`not wide, not small`+ this.small.media)
                    this.match3Columns();
                }
            }
        };
        if (!!this.bigEventListener) {
            if (this.large.addEventListener) {
                this.large.addEventListener("change", this.bigEventListener);
            }
            else {
                this.large.addListener(this.bigEventListener);
            }
        }
        if (this.small.matches) {
            this.match1Columns();
        }
        else if (this.medium.matches) {
            this.match2Columns();
        }
        else if (this.large.matches) {
            this.match4Columns();
        }
        else {
            this.match3Columns();
        }
    }
    match1Columns() {
        this.cols = 1;
        this.absoluteStyle['width'] = '331px';
    }
    match2Columns() {
        this.cols = 2;
        this.absoluteStyle['width'] = '667px';
    }
    match3Columns() {
        this.cols = 3;
        this.absoluteStyle['width'] = '1003px';
    }
    match4Columns() {
        this.cols = 4;
        this.absoluteStyle['width'] = '1339px';
    }
    ngOnInit() {
        this.setMediaQueries();
    }
    ngOnChanges(changes) {
        this.setMediaQueries();
    }
}
DashboardMediaQueryComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: DashboardMediaQueryComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
DashboardMediaQueryComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: DashboardMediaQueryComponent, selector: "app-dashboard-grid", inputs: { offsetSize: "offsetSize" }, usesOnChanges: true, ngImport: i0, template: '', isInline: true });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: DashboardMediaQueryComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'app-dashboard-grid',
                    template: '',
                    styles: []
                }]
        }], ctorParameters: function () { return []; }, propDecorators: { offsetSize: [{
                type: Input
            }] } });

class DashboardCard {
    constructor(elementRef, templateRef) {
        this.elementRef = elementRef;
        this.templateRef = templateRef;
        this.cols = [0, 1, 2, 3, 4];
    }
}
DashboardCard.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: DashboardCard, deps: [{ token: i0.ElementRef }, { token: i0.TemplateRef }], target: i0.ɵɵFactoryTarget.Directive });
DashboardCard.ɵdir = i0.ɵɵngDeclareDirective({ minVersion: "12.0.0", version: "12.2.6", type: DashboardCard, selector: "[iotCard]", inputs: { cardId: "cardId", colspan: "colspan" }, ngImport: i0 });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: DashboardCard, decorators: [{
            type: Directive,
            args: [{ selector: '[iotCard]' }]
        }], ctorParameters: function () { return [{ type: i0.ElementRef }, { type: i0.TemplateRef }]; }, propDecorators: { cardId: [{
                type: Input
            }], colspan: [{
                type: Input
            }] } });
class DashboardGridComponent extends DashboardMediaQueryComponent {
    constructor() {
        super(...arguments);
        this.rowHeight = '55px';
        this._col = [[], [], [], [], []];
        this._templateMap = {};
        this._cardMap = {};
        this.row = [1, 2];
    }
    ngAfterContentInit() {
        this.topLevelPanes.forEach(card => {
            this._templateMap[card.cardId] = card.templateRef;
            this._cardMap[card.cardId] = card;
            if (card.colspan && card.colspan.indexOf(',') != -1) {
                let i = 1;
                card.colspan.split(',').forEach((nAsString) => {
                    this._cardMap[card.cardId].cols[i++] = Number.parseInt(nAsString, 10);
                });
            }
            else if (card.colspan) {
                const defaultColspan = Number.parseInt(card.colspan, 10);
                this._cardMap[card.cardId].cols[1] = defaultColspan;
                this._cardMap[card.cardId].cols[2] = defaultColspan;
                this._cardMap[card.cardId].cols[3] = defaultColspan;
                this._cardMap[card.cardId].cols[4] = defaultColspan;
            }
        });
    }
    ngOnInit() {
        if (!this.col1) {
            this._col[1] = [];
        }
        else {
            this._col[1] = this.col1.split(",");
        }
        if (!this.col2) {
            this._col[2] = [];
        }
        else {
            this._col[2] = this.col2.split(",");
        }
        if (!this.col3) {
            this._col[3] = [];
        }
        else {
            this._col[3] = this.col3.split(",");
        }
        if (!this.col4) {
            this._col[4] = [];
        }
        else {
            this._col[4] = this.col4.split(",");
        }
    }
}
DashboardGridComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: DashboardGridComponent, deps: null, target: i0.ɵɵFactoryTarget.Component });
DashboardGridComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: DashboardGridComponent, selector: "iot-dashboard", inputs: { rowHeight: "rowHeight", col1: "col1", col2: "col2", col3: "col3", col4: "col4" }, queries: [{ propertyName: "topLevelPanes", predicate: DashboardCard }], usesInheritance: true, ngImport: i0, template: `
    <div class="centered-content">
      <mat-grid-list
        [cols]="cols" [rowHeight]="rowHeight" [ngStyle]="absoluteStyle">

        <mat-grid-tile
          *ngFor="let key of _col[cols]"
          [rowspan]="10"

          [colspan]="_cardMap[key].cols[cols] "
        >

          <ng-container [ngTemplateOutlet]="(_templateMap[key] )"
                        [ngTemplateOutletContext]="{
                        columnsAvailable: _cardMap[key].cols[cols] }"
          >
<!--            _cardMap[key].colspan ?  (_cardMap[key].colspan < 0? cols + _cardMap[key].colspan : _cardMap[key].colspan  ) : cols-->
          </ng-container>
        </mat-grid-tile>
      </mat-grid-list>


      <!--            <div style="width: 100px">test</div>-->
    </div>
  `, isInline: true, components: [{ type: i1$3.MatGridList, selector: "mat-grid-list", inputs: ["cols", "gutterSize", "rowHeight"], exportAs: ["matGridList"] }, { type: i1$3.MatGridTile, selector: "mat-grid-tile", inputs: ["rowspan", "colspan"], exportAs: ["matGridTile"] }], directives: [{ type: i4$2.NgStyle, selector: "[ngStyle]", inputs: ["ngStyle"] }, { type: i4$2.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { type: i4$2.NgTemplateOutlet, selector: "[ngTemplateOutlet]", inputs: ["ngTemplateOutletContext", "ngTemplateOutlet"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: DashboardGridComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-dashboard',
                    template: `
    <div class="centered-content">
      <mat-grid-list
        [cols]="cols" [rowHeight]="rowHeight" [ngStyle]="absoluteStyle">

        <mat-grid-tile
          *ngFor="let key of _col[cols]"
          [rowspan]="10"

          [colspan]="_cardMap[key].cols[cols] "
        >

          <ng-container [ngTemplateOutlet]="(_templateMap[key] )"
                        [ngTemplateOutletContext]="{
                        columnsAvailable: _cardMap[key].cols[cols] }"
          >
<!--            _cardMap[key].colspan ?  (_cardMap[key].colspan < 0? cols + _cardMap[key].colspan : _cardMap[key].colspan  ) : cols-->
          </ng-container>
        </mat-grid-tile>
      </mat-grid-list>


      <!--            <div style="width: 100px">test</div>-->
    </div>
  `
                }]
        }], propDecorators: { topLevelPanes: [{
                type: ContentChildren,
                args: [DashboardCard]
            }], rowHeight: [{
                type: Input
            }], col1: [{
                type: Input
            }], col2: [{
                type: Input
            }], col3: [{
                type: Input
            }], col4: [{
                type: Input
            }] } });

/*
 * Copyright (C) Io-Things, BV - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Stefaan Ternier <stefaan@io-things.eu>, 1/6/2020
 */
class IotCardHeader {
    constructor(elRef, renderer) {
        this.elRef = elRef;
        this.renderer = renderer;
        this.padding = false;
        this.renderer.addClass(this.elRef.nativeElement, 'box-sizing');
        this.renderer.setStyle(this.elRef.nativeElement, 'height', `56px`);
    }
    ngOnInit() {
        if (this.padding) {
            this.renderer.setStyle(this.elRef.nativeElement, 'padding', `16px 16px 0 16px`);
        }
    }
}
IotCardHeader.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotCardHeader, deps: [{ token: i0.ElementRef }, { token: i0.Renderer2 }], target: i0.ɵɵFactoryTarget.Directive });
IotCardHeader.ɵdir = i0.ɵɵngDeclareDirective({ minVersion: "12.0.0", version: "12.2.6", type: IotCardHeader, selector: "[iot-card-header]", inputs: { padding: "padding" }, ngImport: i0 });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotCardHeader, decorators: [{
            type: Directive,
            args: [{ selector: '[iot-card-header]' }]
        }], ctorParameters: function () { return [{ type: i0.ElementRef }, { type: i0.Renderer2 }]; }, propDecorators: { padding: [{
                type: Input
            }] } });
class IotCardContent {
    constructor(elRef, renderer) {
        this.elRef = elRef;
        this.renderer = renderer;
        this.padding = false;
        this.renderer.addClass(this.elRef.nativeElement, 'box-sizing');
        this.renderer.setStyle(this.elRef.nativeElement, 'height', `100%`);
    }
    ngOnInit() {
        if (this.padding) {
            this.renderer.setStyle(this.elRef.nativeElement, 'padding', `0 16px 0px 16px`);
        }
    }
}
IotCardContent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotCardContent, deps: [{ token: i0.ElementRef }, { token: i0.Renderer2 }], target: i0.ɵɵFactoryTarget.Directive });
IotCardContent.ɵdir = i0.ɵɵngDeclareDirective({ minVersion: "12.0.0", version: "12.2.6", type: IotCardContent, selector: "[iot-card-content]", inputs: { padding: "padding" }, ngImport: i0 });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotCardContent, decorators: [{
            type: Directive,
            args: [{ selector: '[iot-card-content]' }]
        }], ctorParameters: function () { return [{ type: i0.ElementRef }, { type: i0.Renderer2 }]; }, propDecorators: { padding: [{
                type: Input
            }] } });
class IotCardFooter {
    constructor(elRef, renderer) {
        this.elRef = elRef;
        this.renderer = renderer;
        this.padding = false;
        this.renderer.addClass(this.elRef.nativeElement, 'box-sizing');
        this.renderer.setStyle(this.elRef.nativeElement, 'height', `40px`);
    }
    ngOnInit() {
        if (this.padding) {
            this.renderer.setStyle(this.elRef.nativeElement, 'padding', `0 16px 16px 16px`);
        }
    }
}
IotCardFooter.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotCardFooter, deps: [{ token: i0.ElementRef }, { token: i0.Renderer2 }], target: i0.ɵɵFactoryTarget.Directive });
IotCardFooter.ɵdir = i0.ɵɵngDeclareDirective({ minVersion: "12.0.0", version: "12.2.6", type: IotCardFooter, selector: "[iot-card-footer]", inputs: { padding: "padding" }, ngImport: i0 });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotCardFooter, decorators: [{
            type: Directive,
            args: [{ selector: '[iot-card-footer]' }]
        }], ctorParameters: function () { return [{ type: i0.ElementRef }, { type: i0.Renderer2 }]; }, propDecorators: { padding: [{
                type: Input
            }] } });
class DashboardCardComponent {
    constructor() {
        this.colspan = 1;
        this.showHeader = true;
        this.showTitle = true;
        this.height = "500px";
        this.width = "320px";
        this.cardClass = {
            'iotcol-1': false,
            'iotcol-2': false,
            'iotcol-3': false,
            'iotcol-4': false,
        };
    }
    ngOnInit() {
        this.cardClass['iotcol-' + this.colspan] = true;
    }
    ngOnChanges(changes) {
        if (changes.colspan) {
            this.cardClass['iotcol-' + changes.colspan.previousValue] = false;
            this.cardClass['iotcol-' + changes.colspan.currentValue] = true;
        }
    }
}
DashboardCardComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: DashboardCardComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
DashboardCardComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: DashboardCardComponent, selector: "iot-dashboard-card", inputs: { colspan: "colspan", showHeader: "showHeader", outerTitle: "outerTitle", innerTitle: "innerTitle", showTitle: "showTitle", height: "height", width: "width" }, queries: [{ propertyName: "template", first: true, predicate: TemplateRef, descendants: true }], usesOnChanges: true, ngImport: i0, template: `
    <div class="outer-card">

      <div class="iotcard">
        <div
          class="iot-card-outer-title" [ngStyle]="{'width':width}">
          <div *ngIf="outerTitle">{{outerTitle | translate}}</div>
          <div *ngIf="!outerTitle">&nbsp;</div>
        </div>
        <div [ngClass]="cardClass" class="iot-inner-card iot-card-scss" [ngStyle]="{'height':height}">

          <div class="custheader iot-card-title">
            <ng-content select="[iot-card-header]"></ng-content>
          </div>
          <div class="custmiddle">
            <ng-content select="[iot-card-content]"></ng-content>
          </div>
          <div class="custfooter">
            <ng-content select="[iot-card-footer]"></ng-content>
          </div>
        </div>
      </div>
    </div>`, isInline: true, styles: ["\n\n\n    .custheader {\n      -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */\n      -moz-box-sizing: border-box; /* Firefox, other Gecko */\n      box-sizing: border-box;\n    }\n\n    .custmiddle {\n      flex-grow: 1;\n    }\n\n    .custfooter {\n\n    }\n  "], directives: [{ type: i4$2.NgStyle, selector: "[ngStyle]", inputs: ["ngStyle"] }, { type: i4$2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i4$2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }], pipes: { "translate": i5.TranslatePipe }, encapsulation: i0.ViewEncapsulation.None });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: DashboardCardComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-dashboard-card',
                    template: `
    <div class="outer-card">

      <div class="iotcard">
        <div
          class="iot-card-outer-title" [ngStyle]="{'width':width}">
          <div *ngIf="outerTitle">{{outerTitle | translate}}</div>
          <div *ngIf="!outerTitle">&nbsp;</div>
        </div>
        <div [ngClass]="cardClass" class="iot-inner-card iot-card-scss" [ngStyle]="{'height':height}">

          <div class="custheader iot-card-title">
            <ng-content select="[iot-card-header]"></ng-content>
          </div>
          <div class="custmiddle">
            <ng-content select="[iot-card-content]"></ng-content>
          </div>
          <div class="custfooter">
            <ng-content select="[iot-card-footer]"></ng-content>
          </div>
        </div>
      </div>
    </div>`,
                    encapsulation: ViewEncapsulation.None,
                    styles: [`


    .custheader {
      -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
      -moz-box-sizing: border-box; /* Firefox, other Gecko */
      box-sizing: border-box;
    }

    .custmiddle {
      flex-grow: 1;
    }

    .custfooter {

    }
  `]
                }]
        }], ctorParameters: function () { return []; }, propDecorators: { template: [{
                type: ContentChild,
                args: [TemplateRef]
            }], colspan: [{
                type: Input
            }], showHeader: [{
                type: Input
            }], outerTitle: [{
                type: Input
            }], innerTitle: [{
                type: Input
            }], showTitle: [{
                type: Input
            }], height: [{
                type: Input
            }], width: [{
                type: Input
            }] } });

class TimeOfDayCardComponent {
    constructor() {
        this.title = '';
        this.subject = "PIR Bewegingen";
        this.min = 10;
        this.highest = 5;
        this.today = ['', '', '', '', '', '', ''];
    }
    ngOnInit() {
        let now = moment();
        for (let i = 0; i < 7; i++) {
            this.today[i] = now.format("dd");
            now = now.subtract(1, 'days');
        }
    }
    getTooltip(hour, week) {
        return this.data[week][hour] + " " + this.subject;
    }
    getClass(hour, week) {
        let classRet = { 'cell': true };
        if (!this.data) {
            classRet['grey-1'] = true;
        }
        else {
            if (this.data[week][hour] == 0) {
                classRet['grey-1'] = true;
            }
            else if (this.data[week][hour] < this.val2) {
                classRet['blue-1'] = true;
            }
            else if (this.data[week][hour] < this.val3) {
                classRet['blue-2'] = true;
            }
            else if (this.data[week][hour] < this.val4) {
                classRet['blue-3'] = true;
            }
            else if (this.data[week][hour] <= this.val5) {
                classRet['blue-4'] = true;
            }
        }
        return classRet;
    }
    ngOnChanges(changes) {
        this.lowest = null;
        this.highest = null;
        if (this.data != null) {
            this.data.map(row => row.map(cell => {
                if (cell != 0) {
                    if (this.lowest == null)
                        this.lowest = cell;
                    this.lowest = Math.min(this.lowest, cell);
                    this.highest = Math.max(this.highest, cell);
                }
            }));
        }
        if (this.lowest == null) {
            this.lowest = 1;
        }
        this.lowest = Math.floor(this.lowest);
        this.highest = Math.ceil(this.highest);
        this.highest = Math.max(this.highest, this.min);
        if (this.lowest > 10) {
            this.lowest = Math.floor(this.lowest / 10) * 10;
        }
        const diff = this.highest - this.lowest;
        let step = Math.ceil(diff / 4);
        if (step > 10) {
            step = Math.ceil(step / 10) * 10;
        }
        if (step === 0) {
            step = 1;
        }
        this.val1 = this.lowest;
        this.val2 = this.val1 + step;
        this.val3 = this.val2 + step;
        this.val4 = this.val3 + step;
        this.val5 = this.val4 + step;
    }
    showTooltip(evt, test) {
    }
}
TimeOfDayCardComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: TimeOfDayCardComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
TimeOfDayCardComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: TimeOfDayCardComponent, selector: "iot-time-of-day-card", inputs: { title: "title", outerTitle: "outerTitle", subject: "subject", data: "data", min: "min" }, usesOnChanges: true, ngImport: i0, template: `
    <iot-dashboard-card
      class="time-of-day"
      [colspan]="1"
      [outerTitle]="outerTitle">
      <div  iot-card-header padding="true">{{ title | translate}}</div>
      <div  iot-card-content padding="true">
        <div class="ga-time-of-day-chart" #thisDiv>
          <svg width="272" height="404">
            <g class="header"></g>
            <g class="grid-container" transform="translate(0, 0)">
              <g class="x-axis" transform="translate(0, 322)" fill="none" font-size="10" font-family="sans-serif"
                 text-anchor="middle">
                <path class="domain" stroke="currentColor" d="M0.5,6V0.5H232.5V6"></path>
                <g class="tick" opacity="1" transform="translate(16.57142857142856,0)">
                  <line stroke="currentColor" y2="6"></line>
                  <text fill="currentColor" y="9" dy="0.71em" transform="translate(-13.571428571428573, 0)"
                        style="text-anchor: start;">{{today[6]}}
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(49.71428571428571,0)">
                  <line stroke="currentColor" y2="6"></line>
                  <text fill="currentColor" y="9" dy="0.71em" transform="translate(-13.571428571428573, 0)"
                        style="text-anchor: start;">{{today[5]}}
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(82.85714285714285,0)">
                  <line stroke="currentColor" y2="6"></line>
                  <text fill="currentColor" y="9" dy="0.71em" transform="translate(-13.571428571428573, 0)"
                        style="text-anchor: start;">{{today[4]}}
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(116,0)">
                  <line stroke="currentColor" y2="6"></line>
                  <text fill="currentColor" y="9" dy="0.71em" transform="translate(-13.571428571428573, 0)"
                        style="text-anchor: start;">{{today[3]}}
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(149.14285714285714,0)">
                  <line stroke="currentColor" y2="6"></line>
                  <text fill="currentColor" y="9" dy="0.71em" transform="translate(-13.571428571428573, 0)"
                        style="text-anchor: start;">{{today[2]}}
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(182.2857142857143,0)">
                  <line stroke="currentColor" y2="6"></line>
                  <text fill="currentColor" y="9" dy="0.71em" transform="translate(-13.571428571428573, 0)"
                        style="text-anchor: start;">{{today[1]}}
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(215.42857142857147,0)">
                  <line stroke="currentColor" y2="6"></line>
                  <text fill="currentColor" y="9" dy="0.71em" transform="translate(-13.571428571428573, 0)"
                        style="text-anchor: start;">{{today[0]}}
                  </text>
                </g>
              </g>
              <g class="y-axis" transform="translate(232, 0)" fill="none" font-size="10" font-family="sans-serif"
                 text-anchor="start">
                <path class="domain" stroke="currentColor" d="M6,0.5H0.5V324.5H6"></path>
                <g class="tick" opacity="1" transform="translate(0, 6.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">12a.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 33.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">2a.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 60.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">4a.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 87.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">6a.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 114.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">8a.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 141.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">10a.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 168.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">12p.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 195.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">2p.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 222.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">4p.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 249.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">6p.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 276.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">8p.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 303.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">10p.m.
                  </text>
                </g>
              </g>
              <g class="row " *ngFor="let item of [].constructor(24); let i = index;">
                <g [ngClass]="getClass(i, w)"
                   *ngFor="let week of [].constructor(7); let w = index;">
                  <rect
                    [matTooltip]="getTooltip(i,w)"
                    class="rect" width="30.14285659790039"
                    height="10.5" [attr.x]="1.5+(w * 33.15)"
                    [attr.y]="1.5+ (i * 13.5)"
                  ></rect>
                </g>

              </g>

            </g>
            <g class="legend-group" transform="translate(0, 359)">
              <rect height="10" width="55" x="0" y="0" class="legend-block blue-1"></rect>
              <rect height="10" width="55" x="58" y="0" class="legend-block blue-2"></rect>
              <rect height="10" width="55" x="116" y="0" class="legend-block blue-3"></rect>
              <rect height="10" width="55" x="174" y="0" class="legend-block blue-4"></rect>
              <text x="0" y="25" class="legend-text" style="text-anchor: start;">{{val1}}</text>
              <text x="58" y="25" class="legend-text" style="text-anchor: middle;">{{val2}}</text>
              <text x="116" y="25" class="legend-text" style="text-anchor: middle;">{{val3}}</text>
              <text x="174" y="25" class="legend-text" style="text-anchor: middle;">{{val4}}</text>
              <text x="232" y="25" class="legend-text" style="text-anchor: end;">{{val5}}</text>
            </g>
          </svg>
        </div>

      </div>
      <div  iot-card-footer><ng-content select="[iot-card-footer]"></ng-content></div>
    </iot-dashboard-card>
  `, isInline: true, components: [{ type: DashboardCardComponent, selector: "iot-dashboard-card", inputs: ["colspan", "showHeader", "outerTitle", "innerTitle", "showTitle", "height", "width"] }], directives: [{ type: IotCardHeader, selector: "[iot-card-header]", inputs: ["padding"] }, { type: IotCardContent, selector: "[iot-card-content]", inputs: ["padding"] }, { type: i4$2.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { type: i4$2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i3.MatTooltip, selector: "[matTooltip]", exportAs: ["matTooltip"] }, { type: IotCardFooter, selector: "[iot-card-footer]", inputs: ["padding"] }], pipes: { "translate": i5.TranslatePipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: TimeOfDayCardComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-time-of-day-card',
                    template: `
    <iot-dashboard-card
      class="time-of-day"
      [colspan]="1"
      [outerTitle]="outerTitle">
      <div  iot-card-header padding="true">{{ title | translate}}</div>
      <div  iot-card-content padding="true">
        <div class="ga-time-of-day-chart" #thisDiv>
          <svg width="272" height="404">
            <g class="header"></g>
            <g class="grid-container" transform="translate(0, 0)">
              <g class="x-axis" transform="translate(0, 322)" fill="none" font-size="10" font-family="sans-serif"
                 text-anchor="middle">
                <path class="domain" stroke="currentColor" d="M0.5,6V0.5H232.5V6"></path>
                <g class="tick" opacity="1" transform="translate(16.57142857142856,0)">
                  <line stroke="currentColor" y2="6"></line>
                  <text fill="currentColor" y="9" dy="0.71em" transform="translate(-13.571428571428573, 0)"
                        style="text-anchor: start;">{{today[6]}}
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(49.71428571428571,0)">
                  <line stroke="currentColor" y2="6"></line>
                  <text fill="currentColor" y="9" dy="0.71em" transform="translate(-13.571428571428573, 0)"
                        style="text-anchor: start;">{{today[5]}}
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(82.85714285714285,0)">
                  <line stroke="currentColor" y2="6"></line>
                  <text fill="currentColor" y="9" dy="0.71em" transform="translate(-13.571428571428573, 0)"
                        style="text-anchor: start;">{{today[4]}}
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(116,0)">
                  <line stroke="currentColor" y2="6"></line>
                  <text fill="currentColor" y="9" dy="0.71em" transform="translate(-13.571428571428573, 0)"
                        style="text-anchor: start;">{{today[3]}}
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(149.14285714285714,0)">
                  <line stroke="currentColor" y2="6"></line>
                  <text fill="currentColor" y="9" dy="0.71em" transform="translate(-13.571428571428573, 0)"
                        style="text-anchor: start;">{{today[2]}}
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(182.2857142857143,0)">
                  <line stroke="currentColor" y2="6"></line>
                  <text fill="currentColor" y="9" dy="0.71em" transform="translate(-13.571428571428573, 0)"
                        style="text-anchor: start;">{{today[1]}}
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(215.42857142857147,0)">
                  <line stroke="currentColor" y2="6"></line>
                  <text fill="currentColor" y="9" dy="0.71em" transform="translate(-13.571428571428573, 0)"
                        style="text-anchor: start;">{{today[0]}}
                  </text>
                </g>
              </g>
              <g class="y-axis" transform="translate(232, 0)" fill="none" font-size="10" font-family="sans-serif"
                 text-anchor="start">
                <path class="domain" stroke="currentColor" d="M6,0.5H0.5V324.5H6"></path>
                <g class="tick" opacity="1" transform="translate(0, 6.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">12a.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 33.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">2a.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 60.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">4a.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 87.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">6a.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 114.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">8a.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 141.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">10a.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 168.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">12p.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 195.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">2p.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 222.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">4p.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 249.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">6p.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 276.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">8p.m.
                  </text>
                </g>
                <g class="tick" opacity="1" transform="translate(0, 303.75)">
                  <line stroke="currentColor" x2="6"></line>
                  <text fill="currentColor" x="9" dy="0.32em" transform="translate(30, 0)"
                        style="text-anchor: end;">10p.m.
                  </text>
                </g>
              </g>
              <g class="row " *ngFor="let item of [].constructor(24); let i = index;">
                <g [ngClass]="getClass(i, w)"
                   *ngFor="let week of [].constructor(7); let w = index;">
                  <rect
                    [matTooltip]="getTooltip(i,w)"
                    class="rect" width="30.14285659790039"
                    height="10.5" [attr.x]="1.5+(w * 33.15)"
                    [attr.y]="1.5+ (i * 13.5)"
                  ></rect>
                </g>

              </g>

            </g>
            <g class="legend-group" transform="translate(0, 359)">
              <rect height="10" width="55" x="0" y="0" class="legend-block blue-1"></rect>
              <rect height="10" width="55" x="58" y="0" class="legend-block blue-2"></rect>
              <rect height="10" width="55" x="116" y="0" class="legend-block blue-3"></rect>
              <rect height="10" width="55" x="174" y="0" class="legend-block blue-4"></rect>
              <text x="0" y="25" class="legend-text" style="text-anchor: start;">{{val1}}</text>
              <text x="58" y="25" class="legend-text" style="text-anchor: middle;">{{val2}}</text>
              <text x="116" y="25" class="legend-text" style="text-anchor: middle;">{{val3}}</text>
              <text x="174" y="25" class="legend-text" style="text-anchor: middle;">{{val4}}</text>
              <text x="232" y="25" class="legend-text" style="text-anchor: end;">{{val5}}</text>
            </g>
          </svg>
        </div>

      </div>
      <div  iot-card-footer><ng-content select="[iot-card-footer]"></ng-content></div>
    </iot-dashboard-card>
  `
                }]
        }], ctorParameters: function () { return []; }, propDecorators: { title: [{
                type: Input
            }], outerTitle: [{
                type: Input
            }], subject: [{
                type: Input
            }], data: [{
                type: Input
            }], min: [{
                type: Input
            }] } });

const oneday = 24 * 3600000;
const oneMonth = 31 * 24 * 3600000;
class KeyValueCardComponent {
    constructor() {
        this.click = new EventEmitter();
        this.hover = new EventEmitter();
        this.unHover = new EventEmitter();
        this.displayedColumns = ['label', 'value'];
    }
    ngOnInit() {
    }
    getShowShortTime(entries) {
        if (!entries) {
            return false;
        }
        if (!entries.value) {
            return false;
        }
        if (entries.value === 'undefined') {
            return false;
        }
        if (entries.value < (new Date().getTime() - oneday)) {
            return false;
        }
        return true;
    }
    getShowLongTime(entries) {
        if (!entries)
            return false;
        if (!entries.value)
            return false;
        if (entries.value === 'undefined')
            return false;
        if (entries.value < (new Date().getTime() - oneMonth)) {
            return false;
        }
        if (entries.value > (new Date().getTime() - oneday))
            return false;
        return true;
    }
    getShowVeryLongTime(entries) {
        if (!entries)
            return false;
        if (!entries.value)
            return false;
        if (entries.value === 'undefined')
            return false;
        if (entries.value > (new Date().getTime() - oneMonth))
            return false;
        return true;
    }
    getNoTime(entries) {
        if (!entries)
            return true;
        if (!entries.value)
            return true;
        if (entries.value == 0)
            return true;
        return false;
    }
}
KeyValueCardComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: KeyValueCardComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
KeyValueCardComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: KeyValueCardComponent, selector: "iot-key-value-card", inputs: { outerTitle: "outerTitle", title: "title", keyValueData: "keyValueData" }, outputs: { click: "click", hover: "hover", unHover: "unHover" }, ngImport: i0, template: `
    <iot-dashboard-card
      [colspan]="1"
      [outerTitle]="outerTitle">
      <div *ngIf="title" iot-card-header padding="true">{{title |translate}}</div>
      <div  iot-card-content >
          <table mat-table [dataSource]="keyValueData" style="width:100%">

            <ng-container matColumnDef="label">
              <th mat-header-cell *matHeaderCellDef> Item</th>
              <td
                (click)="click.emit(entries)"
                (mouseenter)="hover.emit(entries)"
                (mouseleave)="unHover.emit()"
                class="text " [ngClass]="{warnrow: entries.error}"
                  mat-cell *matCellDef="let entries"> {{entries.label|translate}}</td>
            </ng-container>


            <ng-container matColumnDef="value">
              <th mat-header-cell *matHeaderCellDef> Cost</th>
              <td class="value text warnrow" [ngClass]="{warnrow: entries.error}"  mat-cell *matCellDef="let entries">

                <div *ngIf="!!entries.value && entries.value > 0 && entries.type =='date' && getShowShortTime(entries)">{{entries.value |date:'H:mm'}}</div>
                <div *ngIf="!!entries.value && entries.value > 0 && entries.type =='date' && getShowLongTime(entries)">{{entries.value |date:'MMM d, H:mm' }}</div>
                <div *ngIf="!!entries.value && entries.value > 0 && entries.type =='date' && getShowVeryLongTime(entries)">{{entries.value |date:'shortDate'}}</div>

                <div *ngIf="entries.type =='date' && getNoTime(entries)"> --</div>
                <div *ngIf="entries.type =='string'">{{entries.value }}</div>
                <div *ngIf="entries.type =='link'"><a [href]="entries.link"> {{entries.value }}</a></div>
                <div *ngIf="entries.type =='boolean' && entries.value">{{entries.trueValue | translate}}</div>
                <div *ngIf="entries.type =='boolean' && !entries.value">{{entries.falseValue | translate}}</div>
                <div *ngIf="entries.type =='booleanIcon' && entries.value">
                  <mat-icon>{{entries.trueValue }}</mat-icon>
                </div>
                <div *ngIf="entries.type =='booleanIcon' && !entries.value">
                  <mat-icon>{{entries.falseValue }}</mat-icon>
                </div>
                <div *ngIf="entries.type =='array'">{{entries.values[entries.value] }}</div>
                <div *ngIf="entries.type =='number'">{{entries.value | number: entries.digitsInfo || '1.0-0'}}{{entries.unit }}</div>
              </td>

            </ng-container>
            <tr mat-row *matRowDef="let row; columns: displayedColumns;"></tr>
          </table>
      </div>
    </iot-dashboard-card>
  `, isInline: true, components: [{ type: DashboardCardComponent, selector: "iot-dashboard-card", inputs: ["colspan", "showHeader", "outerTitle", "innerTitle", "showTitle", "height", "width"] }, { type: i2$3.MatTable, selector: "mat-table, table[mat-table]", exportAs: ["matTable"] }, { type: i2$1.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }, { type: i2$3.MatRow, selector: "mat-row, tr[mat-row]", exportAs: ["matRow"] }], directives: [{ type: i4$2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: IotCardHeader, selector: "[iot-card-header]", inputs: ["padding"] }, { type: IotCardContent, selector: "[iot-card-content]", inputs: ["padding"] }, { type: i2$3.MatColumnDef, selector: "[matColumnDef]", inputs: ["sticky", "matColumnDef"] }, { type: i2$3.MatHeaderCellDef, selector: "[matHeaderCellDef]" }, { type: i2$3.MatHeaderCell, selector: "mat-header-cell, th[mat-header-cell]" }, { type: i2$3.MatCellDef, selector: "[matCellDef]" }, { type: i2$3.MatCell, selector: "mat-cell, td[mat-cell]" }, { type: i4$2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i2$3.MatRowDef, selector: "[matRowDef]", inputs: ["matRowDefColumns", "matRowDefWhen"] }], pipes: { "translate": i5.TranslatePipe, "date": i4$2.DatePipe, "number": i4$2.DecimalPipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: KeyValueCardComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-key-value-card',
                    template: `
    <iot-dashboard-card
      [colspan]="1"
      [outerTitle]="outerTitle">
      <div *ngIf="title" iot-card-header padding="true">{{title |translate}}</div>
      <div  iot-card-content >
          <table mat-table [dataSource]="keyValueData" style="width:100%">

            <ng-container matColumnDef="label">
              <th mat-header-cell *matHeaderCellDef> Item</th>
              <td
                (click)="click.emit(entries)"
                (mouseenter)="hover.emit(entries)"
                (mouseleave)="unHover.emit()"
                class="text " [ngClass]="{warnrow: entries.error}"
                  mat-cell *matCellDef="let entries"> {{entries.label|translate}}</td>
            </ng-container>


            <ng-container matColumnDef="value">
              <th mat-header-cell *matHeaderCellDef> Cost</th>
              <td class="value text warnrow" [ngClass]="{warnrow: entries.error}"  mat-cell *matCellDef="let entries">

                <div *ngIf="!!entries.value && entries.value > 0 && entries.type =='date' && getShowShortTime(entries)">{{entries.value |date:'H:mm'}}</div>
                <div *ngIf="!!entries.value && entries.value > 0 && entries.type =='date' && getShowLongTime(entries)">{{entries.value |date:'MMM d, H:mm' }}</div>
                <div *ngIf="!!entries.value && entries.value > 0 && entries.type =='date' && getShowVeryLongTime(entries)">{{entries.value |date:'shortDate'}}</div>

                <div *ngIf="entries.type =='date' && getNoTime(entries)"> --</div>
                <div *ngIf="entries.type =='string'">{{entries.value }}</div>
                <div *ngIf="entries.type =='link'"><a [href]="entries.link"> {{entries.value }}</a></div>
                <div *ngIf="entries.type =='boolean' && entries.value">{{entries.trueValue | translate}}</div>
                <div *ngIf="entries.type =='boolean' && !entries.value">{{entries.falseValue | translate}}</div>
                <div *ngIf="entries.type =='booleanIcon' && entries.value">
                  <mat-icon>{{entries.trueValue }}</mat-icon>
                </div>
                <div *ngIf="entries.type =='booleanIcon' && !entries.value">
                  <mat-icon>{{entries.falseValue }}</mat-icon>
                </div>
                <div *ngIf="entries.type =='array'">{{entries.values[entries.value] }}</div>
                <div *ngIf="entries.type =='number'">{{entries.value | number: entries.digitsInfo || '1.0-0'}}{{entries.unit }}</div>
              </td>

            </ng-container>
            <tr mat-row *matRowDef="let row; columns: displayedColumns;"></tr>
          </table>
      </div>
    </iot-dashboard-card>
  `
                }]
        }], ctorParameters: function () { return []; }, propDecorators: { outerTitle: [{
                type: Input
            }], title: [{
                type: Input
            }], keyValueData: [{
                type: Input
            }], click: [{
                type: Output
            }], hover: [{
                type: Output
            }], unHover: [{
                type: Output
            }] } });

class PageHeaderComponent {
    constructor() {
        this.back = new EventEmitter();
    }
    ngOnInit() {
    }
}
PageHeaderComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: PageHeaderComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
PageHeaderComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: PageHeaderComponent, selector: "iot-page-header", inputs: { title: "title", subTitle: "subTitle", routerLink: "routerLink", backLabel: "backLabel" }, outputs: { back: "back" }, ngImport: i0, template: `
    <div class="header d-flex flex-column align-items-start">
      <a
        *ngIf="routerLink && backLabel"
        mat-button [routerLink]="routerLink">
        <mat-icon>keyboard_backspace</mat-icon>
        {{backLabel | translate }} </a>
      <a
        *ngIf="!routerLink && backLabel"
        mat-button (click)="back.emit()" >
        <mat-icon>keyboard_backspace</mat-icon>
        {{backLabel | translate }} </a>
      <div class="d-flex flex-row w-100">
        <div class="header-text flex-grow-1">{{title | translate}} </div>

        <ng-content select="[button3]"></ng-content>
        <ng-content select="[button2]"></ng-content>
        <ng-content select="[button1]"></ng-content>
      </div>
      <div
        *ngIf="subTitle"
        class="header-sub-text">{{subTitle | translate}} </div>
    </div>
  `, isInline: true, components: [{ type: i1.MatAnchor, selector: "a[mat-button], a[mat-raised-button], a[mat-icon-button], a[mat-fab],             a[mat-mini-fab], a[mat-stroked-button], a[mat-flat-button]", inputs: ["disabled", "disableRipple", "color", "tabIndex"], exportAs: ["matButton", "matAnchor"] }, { type: i2$1.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }], directives: [{ type: i4$2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i1$2.RouterLinkWithHref, selector: "a[routerLink],area[routerLink]", inputs: ["routerLink", "target", "queryParams", "fragment", "queryParamsHandling", "preserveFragment", "skipLocationChange", "replaceUrl", "state", "relativeTo"] }], pipes: { "translate": i5.TranslatePipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: PageHeaderComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-page-header',
                    template: `
    <div class="header d-flex flex-column align-items-start">
      <a
        *ngIf="routerLink && backLabel"
        mat-button [routerLink]="routerLink">
        <mat-icon>keyboard_backspace</mat-icon>
        {{backLabel | translate }} </a>
      <a
        *ngIf="!routerLink && backLabel"
        mat-button (click)="back.emit()" >
        <mat-icon>keyboard_backspace</mat-icon>
        {{backLabel | translate }} </a>
      <div class="d-flex flex-row w-100">
        <div class="header-text flex-grow-1">{{title | translate}} </div>

        <ng-content select="[button3]"></ng-content>
        <ng-content select="[button2]"></ng-content>
        <ng-content select="[button1]"></ng-content>
      </div>
      <div
        *ngIf="subTitle"
        class="header-sub-text">{{subTitle | translate}} </div>
    </div>
  `
                }]
        }], ctorParameters: function () { return []; }, propDecorators: { title: [{
                type: Input
            }], subTitle: [{
                type: Input
            }], routerLink: [{
                type: Input
            }], backLabel: [{
                type: Input
            }], back: [{
                type: Output
            }] } });

class CustomerSelectModalComponent {
    constructor(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.customerlist = data.customers;
    }
    onNoClick() {
        this.dialogRef.close();
    }
    ngOnInit() {
    }
}
CustomerSelectModalComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: CustomerSelectModalComponent, deps: [{ token: i1$4.MatDialogRef }, { token: MAT_DIALOG_DATA }], target: i0.ɵɵFactoryTarget.Component });
CustomerSelectModalComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: CustomerSelectModalComponent, selector: "iot-customer-select-modal", ngImport: i0, template: `
    <div class="cust-header">
      <h1 mat-dialog-title>{{'DISTRIBUTOR.SELECT_CUST' }}</h1>
      <div class="cust-button-control" >
        <button
          *ngIf="!data['mobile'] "
          mat-button

        >
          <mat-icon>create_new_folder</mat-icon>
          {{'NEW_DIST' }}</button>
        <button
          *ngIf="!data['mobile']"
          mat-button
        >

          <mat-icon>create_new_folder</mat-icon>

          {{'NEW_CUST' }}</button>


      </div>


    </div>
    {{customerlist|async|json}}
    <div mat-dialog-actions class="cust-actions">
      <button mat-button (click)="onNoClick()">{{'CONFIG.CANCEL'}}</button>
      <button mat-button cdkFocusInitial

      >{{'CONFIG.OPEN'}}</button>
    </div>
  `, isInline: true, styles: ["\n    .cust-header {\n      padding-top: 16px;\n      display: flex;\n      flex: 1;\n      justify-content: space-between;\n      margin-top: -8px;\n      min-height: 25px;\n    }\n\n    tbody a:hover.cust-name-link {\n      border-bottom-color: #3367d6;\n      color: #3367d6;\n    }\n\n    .cust-button-control {\n      margin-top: -15px;\n      display: flex;\n      flex-wrap: wrap;\n      flex-direction: row-reverse;\n      margin-left: 15px;\n    }\n\n    .cust-actions {\n      text-transform: capitalize;\n    }\n  "], components: [{ type: i1.MatButton, selector: "button[mat-button], button[mat-raised-button], button[mat-icon-button],             button[mat-fab], button[mat-mini-fab], button[mat-stroked-button],             button[mat-flat-button]", inputs: ["disabled", "disableRipple", "color"], exportAs: ["matButton"] }, { type: i2$1.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }], directives: [{ type: i1$4.MatDialogTitle, selector: "[mat-dialog-title], [matDialogTitle]", inputs: ["id"], exportAs: ["matDialogTitle"] }, { type: i4$2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i1$4.MatDialogActions, selector: "[mat-dialog-actions], mat-dialog-actions, [matDialogActions]" }], pipes: { "json": i4$2.JsonPipe, "async": i4$2.AsyncPipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: CustomerSelectModalComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-customer-select-modal',
                    template: `
    <div class="cust-header">
      <h1 mat-dialog-title>{{'DISTRIBUTOR.SELECT_CUST' }}</h1>
      <div class="cust-button-control" >
        <button
          *ngIf="!data['mobile'] "
          mat-button

        >
          <mat-icon>create_new_folder</mat-icon>
          {{'NEW_DIST' }}</button>
        <button
          *ngIf="!data['mobile']"
          mat-button
        >

          <mat-icon>create_new_folder</mat-icon>

          {{'NEW_CUST' }}</button>


      </div>


    </div>
    {{customerlist|async|json}}
    <div mat-dialog-actions class="cust-actions">
      <button mat-button (click)="onNoClick()">{{'CONFIG.CANCEL'}}</button>
      <button mat-button cdkFocusInitial

      >{{'CONFIG.OPEN'}}</button>
    </div>
  `,
                    styles: [`
    .cust-header {
      padding-top: 16px;
      display: flex;
      flex: 1;
      justify-content: space-between;
      margin-top: -8px;
      min-height: 25px;
    }

    tbody a:hover.cust-name-link {
      border-bottom-color: #3367d6;
      color: #3367d6;
    }

    .cust-button-control {
      margin-top: -15px;
      display: flex;
      flex-wrap: wrap;
      flex-direction: row-reverse;
      margin-left: 15px;
    }

    .cust-actions {
      text-transform: capitalize;
    }
  `
                    ]
                }]
        }], ctorParameters: function () { return [{ type: i1$4.MatDialogRef }, { type: undefined, decorators: [{
                    type: Inject,
                    args: [MAT_DIALOG_DATA]
                }] }]; } });

class DynamicFormWidth {
    constructor(mediaMatcher) {
        this.mediaMatcher = mediaMatcher;
        this.label = "test";
        this.offset = 0;
        this.isBig = true;
        const mediaQueryList = mediaMatcher.matchMedia('(min-width: 1px)');
    }
    get template() {
        return null;
    }
    get pristine() {
        return true;
    }
    get key() {
        return '';
    }
    get newValue() {
        return '';
    }
    ngOnInit() {
        // this.offset.subscribe(o => {
        if (this.mediaQueryList) {
            if (this.mediaQueryList.removeEventListener) {
                this.mediaQueryList.removeEventListener("change", this.eventListener);
            }
            else {
                this.mediaQueryList.removeListener(this.eventListener);
            }
        }
        this.mediaQueryList = this.mediaMatcher.matchMedia('(min-width: ' + (700 + this.offset) + 'px)');
        this.eventListener = (e) => {
            this.isBig = e.matches;
        };
        this.isBig = this.mediaQueryList.matches;
        if (this.mediaQueryList.addEventListener) {
            this.mediaQueryList.addEventListener("change", this.eventListener);
        }
        else {
            this.mediaQueryList.addListener(this.eventListener);
        }
        // });
    }
}
DynamicFormWidth.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: DynamicFormWidth, deps: [{ token: i1$5.MediaMatcher }], target: i0.ɵɵFactoryTarget.Directive });
DynamicFormWidth.ɵdir = i0.ɵɵngDeclareDirective({ minVersion: "12.0.0", version: "12.2.6", type: DynamicFormWidth, selector: "[test]", inputs: { label: "label" }, ngImport: i0 });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: DynamicFormWidth, decorators: [{
            type: Directive,
            args: [{ selector: '[test]' }]
        }], ctorParameters: function () { return [{ type: i1$5.MediaMatcher }]; }, propDecorators: { label: [{
                type: Input
            }] } });

class IotFormComponent {
    constructor() {
        this.readonly = false;
        this.emitAll = false;
        this.alwaysShowDiscard = false;
        this.saveButtonText = "CONFIG.SAVE";
        this.statesDescription = {
            1: { text: '', loading: false, check: false },
            2: { text: 'CONFIG.SUBMITTING', loading: true, check: false },
            3: { text: 'CONFIG.CHANGES_SAVED', loading: false, check: true },
        };
        this.save = new EventEmitter();
        this.discard = new EventEmitter();
        this.active = false;
        this.tabs = [];
        this.formSubscription = new Subscription();
    }
    ngAfterContentInit() {
        setTimeout(() => {
            this.inputTabs.forEach((tab) => {
                if (tab.form) {
                    this.formSubscription.add(tab.form.valueChanges.subscribe((formChange) => {
                        this.pristineCheck();
                    }));
                }
            });
            this.tabs = this.inputTabs.toArray();
        }, 0);
    }
    pristineCheck() {
        let pristine = true;
        this.inputTabs.forEach((tab) => {
            pristine = pristine && tab.pristine;
        });
        this.active = !pristine;
    }
    _save() {
        const toReturn = {};
        this.inputTabs.forEach((tab) => {
            const attr = tab.key;
            if (attr != '') {
                if ((!tab.pristine) || this.emitAll) {
                    toReturn[attr] = tab.newValue;
                }
            }
        });
        this.save.emit(toReturn);
    }
}
IotFormComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotFormComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
IotFormComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotFormComponent, selector: "iot-form", inputs: { readonly: "readonly", state: "state", emitAll: "emitAll", alwaysShowDiscard: "alwaysShowDiscard", saveButtonText: "saveButtonText", statesDescription: "statesDescription", active: "active" }, outputs: { save: "save", discard: "discard" }, queries: [{ propertyName: "inputTabs", predicate: DynamicFormWidth, descendants: true }], ngImport: i0, template: `
    <div
      class="iot-drawer-content">
      <ng-content select="[page-header]"></ng-content>
      <ng-content select="[below-header]"></ng-content>
      <div *ngFor="let tab of tabs; let i = index" class="fields-container"
           role="tabpanel"
           attr.aria-labelledby="{{i}}-tab">
        <ng-container *ngTemplateOutlet="tab.template"></ng-container>
      </div>
    </div>

    <div class="outer-save-bar" *ngIf="!readonly">
      <div class="button-row">
        <button *ngIf="alwaysShowDiscard || active"
                (click)="discard.emit()"
                mat-button>{{'CONFIG.CANCEL'|translate}}
        </button>
        <button [disabled]="!active"
                (click)="_save()"
                mat-raised-button color="primary">{{saveButtonText |translate}}
        </button>

      </div>

      <div  *ngIf="saveButtonText != ''" class="bottom-font">
        <div class="message-outer" *ngIf="statesDescription && statesDescription[state]">
          <mat-spinner
            *ngIf="statesDescription[state].loading"
            class="icon"
            [diameter]="20"
          ></mat-spinner>
          <mat-icon
            *ngIf="statesDescription[state].check"
            class="icon">check_circle_outline
          </mat-icon>
          <div class="message">{{statesDescription[state].text |translate}} </div>
        </div>
      </div>
    </div>
  `, isInline: true, styles: ["\n      .fields-container .outer-margin{\n        padding-right: 24px;\n        padding-top: 16px;\n      }\n\n      .fields-container .outer-margin.nopaddingtop {\n        padding-top: 0px;\n      }\n\n      .outer-save-bar {\n        z-index: 3;\n        background: #fff;\n        bottom: 0;\n        box-shadow: 0 -5px 5px -5px #999;\n\n        left: 0;\n        position: fixed;\n        right: 0;\n        /*height: 36px;*/\n        display: flex;\n        flex-direction: row-reverse;\n        justify-content: space-between;\n\n        padding: 16px;\n      }\n\n      .button-row {\n\n      }\n\n      .icon {\n        margin-right: 8px;\n      }\n\n      .message-outer {\n        display: flex;\n        align-items: center;\n      }\n\n      .bottom-font {\n        color: #3c4043;\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 14px;\n        font-weight: 400;\n        letter-spacing: .0142857143em;\n        line-height: 1.25rem;\n      }\n\n      .message {\n        line-height: 20px;\n        height: 20px;\n      }\n    "], components: [{ type: i1.MatButton, selector: "button[mat-button], button[mat-raised-button], button[mat-icon-button],             button[mat-fab], button[mat-mini-fab], button[mat-stroked-button],             button[mat-flat-button]", inputs: ["disabled", "disableRipple", "color"], exportAs: ["matButton"] }, { type: i2$4.MatSpinner, selector: "mat-spinner", inputs: ["color"] }, { type: i2$1.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }], directives: [{ type: i4$2.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { type: i4$2.NgTemplateOutlet, selector: "[ngTemplateOutlet]", inputs: ["ngTemplateOutletContext", "ngTemplateOutlet"] }, { type: i4$2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }], pipes: { "translate": i5.TranslatePipe }, encapsulation: i0.ViewEncapsulation.None });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotFormComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-form',
                    template: `
    <div
      class="iot-drawer-content">
      <ng-content select="[page-header]"></ng-content>
      <ng-content select="[below-header]"></ng-content>
      <div *ngFor="let tab of tabs; let i = index" class="fields-container"
           role="tabpanel"
           attr.aria-labelledby="{{i}}-tab">
        <ng-container *ngTemplateOutlet="tab.template"></ng-container>
      </div>
    </div>

    <div class="outer-save-bar" *ngIf="!readonly">
      <div class="button-row">
        <button *ngIf="alwaysShowDiscard || active"
                (click)="discard.emit()"
                mat-button>{{'CONFIG.CANCEL'|translate}}
        </button>
        <button [disabled]="!active"
                (click)="_save()"
                mat-raised-button color="primary">{{saveButtonText |translate}}
        </button>

      </div>

      <div  *ngIf="saveButtonText != ''" class="bottom-font">
        <div class="message-outer" *ngIf="statesDescription && statesDescription[state]">
          <mat-spinner
            *ngIf="statesDescription[state].loading"
            class="icon"
            [diameter]="20"
          ></mat-spinner>
          <mat-icon
            *ngIf="statesDescription[state].check"
            class="icon">check_circle_outline
          </mat-icon>
          <div class="message">{{statesDescription[state].text |translate}} </div>
        </div>
      </div>
    </div>
  `,
                    styles: [
                        `
      .fields-container .outer-margin{
        padding-right: 24px;
        padding-top: 16px;
      }

      .fields-container .outer-margin.nopaddingtop {
        padding-top: 0px;
      }

      .outer-save-bar {
        z-index: 3;
        background: #fff;
        bottom: 0;
        box-shadow: 0 -5px 5px -5px #999;

        left: 0;
        position: fixed;
        right: 0;
        /*height: 36px;*/
        display: flex;
        flex-direction: row-reverse;
        justify-content: space-between;

        padding: 16px;
      }

      .button-row {

      }

      .icon {
        margin-right: 8px;
      }

      .message-outer {
        display: flex;
        align-items: center;
      }

      .bottom-font {
        color: #3c4043;
        font-family: Roboto, Arial, sans-serif;
        font-size: 14px;
        font-weight: 400;
        letter-spacing: .0142857143em;
        line-height: 1.25rem;
      }

      .message {
        line-height: 20px;
        height: 20px;
      }
    `
                    ],
                    encapsulation: ViewEncapsulation.None
                }]
        }], ctorParameters: function () { return []; }, propDecorators: { readonly: [{
                type: Input
            }], state: [{
                type: Input
            }], emitAll: [{
                type: Input
            }], alwaysShowDiscard: [{
                type: Input
            }], saveButtonText: [{
                type: Input
            }], statesDescription: [{
                type: Input
            }], save: [{
                type: Output
            }], discard: [{
                type: Output
            }], inputTabs: [{
                type: ContentChildren,
                args: [DynamicFormWidth, { descendants: true }]
            }], active: [{
                type: Input
            }] } });

class IotInputTextComponent extends DynamicFormWidth {
    constructor() {
        super(...arguments);
        this.form = new FormGroup({
            inputId: new FormControl(''),
        });
        this.attribute = 'todo';
        this.value = '';
        this.type = 'text';
        this.disabled = false;
        this.onChange = new EventEmitter();
    }
    get template() {
        return this._template;
    }
    get pristine() {
        return this.form.value.inputId === this.value;
    }
    get key() {
        return this.attribute;
    }
    get newValue() {
        return this.form.value.inputId;
    }
    ngOnInit() {
        super.ngOnInit();
        this.form = new FormGroup({
            inputId: new FormControl({ value: this.value, disabled: this.disabled }),
        });
        // if (this.disabled) {
        //
        // }
    }
    ngOnChanges(changes) {
        if (changes.value && !!this.value) {
            this.form.setValue({ inputId: this.value });
        }
    }
}
IotInputTextComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputTextComponent, deps: null, target: i0.ɵɵFactoryTarget.Component });
IotInputTextComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotInputTextComponent, selector: "iot-input-text", inputs: { label: "label", attribute: "attribute", description: "description", value: "value", type: "type", disabled: "disabled" }, outputs: { onChange: "onChange" }, providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputTextComponent) }], viewQueries: [{ propertyName: "_template", first: true, predicate: TemplateRef, descendants: true }], usesInheritance: true, usesOnChanges: true, ngImport: i0, template: `
    <ng-template>
        <div class="outer-margin">
            <div class="outeroneline" [ngClass]="{'outersmall':!isBig}">
                <div class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
                >
                    <div class="label">{{label | translate}} </div>
                </div>

                <form [formGroup]="form" class="content">
                  <div
                    *ngIf="!!description"
                    class="checkbox-content-description">{{description | translate}}</div>
                    <div class="form-content">

                        <input class="input-class" matInput [type]="type"
                               formControlName="inputId"
                               (ngModelChange)="onChange.emit($event)"

                               >
                        <span class="border-class"></span>
                    </div>
                </form>


            </div>

        </div>
    </ng-template>

    `, isInline: true, styles: ["\n        .outer-margin {\n            /*margin-left: -5px;*/\n\n            padding-right: 50px;\n        }\n\n        .label-outer.label-outer-thin {\n            min-height: 40px;\n        }\n\n        .checkbox-content-description {\n          font-family: Roboto, Arial, sans-serif;\n          font-size: 12px;\n          font-weight: 400;\n          letter-spacing: .025em;\n          line-height: 16px;\n          color: #5f6368;\n          margin-bottom: 13px;\n          margin-top: 2px;\n        }\n\n        .label-outer {\n            flex-basis: 33%;\n            min-height: 64px;\n            min-width: 220px;\n            display: block;\n            padding-top: 13px;\n            color: #3c4043;\n        }\n\n        .label {\n\n            font-family: Roboto, Arial, sans-serif;\n            font-size: 14px;\n            font-weight: 400;\n            height: 20px;\n            color: rgb(60, 64, 67);\n        }\n\n        .outeroneline {\n            display: flex;\n            max-width: 900px;\n        }\n\n\n        .outersmall {\n            flex-direction: column;\n            padding-bottom: 8px;\n        }\n\n\n        .content {\n            flex-direction: column;\n            flex-basis: 67%;\n            min-width: 280px;\n        }\n\n        .form-content {\n\n            width: 100%;\n            height: 46px;\n            padding: 0 16px;\n            border: 1px solid #dadce0;\n            border-radius: 4px;\n            border-top-left-radius: 4px;\n            border-top-right-radius: 4px;\n            border-top-right-radius: 4px;\n            border-bottom-right-radius: 4px;\n            border-bottom-right-radius: 4px;\n            border-bottom-left-radius: 4px;\n            pointer-events: none;\n        }\n\n\n        .form-content:hover {\n            border: 2px solid black;\n        }\n\n        .input-class {\n            height: 100%;\n            width: 100%;\n            display: flex;\n            border: none !important;\n            background-color: transparent;\n            pointer-events: auto;\n            font-family: Roboto, Arial, sans-serif;\n            font-size: 1rem;\n            font-weight: 400;\n            letter-spacing: .00625em;\n        }\n\n        .input-class:focus {\n            outline: none;\n        }\n    "], directives: [{ type: i4$2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i4$3.ɵNgNoValidate, selector: "form:not([ngNoForm]):not([ngNativeValidate])" }, { type: i4$3.NgControlStatusGroup, selector: "[formGroupName],[formArrayName],[ngModelGroup],[formGroup],form:not([ngNoForm]),[ngForm]" }, { type: i4$3.FormGroupDirective, selector: "[formGroup]", inputs: ["formGroup"], outputs: ["ngSubmit"], exportAs: ["ngForm"] }, { type: i4$2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i3$1.MatInput, selector: "input[matInput], textarea[matInput], select[matNativeControl],      input[matNativeControl], textarea[matNativeControl]", inputs: ["id", "disabled", "required", "type", "value", "readonly", "placeholder", "errorStateMatcher", "aria-describedby"], exportAs: ["matInput"] }, { type: i4$3.DefaultValueAccessor, selector: "input:not([type=checkbox])[formControlName],textarea[formControlName],input:not([type=checkbox])[formControl],textarea[formControl],input:not([type=checkbox])[ngModel],textarea[ngModel],[ngDefaultControl]" }, { type: i4$3.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { type: i4$3.FormControlName, selector: "[formControlName]", inputs: ["disabled", "formControlName", "ngModel"], outputs: ["ngModelChange"] }], pipes: { "translate": i5.TranslatePipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputTextComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-input-text',
                    template: `
    <ng-template>
        <div class="outer-margin">
            <div class="outeroneline" [ngClass]="{'outersmall':!isBig}">
                <div class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
                >
                    <div class="label">{{label | translate}} </div>
                </div>

                <form [formGroup]="form" class="content">
                  <div
                    *ngIf="!!description"
                    class="checkbox-content-description">{{description | translate}}</div>
                    <div class="form-content">

                        <input class="input-class" matInput [type]="type"
                               formControlName="inputId"
                               (ngModelChange)="onChange.emit($event)"

                               >
                        <span class="border-class"></span>
                    </div>
                </form>


            </div>

        </div>
    </ng-template>

    `,
                    styles: [`
        .outer-margin {
            /*margin-left: -5px;*/

            padding-right: 50px;
        }

        .label-outer.label-outer-thin {
            min-height: 40px;
        }

        .checkbox-content-description {
          font-family: Roboto, Arial, sans-serif;
          font-size: 12px;
          font-weight: 400;
          letter-spacing: .025em;
          line-height: 16px;
          color: #5f6368;
          margin-bottom: 13px;
          margin-top: 2px;
        }

        .label-outer {
            flex-basis: 33%;
            min-height: 64px;
            min-width: 220px;
            display: block;
            padding-top: 13px;
            color: #3c4043;
        }

        .label {

            font-family: Roboto, Arial, sans-serif;
            font-size: 14px;
            font-weight: 400;
            height: 20px;
            color: rgb(60, 64, 67);
        }

        .outeroneline {
            display: flex;
            max-width: 900px;
        }


        .outersmall {
            flex-direction: column;
            padding-bottom: 8px;
        }


        .content {
            flex-direction: column;
            flex-basis: 67%;
            min-width: 280px;
        }

        .form-content {

            width: 100%;
            height: 46px;
            padding: 0 16px;
            border: 1px solid #dadce0;
            border-radius: 4px;
            border-top-left-radius: 4px;
            border-top-right-radius: 4px;
            border-top-right-radius: 4px;
            border-bottom-right-radius: 4px;
            border-bottom-right-radius: 4px;
            border-bottom-left-radius: 4px;
            pointer-events: none;
        }


        .form-content:hover {
            border: 2px solid black;
        }

        .input-class {
            height: 100%;
            width: 100%;
            display: flex;
            border: none !important;
            background-color: transparent;
            pointer-events: auto;
            font-family: Roboto, Arial, sans-serif;
            font-size: 1rem;
            font-weight: 400;
            letter-spacing: .00625em;
        }

        .input-class:focus {
            outline: none;
        }
    `],
                    providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputTextComponent) }]
                }]
        }], propDecorators: { _template: [{
                type: ViewChild,
                args: [TemplateRef]
            }], label: [{
                type: Input
            }], attribute: [{
                type: Input
            }], description: [{
                type: Input
            }], value: [{
                type: Input
            }], type: [{
                type: Input
            }], disabled: [{
                type: Input
            }], onChange: [{
                type: Output
            }] } });

class IotInputHorizontalLineComponent extends DynamicFormWidth {
    constructor() {
        super(...arguments);
        this.value = "none";
        this.label = "none";
        this.disabled = false;
        this.onChange = new EventEmitter();
    }
    get template() {
        return this._template;
    }
    ngOnInit() {
        // this.template = this.t;
        super.ngOnInit();
    }
}
IotInputHorizontalLineComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputHorizontalLineComponent, deps: null, target: i0.ɵɵFactoryTarget.Component });
IotInputHorizontalLineComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotInputHorizontalLineComponent, selector: "iot-horizontal-line", inputs: { value: "value", label: "label", disabled: "disabled" }, outputs: { onChange: "onChange" }, providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputHorizontalLineComponent) }], viewQueries: [{ propertyName: "_template", first: true, predicate: TemplateRef, descendants: true }], usesInheritance: true, ngImport: i0, template: `

    <ng-template>
      <div *ngIf="!isBig" class="content horizontal-line">
      </div>

      <div *ngIf="isBig" class="horizontal-line big-line">
      </div>
    </ng-template>

  `, isInline: true, styles: ["\n    .horizontal-line {\n      border-bottom: 1px solid #dadce0;\n      margin-right: 17px;\n      margin-bottom: 10px;\n      min-width: 395px;\n    }\n\n    .big-line {\n      max-width: 928px;\n    }\n\n  "], directives: [{ type: i4$2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputHorizontalLineComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-horizontal-line',
                    template: `

    <ng-template>
      <div *ngIf="!isBig" class="content horizontal-line">
      </div>

      <div *ngIf="isBig" class="horizontal-line big-line">
      </div>
    </ng-template>

  `,
                    styles: [`
    .horizontal-line {
      border-bottom: 1px solid #dadce0;
      margin-right: 17px;
      margin-bottom: 10px;
      min-width: 395px;
    }

    .big-line {
      max-width: 928px;
    }

  `],
                    providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputHorizontalLineComponent) }]
                }]
        }], propDecorators: { _template: [{
                type: ViewChild,
                args: [TemplateRef]
            }], value: [{
                type: Input
            }], label: [{
                type: Input
            }], disabled: [{
                type: Input
            }], onChange: [{
                type: Output
            }] } });

class IotInputTextareaComponent extends DynamicFormWidth {
    constructor() {
        super(...arguments);
        this.form = new FormGroup({
            text: new FormControl(''),
        });
        this.attribute = 'todo';
        this.rows = 20;
        this.disabled = false;
        this.onChange = new EventEmitter();
    }
    get template() {
        return this._template;
    }
    get pristine() {
        return this.form.value.text === this.value;
    }
    get key() {
        return this.attribute;
    }
    get newValue() {
        return this.form.value.text;
    }
    ngOnInit() {
        super.ngOnInit();
        if (this.disabled) {
            this.form = new FormGroup({
                text: new FormControl({ value: '', disabled: this.disabled }),
            });
        }
    }
}
IotInputTextareaComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputTextareaComponent, deps: null, target: i0.ɵɵFactoryTarget.Component });
IotInputTextareaComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotInputTextareaComponent, selector: "iot-input-textarea", inputs: { label: "label", attribute: "attribute", value: "value", placeholder: "placeholder", rows: "rows", disabled: "disabled" }, outputs: { onChange: "onChange" }, providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputTextareaComponent) }], viewQueries: [{ propertyName: "_template", first: true, predicate: TemplateRef, descendants: true }], usesInheritance: true, ngImport: i0, template: `
    <ng-template>
      <div class="outer-margin">
        <div class="outeroneline" [ngClass]="{'outersmall':!isBig}">
          <div class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
          >
            <div class="label">{{label | translate}} </div>
          </div>

          <form [formGroup]="form" class="content">

            <textarea class="input-class" matInput type="text"
                      formControlName="text"
                      (ngModelChange)="onChange.emit($event)"
                      [placeholder]="placeholder"
                      [disabled]="disabled"
                      [rows]="rows"
                      [ngModel]="value"></textarea>


          </form>


        </div>

      </div>
    </ng-template>
  `, isInline: true, styles: ["\n    .outer-margin {\n      padding-right: 50px;\n    }\n\n    .label-outer.label-outer-thin {\n      min-height: 40px;\n    }\n\n    .label-outer {\n      flex-basis: 33%;\n      min-height: 64px;\n      min-width: 220px;\n      display: block;\n      padding-top: 13px;\n      color: #3c4043;\n    }\n\n    .label {\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 14px;\n      font-weight: 400;\n      height: 20px;\n      color: rgb(60, 64, 67);\n    }\n\n    .outeroneline {\n      display: flex;\n      max-width: 900px;\n    }\n\n    .outersmall {\n      flex-direction: column;\n      padding-bottom: 8px;\n    }\n\n    .content {\n      flex-direction: column;\n      flex-basis: 67%;\n      min-width: 280px;\n      padding-bottom: 20px;\n    }\n\n    .input-class {\n      height: 100%;\n      width: 100%;\n      display: flex;\n      border: none !important;\n      background-color: transparent;\n      pointer-events: auto;\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 1rem;\n      font-weight: 400;\n      letter-spacing: .00625em;\n    }\n\n    .input-class:focus {\n      outline: none;\n    }\n  "], directives: [{ type: i4$2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i4$3.ɵNgNoValidate, selector: "form:not([ngNoForm]):not([ngNativeValidate])" }, { type: i4$3.NgControlStatusGroup, selector: "[formGroupName],[formArrayName],[ngModelGroup],[formGroup],form:not([ngNoForm]),[ngForm]" }, { type: i4$3.FormGroupDirective, selector: "[formGroup]", inputs: ["formGroup"], outputs: ["ngSubmit"], exportAs: ["ngForm"] }, { type: i3$1.MatInput, selector: "input[matInput], textarea[matInput], select[matNativeControl],      input[matNativeControl], textarea[matNativeControl]", inputs: ["id", "disabled", "required", "type", "value", "readonly", "placeholder", "errorStateMatcher", "aria-describedby"], exportAs: ["matInput"] }, { type: i4$3.DefaultValueAccessor, selector: "input:not([type=checkbox])[formControlName],textarea[formControlName],input:not([type=checkbox])[formControl],textarea[formControl],input:not([type=checkbox])[ngModel],textarea[ngModel],[ngDefaultControl]" }, { type: i4$3.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { type: i4$3.FormControlName, selector: "[formControlName]", inputs: ["disabled", "formControlName", "ngModel"], outputs: ["ngModelChange"] }], pipes: { "translate": i5.TranslatePipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputTextareaComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-input-textarea',
                    template: `
    <ng-template>
      <div class="outer-margin">
        <div class="outeroneline" [ngClass]="{'outersmall':!isBig}">
          <div class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
          >
            <div class="label">{{label | translate}} </div>
          </div>

          <form [formGroup]="form" class="content">

            <textarea class="input-class" matInput type="text"
                      formControlName="text"
                      (ngModelChange)="onChange.emit($event)"
                      [placeholder]="placeholder"
                      [disabled]="disabled"
                      [rows]="rows"
                      [ngModel]="value"></textarea>


          </form>


        </div>

      </div>
    </ng-template>
  `,
                    styles: [`
    .outer-margin {
      padding-right: 50px;
    }

    .label-outer.label-outer-thin {
      min-height: 40px;
    }

    .label-outer {
      flex-basis: 33%;
      min-height: 64px;
      min-width: 220px;
      display: block;
      padding-top: 13px;
      color: #3c4043;
    }

    .label {
      font-family: Roboto, Arial, sans-serif;
      font-size: 14px;
      font-weight: 400;
      height: 20px;
      color: rgb(60, 64, 67);
    }

    .outeroneline {
      display: flex;
      max-width: 900px;
    }

    .outersmall {
      flex-direction: column;
      padding-bottom: 8px;
    }

    .content {
      flex-direction: column;
      flex-basis: 67%;
      min-width: 280px;
      padding-bottom: 20px;
    }

    .input-class {
      height: 100%;
      width: 100%;
      display: flex;
      border: none !important;
      background-color: transparent;
      pointer-events: auto;
      font-family: Roboto, Arial, sans-serif;
      font-size: 1rem;
      font-weight: 400;
      letter-spacing: .00625em;
    }

    .input-class:focus {
      outline: none;
    }
  `],
                    providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputTextareaComponent) }]
                }]
        }], propDecorators: { _template: [{
                type: ViewChild,
                args: [TemplateRef]
            }], label: [{
                type: Input
            }], attribute: [{
                type: Input
            }], value: [{
                type: Input
            }], placeholder: [{
                type: Input
            }], rows: [{
                type: Input
            }], disabled: [{
                type: Input
            }], onChange: [{
                type: Output
            }] } });

class IotInputSelectValueComponent extends DynamicFormWidth {
    constructor() {
        super(...arguments);
        this.form = new FormGroup({
            selectId: new FormControl(''),
        });
        this.attribute = 'todo';
        this.description = 'explain this field';
        this.disabled = false;
        this.onChange = new EventEmitter();
    }
    get template() {
        return this._template;
    }
    get pristine() {
        return this.form.value.selectId === this.value;
    }
    get key() {
        return this.attribute;
    }
    get newValue() {
        return this.form.value.selectId;
    }
    change(event) {
        this.onChange.emit(event);
    }
    ngOnInit() {
        super.ngOnInit();
        if (this.disabled) {
            this.form = new FormGroup({
                selectId: new FormControl({ value: '', disabled: this.disabled }),
            });
        }
        if (this.value) {
            this.form.setValue({ selectId: this.value });
        }
    }
}
IotInputSelectValueComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputSelectValueComponent, deps: null, target: i0.ɵɵFactoryTarget.Component });
IotInputSelectValueComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotInputSelectValueComponent, selector: "iot-input-select-values", inputs: { label: "label", attribute: "attribute", description: "description", value: "value", separator: "separator", values: "values", disabled: "disabled" }, outputs: { onChange: "onChange" }, providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputSelectValueComponent) }], viewQueries: [{ propertyName: "_template", first: true, predicate: TemplateRef, descendants: true }], usesInheritance: true, ngImport: i0, template: `
    <ng-template>
      <div class="outer-margin">
        <div
          *ngIf="label && label !== ''"
          class="outer" [ngClass]="{'outersmall':!isBig}">
          <div class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
          >
            <div class="label">{{label | translate}}</div>

          </div>

          <form [formGroup]="form" class="content">
            <div class="checkbox-content">
              <div class="checkbox-content-description">{{description | translate}}</div>
              <mat-select
                formControlName="selectId"
                (ngModelChange)="change($event)">

                <mat-option *ngFor="let v of values" [value]="v.id">{{v.name}} </mat-option>
              </mat-select>
            </div>
            <div *ngIf="separator" class="separator"></div>
          </form>


        </div>

      </div>
    </ng-template>
  `, isInline: true, styles: ["\n      .separator {\n        border-bottom: 1px solid #dadce0;\n        padding-top: 16px;\n      }\n\n      .checkbox-content-description {\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 12px;\n        font-weight: 400;\n        letter-spacing: .025em;\n        line-height: 16px;\n        color: #5f6368;\n        margin-bottom: 13px;\n        margin-top: 2px;\n      }\n\n      .outer-margin {\n        margin-left: 0px;\n        padding-right: 50px;\n      }\n\n      .label-outer.label-outer-thin {\n        min-height: 40px;\n      }\n\n      .label-outer {\n        flex-basis: 33%;\n        min-height: 64px;\n        min-width: 220px;\n        display: block;\n        padding-top: 13px;\n        color: #3c4043;\n      }\n\n      .label {\n\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 14px;\n        font-weight: 400;\n        height: 20px;\n        color: rgb(60, 64, 67);\n      }\n\n      .outer {\n        display: flex;\n        max-width: 900px;\n        margin-bottom: 16px;\n      }\n\n      .outersmall {\n        flex-direction: column;\n        padding-bottom: 8px;\n      }\n\n      .content {\n        flex-direction: column;\n        flex-basis: 67%;\n        min-width: 280px;\n      }\n\n      .checkbox-content {\n        display: flex;\n        flex-direction: column;\n        color: #3c4043;\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 14px;\n        font-weight: 400;\n        letter-spacing: 0.2px;\n        line-height: 20px;\n      }\n    "], components: [{ type: i1$6.MatSelect, selector: "mat-select", inputs: ["disabled", "disableRipple", "tabIndex"], exportAs: ["matSelect"] }, { type: i4$1.MatOption, selector: "mat-option", exportAs: ["matOption"] }], directives: [{ type: i4$2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i4$2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i4$3.ɵNgNoValidate, selector: "form:not([ngNoForm]):not([ngNativeValidate])" }, { type: i4$3.NgControlStatusGroup, selector: "[formGroupName],[formArrayName],[ngModelGroup],[formGroup],form:not([ngNoForm]),[ngForm]" }, { type: i4$3.FormGroupDirective, selector: "[formGroup]", inputs: ["formGroup"], outputs: ["ngSubmit"], exportAs: ["ngForm"] }, { type: i4$3.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { type: i4$3.FormControlName, selector: "[formControlName]", inputs: ["disabled", "formControlName", "ngModel"], outputs: ["ngModelChange"] }, { type: i4$2.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }], pipes: { "translate": i5.TranslatePipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputSelectValueComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-input-select-values',
                    template: `
    <ng-template>
      <div class="outer-margin">
        <div
          *ngIf="label && label !== ''"
          class="outer" [ngClass]="{'outersmall':!isBig}">
          <div class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
          >
            <div class="label">{{label | translate}}</div>

          </div>

          <form [formGroup]="form" class="content">
            <div class="checkbox-content">
              <div class="checkbox-content-description">{{description | translate}}</div>
              <mat-select
                formControlName="selectId"
                (ngModelChange)="change($event)">

                <mat-option *ngFor="let v of values" [value]="v.id">{{v.name}} </mat-option>
              </mat-select>
            </div>
            <div *ngIf="separator" class="separator"></div>
          </form>


        </div>

      </div>
    </ng-template>
  `,
                    styles: [
                        `
      .separator {
        border-bottom: 1px solid #dadce0;
        padding-top: 16px;
      }

      .checkbox-content-description {
        font-family: Roboto, Arial, sans-serif;
        font-size: 12px;
        font-weight: 400;
        letter-spacing: .025em;
        line-height: 16px;
        color: #5f6368;
        margin-bottom: 13px;
        margin-top: 2px;
      }

      .outer-margin {
        margin-left: 0px;
        padding-right: 50px;
      }

      .label-outer.label-outer-thin {
        min-height: 40px;
      }

      .label-outer {
        flex-basis: 33%;
        min-height: 64px;
        min-width: 220px;
        display: block;
        padding-top: 13px;
        color: #3c4043;
      }

      .label {

        font-family: Roboto, Arial, sans-serif;
        font-size: 14px;
        font-weight: 400;
        height: 20px;
        color: rgb(60, 64, 67);
      }

      .outer {
        display: flex;
        max-width: 900px;
        margin-bottom: 16px;
      }

      .outersmall {
        flex-direction: column;
        padding-bottom: 8px;
      }

      .content {
        flex-direction: column;
        flex-basis: 67%;
        min-width: 280px;
      }

      .checkbox-content {
        display: flex;
        flex-direction: column;
        color: #3c4043;
        font-family: Roboto, Arial, sans-serif;
        font-size: 14px;
        font-weight: 400;
        letter-spacing: 0.2px;
        line-height: 20px;
      }
    `
                    ],
                    providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputSelectValueComponent) }]
                }]
        }], propDecorators: { _template: [{
                type: ViewChild,
                args: [TemplateRef]
            }], label: [{
                type: Input
            }], attribute: [{
                type: Input
            }], description: [{
                type: Input
            }], value: [{
                type: Input
            }], separator: [{
                type: Input
            }], values: [{
                type: Input
            }], disabled: [{
                type: Input
            }], onChange: [{
                type: Output
            }] } });

class IotDatePickerComponent extends DynamicFormWidth {
    constructor() {
        super(...arguments);
        this.timestampInSec = false;
        this.form = new FormGroup({
            dateId: new FormControl(''),
        });
        this.attribute = 'todo';
        this.description = 'explain this field';
        this.type = 'text';
        this.disabled = false;
        this.onChange = new EventEmitter();
    }
    get template() {
        return this._template;
    }
    get pristine() {
        return this.form.value.dateId === this.value;
    }
    get key() {
        return this.attribute;
    }
    get newValue() {
        if (this.timestampInSec) {
            if (this.form.value.dateId.unix) {
                return {
                    seconds: this.form.value.dateId.unix()
                };
            }
            else {
                return {
                    seconds: this.form.value.dateId.getTime() / 1000
                };
            }
        }
        return this.form.value.dateId;
    }
    ngOnInit() {
        super.ngOnInit();
        if (this.value && this.value.seconds) {
            this.value = new Date(this.value.seconds * 1000);
            this.timestampInSec = true;
        }
        this.form.setValue({ dateId: this.value });
        if (this.disabled) {
            this.form = new FormGroup({
                dateId: new FormControl({ value: '', disabled: this.disabled }),
            });
        }
    }
}
IotDatePickerComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotDatePickerComponent, deps: null, target: i0.ɵɵFactoryTarget.Component });
IotDatePickerComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotDatePickerComponent, selector: "iot-date-picker", inputs: { label: "label", attribute: "attribute", description: "description", value: "value", type: "type", disabled: "disabled" }, outputs: { onChange: "onChange" }, providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotDatePickerComponent) }], viewQueries: [{ propertyName: "_template", first: true, predicate: TemplateRef, descendants: true }], usesInheritance: true, ngImport: i0, template: `
    <ng-template>
      <div class="outer-margin">
        <div
          class="outeroneline"
          [ngClass]="{'outersmall':!isBig}">
          <div
            class="label-outer"
            [ngClass]="{'label-outer-thin':!isBig}"
          >
            <div class="label">{{label | translate}} </div>
          </div>
          <form [formGroup]="form" class="content">

            <mat-form-field appearance="outline" class="w-100">
              <mat-label>{{description | translate}}</mat-label>
              <input
                formControlName="dateId"
                (ngModelChange)="onChange.emit($event)"
                matInput [matDatepicker]="picker">
              <mat-datepicker-toggle matSuffix [for]="picker"></mat-datepicker-toggle>
              <mat-datepicker #picker></mat-datepicker>
            </mat-form-field>
            <span class="border-class"></span>
          </form>
        </div>
      </div>
    </ng-template>

  `, isInline: true, styles: ["\n    .outer-margin {\n      /*margin-left: -5px;*/\n\n      padding-right: 50px;\n    }\n\n    .label-outer.label-outer-thin {\n      min-height: 40px;\n    }\n\n    .label-outer {\n      flex-basis: 33%;\n      min-height: 64px;\n      min-width: 220px;\n      display: block;\n      padding-top: 13px;\n      color: #3c4043;\n    }\n\n    .label {\n\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 14px;\n      font-weight: 400;\n      height: 20px;\n      color: rgb(60, 64, 67);\n    }\n\n    .outeroneline {\n      display: flex;\n      max-width: 900px;\n    }\n\n\n    .outersmall {\n      flex-direction: column;\n      padding-bottom: 8px;\n    }\n\n\n    .content {\n      flex-direction: column;\n      flex-basis: 67%;\n      min-width: 280px;\n    }\n\n    .form-content {\n\n      width: 100%;\n      height: 46px;\n      padding: 0 16px;\n      border: 1px solid #dadce0;\n      border-radius: 4px;\n      border-top-left-radius: 4px;\n      border-top-right-radius: 4px;\n      border-top-right-radius: 4px;\n      border-bottom-right-radius: 4px;\n      border-bottom-right-radius: 4px;\n      border-bottom-left-radius: 4px;\n      pointer-events: none;\n    }\n\n\n    .form-content:hover {\n      border: 2px solid black;\n    }\n\n    .input-class {\n      height: 100%;\n      width: 100%;\n      display: flex;\n      border: none !important;\n      background-color: transparent;\n      pointer-events: auto;\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 1rem;\n      font-weight: 400;\n      letter-spacing: .00625em;\n    }\n\n    .input-class:focus {\n      outline: none;\n    }\n  "], components: [{ type: i1$7.MatFormField, selector: "mat-form-field", inputs: ["color", "floatLabel", "appearance", "hideRequiredMarker", "hintLabel"], exportAs: ["matFormField"] }, { type: i2$5.MatDatepickerToggle, selector: "mat-datepicker-toggle", inputs: ["tabIndex", "disabled", "for", "aria-label", "disableRipple"], exportAs: ["matDatepickerToggle"] }, { type: i2$5.MatDatepicker, selector: "mat-datepicker", exportAs: ["matDatepicker"] }], directives: [{ type: i4$2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i4$3.ɵNgNoValidate, selector: "form:not([ngNoForm]):not([ngNativeValidate])" }, { type: i4$3.NgControlStatusGroup, selector: "[formGroupName],[formArrayName],[ngModelGroup],[formGroup],form:not([ngNoForm]),[ngForm]" }, { type: i4$3.FormGroupDirective, selector: "[formGroup]", inputs: ["formGroup"], outputs: ["ngSubmit"], exportAs: ["ngForm"] }, { type: i1$7.MatLabel, selector: "mat-label" }, { type: i4$3.DefaultValueAccessor, selector: "input:not([type=checkbox])[formControlName],textarea[formControlName],input:not([type=checkbox])[formControl],textarea[formControl],input:not([type=checkbox])[ngModel],textarea[ngModel],[ngDefaultControl]" }, { type: i3$1.MatInput, selector: "input[matInput], textarea[matInput], select[matNativeControl],      input[matNativeControl], textarea[matNativeControl]", inputs: ["id", "disabled", "required", "type", "value", "readonly", "placeholder", "errorStateMatcher", "aria-describedby"], exportAs: ["matInput"] }, { type: i2$5.MatDatepickerInput, selector: "input[matDatepicker]", inputs: ["matDatepicker", "min", "max", "matDatepickerFilter"], exportAs: ["matDatepickerInput"] }, { type: i4$3.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { type: i4$3.FormControlName, selector: "[formControlName]", inputs: ["disabled", "formControlName", "ngModel"], outputs: ["ngModelChange"] }, { type: i1$7.MatSuffix, selector: "[matSuffix]" }], pipes: { "translate": i5.TranslatePipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotDatePickerComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-date-picker',
                    template: `
    <ng-template>
      <div class="outer-margin">
        <div
          class="outeroneline"
          [ngClass]="{'outersmall':!isBig}">
          <div
            class="label-outer"
            [ngClass]="{'label-outer-thin':!isBig}"
          >
            <div class="label">{{label | translate}} </div>
          </div>
          <form [formGroup]="form" class="content">

            <mat-form-field appearance="outline" class="w-100">
              <mat-label>{{description | translate}}</mat-label>
              <input
                formControlName="dateId"
                (ngModelChange)="onChange.emit($event)"
                matInput [matDatepicker]="picker">
              <mat-datepicker-toggle matSuffix [for]="picker"></mat-datepicker-toggle>
              <mat-datepicker #picker></mat-datepicker>
            </mat-form-field>
            <span class="border-class"></span>
          </form>
        </div>
      </div>
    </ng-template>

  `,
                    styles: [`
    .outer-margin {
      /*margin-left: -5px;*/

      padding-right: 50px;
    }

    .label-outer.label-outer-thin {
      min-height: 40px;
    }

    .label-outer {
      flex-basis: 33%;
      min-height: 64px;
      min-width: 220px;
      display: block;
      padding-top: 13px;
      color: #3c4043;
    }

    .label {

      font-family: Roboto, Arial, sans-serif;
      font-size: 14px;
      font-weight: 400;
      height: 20px;
      color: rgb(60, 64, 67);
    }

    .outeroneline {
      display: flex;
      max-width: 900px;
    }


    .outersmall {
      flex-direction: column;
      padding-bottom: 8px;
    }


    .content {
      flex-direction: column;
      flex-basis: 67%;
      min-width: 280px;
    }

    .form-content {

      width: 100%;
      height: 46px;
      padding: 0 16px;
      border: 1px solid #dadce0;
      border-radius: 4px;
      border-top-left-radius: 4px;
      border-top-right-radius: 4px;
      border-top-right-radius: 4px;
      border-bottom-right-radius: 4px;
      border-bottom-right-radius: 4px;
      border-bottom-left-radius: 4px;
      pointer-events: none;
    }


    .form-content:hover {
      border: 2px solid black;
    }

    .input-class {
      height: 100%;
      width: 100%;
      display: flex;
      border: none !important;
      background-color: transparent;
      pointer-events: auto;
      font-family: Roboto, Arial, sans-serif;
      font-size: 1rem;
      font-weight: 400;
      letter-spacing: .00625em;
    }

    .input-class:focus {
      outline: none;
    }
  `],
                    providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotDatePickerComponent) }]
                }]
        }], propDecorators: { _template: [{
                type: ViewChild,
                args: [TemplateRef]
            }], label: [{
                type: Input
            }], attribute: [{
                type: Input
            }], description: [{
                type: Input
            }], value: [{
                type: Input
            }], type: [{
                type: Input
            }], disabled: [{
                type: Input
            }], onChange: [{
                type: Output
            }] } });

class IotDefineIdentifierComponent extends DynamicFormWidth {
    constructor() {
        super(...arguments);
        this.prefix = [
            {
                'nid': 'smaxx',
                'name': 'S)maxx'
            },
            {
                'nid': 'iot',
                'name': 'Io-Things'
            },
        ];
        this.form = new FormGroup({
            value: new FormControl(''),
        });
        this.attribute = 'todo';
        this.description = 'explain this field';
        this.disabled = false;
        this.new = '';
        this.thirpartySelection = 'smaxx';
    }
    get template() {
        return this._template;
    }
    get pristine() {
        return JSON.stringify(this.value) === JSON.stringify(this._value);
    }
    get key() {
        return this.attribute;
    }
    get newValue() {
        return this._value;
    }
    change(event) {
    }
    ngOnInit() {
        super.ngOnInit();
        this._value = this.value;
        if (this.disabled) {
            // this.form = new FormGroup({
            //   selectId: new FormControl({value: '', disabled: this.disabled}),
            // });
        }
        this.form.setValue({ value: this.value });
    }
    addIdentifier() {
        this._value = [...this._value, `urn:${this.thirpartySelection}:${this.new}`];
        this.form.setValue({ value: [...this.value, `urn:${this.thirpartySelection}:${this.new}`] });
    }
    remove(id) {
        this._value = this._value.filter(urn => urn !== id);
        this.form.setValue({ value: this._value });
    }
}
IotDefineIdentifierComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotDefineIdentifierComponent, deps: null, target: i0.ɵɵFactoryTarget.Component });
IotDefineIdentifierComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotDefineIdentifierComponent, selector: "iot-define-identifier", inputs: { prefix: "prefix", label: "label", attribute: "attribute", description: "description", value: "value", separator: "separator", disabled: "disabled" }, providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotDefineIdentifierComponent) }], viewQueries: [{ propertyName: "_template", first: true, predicate: TemplateRef, descendants: true }], usesInheritance: true, ngImport: i0, template: `
    <ng-template>
      <div class="outer-margin">
        <div class="outer" [ngClass]="{'outersmall':!isBig}">
          <div class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
          >
            <div class="label">{{label | translate}}</div>
          </div>

          <div class="content">
            <div class="checkbox-content">
              <div class="checkbox-content-description">{{description | translate}}</div>

              <div *ngFor="let id of _value">
                <div>
                  <mat-form-field appearance="outline" class="w-100">
                    <input class="input-class" matInput type="text"
                           [disabled]="true"
                           [ngModel]="id">
                    <span class="border-class"></span>
                    <button
                      (click)="remove(id)"
                      matSuffix mat-icon-button
                      color="primary" aria-label="Example icon button with a menu icon">
                      <mat-icon>delete</mat-icon>
                    </button>

                  </mat-form-field>
                </div>
              </div>

              <div class="d-flex flex-row w-100">

                <div class="select-button mr-2">
                  <mat-form-field appearance="outline">
                    <mat-label>Third party</mat-label>

                    <mat-select
                      [(ngModel)]="thirpartySelection"

                    >
                      <mat-option

                        *ngFor="let p of prefix" [value]="p.nid">{{p.name}} </mat-option>
                    </mat-select>
                  </mat-form-field>
                </div>
                <div class="flex-grow-1">
                  <mat-form-field appearance="outline" class="w-100">
                    <input class="input-class" matInput type="text"
                           (keyup.enter)="addIdentifier()"
                           [disabled]="false"
                           [(ngModel)]="new">
                    <button
                      (click)="addIdentifier()"
                      matSuffix mat-icon-button
                      color="primary" aria-label="Example icon button with a menu icon">
                      <mat-icon>addv</mat-icon>
                    </button>
                  </mat-form-field>
                </div>
              </div>

            </div>
            <div *ngIf="separator" class="separator"></div>
          </div>


        </div>
      </div>
    </ng-template>
  `, isInline: true, styles: ["\n      .separator {\n        border-bottom: 1px solid #dadce0;\n        padding-top: 16px;\n      }\n\n      .checkbox-content-description {\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 12px;\n        font-weight: 400;\n        letter-spacing: .025em;\n        line-height: 16px;\n        color: #5f6368;\n        margin-bottom: 13px;\n        margin-top: 2px;\n      }\n\n      .outer-margin {\n        margin-left: 0px;\n        padding-right: 50px;\n      }\n\n      .label-outer.label-outer-thin {\n        min-height: 40px;\n      }\n\n      .label-outer {\n        flex-basis: 33%;\n        min-height: 64px;\n        min-width: 220px;\n        display: block;\n        padding-top: 13px;\n        color: #3c4043;\n      }\n\n      .label {\n\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 14px;\n        font-weight: 400;\n        height: 20px;\n        color: rgb(60, 64, 67);\n      }\n\n      .outer {\n        display: flex;\n        max-width: 900px;\n        margin-bottom: 16px;\n      }\n\n      .outersmall {\n        flex-direction: column;\n        padding-bottom: 8px;\n      }\n\n      .content {\n        flex-direction: column;\n        flex-basis: 67%;\n        min-width: 280px;\n      }\n\n      .checkbox-content {\n        display: flex;\n        flex-direction: column;\n        color: #3c4043;\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 14px;\n        font-weight: 400;\n        letter-spacing: 0.2px;\n        line-height: 20px;\n      }\n    "], components: [{ type: i1$7.MatFormField, selector: "mat-form-field", inputs: ["color", "floatLabel", "appearance", "hideRequiredMarker", "hintLabel"], exportAs: ["matFormField"] }, { type: i1.MatButton, selector: "button[mat-button], button[mat-raised-button], button[mat-icon-button],             button[mat-fab], button[mat-mini-fab], button[mat-stroked-button],             button[mat-flat-button]", inputs: ["disabled", "disableRipple", "color"], exportAs: ["matButton"] }, { type: i2$1.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }, { type: i1$6.MatSelect, selector: "mat-select", inputs: ["disabled", "disableRipple", "tabIndex"], exportAs: ["matSelect"] }, { type: i4$1.MatOption, selector: "mat-option", exportAs: ["matOption"] }], directives: [{ type: i4$2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i4$2.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { type: i3$1.MatInput, selector: "input[matInput], textarea[matInput], select[matNativeControl],      input[matNativeControl], textarea[matNativeControl]", inputs: ["id", "disabled", "required", "type", "value", "readonly", "placeholder", "errorStateMatcher", "aria-describedby"], exportAs: ["matInput"] }, { type: i4$3.DefaultValueAccessor, selector: "input:not([type=checkbox])[formControlName],textarea[formControlName],input:not([type=checkbox])[formControl],textarea[formControl],input:not([type=checkbox])[ngModel],textarea[ngModel],[ngDefaultControl]" }, { type: i4$3.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { type: i4$3.NgModel, selector: "[ngModel]:not([formControlName]):not([formControl])", inputs: ["name", "disabled", "ngModel", "ngModelOptions"], outputs: ["ngModelChange"], exportAs: ["ngModel"] }, { type: i1$7.MatSuffix, selector: "[matSuffix]" }, { type: i1$7.MatLabel, selector: "mat-label" }, { type: i4$2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }], pipes: { "translate": i5.TranslatePipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotDefineIdentifierComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-define-identifier',
                    template: `
    <ng-template>
      <div class="outer-margin">
        <div class="outer" [ngClass]="{'outersmall':!isBig}">
          <div class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
          >
            <div class="label">{{label | translate}}</div>
          </div>

          <div class="content">
            <div class="checkbox-content">
              <div class="checkbox-content-description">{{description | translate}}</div>

              <div *ngFor="let id of _value">
                <div>
                  <mat-form-field appearance="outline" class="w-100">
                    <input class="input-class" matInput type="text"
                           [disabled]="true"
                           [ngModel]="id">
                    <span class="border-class"></span>
                    <button
                      (click)="remove(id)"
                      matSuffix mat-icon-button
                      color="primary" aria-label="Example icon button with a menu icon">
                      <mat-icon>delete</mat-icon>
                    </button>

                  </mat-form-field>
                </div>
              </div>

              <div class="d-flex flex-row w-100">

                <div class="select-button mr-2">
                  <mat-form-field appearance="outline">
                    <mat-label>Third party</mat-label>

                    <mat-select
                      [(ngModel)]="thirpartySelection"

                    >
                      <mat-option

                        *ngFor="let p of prefix" [value]="p.nid">{{p.name}} </mat-option>
                    </mat-select>
                  </mat-form-field>
                </div>
                <div class="flex-grow-1">
                  <mat-form-field appearance="outline" class="w-100">
                    <input class="input-class" matInput type="text"
                           (keyup.enter)="addIdentifier()"
                           [disabled]="false"
                           [(ngModel)]="new">
                    <button
                      (click)="addIdentifier()"
                      matSuffix mat-icon-button
                      color="primary" aria-label="Example icon button with a menu icon">
                      <mat-icon>addv</mat-icon>
                    </button>
                  </mat-form-field>
                </div>
              </div>

            </div>
            <div *ngIf="separator" class="separator"></div>
          </div>


        </div>
      </div>
    </ng-template>
  `,
                    styles: [
                        `
      .separator {
        border-bottom: 1px solid #dadce0;
        padding-top: 16px;
      }

      .checkbox-content-description {
        font-family: Roboto, Arial, sans-serif;
        font-size: 12px;
        font-weight: 400;
        letter-spacing: .025em;
        line-height: 16px;
        color: #5f6368;
        margin-bottom: 13px;
        margin-top: 2px;
      }

      .outer-margin {
        margin-left: 0px;
        padding-right: 50px;
      }

      .label-outer.label-outer-thin {
        min-height: 40px;
      }

      .label-outer {
        flex-basis: 33%;
        min-height: 64px;
        min-width: 220px;
        display: block;
        padding-top: 13px;
        color: #3c4043;
      }

      .label {

        font-family: Roboto, Arial, sans-serif;
        font-size: 14px;
        font-weight: 400;
        height: 20px;
        color: rgb(60, 64, 67);
      }

      .outer {
        display: flex;
        max-width: 900px;
        margin-bottom: 16px;
      }

      .outersmall {
        flex-direction: column;
        padding-bottom: 8px;
      }

      .content {
        flex-direction: column;
        flex-basis: 67%;
        min-width: 280px;
      }

      .checkbox-content {
        display: flex;
        flex-direction: column;
        color: #3c4043;
        font-family: Roboto, Arial, sans-serif;
        font-size: 14px;
        font-weight: 400;
        letter-spacing: 0.2px;
        line-height: 20px;
      }
    `
                    ],
                    providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotDefineIdentifierComponent) }]
                }]
        }], propDecorators: { _template: [{
                type: ViewChild,
                args: [TemplateRef]
            }], prefix: [{
                type: Input
            }], label: [{
                type: Input
            }], attribute: [{
                type: Input
            }], description: [{
                type: Input
            }], value: [{
                type: Input
            }], separator: [{
                type: Input
            }], disabled: [{
                type: Input
            }] } });

class IotInputPlaceholderComponent extends DynamicFormWidth {
    constructor() {
        super(...arguments);
        this.fullWidth = false;
        this.attribute = 'todo';
        this.type = 'text';
        this.disabled = false;
        this.onChange = new EventEmitter();
    }
    get template() {
        return this._template;
    }
    get pristine() {
        // return this.form.value.inputId === this.value;
        return true;
    }
    get key() {
        return '';
    }
    get newValue() {
        return '';
    }
    ngOnInit() {
        super.ngOnInit();
        if (this.disabled) {
            // this.form = new FormGroup({
            //   inputId: new FormControl({value:'', disabled: this.disabled}),
            // });
        }
    }
}
IotInputPlaceholderComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputPlaceholderComponent, deps: null, target: i0.ɵɵFactoryTarget.Component });
IotInputPlaceholderComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotInputPlaceholderComponent, selector: "iot-input-placeholder", inputs: { label: "label", fullWidth: "fullWidth", attribute: "attribute", value: "value", type: "type", disabled: "disabled" }, outputs: { onChange: "onChange" }, providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputPlaceholderComponent) }], viewQueries: [{ propertyName: "_template", first: true, predicate: TemplateRef, descendants: true }], usesInheritance: true, ngImport: i0, template: `
    <ng-template>
      <div *ngIf="!disabled && fullWidth">
        <ng-content select="[full-width]"></ng-content>
      </div>
      <div *ngIf="!disabled && !fullWidth" class="outer-margin">
            <div class="outeroneline" [ngClass]="{'outersmall':!isBig}">
                <div class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
                >
                    <div class="label">{{label | translate}} </div>
                </div>

                <div class="content">
                    <ng-content select="[field-positioned]"></ng-content>

                </div>


            </div>

        </div>
    </ng-template>

    `, isInline: true, styles: ["\n        .outer-margin {\n            padding-right: 50px;\n        }\n\n        .label-outer.label-outer-thin {\n            min-height: 40px;\n        }\n\n        .label-outer {\n            flex-basis: 33%;\n            min-height: 64px;\n            min-width: 220px;\n            display: block;\n            padding-top: 13px;\n            color: #3c4043;\n        }\n\n        .label {\n\n            font-family: Roboto, Arial, sans-serif;\n            font-size: 14px;\n            font-weight: 400;\n            height: 20px;\n            color: rgb(60, 64, 67);\n        }\n\n        .outeroneline {\n            display: flex;\n            max-width: 900px;\n        }\n\n\n        .outersmall {\n            flex-direction: column;\n            padding-bottom: 8px;\n        }\n\n\n        .content {\n            flex-direction: column;\n            flex-basis: 67%;\n            min-width: 280px;\n        }\n\n\n\n\n    "], directives: [{ type: i4$2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i4$2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }], pipes: { "translate": i5.TranslatePipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputPlaceholderComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-input-placeholder',
                    template: `
    <ng-template>
      <div *ngIf="!disabled && fullWidth">
        <ng-content select="[full-width]"></ng-content>
      </div>
      <div *ngIf="!disabled && !fullWidth" class="outer-margin">
            <div class="outeroneline" [ngClass]="{'outersmall':!isBig}">
                <div class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
                >
                    <div class="label">{{label | translate}} </div>
                </div>

                <div class="content">
                    <ng-content select="[field-positioned]"></ng-content>

                </div>


            </div>

        </div>
    </ng-template>

    `,
                    styles: [`
        .outer-margin {
            padding-right: 50px;
        }

        .label-outer.label-outer-thin {
            min-height: 40px;
        }

        .label-outer {
            flex-basis: 33%;
            min-height: 64px;
            min-width: 220px;
            display: block;
            padding-top: 13px;
            color: #3c4043;
        }

        .label {

            font-family: Roboto, Arial, sans-serif;
            font-size: 14px;
            font-weight: 400;
            height: 20px;
            color: rgb(60, 64, 67);
        }

        .outeroneline {
            display: flex;
            max-width: 900px;
        }


        .outersmall {
            flex-direction: column;
            padding-bottom: 8px;
        }


        .content {
            flex-direction: column;
            flex-basis: 67%;
            min-width: 280px;
        }




    `],
                    providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputPlaceholderComponent) }]
                }]
        }], propDecorators: { _template: [{
                type: ViewChild,
                args: [TemplateRef]
            }], label: [{
                type: Input
            }], fullWidth: [{
                type: Input
            }], attribute: [{
                type: Input
            }], value: [{
                type: Input
            }], type: [{
                type: Input
            }], disabled: [{
                type: Input
            }], onChange: [{
                type: Output
            }] } });

class IotInputLocationComponent extends DynamicFormWidth {
    constructor() {
        super(...arguments);
        this.zoom = 15;
        this.form = new FormGroup({
            inputId: new FormControl(''),
        });
        this.attribute = 'todo';
        this.type = 'text';
        this.disabled = false;
        this.onChange = new EventEmitter();
    }
    get template() {
        return this._template;
    }
    get pristine() {
        return this.lat == this.latCopy && this.lng == this.lngCopy && this.zoom == this.zoomCopy;
    }
    get key() {
        return this.attribute;
    }
    get newValue() {
        return {
            latitude: this.latCopy,
            longitude: this.lngCopy,
            zoom: this.zoomCopy
        };
    }
    markerChange(event) {
        this.latCopy = event.lat || event.coords.lat;
        this.lngCopy = event.lng || event.coords.lng;
        this.form.setValue({ inputId: '' + this.lat });
        this.onChange.emit(event.coords || event);
    }
    zoomChange(event) {
        this.zoomCopy = event;
        this.form.setValue({ inputId: '' + event });
    }
    ngOnInit() {
        super.ngOnInit();
        this.latCopy = this.lat;
        this.lngCopy = this.lng;
        this.zoomCopy = this.zoom;
        if (this.disabled) {
            this.form = new FormGroup({
                inputId: new FormControl({ value: '' + this.lat, disabled: this.disabled }),
            });
        }
    }
}
IotInputLocationComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputLocationComponent, deps: null, target: i0.ɵɵFactoryTarget.Component });
IotInputLocationComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotInputLocationComponent, selector: "iot-input-location", inputs: { zoom: "zoom", lat: "lat", lng: "lng", label: "label", attribute: "attribute", value: "value", type: "type", disabled: "disabled" }, outputs: { onChange: "onChange" }, providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputLocationComponent) }], viewQueries: [{ propertyName: "_template", first: true, predicate: TemplateRef, descendants: true }], usesInheritance: true, ngImport: i0, template: `
    <ng-template>
        <div class="outer-margin">
            <div class="outeroneline" [ngClass]="{'outersmall':!isBig}">
                <div class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
                >
                    <div class="label">{{label | translate}} </div>
                </div>

              <div class="content ">
                <agm-map
                  class="input-map"
                  [zoom]="zoom"
                  [mapTypeId]="'roadmap'"
                  (zoomChange)="zoomChange($event)"
                  (centerChange)="markerChange($event)"
                  [latitude]="lat" [longitude]="lng">

                  <agm-marker
                              [markerDraggable]="true"
                              (dragEnd)="markerChange($event)"
                              [latitude]="latCopy"
                              [longitude]="lngCopy">
                  </agm-marker>
                </agm-map>

              </div>



            </div>

        </div>
    </ng-template>

    `, isInline: true, styles: ["\n    agm-map {\n      height: 200px;\n    }\n        .outer-margin {\n            /*margin-left: -5px;*/\n\n            padding-right: 50px;\n        }\n\n        .label-outer.label-outer-thin {\n            min-height: 40px;\n        }\n\n        .label-outer {\n            flex-basis: 33%;\n            min-height: 64px;\n            min-width: 220px;\n            display: block;\n            padding-top: 13px;\n            color: #3c4043;\n        }\n\n        .label {\n\n            font-family: Roboto, Arial, sans-serif;\n            font-size: 14px;\n            font-weight: 400;\n            height: 20px;\n            color: rgb(60, 64, 67);\n        }\n\n        .outeroneline {\n            display: flex;\n            max-width: 900px;\n        }\n\n\n        .outersmall {\n            flex-direction: column;\n            padding-bottom: 8px;\n        }\n\n\n        .content {\n            flex-direction: column;\n            flex-basis: 67%;\n            min-width: 280px;\n          border: 1px solid #dadce0;\n          border-radius: 4px;\n        }\n\n        .form-content > div {\n\n\n          border: 2px solid red;\n          border-radius: 25px;\n\n        }\n\n\n\n    .input-map {\n\n      height: 300px;\n      width: 100%;\n      display: flex;\n      border: none !important;\n      background-color: transparent;\n      pointer-events: auto;\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 1rem;\n      font-weight: 400;\n      letter-spacing: .00625em;\n    }\n    "], components: [{ type: i1$8.AgmMap, selector: "agm-map", inputs: ["longitude", "latitude", "zoom", "mapDraggable", "disableDoubleClickZoom", "disableDefaultUI", "scrollwheel", "keyboardShortcuts", "styles", "usePanning", "fitBounds", "scaleControl", "mapTypeControl", "panControl", "rotateControl", "fullscreenControl", "mapTypeId", "clickableIcons", "showDefaultInfoWindow", "gestureHandling", "tilt", "minZoom", "maxZoom", "controlSize", "backgroundColor", "draggableCursor", "draggingCursor", "zoomControl", "zoomControlOptions", "streetViewControl", "streetViewControlOptions", "fitBoundsPadding", "scaleControlOptions", "mapTypeControlOptions", "panControlOptions", "rotateControlOptions", "fullscreenControlOptions", "restriction"], outputs: ["mapClick", "mapRightClick", "mapDblClick", "centerChange", "boundsChange", "mapTypeIdChange", "idle", "zoomChange", "mapReady", "tilesLoaded"] }], directives: [{ type: i4$2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i1$8.AgmMarker, selector: "agm-marker", inputs: ["latitude", "longitude", "title", "label", "markerDraggable", "iconUrl", "openInfoWindow", "opacity", "visible", "zIndex", "animation", "markerClickable"], outputs: ["markerClick", "dragStart", "drag", "dragEnd", "mouseOver", "mouseOut", "animationChange", "markerDblClick", "markerRightClick"] }], pipes: { "translate": i5.TranslatePipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputLocationComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-input-location',
                    template: `
    <ng-template>
        <div class="outer-margin">
            <div class="outeroneline" [ngClass]="{'outersmall':!isBig}">
                <div class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
                >
                    <div class="label">{{label | translate}} </div>
                </div>

              <div class="content ">
                <agm-map
                  class="input-map"
                  [zoom]="zoom"
                  [mapTypeId]="'roadmap'"
                  (zoomChange)="zoomChange($event)"
                  (centerChange)="markerChange($event)"
                  [latitude]="lat" [longitude]="lng">

                  <agm-marker
                              [markerDraggable]="true"
                              (dragEnd)="markerChange($event)"
                              [latitude]="latCopy"
                              [longitude]="lngCopy">
                  </agm-marker>
                </agm-map>

              </div>



            </div>

        </div>
    </ng-template>

    `,
                    styles: [`
    agm-map {
      height: 200px;
    }
        .outer-margin {
            /*margin-left: -5px;*/

            padding-right: 50px;
        }

        .label-outer.label-outer-thin {
            min-height: 40px;
        }

        .label-outer {
            flex-basis: 33%;
            min-height: 64px;
            min-width: 220px;
            display: block;
            padding-top: 13px;
            color: #3c4043;
        }

        .label {

            font-family: Roboto, Arial, sans-serif;
            font-size: 14px;
            font-weight: 400;
            height: 20px;
            color: rgb(60, 64, 67);
        }

        .outeroneline {
            display: flex;
            max-width: 900px;
        }


        .outersmall {
            flex-direction: column;
            padding-bottom: 8px;
        }


        .content {
            flex-direction: column;
            flex-basis: 67%;
            min-width: 280px;
          border: 1px solid #dadce0;
          border-radius: 4px;
        }

        .form-content > div {


          border: 2px solid red;
          border-radius: 25px;

        }



    .input-map {

      height: 300px;
      width: 100%;
      display: flex;
      border: none !important;
      background-color: transparent;
      pointer-events: auto;
      font-family: Roboto, Arial, sans-serif;
      font-size: 1rem;
      font-weight: 400;
      letter-spacing: .00625em;
    }
    `],
                    providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputLocationComponent) }]
                }]
        }], propDecorators: { _template: [{
                type: ViewChild,
                args: [TemplateRef]
            }], zoom: [{
                type: Input
            }], lat: [{
                type: Input
            }], lng: [{
                type: Input
            }], label: [{
                type: Input
            }], attribute: [{
                type: Input
            }], value: [{
                type: Input
            }], type: [{
                type: Input
            }], disabled: [{
                type: Input
            }], onChange: [{
                type: Output
            }] } });

class IotInputCheckboxComponent extends DynamicFormWidth {
    constructor() {
        super(...arguments);
        this.form = new FormGroup({
            checkboxControl: new FormControl(false),
        });
        this.leftLabel = '';
        this.attribute = 'todo';
        this.description = 'explain this field';
        this.disabled = false;
        this.inverse = false;
        this.onChange = new EventEmitter();
    }
    get template() {
        return this._template;
    }
    get pristine() {
        if (this.inverse) {
            return (!!this.form.value.checkboxControl) === !this.value;
        }
        return (!!this.form.value.checkboxControl) === this.value;
    }
    get key() {
        return this.attribute;
    }
    get newValue() {
        console.log('checkbox value is ', this.attribute, this.form.value.checkboxControl);
        if (this.inverse) {
            return !this.form.value.checkboxControl;
        }
        return !!this.form.value.checkboxControl;
    }
    change(event) {
        this.onChange.emit(event);
    }
    ngOnInit() {
        super.ngOnInit();
        if (this.disabled) {
            this.form = new FormGroup({
                checkboxControl: new FormControl({ value: '', disabled: this.disabled }),
            });
        }
        if (this.inverse) {
            this.form.setValue({ checkboxControl: !this.value });
        }
        else {
            this.form.setValue({ checkboxControl: this.value });
        }
    }
    ngOnChanges(changes) {
        if (changes.value) {
            if (this.inverse) {
                this.form.setValue({ checkboxControl: !this.value });
            }
            else {
                this.form.setValue({ checkboxControl: !!this.value });
            }
        }
    }
}
IotInputCheckboxComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputCheckboxComponent, deps: null, target: i0.ɵɵFactoryTarget.Component });
IotInputCheckboxComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotInputCheckboxComponent, selector: "iot-input-checkbox", inputs: { leftLabel: "leftLabel", label: "label", attribute: "attribute", description: "description", value: "value", separator: "separator", disabled: "disabled", inverse: "inverse" }, outputs: { onChange: "onChange" }, providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputCheckboxComponent) }], viewQueries: [{ propertyName: "_template", first: true, predicate: TemplateRef, descendants: true }], usesInheritance: true, usesOnChanges: true, ngImport: i0, template: `
    <ng-template>
      <div
        [ngClass]="{'outer-margin':true, 'nopaddingtop' : leftLabel === ''}"
        >
        <div

          class="outer" [ngClass]="{'outersmall':!isBig}">
          <div
            class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
          >
            <div class="label">{{leftLabel | translate}}</div>
          </div>

          <form [formGroup]="form" class="content">
            <div class="checkbox-content">
              <div
                *ngIf="description !== ''"
                class="checkbox-content-description">{{description | translate}}</div>

              <mat-checkbox
                formControlName="checkboxControl"
                            (ngModelChange)="change($event)">
                {{label | translate}}
              </mat-checkbox>
            </div>
            <div *ngIf="separator" class="separator"></div>
          </form>


        </div>

      </div>
    </ng-template>
  `, isInline: true, styles: ["\n      .separator {\n        border-bottom: 1px solid #dadce0;\n        padding-top: 16px;\n      }\n\n      .checkbox-content-description {\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 12px;\n        font-weight: 400;\n        letter-spacing: .025em;\n        line-height: 16px;\n        color: #5f6368;\n        margin-bottom: 13px;\n        margin-top: 2px;\n      }\n\n      .outer-margin {\n        margin-left: 0px;\n        padding-right: 50px;\n      }\n\n      .label-outer.label-outer-thin {\n        min-height: 40px;\n      }\n\n      .label-outer {\n        flex-basis: 33%;\n        min-height: 64px;\n        min-width: 220px;\n        display: block;\n        padding-top: 13px;\n        color: #3c4043;\n      }\n\n      .label {\n\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 14px;\n        font-weight: 400;\n        height: 20px;\n        color: rgb(60, 64, 67);\n      }\n\n      .outer {\n        display: flex;\n        max-width: 900px;\n        margin-bottom: 16px;\n      }\n\n      .outersmall {\n        flex-direction: column;\n        padding-bottom: 8px;\n      }\n\n      .content {\n        flex-direction: column;\n        flex-basis: 67%;\n        min-width: 280px;\n      }\n\n      .checkbox-content {\n        display: flex;\n        flex-direction: column;\n        color: #3c4043;\n        font-family: Roboto, Arial, sans-serif;\n        font-size: 14px;\n        font-weight: 400;\n        letter-spacing: 0.2px;\n        line-height: 20px;\n      }\n    "], components: [{ type: i1$9.MatCheckbox, selector: "mat-checkbox", inputs: ["disableRipple", "color", "tabIndex", "aria-label", "aria-labelledby", "id", "labelPosition", "name", "required", "checked", "disabled", "indeterminate", "aria-describedby", "value"], outputs: ["change", "indeterminateChange"], exportAs: ["matCheckbox"] }], directives: [{ type: i4$2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i4$3.ɵNgNoValidate, selector: "form:not([ngNoForm]):not([ngNativeValidate])" }, { type: i4$3.NgControlStatusGroup, selector: "[formGroupName],[formArrayName],[ngModelGroup],[formGroup],form:not([ngNoForm]),[ngForm]" }, { type: i4$3.FormGroupDirective, selector: "[formGroup]", inputs: ["formGroup"], outputs: ["ngSubmit"], exportAs: ["ngForm"] }, { type: i4$2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i4$3.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { type: i4$3.FormControlName, selector: "[formControlName]", inputs: ["disabled", "formControlName", "ngModel"], outputs: ["ngModelChange"] }], pipes: { "translate": i5.TranslatePipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputCheckboxComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-input-checkbox',
                    template: `
    <ng-template>
      <div
        [ngClass]="{'outer-margin':true, 'nopaddingtop' : leftLabel === ''}"
        >
        <div

          class="outer" [ngClass]="{'outersmall':!isBig}">
          <div
            class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
          >
            <div class="label">{{leftLabel | translate}}</div>
          </div>

          <form [formGroup]="form" class="content">
            <div class="checkbox-content">
              <div
                *ngIf="description !== ''"
                class="checkbox-content-description">{{description | translate}}</div>

              <mat-checkbox
                formControlName="checkboxControl"
                            (ngModelChange)="change($event)">
                {{label | translate}}
              </mat-checkbox>
            </div>
            <div *ngIf="separator" class="separator"></div>
          </form>


        </div>

      </div>
    </ng-template>
  `,
                    styles: [
                        `
      .separator {
        border-bottom: 1px solid #dadce0;
        padding-top: 16px;
      }

      .checkbox-content-description {
        font-family: Roboto, Arial, sans-serif;
        font-size: 12px;
        font-weight: 400;
        letter-spacing: .025em;
        line-height: 16px;
        color: #5f6368;
        margin-bottom: 13px;
        margin-top: 2px;
      }

      .outer-margin {
        margin-left: 0px;
        padding-right: 50px;
      }

      .label-outer.label-outer-thin {
        min-height: 40px;
      }

      .label-outer {
        flex-basis: 33%;
        min-height: 64px;
        min-width: 220px;
        display: block;
        padding-top: 13px;
        color: #3c4043;
      }

      .label {

        font-family: Roboto, Arial, sans-serif;
        font-size: 14px;
        font-weight: 400;
        height: 20px;
        color: rgb(60, 64, 67);
      }

      .outer {
        display: flex;
        max-width: 900px;
        margin-bottom: 16px;
      }

      .outersmall {
        flex-direction: column;
        padding-bottom: 8px;
      }

      .content {
        flex-direction: column;
        flex-basis: 67%;
        min-width: 280px;
      }

      .checkbox-content {
        display: flex;
        flex-direction: column;
        color: #3c4043;
        font-family: Roboto, Arial, sans-serif;
        font-size: 14px;
        font-weight: 400;
        letter-spacing: 0.2px;
        line-height: 20px;
      }
    `
                    ],
                    providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputCheckboxComponent) }]
                }]
        }], propDecorators: { _template: [{
                type: ViewChild,
                args: [TemplateRef]
            }], leftLabel: [{
                type: Input
            }], label: [{
                type: Input
            }], attribute: [{
                type: Input
            }], description: [{
                type: Input
            }], value: [{
                type: Input
            }], separator: [{
                type: Input
            }], disabled: [{
                type: Input
            }], inverse: [{
                type: Input
            }], onChange: [{
                type: Output
            }] } });

class IotInputLabelsComponent extends DynamicFormWidth {
    constructor() {
        super(...arguments);
        this.separatorKeysCodes = [ENTER, COMMA];
        this.form = new FormGroup({
            inputId: new FormControl(''),
        });
        this.attribute = 'todo';
        this.value = [];
        this.copyArray = [];
        this.type = 'text';
        this.disabled = false;
        this.onChange = new EventEmitter();
    }
    get template() {
        return this._template;
    }
    get pristine() {
        return JSON.stringify(this.value) == JSON.stringify(this.copyArray);
        // return this.value.toString() === this.copyArray.toString();
    }
    get key() {
        return this.attribute;
    }
    get newValue() {
        return this.copyArray;
    }
    remove(fruit) {
        const index = this.copyArray.indexOf(fruit);
        if (index >= 0) {
            this.copyArray.splice(index, 1);
        }
        this.onChange.emit(this.copyArray);
    }
    add(event) {
        const value = (event.value || '').trim();
        if (value) {
            this.copyArray.push(value);
        }
        this.form.setValue({ inputId: null });
        this.onChange.emit(this.copyArray);
    }
    ngOnInit() {
        super.ngOnInit();
        this.form = new FormGroup({
            inputId: new FormControl({ value: null, disabled: this.disabled }),
        });
    }
    ngOnChanges(changes) {
        if (changes.value && !!this.value) {
            this.copyArray = [...this.value];
        }
    }
}
IotInputLabelsComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputLabelsComponent, deps: null, target: i0.ɵɵFactoryTarget.Component });
IotInputLabelsComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.2.6", type: IotInputLabelsComponent, selector: "iot-input-labels", inputs: { label: "label", attribute: "attribute", description: "description", value: "value", type: "type", disabled: "disabled" }, outputs: { onChange: "onChange" }, providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputLabelsComponent) }], viewQueries: [{ propertyName: "_template", first: true, predicate: TemplateRef, descendants: true }, { propertyName: "fruitInput", first: true, predicate: ["labelInput"], descendants: true }], usesInheritance: true, usesOnChanges: true, ngImport: i0, template: `
    <ng-template>
      <div class="outer-margin">
        <div class="outeroneline" [ngClass]="{'outersmall':!isBig}">
          <div class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
          >
            <div class="label">{{label | translate}} </div>
          </div>

          <form [formGroup]="form" class="content">
            <div
              *ngIf="!!description"
              class="checkbox-content-description">{{description | translate}}</div>

            <div class="form-label">
              <mat-chip-list #chipList aria-label="Fruit selection">
                <mat-chip
                  *ngFor="let label of copyArray"
                  [selectable]="true"
                  [removable]="true"
                  (removed)="remove(label)">
                  {{label}}
                  <mat-icon matChipRemove>cancel</mat-icon>
                </mat-chip>
                <input
                  class="label-input"
                  [placeholder]="'DEVICE.NEW_LABEL' | translate"
                  #labelInput
                  formControlName="inputId"
                  [matChipInputFor]="chipList"
                  [matChipInputSeparatorKeyCodes]="separatorKeysCodes"
                  (matChipInputTokenEnd)="add($event)">
              </mat-chip-list>
            </div>
            <!--                    <div class="form-content">-->

            <!--                        <input class="input-class" matInput [type]="type"-->
            <!--                               formControlName="inputId"-->
            <!--                               (ngModelChange)="onChange.emit($event)"-->

            <!--                               >-->
            <!--                        <span class="border-class"></span>-->
            <!--                    </div>-->
          </form>


        </div>

      </div>
    </ng-template>

  `, isInline: true, styles: ["\n    input.label-input {\n      background: transparent;\n      border-width: 0;\n    }\n\n    .outer-margin {\n      /*margin-left: -5px;*/\n\n      padding-right: 50px;\n    }\n\n    .label-outer.label-outer-thin {\n      min-height: 40px;\n    }\n\n    .checkbox-content-description {\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 12px;\n      font-weight: 400;\n      letter-spacing: .025em;\n      line-height: 16px;\n      color: #5f6368;\n      margin-bottom: 13px;\n      margin-top: 2px;\n    }\n\n    .label-outer {\n      flex-basis: 33%;\n      min-height: 64px;\n      min-width: 220px;\n      display: block;\n      padding-top: 13px;\n      color: #3c4043;\n    }\n\n    .label {\n\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 14px;\n      font-weight: 400;\n      height: 20px;\n      color: rgb(60, 64, 67);\n    }\n\n    .outeroneline {\n      display: flex;\n      max-width: 900px;\n    }\n\n\n    .outersmall {\n      flex-direction: column;\n      padding-bottom: 8px;\n    }\n\n\n    .content {\n      flex-direction: column;\n      flex-basis: 67%;\n      min-width: 280px;\n    }\n\n    .form-content {\n\n      width: 100%;\n      height: 46px;\n      padding: 0 16px;\n      border: 1px solid #dadce0;\n      border-radius: 4px;\n      border-top-left-radius: 4px;\n      border-top-right-radius: 4px;\n      border-top-right-radius: 4px;\n      border-bottom-right-radius: 4px;\n      border-bottom-right-radius: 4px;\n      border-bottom-left-radius: 4px;\n      pointer-events: none;\n    }\n\n\n    .form-content:hover {\n      border: 2px solid black;\n    }\n\n    .input-class {\n      height: 100%;\n      width: 100%;\n      display: flex;\n      border: none !important;\n      background-color: transparent;\n      pointer-events: auto;\n      font-family: Roboto, Arial, sans-serif;\n      font-size: 1rem;\n      font-weight: 400;\n      letter-spacing: .00625em;\n    }\n\n    .input-class:focus {\n      outline: none;\n    }\n  "], components: [{ type: i1$a.MatChipList, selector: "mat-chip-list", inputs: ["aria-orientation", "multiple", "compareWith", "value", "required", "placeholder", "disabled", "selectable", "tabIndex", "errorStateMatcher"], outputs: ["change", "valueChange"], exportAs: ["matChipList"] }, { type: i2$1.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }], directives: [{ type: i4$2.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { type: i4$3.ɵNgNoValidate, selector: "form:not([ngNoForm]):not([ngNativeValidate])" }, { type: i4$3.NgControlStatusGroup, selector: "[formGroupName],[formArrayName],[ngModelGroup],[formGroup],form:not([ngNoForm]),[ngForm]" }, { type: i4$3.FormGroupDirective, selector: "[formGroup]", inputs: ["formGroup"], outputs: ["ngSubmit"], exportAs: ["ngForm"] }, { type: i4$2.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { type: i4$2.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { type: i1$a.MatChip, selector: "mat-basic-chip, [mat-basic-chip], mat-chip, [mat-chip]", inputs: ["color", "disableRipple", "tabIndex", "selected", "value", "selectable", "disabled", "removable"], outputs: ["selectionChange", "destroyed", "removed"], exportAs: ["matChip"] }, { type: i1$a.MatChipRemove, selector: "[matChipRemove]" }, { type: i4$3.DefaultValueAccessor, selector: "input:not([type=checkbox])[formControlName],textarea[formControlName],input:not([type=checkbox])[formControl],textarea[formControl],input:not([type=checkbox])[ngModel],textarea[ngModel],[ngDefaultControl]" }, { type: i1$a.MatChipInput, selector: "input[matChipInputFor]", inputs: ["matChipInputSeparatorKeyCodes", "placeholder", "id", "matChipInputFor", "matChipInputAddOnBlur", "disabled"], outputs: ["matChipInputTokenEnd"], exportAs: ["matChipInput", "matChipInputFor"] }, { type: i4$3.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { type: i4$3.FormControlName, selector: "[formControlName]", inputs: ["disabled", "formControlName", "ngModel"], outputs: ["ngModelChange"] }], pipes: { "translate": i5.TranslatePipe } });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotInputLabelsComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'iot-input-labels',
                    template: `
    <ng-template>
      <div class="outer-margin">
        <div class="outeroneline" [ngClass]="{'outersmall':!isBig}">
          <div class="label-outer" [ngClass]="{'label-outer-thin':!isBig}"
          >
            <div class="label">{{label | translate}} </div>
          </div>

          <form [formGroup]="form" class="content">
            <div
              *ngIf="!!description"
              class="checkbox-content-description">{{description | translate}}</div>

            <div class="form-label">
              <mat-chip-list #chipList aria-label="Fruit selection">
                <mat-chip
                  *ngFor="let label of copyArray"
                  [selectable]="true"
                  [removable]="true"
                  (removed)="remove(label)">
                  {{label}}
                  <mat-icon matChipRemove>cancel</mat-icon>
                </mat-chip>
                <input
                  class="label-input"
                  [placeholder]="'DEVICE.NEW_LABEL' | translate"
                  #labelInput
                  formControlName="inputId"
                  [matChipInputFor]="chipList"
                  [matChipInputSeparatorKeyCodes]="separatorKeysCodes"
                  (matChipInputTokenEnd)="add($event)">
              </mat-chip-list>
            </div>
            <!--                    <div class="form-content">-->

            <!--                        <input class="input-class" matInput [type]="type"-->
            <!--                               formControlName="inputId"-->
            <!--                               (ngModelChange)="onChange.emit($event)"-->

            <!--                               >-->
            <!--                        <span class="border-class"></span>-->
            <!--                    </div>-->
          </form>


        </div>

      </div>
    </ng-template>

  `,
                    styles: [`
    input.label-input {
      background: transparent;
      border-width: 0;
    }

    .outer-margin {
      /*margin-left: -5px;*/

      padding-right: 50px;
    }

    .label-outer.label-outer-thin {
      min-height: 40px;
    }

    .checkbox-content-description {
      font-family: Roboto, Arial, sans-serif;
      font-size: 12px;
      font-weight: 400;
      letter-spacing: .025em;
      line-height: 16px;
      color: #5f6368;
      margin-bottom: 13px;
      margin-top: 2px;
    }

    .label-outer {
      flex-basis: 33%;
      min-height: 64px;
      min-width: 220px;
      display: block;
      padding-top: 13px;
      color: #3c4043;
    }

    .label {

      font-family: Roboto, Arial, sans-serif;
      font-size: 14px;
      font-weight: 400;
      height: 20px;
      color: rgb(60, 64, 67);
    }

    .outeroneline {
      display: flex;
      max-width: 900px;
    }


    .outersmall {
      flex-direction: column;
      padding-bottom: 8px;
    }


    .content {
      flex-direction: column;
      flex-basis: 67%;
      min-width: 280px;
    }

    .form-content {

      width: 100%;
      height: 46px;
      padding: 0 16px;
      border: 1px solid #dadce0;
      border-radius: 4px;
      border-top-left-radius: 4px;
      border-top-right-radius: 4px;
      border-top-right-radius: 4px;
      border-bottom-right-radius: 4px;
      border-bottom-right-radius: 4px;
      border-bottom-left-radius: 4px;
      pointer-events: none;
    }


    .form-content:hover {
      border: 2px solid black;
    }

    .input-class {
      height: 100%;
      width: 100%;
      display: flex;
      border: none !important;
      background-color: transparent;
      pointer-events: auto;
      font-family: Roboto, Arial, sans-serif;
      font-size: 1rem;
      font-weight: 400;
      letter-spacing: .00625em;
    }

    .input-class:focus {
      outline: none;
    }
  `],
                    providers: [{ provide: DynamicFormWidth, useExisting: forwardRef(() => IotInputLabelsComponent) }]
                }]
        }], propDecorators: { _template: [{
                type: ViewChild,
                args: [TemplateRef]
            }], fruitInput: [{
                type: ViewChild,
                args: ['labelInput']
            }], label: [{
                type: Input
            }], attribute: [{
                type: Input
            }], description: [{
                type: Input
            }], value: [{
                type: Input
            }], type: [{
                type: Input
            }], disabled: [{
                type: Input
            }], onChange: [{
                type: Output
            }] } });

class IotUiLibModule {
}
IotUiLibModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotUiLibModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
IotUiLibModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotUiLibModule, declarations: [SideNavBarComponent,
        TopNavBarComponent,
        AccountMenuDisplayButtonComponent,
        LanguageSelectorComponent,
        CustomerSelectorComponent,
        CustomerSelectorButtonComponent,
        SideNavBarComponent,
        SingleListEntryComponent,
        CompositeListEntryComponent,
        CustomerSelectModalComponent,
        DashboardGridComponent,
        DashboardCardComponent,
        DashboardCard,
        IotCardFooter, IotCardHeader, IotCardContent,
        DashboardMediaQueryComponent,
        TimeOfDayCardComponent,
        KeyValueCardComponent,
        PageHeaderComponent,
        IotFormComponent,
        IotInputTextComponent,
        IotInputHorizontalLineComponent,
        IotInputTextareaComponent,
        IotInputSelectValueComponent,
        IotInputPlaceholderComponent,
        IotDatePickerComponent,
        IotDefineIdentifierComponent,
        IotInputLocationComponent,
        IotInputCheckboxComponent,
        IotInputLabelsComponent,
        DynamicFormWidth], imports: [CommonModule,
        MatButtonModule,
        MatIconModule,
        MatMenuModule,
        MatTooltipModule,
        MatCardModule,
        MatSidenavModule,
        RouterModule,
        MatListModule,
        MatDialogModule, i5.TranslateModule, MatGridListModule,
        MatTableModule,
        MatProgressSpinnerModule,
        FormsModule,
        ReactiveFormsModule,
        MatSelectModule,
        MatDatepickerModule,
        MatInputModule,
        AgmCoreModule,
        MatCheckboxModule,
        MatChipsModule], exports: [SideNavBarComponent,
        TopNavBarComponent,
        AccountMenuDisplayButtonComponent,
        LanguageSelectorComponent,
        CustomerSelectorComponent,
        CustomerSelectorButtonComponent,
        DashboardGridComponent,
        DashboardCard,
        IotCardFooter, IotCardHeader, IotCardContent,
        DashboardCardComponent,
        TimeOfDayCardComponent,
        KeyValueCardComponent,
        PageHeaderComponent,
        IotFormComponent,
        IotInputTextComponent,
        IotInputHorizontalLineComponent,
        IotInputTextareaComponent,
        IotInputSelectValueComponent,
        IotInputPlaceholderComponent,
        IotDefineIdentifierComponent,
        IotDatePickerComponent,
        IotInputLocationComponent,
        IotInputCheckboxComponent,
        IotInputLabelsComponent,
        DynamicFormWidth] });
IotUiLibModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotUiLibModule, imports: [[
            CommonModule,
            MatButtonModule,
            MatIconModule,
            MatMenuModule,
            MatTooltipModule,
            MatCardModule,
            MatSidenavModule,
            RouterModule,
            MatListModule,
            MatDialogModule,
            TranslateModule.forRoot(),
            MatGridListModule,
            MatTableModule,
            MatProgressSpinnerModule,
            FormsModule,
            ReactiveFormsModule,
            MatSelectModule,
            MatDatepickerModule,
            MatInputModule,
            AgmCoreModule,
            MatCheckboxModule,
            MatChipsModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.2.6", ngImport: i0, type: IotUiLibModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [
                        SideNavBarComponent,
                        TopNavBarComponent,
                        AccountMenuDisplayButtonComponent,
                        LanguageSelectorComponent,
                        CustomerSelectorComponent,
                        CustomerSelectorButtonComponent,
                        SideNavBarComponent,
                        SingleListEntryComponent,
                        CompositeListEntryComponent,
                        CustomerSelectModalComponent,
                        DashboardGridComponent,
                        DashboardCardComponent,
                        DashboardCard,
                        IotCardFooter, IotCardHeader, IotCardContent,
                        DashboardMediaQueryComponent,
                        TimeOfDayCardComponent,
                        KeyValueCardComponent,
                        PageHeaderComponent,
                        IotFormComponent,
                        IotInputTextComponent,
                        IotInputHorizontalLineComponent,
                        IotInputTextareaComponent,
                        IotInputSelectValueComponent,
                        IotInputPlaceholderComponent,
                        IotDatePickerComponent,
                        IotDefineIdentifierComponent,
                        IotInputLocationComponent,
                        IotInputCheckboxComponent,
                        IotInputLabelsComponent,
                        DynamicFormWidth
                    ],
                    exports: [
                        SideNavBarComponent,
                        TopNavBarComponent,
                        AccountMenuDisplayButtonComponent,
                        LanguageSelectorComponent,
                        CustomerSelectorComponent,
                        CustomerSelectorButtonComponent,
                        DashboardGridComponent,
                        DashboardCard,
                        IotCardFooter, IotCardHeader, IotCardContent,
                        DashboardCardComponent,
                        TimeOfDayCardComponent,
                        KeyValueCardComponent,
                        PageHeaderComponent,
                        IotFormComponent,
                        IotInputTextComponent,
                        IotInputHorizontalLineComponent,
                        IotInputTextareaComponent,
                        IotInputSelectValueComponent,
                        IotInputPlaceholderComponent,
                        IotDefineIdentifierComponent,
                        IotDatePickerComponent,
                        IotInputLocationComponent,
                        IotInputCheckboxComponent,
                        IotInputLabelsComponent,
                        DynamicFormWidth
                    ],
                    imports: [
                        CommonModule,
                        MatButtonModule,
                        MatIconModule,
                        MatMenuModule,
                        MatTooltipModule,
                        MatCardModule,
                        MatSidenavModule,
                        RouterModule,
                        MatListModule,
                        MatDialogModule,
                        TranslateModule.forRoot(),
                        MatGridListModule,
                        MatTableModule,
                        MatProgressSpinnerModule,
                        FormsModule,
                        ReactiveFormsModule,
                        MatSelectModule,
                        MatDatepickerModule,
                        MatInputModule,
                        AgmCoreModule,
                        MatCheckboxModule,
                        MatChipsModule
                    ]
                }]
        }] });

/*
 * Public API Surface of iot-ui-lib
 */

/**
 * Generated bundle index. Do not edit.
 */

export { AccountMenuDisplayButtonComponent, CustomerSelectorButtonComponent, CustomerSelectorComponent, DashboardCard, DashboardCardComponent, DashboardGridComponent, DynamicFormWidth, IotCardContent, IotCardFooter, IotCardHeader, IotDatePickerComponent, IotDefineIdentifierComponent, IotFormComponent, IotInputCheckboxComponent, IotInputHorizontalLineComponent, IotInputLabelsComponent, IotInputLocationComponent, IotInputPlaceholderComponent, IotInputSelectValueComponent, IotInputTextComponent, IotInputTextareaComponent, IotUiLibModule, IotUiLibService, KeyValueCardComponent, LanguageSelectorComponent, PageHeaderComponent, SideNavBarComponent, TimeOfDayCardComponent, TopNavBarComponent };
//# sourceMappingURL=iot-ui-lib.js.map
